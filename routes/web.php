<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/about', function () {
    return view('about');
});

Route::get('/cache', function() {
    $exitCode = \Artisan::call('cache:clear');
    // return what you want
});

Route::get('/storage', function() {
    $exitCode = \Artisan::call('storage:link');
    // return what you want
});

Route::post('kecamatan/{id}', function ($id) {
    $course = App\Models\tbl_kecamatan::where('id_kota',$id)->get();
    return response()->json($course);
});

Route::post('kriteria/{id}', function ($id) {
    $course = App\Models\tbl_nspm_kriteria::where('id_nspm_kriteria',$id)->get();
    return response()->json($course);
});

//kang jogja
Route::get('/kangjogja', [App\Http\Controllers\kangjogjapublikController::class, 'index'])->name('master.layanan.kangjogja');
Route::get('/kangjogja/tentangkami', [App\Http\Controllers\kangjogjapublikController::class, 'about'])->name('master.layanan.kangjogja.tentangkami');
Route::get('/kangjogja/tukang', [App\Http\Controllers\kangjogjapublikController::class, 'tukang'])->name('master.layanan.kangjogja.tukang');
Route::get('/kangjogja/toko', [App\Http\Controllers\kangjogjapublikController::class, 'toko'])->name('master.layanan.kangjogja.toko');
Route::post('/kangjogja/tukang/search', [App\Http\Controllers\kangjogjapublikController::class, 'search'])->name('master.layanan.kangjogja.search');
Route::get('/kangjogja/tukang/search', [App\Http\Controllers\kangjogjapublikController::class, 'search'])->name('master.layanan.kangjogja.search');
Route::post('/kangjogja/toko/search', [App\Http\Controllers\kangjogjapublikController::class, 'search_toko'])->name('master.layanan.kangjogja.search_toko');
Route::get('/kangjogja/toko/search', [App\Http\Controllers\kangjogjapublikController::class, 'search_toko'])->name('master.layanan.kangjogja.search_toko');
Route::get('/kangjogja/toko/peta', [App\Http\Controllers\kangjogjapublikController::class, 'peta'])->name('master.layanan.kangjogja.peta');

//unduh
Route::get('/unduh/menu', [App\Http\Controllers\unduhpublikController::class, 'menu'])->name('master.unduh.menu');
Route::get('/unduh/poster/all', [App\Http\Controllers\unduhpublikController::class, 'poster'])->name('master.unduh.poster.all');
Route::get('/unduh/buku/all', [App\Http\Controllers\unduhpublikController::class, 'buku'])->name('master.unduh.buku.all');
Route::get('/unduh/peraturan/all', [App\Http\Controllers\unduhpublikController::class, 'peraturan'])->name('master.unduh.peraturan.all');
Route::get('/unduh/sk/all', [App\Http\Controllers\unduhpublikController::class, 'sk'])->name('master.unduh.sk.all');
Route::get('/unduh/covid/all', [App\Http\Controllers\unduhpublikController::class, 'covid'])->name('master.unduh.covid.all');
Route::get('/unduh/spesifikasi/all', [App\Http\Controllers\unduhpublikController::class, 'spesifikasi'])->name('master.unduh.spesifikasi.all');

//berita
Route::get('/berita/all', [App\Http\Controllers\beritapublikController::class, 'all'])->name('master.informasi.berita.all');
Route::post('/berita/all', [App\Http\Controllers\beritapublikController::class, 'all'])->name('master.informasi.berita.all');
Route::get('/berita/detail/{id}', [App\Http\Controllers\beritapublikController::class, 'detail'])->name('master.informasi.berita.detail');

//artikel
Route::get('/artikel/all', [App\Http\Controllers\artikelpublikController::class, 'all'])->name('master.informasi.artikel.all');
Route::post('/artikel/all', [App\Http\Controllers\artikelpublikController::class, 'all'])->name('master.informasi.artikel.all');
Route::get('/artikel/detail/{id}', [App\Http\Controllers\artikelpublikController::class, 'detail'])->name('master.informasi.artikel.detail');

//agenda
Route::get('/agenda/all', [App\Http\Controllers\agendapublikController::class, 'all'])->name('master.informasi.agenda.all');
Route::get('/agenda/detail/{id}', [App\Http\Controllers\agendapublikController::class, 'detail'])->name('master.informasi.agenda.detail');
Route::get('/agenda/schedulle', [App\Http\Controllers\agendapublikController::class, 'schedulle'])->name('master.informasi.agenda.all');
Route::post('/agenda/schedulle', [App\Http\Controllers\agendapublikController::class, 'schedulle'])->name('master.informasi.agenda.all');

//Event
Route::get('/event/all', [App\Http\Controllers\eventpublikController::class, 'all'])->name('master.informasi.event.all');
Route::get('/event/detail/{id}', [App\Http\Controllers\eventpublikController::class, 'detail'])->name('master.informasi.event.detail');

//konstruksi
Route::get('/konstruksi/all', [App\Http\Controllers\konstruksipublikController::class, 'all'])->name('master.layanan.konstruksi.all');

//kangjogja
Route::get('/menu/kangjogja', [App\Http\Controllers\kangjogjaController::class, 'admin'])->name('master.layanan.menu.admin');
//kangjogjaadd
Route::post('/menu/kangjogja/create_kangjogja', [App\Http\Controllers\kangjogjaController::class, 'create_kangjogja'])->name('master.layanan.menu.kangjogja');
//kangjogjaupdate
Route::post('/menu/kangjogja/update_kangjogja/{id}', [App\Http\Controllers\kangjogjaController::class, 'update_kangjogja'])->name('master.layanan.menu.kangjogja');
//kangjogjaadd
Route::post('/menu/kangjogja/create_toko', [App\Http\Controllers\kangjogjaController::class, 'create_toko'])->name('master.layanan.menu.kangjogja');
//kangjogjaupdate
Route::post('/menu/kangjogja/update_toko/{id}', [App\Http\Controllers\kangjogjaController::class, 'update_toko'])->name('master.layanan.menu.kangjogja');
//kangjogjadestroy
Route::get('/menu/kangjogja/destroy_tukang/{id}', [App\Http\Controllers\kangjogjaController::class, 'destroy_tukang'])->name('master.layanan.menu.destroy_tukang');
//kangjogjadestroytoko
Route::get('/menu/kangjogja/destroy_toko/{id}', [App\Http\Controllers\kangjogjaController::class, 'destroy_toko'])->name('master.layanan.menu.destroy_toko');
//kangjogjaedittukang
Route::get('/menu/kangjogja/edit_tukang/{id}', [App\Http\Controllers\kangjogjaController::class, 'edit_tukang'])->name('master.layanan.menu.edit_tukang');
//kangjogjaedittoko
Route::get('/menu/kangjogja/edit_toko/{id}', [App\Http\Controllers\kangjogjaController::class, 'edit_toko'])->name('master.layanan.menu.edit_toko');


//npsm
Route::get('/nspm/all', [App\Http\Controllers\npsmpublikController::class, 'all'])->name('master.layanan.nspm.all');
Route::get('/nspm/detail/{id}', [App\Http\Controllers\npsmpublikController::class, 'detail'])->name('master.layanan.nspm.detail');
Route::get('/nspm/detail/kriteria/{id}', [App\Http\Controllers\npsmpublikController::class, 'kriteria'])->name('master.layanan.nspm.detail');

Route::get('/menu/nspm', [App\Http\Controllers\nspmController::class, 'nspm'])->name('master.layanan.nspm.nspm1');
Route::post('/menu/nspm_kategori/add_kategori', [App\Http\Controllers\nspmController::class, 'add_kategori'])->name('master.layanan.nspm.add_kategori');
Route::post('/menu/nspm_kriteria/add_kriteria', [App\Http\Controllers\nspmController::class, 'add_kriteria'])->name('master.layanan.nspm.add_kriteria');
Route::post('/menu/nspm_kriteria1/add_kriteria1', [App\Http\Controllers\nspmController::class, 'add_kriteria1'])->name('master.layanan.nspm.add_kriteria1');
Route::post('/menu/nspm/add_nspm', [App\Http\Controllers\nspmController::class, 'add_nspm'])->name('master.layanan.nspm.add_nspm');

Route::post('/menu/nspm_kategori/update/{id}', [App\Http\Controllers\nspmController::class, 'kategori_update'])->name('master.layanan.nspm.kategori_update');
Route::post('/menu/nspm_kriteria/update/{id}', [App\Http\Controllers\nspmController::class, 'kriteria_update'])->name('master.layanan.nspm.kriteria_update');
Route::post('/menu/nspm_kriteria1/update/{id}', [App\Http\Controllers\nspmController::class, 'kriteria1_update'])->name('master.layanan.nspm.kriteria1_update');
Route::post('/menu/nspm/update_nspm/{id}', [App\Http\Controllers\nspmController::class, 'update_nspm'])->name('master.layanan.nspm.update_nspm');

Route::get('/menu/nspm_kategori/destroy_kategori/{id}', [App\Http\Controllers\nspmController::class, 'destroy_kategori'])->name('master.layanan.nspm.destroy_kategori');
Route::get('/menu/nspm_kriteria/destroy_kriteria/{id}', [App\Http\Controllers\nspmController::class, 'destroy_kriteria'])->name('master.layanan.nspm.destroy_kriteria');
Route::get('/menu/nspm_kriteria1/destroy_kriteria1/{id}', [App\Http\Controllers\nspmController::class, 'destroy_kriteria1'])->name('master.layanan.nspm.destroy_kriteria1');
Route::get('/menu/nspm/destroy_nspm/{id}', [App\Http\Controllers\nspmController::class, 'destroy_nspm'])->name('master.layanan.nspm.destroy_nspm');

//Route::post('/hbb/search', function () { return view('master.layanan.harga.search'); });

//Route::get('/hbb/search', function () { return view('master.layanan.harga.search'); });

Route::get('/konsultasi/tanya', [App\Http\Controllers\konsultasipublikController::class, 'tanya'])->name('master.layanan.konsultasi.tanya');
Route::post('/konsultasi/create', [App\Http\Controllers\konsultasipublikController::class, 'create'])->name('master.layanan.konsultasi.create');
Route::get('/konsultasi/all', [App\Http\Controllers\konsultasipublikController::class, 'all'])->name('master.layanan.konsultasi.all');
Route::get('/konsultasi/detail/{id}', [App\Http\Controllers\konsultasipublikController::class, 'detail'])->name('master.layanan.konsultasi.detail');

Route::get('/hbb/all', [App\Http\Controllers\hargapublikController::class, 'all'])->name('master.layanan.harga.all');
Route::get('/hbb/all2', [App\Http\Controllers\hargapublikController::class, 'all2'])->name('master.layanan.harga.all2');
Route::get('/hbb/detail/{id}', [App\Http\Controllers\hargapublikController::class, 'detail'])->name('master.layanan.harga.detail');
Route::post('/hbb/search', [App\Http\Controllers\hargapublikController::class, 'search'])->name('master.layanan.harga.search');
Route::get('/hbb/search', [App\Http\Controllers\hargapublikController::class, 'search'])->name('master.layanan.harga.searchby');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//Master
Route::get('/menu', [App\Http\Controllers\tbl_sliderController::class, 'index'])->name('master.menu.index');

//slider
Route::get('/menu/add', [App\Http\Controllers\tbl_sliderController::class, 'add'])->name('master.menu.add');
Route::post('/menu/create', [App\Http\Controllers\tbl_sliderController::class, 'create'])->name('master.menu.create');
Route::get('/menu/edit/{id}', [App\Http\Controllers\tbl_sliderController::class, 'edit'])->name('master.menu.edit');
Route::post('/menu/update/{id_slider}', [App\Http\Controllers\tbl_sliderController::class, 'update'])->name('master.menu.update');
Route::post('/menu/kontak/create_peta', [App\Http\Controllers\tbl_sliderController::class, 'create_peta'])->name('master.menu.create_peta');
Route::post('/menu/kontak/update_peta/{id_slider}', [App\Http\Controllers\tbl_sliderController::class, 'update_peta'])->name('master.menu.update_peta');
Route::get('/menu/video/add_video', [App\Http\Controllers\tbl_sliderController::class, 'add_video'])->name('master.menu.update_peta');
Route::post('/menu/video/create_video', [App\Http\Controllers\tbl_sliderController::class, 'create_video'])->name('master.menu.update_peta');
Route::get('/menu/video/edit_video/{id}', [App\Http\Controllers\tbl_sliderController::class, 'edit_video'])->name('master.menu.edit');
Route::post('/menu/video/update_video/{id}', [App\Http\Controllers\tbl_sliderController::class, 'update_video'])->name('master.menu.update_video');
Route::get('/menu/video/destroy_video/{id}', [App\Http\Controllers\tbl_sliderController::class, 'destroy_video'])->name('master.menu.destroy_video');
Route::get('/menu/kontak/destroy_peta/{id}', [App\Http\Controllers\tbl_sliderController::class, 'destroy_peta'])->name('master.menu.destroy_peta');
Route::get('/menu/popup/destroy_popup/{id}', [App\Http\Controllers\tbl_sliderController::class, 'destroy_popup'])->name('master.menu.destroy_popup');
Route::post('/menu/popup/create_popup', [App\Http\Controllers\tbl_sliderController::class, 'create_popup'])->name('master.menu.create_popup');
Route::post('/menu/popup/update_popup/{id}', [App\Http\Controllers\tbl_sliderController::class, 'update_popup'])->name('master.menu.update_popup');

//kontak
Route::post('/menu/kontak/create', [App\Http\Controllers\tbl_sliderController::class, 'create_kontak'])->name('master.menu.kontak.create');
Route::post('/menu/kontak/update/{id}', [App\Http\Controllers\tbl_sliderController::class, 'update_kontak'])->name('master.menu.kontak.update');

//Users
Route::get('/usersmanagement/users', [App\Http\Controllers\usersmanagementController::class, 'index'])->name('master.usersmanagement.users.index');
Route::get('/usersmanagement/users/register', [App\Http\Controllers\usersmanagementController::class, 'register'])->middleware('can:isSuperUser')->name('master.usersmanagement.users.register');
Route::post('/usersmanagement/users/create_users', [App\Http\Controllers\usersmanagementController::class, 'create_users'])->middleware('can:isSuperUser')->name('master.usersmanagement.users.create_users');
Route::get('/usersmanagement/users/destroy_users/{id}', [App\Http\Controllers\usersmanagementController::class, 'destroy_users'])->name('master.usersmanagement.users.destroy_users');
Route::get('/usersmanagement/users/edit_users/{id}', [App\Http\Controllers\usersmanagementController::class, 'edit_users'])->middleware('can:isSuperUser')->name('master.usersmanagement.users.edit_users');
Route::post('/usersmanagement/users/update_users/{id}', [App\Http\Controllers\usersmanagementController::class, 'update_users'])->middleware('can:isSuperUser')->name('master.usersmanagement.users.update_users');
Route::get('/usersmanagement/roles/add', [App\Http\Controllers\usersmanagementController::class, 'add'])->middleware('can:isSuperUser')->name('master.usersmanagement.roles.add');
Route::get('/usersmanagement/roles/edit/{id}', [App\Http\Controllers\usersmanagementController::class, 'edit'])->middleware('can:isSuperUser')->name('master.usersmanagement.roles.edit');
Route::post('/usersmanagement/roles/create', [App\Http\Controllers\usersmanagementController::class, 'create'])->middleware('can:isSuperUser')->name('master.usersmanagement.roles.create');
Route::post('/usersmanagement/roles/update/{id}', [App\Http\Controllers\usersmanagementController::class, 'update'])->middleware('can:isSuperUser')->name('master.usersmanagement.roles.update');
Route::get('/usersmanagement/roles/destroy/{id}', [App\Http\Controllers\usersmanagementController::class, 'destroy'])->middleware('can:isSuperUser')->name('master.usersmanagement.roles.destroy');

//link
Route::get('/menu/link/add', [App\Http\Controllers\tbl_sliderController::class, 'add_link'])->name('master.menu.link.add');
Route::get('/menu/link/edit/{id}', [App\Http\Controllers\tbl_sliderController::class, 'edit_link'])->name('master.menu.link.edit');
Route::post('/menu/link/create', [App\Http\Controllers\tbl_sliderController::class, 'create_link'])->name('master.menu.link.create');
Route::post('/menu/link/update/{id}', [App\Http\Controllers\tbl_sliderController::class, 'update_link'])->name('master.menu.link.update');
Route::get('/menu/link/destroy_link/{id}', [App\Http\Controllers\tbl_sliderController::class, 'destroy_link'])->name('master.menu.link.destroy');

//Berita
Route::get('/informasi/berita', [App\Http\Controllers\beritaController::class, 'index'])->name('master.informasi.berita.index');
Route::get('/informasi/berita/add', [App\Http\Controllers\beritaController::class, 'add'])->name('master.informasi.berita.add');
Route::get('/informasi/berita/edit/{id}', [App\Http\Controllers\beritaController::class, 'edit'])->name('master.informasi.berita.edit');
Route::post('/informasi/berita/create', [App\Http\Controllers\beritaController::class, 'create'])->name('master.informasi.berita.create');
Route::post('/informasi/berita/update/{id}', [App\Http\Controllers\beritaController::class, 'update'])->name('master.informasi.berita.update');
Route::get('/informasi/berita/destroy/{id}', [App\Http\Controllers\beritaController::class, 'destroy'])->name('master.informasi.berita.destroy');

//Artikel
Route::get('/informasi/artikel', [App\Http\Controllers\artikelController::class, 'index'])->name('master.informasi.artikel.index');
Route::get('/informasi/artikel/add', [App\Http\Controllers\artikelController::class, 'add'])->name('master.informasi.artikel.add');
Route::get('/informasi/artikel/edit/{id}', [App\Http\Controllers\artikelController::class, 'edit'])->name('master.informasi.artikel.edit');
Route::post('/informasi/artikel/create', [App\Http\Controllers\artikelController::class, 'create'])->name('master.informasi.artikel.create');
Route::post('/informasi/artikel/update/{id}', [App\Http\Controllers\artikelController::class, 'update'])->name('master.informasi.artikel.update');
Route::get('/informasi/artikel/destroy/{id}', [App\Http\Controllers\artikelController::class, 'destroy'])->name('master.informasi.artikel.destroy');

//Agenda
Route::get('/informasi/agenda', [App\Http\Controllers\agendaController::class, 'index'])->name('master.informasi.agenda.index');
Route::get('/informasi/agenda/add', [App\Http\Controllers\agendaController::class, 'add'])->name('master.informasi.agenda.add');
Route::get('/informasi/agenda/edit/{id}', [App\Http\Controllers\agendaController::class, 'edit'])->name('master.informasi.agenda.edit');
Route::post('/informasi/agenda/create', [App\Http\Controllers\agendaController::class, 'create'])->name('master.informasi.agenda.create');
Route::post('/informasi/agenda/update/{id}', [App\Http\Controllers\agendaController::class, 'update'])->name('master.informasi.agenda.update');
Route::get('/informasi/agenda/destroy/{id}', [App\Http\Controllers\agendaController::class, 'destroy'])->name('master.informasi.agenda.destroy');

//Event
Route::get('/informasi/event', [App\Http\Controllers\eventController::class, 'index'])->name('master.informasi.event.index');
Route::get('/informasi/event/add', [App\Http\Controllers\eventController::class, 'add'])->name('master.informasi.event.add');
Route::get('/informasi/event/edit/{id}', [App\Http\Controllers\eventController::class, 'edit'])->name('master.informasi.event.edit');
Route::post('/informasi/event/create_event', [App\Http\Controllers\eventController::class, 'create_event'])->name('master.informasi.event.create');
Route::post('/informasi/event/create', [App\Http\Controllers\eventController::class, 'create'])->name('master.informasi.event.create');
Route::post('/informasi/event/update/{id}', [App\Http\Controllers\eventController::class, 'update'])->name('master.informasi.event.update');
Route::post('/informasi/event/update_event/{id}', [App\Http\Controllers\eventController::class, 'update_event'])->name('master.informasi.event.update');
Route::get('/informasi/event/destroy/{id}', [App\Http\Controllers\eventController::class, 'destroy'])->name('master.informasi.event.destroy');

//Event
Route::get('/peta/all', [App\Http\Controllers\PetaController::class, 'all'])->name('master.informasi.peta.all');

//Poster
Route::get('/unduh/poster', [App\Http\Controllers\posterController::class, 'index'])->name('master.unduh.poster.index');
Route::get('/unduh/poster/add', [App\Http\Controllers\posterController::class, 'add'])->name('master.unduh.poster.add');
Route::get('/unduh/poster/edit/{id}', [App\Http\Controllers\posterController::class, 'edit'])->name('master.unduh.poster.edit');
Route::post('/unduh/poster/create', [App\Http\Controllers\posterController::class, 'create'])->name('master.unduh.poster.create');
Route::post('/unduh/poster/update/{id}', [App\Http\Controllers\posterController::class, 'update'])->name('master.unduh.poster.update');
Route::get('/unduh/poster/destroy/{id}', [App\Http\Controllers\posterController::class, 'destroy'])->name('master.unduh.poster.destroy');

//Buku
Route::get('/unduh/buku', [App\Http\Controllers\bukuController::class, 'index'])->name('master.unduh.buku.index');
Route::get('/unduh/buku/add', [App\Http\Controllers\bukuController::class, 'add'])->name('master.unduh.buku.add');
Route::get('/unduh/buku/edit/{id}', [App\Http\Controllers\bukuController::class, 'edit'])->name('master.unduh.buku.edit');
Route::post('/unduh/buku/create', [App\Http\Controllers\bukuController::class, 'create'])->name('master.unduh.buku.create');
Route::post('/unduh/buku/update/{id}', [App\Http\Controllers\bukuController::class, 'update'])->name('master.unduh.buku.update');
Route::get('/unduh/buku/destroy/{id}', [App\Http\Controllers\bukuController::class, 'destroy'])->name('master.unduh.buku.destroy');

//Peraturan
Route::get('/unduh/peraturan', [App\Http\Controllers\peraturanController::class, 'index'])->name('master.unduh.peraturan.index');
Route::get('/unduh/peraturan/add', [App\Http\Controllers\peraturanController::class, 'add'])->name('master.unduh.peraturan.add');
Route::get('/unduh/peraturan/edit/{id}', [App\Http\Controllers\peraturanController::class, 'edit'])->name('master.unduh.peraturan.edit');
Route::post('/unduh/peraturan/create', [App\Http\Controllers\peraturanController::class, 'create'])->name('master.unduh.peraturan.create');
Route::post('/unduh/peraturan/update/{id}', [App\Http\Controllers\peraturanController::class, 'update'])->name('master.unduh.peraturan.update');
Route::get('/unduh/peraturan/destroy/{id}', [App\Http\Controllers\peraturanController::class, 'destroy'])->name('master.unduh.peraturan.destroy');

//Spekteknis
Route::get('/unduh/spektek', [App\Http\Controllers\spektekController::class, 'index'])->name('master.unduh.spektek.index');
Route::get('/unduh/spektek/add', [App\Http\Controllers\spektekController::class, 'add'])->name('master.unduh.spektek.add');
Route::get('/unduh/spektek/edit/{id}', [App\Http\Controllers\spektekController::class, 'edit'])->name('master.unduh.spektek.edit');
Route::post('/unduh/spektek/create', [App\Http\Controllers\spektekController::class, 'create'])->name('master.unduh.spektek.create');
Route::post('/unduh/spektek/update/{id}', [App\Http\Controllers\spektekController::class, 'update'])->name('master.unduh.spektek.update');
Route::get('/unduh/spektek/destroy/{id}', [App\Http\Controllers\spektekController::class, 'destroy'])->name('master.unduh.spektek.destroy');

//sk
Route::get('/unduh/sk', [App\Http\Controllers\skController::class, 'index'])->name('master.unduh.sk.index');
Route::get('/unduh/sk/add', [App\Http\Controllers\skController::class, 'add'])->name('master.unduh.sk.add');
Route::get('/unduh/sk/edit/{id}', [App\Http\Controllers\skController::class, 'edit'])->name('master.unduh.sk.edit');
Route::post('/unduh/sk/create', [App\Http\Controllers\skController::class, 'create'])->name('master.unduh.sk.create');
Route::post('/unduh/sk/update/{id}', [App\Http\Controllers\skController::class, 'update'])->name('master.unduh.sk.update');
Route::get('/unduh/sk/destroy/{id}', [App\Http\Controllers\skController::class, 'destroy'])->name('master.unduh.sk.destroy');

//covid
Route::get('/unduh/covid', [App\Http\Controllers\covidController::class, 'index'])->name('master.unduh.covid.index');
Route::get('/unduh/covid/add', [App\Http\Controllers\covidController::class, 'add'])->name('master.unduh.covid.add');
Route::get('/unduh/covid/edit/{id}', [App\Http\Controllers\covidController::class, 'edit'])->name('master.unduh.covid.edit');
Route::post('/unduh/covid/create', [App\Http\Controllers\covidController::class, 'create'])->name('master.unduh.covid.create');
Route::post('/unduh/covid/update/{id}', [App\Http\Controllers\covidController::class, 'update'])->name('master.unduh.covid.update');
Route::get('/unduh/covid/destroy/{id}', [App\Http\Controllers\covidController::class, 'destroy'])->name('master.unduh.covid.destroy');

//harga
Route::get('/layanan/harga', [App\Http\Controllers\hargaController::class, 'index'])->name('master.layanan.harga.index');
Route::get('/layanan/harga/add', [App\Http\Controllers\hargaController::class, 'add'])->name('master.layanan.harga.add');
Route::get('/layanan/harga/edit/{id}', [App\Http\Controllers\hargaController::class, 'edit'])->name('master.layanan.harga.edit');
Route::post('/layanan/harga/create', [App\Http\Controllers\hargaController::class, 'create'])->name('master.layanan.harga.create');
Route::post('/layanan/harga/update/{id}', [App\Http\Controllers\hargaController::class, 'update'])->name('master.layanan.harga.update');
Route::get('/layanan/harga/destroy/{id}', [App\Http\Controllers\hargaController::class, 'destroy'])->name('master.layanan.harga.destroy');
Route::post('/layanan/harga/importexcel', [App\Http\Controllers\hargaController::class, 'importexcel'])->name('importexcel');

//harga New
Route::get('/layanan/hbb', [App\Http\Controllers\TblhargaController::class, 'index'])->name('master.layanan.hbb.index');
Route::post('/layanan/hbb', [App\Http\Controllers\TblhargaController::class, 'index'])->name('master.layanan.hbb.index');
Route::get('/layanan/barang', [App\Http\Controllers\TblhargaController::class, 'barang'])->name('master.layanan.barang.index');
Route::get('/layanan/hbb/add', [App\Http\Controllers\TblhargaController::class, 'add'])->name('master.layanan.hbb.add');
Route::get('/layanan/hbb/edit/{id}', [App\Http\Controllers\TblhargaController::class, 'edit'])->name('master.layanan.hbb.edit');
Route::post('/layanan/hbb/create', [App\Http\Controllers\TblhargaController::class, 'create'])->name('master.layanan.hbb.create');
Route::post('/layanan/hbb/update/{id}', [App\Http\Controllers\TblhargaController::class, 'update'])->name('master.layanan.hbb.update');
Route::get('/layanan/hbb/destroy/{id}', [App\Http\Controllers\TblhargaController::class, 'destroy'])->name('master.layanan.hbb.destroy');
Route::post('/layanan/hbb/importexcelnew', [App\Http\Controllers\TblhargaController::class, 'importexcelnew'])->name('importexcelnew');
Route::post('/layanan/hbb/importexcelnewbarang', [App\Http\Controllers\TblhargaController::class, 'importexcelnewbarang'])->name('importexcelnewbarang');
Route::get('/layanan/hbb/add_barang', [App\Http\Controllers\TblhargaController::class, 'add_barang'])->name('master.layanan.hbb.add_barang');
Route::post('/layanan/hbb/create_barang', [App\Http\Controllers\TblhargaController::class, 'create_barang'])->name('master.layanan.hbb.create');
Route::get('/layanan/hbb/destroy_barang/{id}', [App\Http\Controllers\TblhargaController::class, 'destroy_barang'])->name('master.layanan.hbb.destroy');
Route::get('/layanan/hbb/edit_barang/{id}', [App\Http\Controllers\TblhargaController::class, 'edit_barang'])->name('master.layanan.hbb.edit_barang');
Route::post('/layanan/hbb/update_barang/{id}', [App\Http\Controllers\TblhargaController::class, 'update_barang'])->name('master.layanan.hbb.update_barang');
Route::get('/layanan/lokasi', [App\Http\Controllers\Lokasi\TblKecamatanController::class, 'index'])->name('master.layanan.lokasi.index');
Route::post('/layanan/lokasi/create', [App\Http\Controllers\Lokasi\TblKecamatanController::class, 'create'])->name('master.layanan.lokasi.create');
Route::get('/layanan/lokasi/destroy/{id}', [App\Http\Controllers\Lokasi\TblKecamatanController::class, 'destroy'])->name('master.layanan.lokasi.destroy');
Route::post('/layanan/lokasi/update/{id}', [App\Http\Controllers\Lokasi\TblKecamatanController::class, 'update'])->name('master.layanan.lokasi.update');
Route::post('/layanan/hbb/destroy_search', [App\Http\Controllers\TblhargaController::class, 'destroy_search'])->name('master.layanan.hbb.destroy');

Route::get('/layanan/unduh/all', [App\Http\Controllers\TbluploadhargapublikController::class, 'all'])->name('master.layanan.lokasi.index');

Route::get('/layanan/unduh', [App\Http\Controllers\TbluploadhargaController::class, 'index'])->name('master.layanan.lokasi.index');
Route::get('/layanan/unduh/add', [App\Http\Controllers\TbluploadhargaController::class, 'add'])->name('master.layanan.lokasi.index');
Route::post('/layanan/unduh/create', [App\Http\Controllers\TbluploadhargaController::class, 'create'])->name('master.layanan.lokasi.create');
Route::get('/layanan/unduh/destroy/{id}', [App\Http\Controllers\TbluploadhargaController::class, 'destroy'])->name('master.layanan.lokasi.destroy');
Route::post('/layanan/unduh/update/{id}', [App\Http\Controllers\TbluploadhargaController::class, 'update'])->name('master.layanan.lokasi.update');
Route::get('/layanan/unduh/edit/{id}', [App\Http\Controllers\TbluploadhargaController::class, 'edit'])->name('master.layanan.lokasi.update');


Route::get('/layanan/jenis', [App\Http\Controllers\hargaController::class, 'jenis'])->name('master.layanan.jenis.jenis');
Route::get('/layanan/jenis/add_jenis', [App\Http\Controllers\hargaController::class, 'add_jenis'])->name('master.layanan.jenis.add');
Route::get('/layanan/jenis/edit_jenis/{id}', [App\Http\Controllers\hargaController::class, 'edit_jenis'])->name('master.layanan.jenis.edit');
Route::post('/layanan/jenis/create_jenis', [App\Http\Controllers\hargaController::class, 'create_jenis'])->name('master.layanan.jenis.create');
Route::post('/layanan/jenis/update_jenis/{id}', [App\Http\Controllers\hargaController::class, 'update_jenis'])->name('master.layanan.jenis.update');
Route::get('/layanan/jenis/destroy_jenis/{id}', [App\Http\Controllers\hargaController::class, 'destroy_jenis'])->name('master.layanan.jenis.destroy');

//konstruksi
Route::get('/layanan/konstruksi', [App\Http\Controllers\konstruksiController::class, 'index'])->name('master.layanan.konstruksi.index');
Route::get('/layanan/konstruksi/add', [App\Http\Controllers\konstruksiController::class, 'add'])->name('master.layanan.konstruksi.add');
Route::get('/layanan/konstruksi/edit/{id}', [App\Http\Controllers\konstruksiController::class, 'edit'])->name('master.layanan.konstruksi.edit');
Route::post('/layanan/konstruksi/create', [App\Http\Controllers\konstruksiController::class, 'create'])->name('master.layanan.konstruksi.create');
Route::post('/layanan/konstruksi/update/{id}', [App\Http\Controllers\konstruksiController::class, 'update'])->name('master.layanan.konstruksi.update');
Route::get('/layanan/konstruksi/destroy/{id}', [App\Http\Controllers\konstruksiController::class, 'destroy'])->name('master.layanan.konstruksi.destroy');

//konsultasi
Route::get('/layanan/konsultasi', [App\Http\Controllers\konsultasiController::class, 'index'])->name('master.layanan.konsultasi.index');
Route::get('/layanan/konsultasi/add', [App\Http\Controllers\konsultasiController::class, 'add'])->name('master.layanan.konsultasi.add');
Route::get('/layanan/konsultasi/edit/{id}', [App\Http\Controllers\konsultasiController::class, 'edit'])->name('master.layanan.konsultasi.edit');
Route::post('/layanan/konsultasi/create', [App\Http\Controllers\konsultasiController::class, 'create'])->name('master.layanan.konsultasi.create');
Route::post('/layanan/konsultasi/update/{id}', [App\Http\Controllers\konsultasiController::class, 'update'])->name('master.layanan.konsultasi.update');
Route::get('/layanan/konsultasi/destroy/{id}', [App\Http\Controllers\konsultasiController::class, 'destroy'])->name('master.layanan.konsultasi.destroy');
Route::get('/layanan/konsultasi/edit_kategori/{id}', [App\Http\Controllers\konsultasiController::class, 'edit_kategori'])->name('master.layanan.konsultasi.edit_kategori');
Route::post('/layanan/konsultasi/update_kategori/{id}', [App\Http\Controllers\konsultasiController::class, 'update_kategori'])->name('master.layanan.konsultasi.update_kategori');
Route::get('/layanan/konsultasi/destroy_kategori/{id}', [App\Http\Controllers\konsultasiController::class, 'destroy_kategori'])->name('master.layanan.konsultasi.destroy_kategori');

//harga
Route::get('/harga/all', [App\Http\Controllers\TblhargapublikController::class, 'all'])->name('master.layanan.hbb.all');
Route::post('/harga/all', [App\Http\Controllers\TblhargapublikController::class, 'all'])->name('master.layanan.hbb.all');
Route::get('/harga/all2', [App\Http\Controllers\TblhargapublikController::class, 'all'])->name('master.layanan.hbb.all');
Route::get('/harga/statistik', [App\Http\Controllers\Harga\StatistikController::class, 'all'])->name('master.layanan.hbb.statistik');
Route::post('/harga/statistik', [App\Http\Controllers\Harga\StatistikController::class, 'all'])->name('master.layanan.hbb.statistik');
