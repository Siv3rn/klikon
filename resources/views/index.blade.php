@extends('layouts.lay')

@section('content')
<div class="banner-carousel banner-carousel-1 mb-0">
          <?php
          $banner = DB::table('tbl_slider')
		  ->select('*')
          ->where('status_slider', '=', 'aktif')
          ->get();
          ?>
		  @foreach ($banner as $b) 
            <div class="banner-carousel-item">
              <div id="imageContainer">
                <div class="box">
                 <img class="gmb" src="{{asset('images/slider/'.$b->image_slider)}}" width='100%' alt="service-icon">
               
               <p class='tengah1' data-animation-in="fadeIn">
                
                  <b><font size='7vw' color='#fff' >{{$b->judul_slider}}</font></b><br/>
                  <font size='4' color='#fff'>{{$b->keterangan_slider}}</font>
                
                </p>
                
                </div>
               </div>
            </div>
          @endforeach

</div>
  


<section id="ts-service-area" class="footer">
  <div class="container">
    <div class="row">    
            <div class="col-lg-8">
            <h3 class="widget-title ">Tentang Klinik Konstruksi</h3>
              <p class="text-white">Klinik konstruksi untuk mewujudkan tertib penyelenggaraan jasa konstruksi layanan klinik konsultasi konstruksi oleh kementerian pekerjaan umum dan perumahan rakyat bertujuan untuk meminimalisir permasalahan konstruksi di tingkat nasional dan daerah yang ditangani oleh tim ahli sesuai bidang, diantaranya pengadaan barang/jasa, kontrak konstruksi, dan keselamatan konstruksi.</p>
            </div><!-- Col end -->

            <div class="col-lg-4 px-5">
              <div class="ts-service-box d-flex text-end">
              <img loading="lazy" src="{{asset('constra/images/bulat.png')}}" width='65%' alt="service-icon">
              </div><!-- Service 1 end -->
            </div><!-- Col end -->

    </div><!-- Content row end -->

  </div>
  <!--/ Container end -->
</section><!-- Service 1 end -->


<section id="ts-service-area" class="copyright">
  <div class="container">
    <div class="row">    
            <div class="col-lg-4">
                    <img loading="lazy" src="{{asset('/images/all.png')}}" width='90%' alt="service-icon">&nbsp;
            </div><!-- Col end -->

            <div class="col-lg-8">
              <div class="ts-service-box d-flex">
                  <div class="ts-service-box-info">
                  <h2 class="widget-title">Alur Konsultasi</h2>
                    <p>
                    <font color='000000'>Panduan bagi pengguna Klinik Konstruksi agar dapat menggunakan layanan konsultasi.</font>   
                    </p> <br><br>
                    <img loading="lazy" class='item' src="{{asset('/constra/images/1.png')}}" width='15%' alt="service-icon">&nbsp;
                    <img loading="lazy" class='item' src="{{asset('/constra/images/2.png')}}" width='15%' alt="service-icon">&nbsp;
                    <img loading="lazy" class='item' src="{{asset('/constra/images/3.png')}}" width='15%' alt="service-icon">&nbsp;
                    <img loading="lazy" class='item' src="{{asset('/constra/images/4.png')}}" width='15%' alt="service-icon">&nbsp;
                    <img loading="lazy" class='item' src="{{asset('/constra/images/5.png')}}" width='15%' alt="service-icon">&nbsp;
                    <img loading="lazy" class='item' src="{{asset('/constra/images/6.png')}}" width='15%' alt="service-icon">&nbsp;
                    
                    <br><br>
                    <a class="btn btn-blue" href="<?php echo url("/konsultasi/tanya/#tanya"); ?>">&nbsp; &nbsp;Hubungi Kami&nbsp; &nbsp;</a>
                  </div>
              </div><!-- Service 1 end -->
            </div><!-- Col end -->

    </div><!-- Content row end -->

  </div>
  <!--/ Container end -->
</section><!-- Service end -->

<?php /*
<section id="facts" class="facts-area dark-bg">
  <div class="container">
    <div class="facts-wrapper">
        <div class="row">
          <div class="col-md-3 col-sm-6 ts-facts">
              <div class="ts-facts-img">
                <img loading="lazy" src="{{asset('constra/images/icon-image/law.png')}}" width='29%' alt="facts-img">
              </div>
              <div class="ts-facts-content">
                <h2 class="ts-facts-num"><span class="counterUp" data-count="
                <?php 
                $hit = \DB::table('tbl_peraturan')->get();
                $hit = $hit->count();
                ?>
                {{$hit}}
                ">{{$hit}}</span></h2>
                <h3 class="ts-facts-title">Total Data Peraturan</h3>
              </div>
          </div><!-- Col end -->

          <div class="col-md-3 col-sm-6 ts-facts mt-5 mt-sm-0">
              <div class="ts-facts-img">
                <img loading="lazy" src="{{asset('constra/images/icon-image/putusan.png')}}" width='29%' alt="facts-img">
              </div>
              <div class="ts-facts-content">
                <h2 class="ts-facts-num"><span class="counterUp" data-count="
                <?php 
                $hit2 = \DB::table('tbl_sk')->get();
                $hit2 = $hit2->count();
                ?>
                {{$hit2}}
                ">{{$hit2}}</span></h2>
                <h3 class="ts-facts-title">Total Data SK</h3>
              </div>
          </div><!-- Col end -->

          <div class="col-md-3 col-sm-6 ts-facts mt-5 mt-md-0">
              <div class="ts-facts-img">
                <img loading="lazy" src="{{asset('constra/images/icon-image/konsul.png')}}" width='37%' alt="facts-img">
              </div>
              <div class="ts-facts-content">
                <h2 class="ts-facts-num"><span class="counterUp" 
                <h2 class="ts-facts-num"><span class="counterUp" data-count="
                <?php 
                $hit3 = \DB::table('tbl_konsultasi')->get();
                $hit3 = $hit3->count();
                ?>
                {{$hit3}}
                ">{{$hit3}}</span></h2>
                <h3 class="ts-facts-title">Total Data Konsultasi</h3>
              </div>
          </div><!-- Col end -->

          <div class="col-md-3 col-sm-6 ts-facts mt-5 mt-md-0">
              <div class="ts-facts-img">
                <img loading="lazy" src="{{asset('constra/images/icon-image/bahan.png')}}" width='28%' alt="facts-img">
              </div>
              <div class="ts-facts-content">
                <h2 class="ts-facts-num"><span class="counterUp" 
                <h2 class="ts-facts-num"><span class="counterUp" data-count="
                <?php 
                $hit4 = \DB::table('harga')->get();
                $hit4 = $hit4->count();
                ?>
                {{$hit4}}
                ">{{$hit4}}</span></h2>
                <h3 class="ts-facts-title">Total Data Harga</h3>
              </div>
          </div><!-- Col end -->

        </div> <!-- Facts end -->
    </div>
    <!--/ Content row end -->
  </div>
  <!--/ Container end -->
</section><!-- Facts end -->
*/ ?>

<section id="layanan" class="section-padding">
  <div class="container">
    <div class="row text-center">
        <div class="col-12">
          <h3 class="section-sub-title widget-title">Layanan Kami</h3>
        </div>
    </div>
    <!--/ Title row end -->

    <div class="row">

    <div class="col-lg-3">
        <div class="card">
            <div class="ts-service-image-wrapper">
              <center>
              <a  href="<?php echo url('/konsultasi/all'); ?>" >
              <img class='item' loading="lazy" style="border-top-left-radius:15px; border-top-right-radius:15px;" width='252px' height='149' src="{{asset('constra/images/icon-image/konsultasi.png')}}" alt="service-image">
              </a>  
              </center>
            </div>
            <div class="">
              <div class="ts-service-info"><center>
                  <h3 class="service-box"><a href="<?php echo url('/konsultasi/all'); ?>">Informasi Data Konsultasi</a></h3>
                  <a  href="<?php echo url('/konsultasi/all'); ?>" >Kumpulan Informasi dan Jawaban konsultasi terkait berbagai kategori<br/>
                  </a>
                  
                <br/><br/>
              </div>
              <div class="text-center"> 
                <a href="<?php echo url('/konsultasi/all'); ?>" type="button" class="btn btn-dark btn-sm">Selengkapnya</a><br><br>
              </div>
              
            </div>
        </div><!-- Service3 end -->
      </div><!-- Col 3 end -->

      <div class="col-lg-3">
        <div class="card">
            <div class="ts-service-image-wrapper">
              <center>
              <a href="<?php echo url('/konstruksi/all'); ?>">
              <img class='item' loading="lazy" style="border-top-left-radius:15px; border-top-right-radius:15px;" width='252px' height='150' src="{{asset('constra/images/icon-image/konstruksinew.png')}}" alt="service-image">
              </a>  
              </center>
            </div>
            <div class="">
              <div class="ts-service-info"><center>
                  <h3 class="service-box-title"><a href="<?php echo url('/konstruksi/all'); ?>">Informasi Data Konstruksi</a></h3>
                  <a href="<?php echo url('/konstruksi/all'); ?>">Kumpulan Informasi Data Tenaga ahli dan Tenaga Terampil berdasarkan tahun
                  </a>
                <br/><br/><br/>
              </div>
              <div class="text-center"> 
                <a href="<?php echo url('/konstruksi/all'); ?>" type="button" class="btn btn-dark btn-sm">Selengkapnya</a><br><br>
              </div>
            </div>
        </div><!-- Service3 end -->
      </div><!-- Col 3 end -->


      <div class="col-lg-3">
        <div class="card">
            <div class="ts-service-image-wrapper">
              <center>
              <a href="<?php echo url('/harga/all'); ?>" >
              <img class='item' loading="lazy" style="border-top-left-radius:15px; border-top-right-radius:15px;" width='252px' height='150' src="{{asset('constra/images/icon-image/hbb.png')}}" alt="service-image">
              </a>
              </center>
            </div>
              <div class="">
                <div class="ts-service-info"><center>
                    <h3 class="service-box-title"><a href="<?php echo url('/harga/all'); ?>">Harga Bahan Bangunan</a></h3>
                    <a href="<?php echo url('/harga/all'); ?>" >Kumpulan informasi harga bahan bangunan yang ada di Wilayah Daerah Istimewa Yogyakarta
                    </a>
                  <br/><br/><br/>
                </div>
                <div class="text-center"> 
                <a href="<?php echo url('/harga/all'); ?>" type="button" class="btn btn-dark btn-sm">Selengkapnya</a><br><br>
              </div>
              </div>
          </div><!-- Service3 end -->
        </div><!-- Col 3 end -->

        <div class="col-lg-3">
        <div class="card">
            <div class="ts-service-image-wrapper">
              <center><a href="<?php echo url('/nspm/all'); ?>">
              <img class='item' loading="lazy" style="border-top-left-radius:15px; border-top-right-radius:15px;" width='252px' height='150' src="{{asset('constra/images/icon-image/kerjasama.jpg')}}" alt="service-image">
              </a></center>
            </div>
              <div class="">
                <div class="ts-service-info"><center>
                    <h3 class="service-box-title"><a href="<?php echo url('/nspm/all'); ?>">Data NSPM</a></h3>
                    <a href="<?php echo url('/nspm/all'); ?>">Data NSPM kategorinya, Umum, Karya Cipta, Bina Marga, ESDM, Perumahan Permukiman dan Sumber Daya Air
                    </a>
                  <br/><br/>
                  <div class="text-center"> 
                    <a href="<?php echo url('/nspm/all'); ?>" type="button" class="btn btn-dark btn-sm">Selengkapnya</a><br><br>
                  </div>
                </div>
              </div>
          </div><!-- Service3 end -->
        </div><!-- Col 3 end -->

    </div>
    <!--/ Content row end -->
    
  </div>
  <!--/ Container end -->
</section>

<?php /*
<!--/ News end -->
<section class="facts-area dark-bg">
  <div class="container">
  <div class="row text-center">
        <div class="col-12">
          <h2 class="section-title">Peta Sebaran Toko Bahan Bangunan</h2>
          <h3 class="section-sub-title">DIY</h3>
        </div>
    </div>

    <div class="action-style-box">
        <div class="row align-items-center">
          <div class="col-md-12">
          <iframe src="https://www.google.com/maps/d/embed?mid=1Cc5m6V2nLFUX1f9ep_T_f3AV0sqg9MSr" width="1100" height="600"></iframe>
          </div><!-- col end -->
        </div><!-- row end -->
    </div><!-- Action style box -->
   
  </div><!-- Container end -->
</section><!-- Action end -->
*/ ?>

<?php
      $popup = DB::table('tbl_popup')
		  ->select('*')
          ->where('status_popup', '=', 'aktif')
          ->get();
          ?>
		  @foreach ($popup as $p)
<div class="popup-wrapper" id="popup">
	<div class="popup-container">

		<!-- Konten popup, silahkan ganti sesuai kebutughan -->
		<center><h4>{{$p->judul_popup}}</h4></center><br/>
          <center>
          <a href="<?php echo url("$p->url_popup"); ?>">
      <img src="{{asset('storage/images/popup/'.$p->image_popup)}}" alt="{{$p->image_popup}}" width='65%' class="brand-image img-circle elevation-2" style="opacity: .8">
          </a>      
          </center>
		<a class="popup-close" href="#popup">X</a>
    
    <center>
		<a class="btn btn-info" href="<?php echo url("$p->url_popup"); ?>">Detail</a>
    <button type="button" class="btn btn-info" data-dismiss="modal"><b>Close</b></button>
    </center>
	</div>
</div>
      @endforeach

<link rel="stylesheet" href="{{url('/constra/css/demo.css')}}">
<link rel="stylesheet" href="{{url('/constra/css/text1.css')}}">
    @endsection