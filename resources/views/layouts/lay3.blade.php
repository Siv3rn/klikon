<!DOCTYPE html>
<html lang="en-US" dir="ltr">

  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!-- ===============================================-->
    <!--    Document Title-->
    <!-- ===============================================-->
    <title>KANG JOGJA</title>


    <!-- ===============================================-->
    <!--    Favicons-->
    <!-- ===============================================-->
    <link rel="icon" type="image/png" href="{{url('/constra/images/fvcn.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" type="image/png" href="{{url('/constra/images/fvcn.png')}}">
    <meta name="theme-color" content="#ffffff">


    <!-- ===============================================-->
    <!--    Stylesheets-->
    <!-- ===============================================-->
    <link href="{{url('v2/assets/css/theme.css')}}" rel="stylesheet" />
  <link rel="stylesheet" href="{{url('/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{url('/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{url('/plugins/datatables-buttons/css/buttons.bootstrap4.min.css')}}">
  </head>


  <body>

    <!-- ===============================================-->
    <!--    Main Content-->
    <!-- ===============================================-->
    <main class="main" id="top">
      <nav class="navbar navbar-expand-lg navbar-light fixed-top py-3 d-block navbar-klean" data-navbar-on-scroll="data-navbar-on-scroll">
        <div class="container">
        <a class="navbar-brand" href="<?php echo url("/"); ?>"> 
            <img class="me-3 d-inline-block" src="{{asset('/images/logo.png')}}" alt="logo">
        </a>
          <button class="navbar-toggler collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
          <div class="collapse navbar-collapse border-top border-lg-0 mt-4 mt-lg-0" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto pt-2 pt-lg-0 font-base">
              <li class="nav-item px-2" data-anchor="data-anchor"><a class="nav-link" aria-current="page" href="<?php echo url("/kangjogja"); ?>">Beranda</a></li>
              <li class="nav-item px-2" data-anchor="data-anchor"><a class="nav-link" href="<?php echo url("/kangjogja/tukang"); ?>">Tukang</a></li>
              <li class="nav-item px-2" data-anchor="data-anchor"><a class="nav-link" href="<?php echo url("/kangjogja/toko"); ?>">Toko Material</a></li>
              <li class="nav-item px-2" data-anchor="data-anchor"><a class="nav-link" href="<?php echo url("/kangjogja/tentangkami"); ?>">Tentang Kami</a></li>
              <li class="nav-item px-2" data-anchor="data-anchor"><a class="nav-link" href="<?php echo url("/"); ?>">Klinik Konstruksi</a></li>
            </ul>
            <form class="ps-lg-5">
              <a class="btn btn-light shadow-klean order-0" href="/login" type="submit"><span class="text-gradient fw-bold">Login</span></a>
            </form>
          </div>
        </div>
      </nav>
      @yield('content')

      <section class="pb-0 pt-6">

        <div class="container">
          
          <hr class="text-100 mb-0" />
          <div class="row justify-content-md-between justify-content-evenly py-3">
            <div class="col-12 col-sm-8 col-md-6 col-lg-auto text-center text-md-start">
              <p class="fs--1 my-2 fw-bold">All rights Reserved &copy; Pemerintah Daerah Daerah Istimewa Yogyakarta, 2022</p>
            </div>
            <div class="col-12 col-sm-8 col-md-6">
              <p class="fs--1 my-2 text-center text-md-end"> Made with&nbsp;
                <svg class="bi bi-suit-heart-fill" xmlns="http://www.w3.org/2000/svg" width="12" height="12" fill="#EB6453" viewBox="0 0 16 16">
                  <path d="M4 1c2.21 0 4 1.755 4 3.92C8 2.755 9.79 1 12 1s4 1.755 4 3.92c0 3.263-3.234 4.414-7.608 9.608a.513.513 0 0 1-.784 0C3.234 9.334 0 8.183 0 4.92 0 2.755 1.79 1 4 1z"></path>
                </svg>&nbsp;by&nbsp;<a class="fw-bold text-500" href="https://klinikkonstruksi.jogjaprov.go.id" target="_blank">Klinik Konstruksi Dev. </a>
              </p>
            </div>
          </div>
		  
        </div>
      </section>
    </main>
    <!-- ===============================================-->
    <!--    End of Main Content-->
    <!-- ===============================================-->




    <!-- ===============================================-->
    <!--    JavaScripts-->
    <!-- ===============================================-->
    <script src="{{url('v2/vendors/@popperjs/popper.min.js')}}"></script>
    <script src="{{url('v2/vendors/bootstrap/bootstrap.min.js')}}"></script>
    <script src="{{url('v2/vendors/is/is.min.js')}}"></script>
    <script src="https://polyfill.io/v3/polyfill.min.js?features=window.scroll"></script>

    </body>

</html>