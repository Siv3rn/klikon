<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Klinik Konstruksi | Dashboard</title>
  
    <!-- Favicon
    ================================================== -->
    <link rel="icon" href="{{url('/constra/images/fvcn.png')}}">

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{url('/plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="{{url('/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{url('/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <!-- JQVMap -->
  <link rel="stylesheet" href="{{url('/plugins/jqvmap/jqvmap.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{url('/dist/css/adminlte.min.css')}}">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{url('/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{url('/plugins/daterangepicker/daterangepicker.css')}}">
  <!-- summernote -->
  <link rel="stylesheet" href="{{url('/plugins/summernote/summernote-bs4.min.css')}}">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  
<style>
  .cap   { text-transform: capitalize; }
  .up   { text-transform: uppercase; }
</style>

  <!-- DataTables -->
  <link rel="stylesheet" href="{{url('/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{url('/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{url('/plugins/datatables-buttons/css/buttons.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{url('/constra/css/load.css')}}">
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Preloader -->
  <!--
  <div class="preloader flex-column justify-content-center align-items-center">
    <div id="loading">
		<div class="logoLoader"></div>
		<span class="loader"></span>
		<div class="textLoader">
			<center>
			<b>Please Wait ... </b>
			</center>
		</div>
	</div>
  </div>
-->

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">

      
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="fas fa-user-plus"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          
          <div class="dropdown-divider"></div>
          <a href="<?php  echo url('/usersmanagement/users/edit_users/'.Auth::user()->id); ?>" class="dropdown-item">
            <i class="far fa-address-card"></i> Update ({{ Auth::user()->name }})
          </a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="{{ route('logout') }}"
             onclick="event.preventDefault();
             document.getElementById('logout-form').submit();">
                                        
            <i class="fas fa-sign-out-alt"></i> Log Out
          </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
            </form>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-widget="fullscreen" href="#" role="button">
          <i class="fas fa-expand-arrows-alt"></i>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
          <i class="fas fa-th-large"></i>
        </a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/home" class="brand-link">
      <img src="{{url('/constra/images/fvcn.png')}}" alt="PU" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">Admin Klinik Konstruksi</span>
    </a>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          
          <li class="nav-item">
            <a href="<?php echo url('/home'); ?>" class="nav-link
            @if(Request::getRequestUri() == '/home')
                active 
            @endif
            ">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item 
            @if(Request::getRequestUri() == '/menu')
            menu-open 
            @endif
            @if(Request::getRequestUri() == '/menu/add')
            menu-open 
            @endif
            @if(Request::getRequestUri() == '/menu/link/add')
            menu-open 
            @endif
            @if(Request::getRequestUri() == '/menu/video/add_video')
            menu-open 
            @endif
            @if(Request::getRequestUri() == '/menu/nspm')
            menu-open 
            @endif

            @if(Request::getRequestUri() == '/menu/kangjogja')
            menu-open 
            @endif

            @if(Request::getRequestUri() == '/menu/toko')
            menu-open 
            @endif
            <?php
            $cut = Request::getRequestUri();
            $poin = preg_replace("/[^a-zA-Z]/", "", $cut);
            ?>
            @if($poin == 'menuedit')
            menu-open 
            @endif
            <?php
            $cut = Request::getRequestUri();
            $poin = preg_replace("/[^a-zA-Z]/", "", $cut);
            ?>
            @if($poin == 'menulinkedit')
            menu-open 
            @endif
            <?php
            $cut = Request::getRequestUri();
            $poin = preg_replace("/[^a-zA-Z]/", "", $cut);
            ?>
            @if($poin == 'menuvideoeditvideo')
            menu-open 
            @endif
            <?php
            $cut = Request::getRequestUri();
            $poin = preg_replace("/[^a-zA-Z]/", "", $cut);
            ?>
            @if($poin == 'menunspmkategori')
            menu-open 
            @endif
            <?php
            $cut = Request::getRequestUri();
            $poin = preg_replace("/[^a-zA-Z]/", "", $cut);
            ?>
            @if($poin == 'menukangjogjaedittukang')
            menu-open 
            @endif
            @if($poin == 'menukangjogjaedittoko')
            menu-open 
            @endif
          ">
            <a href="#" class="nav-link 
            @if(Request::getRequestUri() == '/menu')
                active 
            @endif
            @if(Request::getRequestUri() == '/menu/add')
                active 
            @endif 

            @if(Request::getRequestUri() == '/menu/nspm')
                active 
            @endif 
            
            @if(Request::getRequestUri() == '/menu/kangjogja')
                active 
            @endif 

            @if(Request::getRequestUri() == '/menu/toko')
                active 
            @endif 
            
            @if(Request::getRequestUri() == '/menu/nspm_kategori')
                active 
            @endif 

            
            @if(Request::getRequestUri() == '/menu/nspm_kriteria')
                active 
            @endif 

            
            @if(Request::getRequestUri() == '/menu/nspm_kriteria1')
                active 
            @endif 

            @if(Request::getRequestUri() == '/menu/link/add')
                active 
            @endif 
            @if(Request::getRequestUri() == '/menu/video/add_video')
                active 
            @endif
            @if($poin == 'menuedit')
                active 
            @endif
            @if($poin == 'menulinkedit')
                active 
            @endif
            @if($poin == 'menuvideoeditvideo')
                active 
            @endif

            @if($poin == 'menukangjogjaedittukang')
             active
            @endif

            @if($poin == 'menukangjogjaedittoko')
             active
            @endif
            ">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Master
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo url('/menu'); ?>" class="nav-link 
                @if(Request::getRequestUri() == '/menu')
                active 
                @endif
                @if(Request::getRequestUri() == '/menu/add')
                active 
                @endif
                @if(Request::getRequestUri() == '/menu/link/add')
                active 
                @endif
                @if(Request::getRequestUri() == '/menu/video/add_video')
                active 
                @endif
                @if($poin == 'menuedit')
                active 
                @endif
                @if($poin == 'menulinkedit')
                active 
                @endif
                @if($poin == 'menuvideoeditvideo')
                active 
                @endif
                ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Menu Management</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="<?php echo url('/menu/kangjogja'); ?>" class="nav-link
                @if(Request::getRequestUri() == '/menu/kangjogja')
                active 
                @endif 

                @if(Request::getRequestUri() == '/menu/toko')
                    active 
                @endif
                
                @if($poin == 'menukangjogjaedittukang')
                active 
                @endif

                @if($poin == 'menukangjogjaedittoko')
                active 
                @endif
                ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Kang Jogja</p>
                </a>
              </li>

              <li class="nav-item
              
              @if(Request::getRequestUri() == '/menu/nspm')
                  active 
              @endif 
              
              ">
                <a href="<?php echo url('/menu/nspm'); ?>" class="nav-link 
                
              @if(Request::getRequestUri() == '/menu/nspm')
                  active 
              @endif 
              ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>NSPM</p>
                </a>
              </li>

            </ul>
          </li>

          <li class="nav-item
            @if(Request::getRequestUri() == '/informasi/berita')
            menu-open 
            @endif
            @if(Request::getRequestUri() == '/informasi/berita/add')
            menu-open 
            @endif
            <?php
            $cut = Request::getRequestUri();
            $poin = preg_replace("/[^a-zA-Z]/", "", $cut);
            ?>
            @if($poin == 'informasiberitaedit')
            menu-open 
            @endif

            @if(Request::getRequestUri() == '/informasi/artikel')
            menu-open 
            @endif
            @if(Request::getRequestUri() == '/informasi/artikel/add')
            menu-open 
            @endif
            <?php
            $cut = Request::getRequestUri();
            $poin = preg_replace("/[^a-zA-Z]/", "", $cut);
            ?>
            @if($poin == 'informasiagendaedit')
            menu-open 
            @endif

            @if(Request::getRequestUri() == '/informasi/agenda')
            menu-open 
            @endif
            @if(Request::getRequestUri() == '/informasi/agenda/add')
            menu-open 
            @endif
            <?php
            $cut = Request::getRequestUri();
            $poin = preg_replace("/[^a-zA-Z]/", "", $cut);
            ?>
            @if($poin == 'informasieventedit')
            menu-open 
            @endif
            
            @if(Request::getRequestUri() == '/informasi/event')
            menu-open 
            @endif
            @if(Request::getRequestUri() == '/informasi/event/add')
            menu-open 
            @endif
            <?php
            $cut = Request::getRequestUri();
            $poin = preg_replace("/[^a-zA-Z]/", "", $cut);
            ?>
            @if($poin == 'informasiagendaedit')
            menu-open 
            @endif
            ">
            <a href="#" class="nav-link 
            @if(Request::getRequestUri() == '/informasi/berita')
                active 
            @endif
            @if(Request::getRequestUri() == '/informasi/berita/add')
            active 
            @endif
            @if($poin == 'informasiberitaedit')
            active 
            @endif
            @if(Request::getRequestUri() == '/informasi/artikel')
                active 
            @endif
            @if(Request::getRequestUri() == '/informasi/artikel/add')
            active 
            @endif
            @if($poin == 'informasiartikeledit')
            active 
            @endif
            @if(Request::getRequestUri() == '/informasi/agenda')
                active 
            @endif
            @if(Request::getRequestUri() == '/informasi/agenda/add')
            active 
            @endif
            @if($poin == 'informasiagendaedit')
            active 
            @endif
            @if(Request::getRequestUri() == '/informasi/event')
                active 
            @endif
            @if(Request::getRequestUri() == '/informasi/event/add')
            active 
            @endif
            @if($poin == 'informasieventedit')
            active 
            @endif
            ">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Informasi
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo url('/informasi/berita'); ?>" class="nav-link
                @if(Request::getRequestUri() == '/informasi/berita')
                active 
            @endif
            @if(Request::getRequestUri() == '/informasi/berita/add')
            active 
            @endif
            @if($poin == 'informasiberitaedit')
            active 
            @endif
                ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Berita</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="<?php echo url('/informasi/artikel'); ?>" class="nav-link 
              @if($poin == 'informasiartikeledit')
                active
                @endif
                @if(Request::getRequestUri() == '/informasi/artikel/add')
                active
                @endif
                @if(Request::getRequestUri() == '/informasi/artikel')
                active
                @endif
                ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Artikel</p>
                </a>
              </li>
              
              <li class="nav-item">
                <a href="<?php echo url('/informasi/agenda'); ?>" class="nav-link 
                @if(Request::getRequestUri() == '/informasi/agenda')
                active 
                @endif
                @if(Request::getRequestUri() == '/informasi/agenda/add')
                active 
                @endif
                @if($poin == 'informasiagendaedit')
                active 
                @endif
                    ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Agenda</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="<?php echo url('/informasi/event'); ?>" class="nav-link 
                @if(Request::getRequestUri() == '/informasi/event')
                active 
                @endif
                @if(Request::getRequestUri() == '/informasi/event/add')
                active 
                @endif
                @if($poin == 'informasieventedit')
                active 
                @endif
                    ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Event</p>
                </a>
              </li>

            </ul>
          </li>

          <li class="nav-item  
            @if(Request::getRequestUri() == '/unduh/poster')
            menu-open 
            @endif
            @if(Request::getRequestUri() == '/unduh/poster/add')
            menu-open 
            @endif
            @if(Request::getRequestUri() == '/unduh/buku')
            menu-open 
            @endif
            @if(Request::getRequestUri() == '/unduh/buku/add')
            menu-open 
            @endif
            
            @if(Request::getRequestUri() == '/unduh/peraturan')
            menu-open 
            @endif
            
            @if(Request::getRequestUri() == '/unduh/peraturan/add')
            menu-open 
            @endif
            
            @if(Request::getRequestUri() == '/unduh/sk')
            menu-open 
            @endif
            
            @if(Request::getRequestUri() == '/unduh/sk/add')
            menu-open 
            @endif
            
            @if(Request::getRequestUri() == '/unduh/covid')
            menu-open 
            @endif

            @if(Request::getRequestUri() == '/unduh/covid/add')
            menu-open 
            @endif

            @if(Request::getRequestUri() == '/unduh/spektek/add')
            menu-open 
            @endif

            @if(Request::getRequestUri() == '/unduh/spektek')
            menu-open 
            @endif
            <?php
            $cut = Request::getRequestUri();
            $poin = preg_replace("/[^a-zA-Z]/", "", $cut);
            ?>
            @if($poin == 'unduhposteredit')
            menu-open 
            @endif
            @if($poin == 'unduhbukuedit')
            menu-open 
            @endif
            
            @if($poin == 'unduhperaturanedit')
            menu-open 
            @endif
            
            @if($poin == 'unduhskedit')
            menu-open 
            @endif
            
            @if($poin == 'unduhcovidedit')
            menu-open 
            @endif

            @if($poin == 'unduhspektekedit')
            menu-open 
            @endif
            ">
            <a href="#" class="nav-link
            @if(Request::getRequestUri() == '/unduh/poster')
                active 
            @endif
            @if(Request::getRequestUri() == '/unduh/poster/add')
                active 
            @endif 
            @if($poin == 'unduhposteredit')
                active 
            @endif

            @if(Request::getRequestUri() == '/unduh/buku')
                active 
            @endif
            @if(Request::getRequestUri() == '/unduh/buku/add')
                active 
            @endif 
            @if($poin == 'unduhbukuedit')
                active 
            @endif
            
            @if(Request::getRequestUri() == '/unduh/peraturan')
                active 
            @endif
            @if(Request::getRequestUri() == '/unduh/peraturan/add')
                active 
            @endif 
            @if($poin == 'unduhperaturanedit')
                active 
            @endif
            
            @if(Request::getRequestUri() == '/unduh/spektek')
                active 
            @endif
            @if(Request::getRequestUri() == '/unduh/spektek/add')
                active 
            @endif 
            @if($poin == 'unduhspektekedit')
                active 
            @endif

            @if(Request::getRequestUri() == '/unduh/sk')
                active 
            @endif
            @if(Request::getRequestUri() == '/unduh/sk/add')
                active 
            @endif 
            @if($poin == 'unduhskedit')
                active 
            @endif
            
            @if(Request::getRequestUri() == '/unduh/covid')
                active 
            @endif
            @if(Request::getRequestUri() == '/unduh/covid/add')
                active 
            @endif 
            @if($poin == 'unduhcovidedit')
                active 
            @endif
            ">
              <i class="nav-icon fas fa-folder"></i>
              <p>
                Unduh
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo url('/unduh/poster'); ?>" class="nav-link  
              @if(Request::getRequestUri() == '/unduh')
                active 
                @endif
              @if(Request::getRequestUri() == '/unduh/poster')
                active 
                @endif
                @if(Request::getRequestUri() == '/unduh/poster/add')
                    active 
                @endif 
                @if($poin == 'unduhposteredit')
                    active 
                @endif
                ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Poster</p>
                </a>
              </li>
              
              <li class="nav-item">
                <a href="<?php echo url('/unduh/buku'); ?>" class="nav-link
            @if(Request::getRequestUri() == '/unduh/buku')
                active 
            @endif
            @if(Request::getRequestUri() == '/unduh/buku/add')
                active 
            @endif 
            @if($poin == 'unduhbukuedit')
                active 
            @endif
                ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Buku</p>
                </a>
              </li>
    
              <li class="nav-item">
                <a href="<?php echo url('/unduh/peraturan'); ?>" class="nav-link                
            @if(Request::getRequestUri() == '/unduh/peraturan')
                active 
            @endif
            @if(Request::getRequestUri() == '/unduh/peraturan/add')
                active 
            @endif 
            @if($poin == 'unduhperaturanedit')
                active 
            @endif
                ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Peraturan</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="<?php echo url('/unduh/spektek'); ?>" class="nav-link
                
            @if(Request::getRequestUri() == '/unduh/spektek')
                active 
            @endif
            @if(Request::getRequestUri() == '/unduh/spektek/add')
                active 
            @endif 
            @if($poin == 'unduhspektekedit')
                active 
            @endif
                ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Spesifikasi T.K.B Gedung</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="<?php echo url('/unduh/sk'); ?>" class="nav-link
            @if(Request::getRequestUri() == '/unduh/sk')
                active 
            @endif
            @if(Request::getRequestUri() == '/unduh/sk/add')
                active 
            @endif 
            @if($poin == 'unduhskedit')
                active 
            @endif
                ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>SK Harga Bhn Bangunan</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="<?php echo url('/unduh/covid'); ?>" class="nav-link
                
            @if(Request::getRequestUri() == '/unduh/covid')
                active 
            @endif
            @if(Request::getRequestUri() == '/unduh/covid/add')
                active 
            @endif 
            @if($poin == 'unduhcovidedit')
                active 
            @endif 
            ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Tematik Covid19</p>
                </a>
              </li>

            </ul>
          </li>

          <li class="nav-item 
          @if(Request::getRequestUri() == '/usersmanagement/users')
            menu-open 
            @endif
            @if(Request::getRequestUri() == '/usersmanagement/users/register')
            menu-open 
            @endif
            @if(Request::getRequestUri() == '/usersmanagement/roles/add')
            menu-open 
            @endif
            <?php
            $cut = Request::getRequestUri();
            $poin = preg_replace("/[^a-zA-Z]/", "", $cut);
            ?>
            @if($poin == 'usersmanagementuserseditusers')
            menu-open 
            @endif
            @if($poin == 'usersmanagementroleseditusers')
            menu-open 
            @endif
            ">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-user-plus"></i>
              <p>
                Users Management
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo url('/usersmanagement/users'); ?>" class="nav-link
                @if(Request::getRequestUri() == '/usersmanagement/users')
                active
                @endif
                @if(Request::getRequestUri() == '/usersmanagement/users/register')
                active 
                @endif
                @if(Request::getRequestUri() == '/usersmanagement/roles/add')
                active 
                @endif
                <?php
                $cut = Request::getRequestUri();
                $poin = preg_replace("/[^a-zA-Z]/", "", $cut);
                ?>
                @if($poin == 'usersmanagementuserseditusers')
                active 
                @endif
                @if($poin == 'usersmanagementrolesedit')
                active 
                @endif
                ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Users Data</p>
                </a>
              </li>

            </ul>
          </li>

          <li class="nav-item
          @if(Request::getRequestUri() == '/layanan/harga')
            menu-open 
            @endif
            @if(Request::getRequestUri() == '/layanan/harga/add')
            menu-open 
            @endif
          @if(Request::getRequestUri() == '/layanan/hbb')
            menu-open 
            @endif
            @if(Request::getRequestUri() == '/layanan/hbb/add')
            menu-open 
            @endif
            <?php
            $cut = Request::getRequestUri();
            $poin = preg_replace("/[^a-zA-Z]/", "", $cut);
            ?>
            @if($poin == 'layananhargaedit')
            menu-open 
            @endif

            @if(Request::getRequestUri() == '/layanan/jenis')
            menu-open 
            @endif
            @if(Request::getRequestUri() == '/layanan/jenis/add_jenis')
            menu-open 
            @endif

            @if($poin == 'layananhbbedit')
            menu-open 
            @endif

            @if($poin == 'layananhbbeditbarang')
            menu-open 
            @endif
            <?php
            $cut = Request::getRequestUri();
            $poin = preg_replace("/[^a-zA-Z]/", "", $cut);
            ?>
            @if($poin == 'layananjeniseditjenis')
            menu-open 
            @endif

            @if(Request::getRequestUri() == '/layanan/konsultasi')
            menu-open 
            @endif
            @if(Request::getRequestUri() == '/layanan/konsultasi/add')
            menu-open 
            @endif
            <?php
            $cut = Request::getRequestUri();
            $poin = preg_replace("/[^a-zA-Z]/", "", $cut);
            ?>
            @if($poin == 'layanankonsultasiedit')
            menu-open 
            @endif
            
            @if(Request::getRequestUri() == '/layanan/konstruksi')
            menu-open 
            @endif
            @if(Request::getRequestUri() == '/layanan/konstruksi/add')
            menu-open 
            @endif
            <?php
            $cut = Request::getRequestUri();
            $poin = preg_replace("/[^a-zA-Z]/", "", $cut);
            ?>
            @if($poin == 'layanankonstruksiedit')
            menu-open 
            @endif

            @if(Request::getRequestUri() == '/layanan/barang')
                    menu-open 
                    @endif 
                    @if(Request::getRequestUri() == '/layanan/barang/add')
                    menu-open 
                    @endif 
                    @if(Request::getRequestUri() == '/layanan/hbb/add_barang')
                    menu-open 
                    @endif 
                    @if($poin == 'layananbarangedit')
                    menu-open 
                    @endif

                    @if(Request::getRequestUri() == '/layanan/lokasi')
                    menu-open 
                    @endif 
                    @if(Request::getRequestUri() == '/layanan/lokasi/add')
                    menu-open 
                    @endif 
                    @if($poin == 'layananlokasiedit')
                    menu-open 
                    @endif

                    @if(Request::getRequestUri() == '/layanan/unduh')
                    menu-open 
                    @endif 
                    @if(Request::getRequestUri() == '/layanan/unduh/add')
                    menu-open 
                    @endif 
                    @if($poin == 'layananunduhedit')
                    menu-open 
                    @endif
            ">
            <a href="#" class="nav-link
            @if(Request::getRequestUri() == '/layanan/harga')
            menu-open  
            @endif
            @if(Request::getRequestUri() == '/layanan/harga/add')
            menu-open  
            @endif 

            @if(Request::getRequestUri() == '/layanan/jenis')
            menu-open  
            @endif 
            @if(Request::getRequestUri() == '/layanan/jenis/add_jenis')
            menu-open  
            @endif 

            @if(Request::getRequestUri() == '/layanan/konstruksi')
            menu-open  
            @endif 

            @if(Request::getRequestUri() == '/layanan/konsultasi')
            menu-open  
            @endif 
            
            @if(Request::getRequestUri() == '/layanan/konsultasi/add')
            menu-open  
            @endif 

            @if(Request::getRequestUri() == '/layanan/konstruksi/add')
            menu-open  
            @endif 

            @if($poin == 'layananhargaedit')
            menu-open  
            @endif
            @if($poin == 'layananjeniseditjenis')
            menu-open  
            @endif
            @if($poin == 'layanankonsultasiedit')
            menu-open  
            @endif
            @if($poin == 'layanankonstruksiedit')
            menu-open  
            @endif

            @if(Request::getRequestUri() == '/layanan/barang')
                    menu-open 
                    @endif 
                    @if(Request::getRequestUri() == '/layanan/barang/add')
                    menu-open 
                    @endif 
                    @if($poin == 'layananbarangedit')
                    menu-open 
                    @endif

                    @if(Request::getRequestUri() == '/layanan/lokasi')
                    menu-open 
                    @endif 
                    @if(Request::getRequestUri() == '/layanan/lokasi/add')
                    menu-open 
                    @endif 
                    @if($poin == 'layananlokasiedit')
                    menu-open 
                    @endif
            ">
              <i class="nav-icon fas fa-store"></i>
              <p>
                Layanan
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo url('/layanan/harga'); ?>" class="nav-link
                @if(Request::getRequestUri() == '/layanan/harga')
                active 
                @endif 
                @if(Request::getRequestUri() == '/layanan/harga/add')
                    active 
                @endif 
                
                @if($poin == 'layananhargaedit')
                active 
                @endif
                
                ">
                  <i class="fas fa-shopping-cart nav-icon"></i>
                  <p>HSBB V1</p>
                </a>
              </li>

              <li class="nav-item
              @if(Request::getRequestUri() == '/layanan/hbb')
                menu-open 
                @endif 
                @if(Request::getRequestUri() == '/layanan/hbb/add')
                menu-open  
                @endif 
                @if($poin == 'layananhbbedit')
                menu-open  
                @endif
                @if($poin == 'layananhbbeditbarang')
                menu-open  
                @endif

                @if(Request::getRequestUri() == '/layanan/jenis')
                menu-open  
                @endif                
                @if(Request::getRequestUri() == '/layanan/jenis/add_jenis')
                menu-open  
                @endif
                @if($poin == 'layananhargaedit')
                menu-open  
                @endif
                @if($poin == 'layananjeniseditjenis')
                menu-open  
                @endif
                @if(Request::getRequestUri() == '/layanan/barang')
                    menu-open 
                    @endif 
                    @if(Request::getRequestUri() == '/layanan/barang/add')
                    menu-open 
                    @endif 
                    @if($poin == 'layananbarangedit')
                    menu-open 
                    @endif

                    @if(Request::getRequestUri() == '/layanan/bahan')
                    menu-open 
                    @endif 
                    @if(Request::getRequestUri() == '/layanan/bahan/add')
                    menu-open 
                    @endif 
                    @if(Request::getRequestUri() == '/layanan/hbb/add_barang')
                    menu-open 
                    @endif 
                    @if($poin == 'layananbahanedit')
                    menu-open 
                    @endif

                    @if(Request::getRequestUri() == '/layanan/lokasi')
                    menu-open 
                    @endif 
                    @if(Request::getRequestUri() == '/layanan/lokasi/add')
                    menu-open 
                    @endif 
                    @if($poin == 'layananlokasiedit')
                    menu-open 
                    @endif

                    @if(Request::getRequestUri() == '/layanan/unduh')
                    menu-open 
                    @endif 
                    @if(Request::getRequestUri() == '/layanan/unduh/add')
                    menu-open 
                    @endif 
                    @if($poin == 'layananunduhedit')
                    menu-open 
                    @endif
                ">
                <a href="#" class="nav-link
                @if(Request::getRequestUri() == '/layanan/hbb')
                active 
                @endif 
                @if(Request::getRequestUri() == '/layanan/hbb/add')
                    active 
                @endif 
                @if($poin == 'layananhbbedit')
                active 
                @endif
                @if($poin == 'layananhbbeditbarang')
                active 
                @endif

                @if(Request::getRequestUri() == '/layanan/jenis')
                active 
                @endif                
                @if(Request::getRequestUri() == '/layanan/jenis/add_jenis')
                active 
                @endif
                @if($poin == 'layananhargaedit')
                active 
                @endif
                @if($poin == 'layananjeniseditjenis')
                active 
                @endif

                @if(Request::getRequestUri() == '/layanan/barang')
                    active 
                    @endif 
                    @if(Request::getRequestUri() == '/layanan/barang/add')
                    active 
                    @endif
                    @if(Request::getRequestUri() == '/layanan/hbb/add_barang')
                    active 
                    @endif  
                    @if($poin == 'layananbarangedit')
                    active 
                    @endif

                    @if(Request::getRequestUri() == '/layanan/lokasi')
                    active 
                    @endif 
                    @if(Request::getRequestUri() == '/layanan/lokasi/add')
                    active 
                    @endif 
                    @if($poin == 'layananlokasiedit')
                    active 
                    @endif

                    @if(Request::getRequestUri() == '/layanan/unduh')
                    active 
                    @endif 
                    @if(Request::getRequestUri() == '/layanan/unduh/add')
                    active 
                    @endif 
                    @if($poin == 'layananunduhedit')
                    active 
                    @endif
                ">
                  <i class="nav-icon fas fa-shopping-cart "></i>
                  <p>
                    HSBB V2
                    <i class="fas fa-angle-left right"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="<?php echo url('/layanan/jenis'); ?>" class="nav-link
                    @if(Request::getRequestUri() == '/layanan/bahan')
                    active 
                    @endif 
                    @if(Request::getRequestUri() == '/layanan/bahan/add')
                    active 
                    @endif 
                    @if($poin == 'layananbahanedit')
                    active 
                    @endif

                    
                    @if(Request::getRequestUri() == '/layanan/jenis')
                    active 
                    @endif                
                    @if(Request::getRequestUri() == '/layanan/jenis/add_jenis')
                    active 
                    @endif
                    @if($poin == 'layananjeniseditjenis')
                    active 
                    @endif
                    ">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Jenis Bahan</p>
                    </a>
                  </li>

                  <li class="nav-item">
                    <a href="<?php echo url('/layanan/barang'); ?>" class="nav-link
                    @if(Request::getRequestUri() == '/layanan/barang')
                    active 
                    @endif 
                    @if(Request::getRequestUri() == '/layanan/barang/add')
                    active 
                    @endif 
                    @if(Request::getRequestUri() == '/layanan/hbb/add_barang')
                    active 
                    @endif 
                    @if($poin == 'layananbarangedit')
                    active 
                    @endif
                    ">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Barang</p>
                    </a>
                  </li>

                  <li class="nav-item">
                    <a href="<?php echo url('/layanan/lokasi'); ?>" class="nav-link
                    @if(Request::getRequestUri() == '/layanan/lokasi')
                    active 
                    @endif 
                    @if(Request::getRequestUri() == '/layanan/lokasi/add')
                    active 
                    @endif 
                    @if($poin == 'layananlokasiedit')
                    active 
                    @endif
                    ">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Lokasi</p>
                    </a>
                  </li>
                  
                  <li class="nav-item">
                    <a href="<?php echo url('/layanan/hbb'); ?>" class="nav-link
                    @if(Request::getRequestUri() == '/layanan/hbb')
                    active 
                    @endif 
                    @if(Request::getRequestUri() == '/layanan/hbb/add')
                    active 
                    @endif 
                    @if($poin == 'layananhbbedit')
                    active 
                    @endif
                    ">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Harga</p>
                    </a>
                  </li>

                  <li class="nav-item">
                    <a href="<?php echo url('/layanan/unduh'); ?>" class="nav-link
                    @if(Request::getRequestUri() == '/layanan/unduh')
                    active 
                    @endif 
                    @if(Request::getRequestUri() == '/layanan/unduh/add')
                    active 
                    @endif 
                    @if($poin == 'layananunduhedit')
                    active 
                    @endif
                    ">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Unduh HSBB</p>
                    </a>
                  </li>

                </ul>
              </li>
              
              <li class="nav-item">
                <a href="<?php echo url('/layanan/konstruksi'); ?>" class="nav-link
                @if(Request::getRequestUri() == '/layanan/konstruksi')
                active 
                @endif 
                @if(Request::getRequestUri() == '/layanan/konstruksi/add')
                    active 
                @endif 
                @if($poin == 'layanankonstruksiedit')
                active 
                @endif
                ">
                  <i class="far fa-building nav-icon"></i>
                  <p>Konstruksi</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="<?php echo url('/layanan/konsultasi'); ?>" class="nav-link
                @if(Request::getRequestUri() == '/layanan/konsultasi')
                active 
                @endif 
                @if(Request::getRequestUri() == '/layanan/konsultasi/add')
                    active 
                @endif 
                @if($poin == 'layanankonsultasiedit')
                active 
                @endif
                ">
                  <i class="fas fa-volume-up nav-icon"></i>
                  <p>Konsultasi &nbsp;
                    <?php 
                    $count = \DB::table('tbl_konsultasi')
                    ->select(\DB::raw('COUNT(id_konsultasi) as hit'))
                    ->where('tgl_jawaban', '=', '')
                    ->orWhereNull('tgl_jawaban')
                    ->get();
                    ?>
                    <span class="badge badge-danger right">
                    @foreach($count as $cc){{$cc->hit}}@endforeach
                    </span>
                  </p>
                </a>
              </li>

            </ul>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

@yield('content')

<!-- /.content-wrapper -->
<footer class="main-footer">
    <strong>Copyright &copy; 2022 <a href="/">Klinik Konstruksi</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 1.1.0
    </div>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
    <script src="{{url('/plugins/jquery/jquery.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
    <script src="{{url('/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
    <script src="{{url('/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- ChartJS -->
    <script src="{{url('/plugins/chart.js/Chart.min.js')}}"></script>
<!-- Sparkline -->
    <script src="{{url('/plugins/sparklines/sparkline.js')}}"></script>
<!-- JQVMap -->
    <script src="{{url('/plugins/jqvmap/jquery.vmap.min.js')}}"></script>
    <script src="{{url('/plugins/jqvmap/maps/jquery.vmap.usa.js')}}"></script>
<!-- jQuery Knob Chart -->
    <script src="{{url('/plugins/jquery-knob/jquery.knob.min.js')}}"></script>
<!-- daterangepicker -->
    <script src="{{url('/plugins/moment/moment.min.js')}}"></script>
    <script src="{{url('/plugins/daterangepicker/daterangepicker.js')}}"></script>
<!-- Tempusdominus Bootstrap 4 -->
    <script src="{{url('/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
<!-- Summernote -->
    <script src="{{url('/plugins/summernote/summernote-bs4.min.js')}}"></script>
<!-- overlayScrollbars -->
    <script src="{{url('/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
<!-- AdminLTE App -->
    <script src="{{url('/dist/js/adminlte.js')}}"></script>
<!-- AdminLTE for demo purposes -->
    <script src="{{url('/dist/js/demo.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="{{url('/dist/js/pages/dashboard.js')}}"></script>

    <script src="{{url('/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{url('/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{url('/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{url('/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
    <script src="{{url('/plugins/datatables-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{url('/plugins/datatables-buttons/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{url('/plugins/jszip/jszip.min.js')}}"></script>
    <script src="{{url('/plugins/pdfmake/pdfmake.min.js')}}"></script>
    <script src="{{url('/plugins/pdfmake/vfs_fonts.js')}}"></script>
    <script src="{{url('/plugins/datatables-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{url('/plugins/datatables-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{url('/plugins/datatables-buttons/js/buttons.colVis.min.js')}}"></script>
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
<script>
  $(function () {
    // Summernote
    $('#summernote').summernote()
    $('#summernote1').summernote()

    // CodeMirror
    CodeMirror.fromTextArea(document.getElementById("codeMirrorDemo"), {
      mode: "htmlmixed",
      theme: "monokai"
    });
  })
</script>
<script src="{{url('/constra/js/loader.js')}}"></script>
</body>
</html>