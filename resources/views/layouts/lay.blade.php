<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

  <!-- Basic Page Needs
================================================== -->
  <meta charset="utf-8">
  <title>Klinik Konstruksi DIY</title>

  <!-- Mobile Specific Metas
================================================== -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="PU APLIKASI KONSULTASI|DEV : ANGGA IBNU SAPUTRA">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=5.0">

  <!-- Favicon
================================================== -->
  <link rel="icon" type="image/png" href="{{url('/constra/images/fvcn.png')}}">

  <!-- CSS
================================================== -->

  <link rel="stylesheet" href="{{url('/constra/css/kenburn.css')}}">
  <!-- Bootstrap -->
  <link rel="stylesheet" href="{{url('constra/plugins/bootstrap/bootstrap.min.css')}}">
  <!-- FontAwesome -->
  <link rel="stylesheet" href="{{url('/constra/plugins/fontawesome/css/all.min.css')}}">
  <!-- Animation -->
  <link rel="stylesheet" href="{{url('/constra/plugins/animate-css/animate.css')}}">
  <!-- slick Carousel -->
  <link rel="stylesheet" href="{{url('/constra/plugins/slick/slick.css')}}">
  <link rel="stylesheet" href="{{url('/constra/plugins/slick/slick-theme.css')}}">
  <!-- Colorbox -->
  <link rel="stylesheet" href="{{url('/constra/plugins/colorbox/colorbox.css')}}">
  <!-- Template styles-->
  <link rel="stylesheet" href="{{url('/constra/css/style.css')}}">
  <link rel="stylesheet" href="{{url('/constra/css/costum.css')}}">
  <style type="text/css">

  
   p {
      /* Center horizontally*/
      text-align: justify;
      }
  </style>
  <link rel="stylesheet" href="{{url('/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{url('/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{url('/plugins/datatables-buttons/css/buttons.bootstrap4.min.css')}}">
  {{-- mapbox --}}
  <link href='https://api.mapbox.com/mapbox-gl-js/v2.4.1/mapbox-gl.css' rel='stylesheet' />
  <script src='https://api.mapbox.com/mapbox-gl-js/v2.4.1/mapbox-gl.js'></script>

  <!-- map open street -->
  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js" integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew==" crossorigin=""></script>
    <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>


</head>
<body>
<!--
<div id="loading">
		<div class="logoLoader"></div>
		<span class="loader"></span>
		<div class="textLoader">
			<center>
			<b>Please Wait ... </b>
			</center>
		</div>
	</div>
    -->
  
<!-- Header start -->
<!--Komen-->
<header id="header" class="header-one">

  <div class="site-navigation bg-white fixed-top">
    <div class="">
        <div class="row">
          <div class="col-lg-12">
              <nav class="navbar navbar-expand-lg navbar-dark mx-5">
              
              <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
              <?php
              $ktk = \DB::table('kontak')
                ->select('*')
                    ->get();
                    ?>
                @foreach ($ktk as $k) 
                <a href="<?php echo url("/"); ?>">
                <img width='50%' loading="" src="{{asset('storage/images/logo/'.$k->logo)}}" alt="logo">     
                </a>
                  @endforeach
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".navbar-collapse" aria-controls="navbar-collapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                </div>
                <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12">    
                </div>
                <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
                <div id="navbar-collapse" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav mr-auto">
                      <li class="nav-item dropdown 
                      @if(Request::getRequestUri() == '/')
                      active
                      @endif
                      ">
                        <a href="<?php echo url("/"); ?>" class="nav-link dropdown-toggle">
                        @if(Request::getRequestUri() == '/')
                        <font size='2' color='#000'>Beranda</font>
                        @else 
                        <div class='custom_list_item'>
                        <font size='2' color='#000'>Beranda</font>
                          </div> 
                        @endif 
                        </a>
                      </li>

                      <?php $result = preg_replace("/[^0-9]/", "", \Request::getRequestUri()); ?>
                      @if(Request::getRequestUri() == '/' or Request::getRequestUri() <> '/')
                      <li class="nav-item dropdown
                      @if(Request::getRequestUri() == '/konsultasi/all')
                      active
                      @endif
                      
                      @if(Request::getRequestUri() == '/konstruksi/all')
                      active
                      @endif
                      
                      @if(Request::getRequestUri() == '/hbb/all')
                      active
                      @endif
                      
                      @if(Request::getRequestUri() == '/harga/all')
                      active
                      @endif

                      @if(Request::getRequestUri() == '/harga/statistik')
                      active
                      @endif
                      
                      @if(Request::getRequestUri() == '/nspm/all')
                      active
                      @endif

                      @if(Request::getRequestUri() == '/konsultasi/tanya')
                      active
                      @endif

                      @if(Request::getRequestUri() == '/peta/all')
                      active
                      @endif

                      @if(Request::getRequestUri() == '/layanan/unduh/all')
                      active
                      @endif
                      
                      <?php if(\Request::getRequestUri() == '/konsultasi/detail/'.$result){
                       echo'active';
                      }
                      ?>
                      <?php if(\Request::getRequestUri() == '/nspm/detail/'.$result){
                       echo'active';
                      }
                      ?>
                      <?php if(\Request::getRequestUri() == '/nspm/detail/kriteria/'.$result){
                       echo'active';
                      }
                      ?>
                      ">
                          <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
                          @if(Request::getRequestUri() == '/konsultasi/all'
                          OR Request::getRequestUri() == '/konsultasi/tanya' 
                          OR Request::getRequestUri() == '/konsultasi/detail/'.preg_replace("/[^0-9]/", "", \Request::getRequestUri())
                          OR Request::getRequestUri() == '/konstruksi/all' 
                          OR Request::getRequestUri() == '/hbb/all' 
                          OR Request::getRequestUri() == '/harga/all' 
                          OR Request::getRequestUri() == '/layanan/unduh/all' 
                          OR Request::getRequestUri() == '/peta/all' 
                          OR Request::getRequestUri() == '/harga/statistik') 
                          <font size='2' color='#000'>Layanan</font>
                           <i class="fa fa-angle-down"></i>
                          @else
                          <div class='custom_list_item'>
                          <font size='2' color='#000'>Layanan</font>
                           <i class="fa fa-angle-down"></i> 
                          </div>
                          @endif
                          </a>
                          <ul class="dropdown-menu" role="menu">
                            <li>
                              <a href="<?php echo url("/konsultasi/all"); ?>">
                              <div class='custom_list_item'>
                              <font size='2' color='#000'>Konsultasi</font>
                              </div>
                              </a></li>
                            <li>
                              <a href="<?php echo url("/konstruksi/all"); ?>">
                              <div class='custom_list_item'>
                              <font size='2' color='#000'>Konstruksi</font>
                              </div>                              
                              </a></li>
                            <li class="nav-item dropdown-submenu">
                                <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
                                  <div class='custom_list_item'>
                                  <font size='2' color='#000'>Harga Bahan Bangunan</font>
                                  <i class="fa fa-angle-down"></i>
                                  </div>
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo url("/harga/all"); ?>"><font size='2' color='#000'>Data Harga Bahan Bangunan</font></a></li> 
                                    <li><a href="<?php echo url("/harga/statistik"); ?>"><font size='2' color='#000'>Statistik Harga Bahan Bangunan</font></a></li> 
                                    <!-- <li><a href="<?php echo url("/peta/all"); ?>">Peta HSBB</a></li>       
                                    <li><a href="<?php echo url("/layanan/unduh/all"); ?>">Unduh HSBB</a></li> -->                              
                                </ul>
                            </li>
                          </ul>
                      </li>

                      @else
                      <li class="nav-item dropdown">
                        <a href="<?php echo url("/#layanan"); ?>" class="nav-link dropdown-toggle">
                              <div class='custom_list_item'>
                              <font size='2' color='#000'>Layanan</font>                                 
                           <i class="fa fa-angle-down"></i> 
                              </div>
                        </a>
                      </li>
                      @endif

                      <li class="nav-item dropdown
                      @if(Request::getRequestUri() == '/berita/all')
                      active
                      @endif
                      
                      @if(Request::getRequestUri() == '/artikel/all')
                      active
                      @endif
                      
                      @if(Request::getRequestUri() == '/event/all')
                      active
                      @endif
                      
                      @if(Request::getRequestUri() == '/layanan/unduh/all')
                      active
                      @endif

                      @if(Request::getRequestUri() == '/peta/all')
                      active
                      @endif
                                 
                      
                      @if(Request::getRequestUri() == '/agenda/schedulle')
                      active
                      @endif                        
                      

                      @if(Request::getRequestUri() <> '/')
                      <?php if(\Request::getRequestUri() == '/berita/detail/'.$result){
                       echo'active';
                      }
                      ?>
                      
                      <?php if(\Request::getRequestUri() == '/artikel/detail/'.$result){
                       echo'active';
                      }
                      ?>

                      <?php if(\Request::getRequestUri() == '/agenda/detail/'.$result){
                       echo'active';
                      }
                      ?>
                      @endif
                      ">
                          <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
                          @if(Request::getRequestUri() == '/berita/all' 
                          OR Request::getRequestUri() == '/artikel/all' 
                          OR Request::getRequestUri() == '/agenda/schedulle' 
                          OR Request::getRequestUri() == '/event/all' 
                          OR Request::getRequestUri() == '/berita/detail/'.preg_replace("/[^0-9]/", "", \Request::getRequestUri())
                          OR Request::getRequestUri() == '/artikel/detail/'.preg_replace("/[^0-9]/", "", \Request::getRequestUri())
                          OR Request::getRequestUri() == '/agenda/detail/'.preg_replace("/[^0-9]/", "", \Request::getRequestUri()))
                          <font size='2' color='#000'>Informasi</font>
                           <i class="fa fa-angle-down"></i> 
                          @else
                          <div class='custom_list_item'>
                          <font size='2' color='#000'>Informasi</font>
                           <i class="fa fa-angle-down"></i>
                          </div>
                          @endif
                          </a>
                          <ul class="dropdown-menu" role="menu">
                            <li><a href="<?php echo url("/berita/all"); ?>"><font size='2' color='#000'>Berita</font></a></li>
                            <li><a href="<?php echo url("/artikel/all"); ?>"><font size='2' color='#000'>Artikel</font></a></li>
                            <li><a href="<?php echo url("/agenda/schedulle"); ?>"><font size='2' color='#000'>Agenda</font></a></li>
                            <?php /*
                            <li><a href="<?php echo url("/event/all"); ?>">Event</a></li>
                           
                            <li>
                                <a href="<?php echo url("/about"); ?>">Tentang Kami</a>
                            </li>
                            */
                            ?>
                          </ul>
                      </li>

                      <li class="nav-item dropdown
                      @if(Request::getRequestUri() == '/unduh/menu')
                      active
                      @endif 

                      @if(Request::getRequestUri() == '/unduh/buku/all')
                      active
                      @endif 
                      
                      @if(Request::getRequestUri() == '/unduh/poster/all')
                      active
                      @endif 
                      
                      @if(Request::getRequestUri() == '/unduh/peraturan/all')
                      active
                      @endif 

                      @if(Request::getRequestUri() == '/unduh/sk/all')
                      active
                      @endif 

                      @if(Request::getRequestUri() == '/unduh/spesifikasi/all')
                      active
                      @endif 

                      @if(Request::getRequestUri() == '/unduh/covid/all')
                      active
                      @endif 
                      
                      ">
                      <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
                      @if(Request::getRequestUri() == '/unduh/menu'
                          OR Request::getRequestUri() == '/unduh/poster/all'
                          OR Request::getRequestUri() == '/unduh/buku/all'
                          OR Request::getRequestUri() == '/unduh/peraturan/all'
                          OR Request::getRequestUri() == '/unduh/spesifikasi/all'
                          OR Request::getRequestUri() == '/unduh/sk/all'
                          OR Request::getRequestUri() == '/unduh/covid/all')
                          <font size='2' color='#000'>Unduh</font>
                           <i class="fa fa-angle-down"></i> 
                          @else
                          <div class='custom_list_item'>
                          <font size='2' color='#000'>Unduh</font>
                           <i class="fa fa-angle-down"></i>
                          </div>
                          @endif
                          </a>
                          <ul class="dropdown-menu" role="menus">
                            <li><a href="<?php echo url("/unduh/poster/all"); ?>"><font size='2' color='#000'>Poster</font></a></li>
                            <li><a href="<?php echo url("/unduh/buku/all"); ?>"><font size='2' color='#000'>Buku</font></a></li>
                            <li><a href="<?php echo url("/unduh/peraturan/all"); ?>"><font size='2' color='#000'>Peraturan</font></a></li>
                            <li><a href="<?php echo url("/unduh/spesifikasi/all"); ?>"><font size='2' color='#000'>Spesifikasi</font></a></li>
                            <li><a href="<?php echo url("/unduh/sk/all"); ?>"><font size='2' color='#000'>SK</font></a></li>
                            <li><a href="<?php echo url("/unduh/covid/all"); ?>"><font size='2' color='#000'>Covid 19</font></a></li>
                            <?php /*
                            <li>
                                <a href="<?php echo url("/about"); ?>">Tentang Kami</a>
                            </li>
                            */
                            ?>
                          </ul>
                      </li>                      

                      <li class="nav-item dropdown
                      <?php if(\Request::getRequestUri() == '/nspm/detail/'.$result){
                       echo'active';
                      }
                      ?>

                      <?php if(\Request::getRequestUri() == '/nspm/detail/kriteria/'.$result){
                       echo'active';
                      }
                      ?>

                      <?php if(\Request::getRequestUri() == '/nspm/all'){
                       echo'active';
                      }
                      ?>
                      ">
                        <a href="<?php echo url("/nspm/all"); ?>" class="nav-link dropdown-toggle">
                          @if(Request::getRequestUri() == '/nspm/detail/'.preg_replace("/[^0-9]/", "", \Request::getRequestUri())
                          OR Request::getRequestUri() == '/nspm/detail/kriteria/'.preg_replace("/[^0-9]/", "", \Request::getRequestUri())
                          OR Request::getRequestUri() == '/nspm/all')
                          <font size='2' color='#000'>NSPM</font>
                        @else 
                        <div class='custom_list_item'>
                        <font size='2' color='#000'>NSPM</font>
                          </div> 
                        @endif 
                        </a>
                      </li>

                      <li class="nav-item dropdown">
                          <a href="<?php echo url("/kangjogja"); ?>" class="nav-link dropdown-toggle">
                            <div class='custom_list_item'>
                            <font size='2' color='#000'>Kang Jogja</font>
                            </div>
                          </a>
                      </li>
                      
                      <li class="nav-item dropdown
                  
                      <?php if(\Request::getRequestUri() == '/about'){
                       echo'active';
                      }
                      ?>
                      ">
                        <a href="<?php echo url("/about"); ?>" class="nav-link dropdown-toggle">
                        @if(Request::getRequestUri() == '/about')
                        <font size='2' color='#000'>Tentang Kami</font>
                        @else 
                        <div class='custom_list_item'>
                        <font size='2' color='#000'>Tentang Kami</font>
                          </div> 
                        @endif 
                        </a>
                      </li>
              
                    </ul>
                </div>
                </div>
              </nav>
          </div>
          <!--/ Col end -->
        </div>
        <!--/ Row end -->

        <!-- <div class="nav-search">
          <span id="search"><i class="fa fa-search"></i></span>
        </div> Search end -->

        <div class="search-block" style="display: none;">
          <label for="search-field" class="w-100 mb-0">
            <input type="text" class="form-control" id="search-field" placeholder="Type what you want and enter">
          </label>
          <span class="search-close">&times;</span>
        </div><!-- Site search end -->
    </div>
    <!--/ Container end -->

  </div>
  <!--/ Navigation end -->
</header>
<!--/ Header end -->

            @yield('content')

  <footer id="footer" class="footer bg-overlay">
    <div class="">
      <div class="container">
        <div class="row justify-content-between">
          <div class="col-lg-4 col-md-6 footer-widget footer-about">
            <br/><br/>
            <h3 class="widget-title">External Links</h3>
            <?php
                $link = \DB::table('tbl_link')
                ->select('*')
                ->get();
                ?>
                @foreach ($link as $lk) 
            
                <a href='{{$lk->url_link}}' target=_'blank'><img src="{{asset('storage/images/link/'.$lk->image_link)}}" width='150px' class='img-responsive1'></a> 
           
            @endforeach  
			
			<br/><br/>
        </div><!-- Col end -->
          <?php
          $kontak = \DB::table('kontak')
		  ->select('*')
          ->get();
          ?>
		  @foreach ($kontak as $ktk) 
          <div class="col-lg-4 col-md-6 footer-widget mt-5 mt-md-0">
            <br/><br/>
            <h3 class="widget-title">Kontak Kami</h3>
            <div class="working-hours">
            <address>
            <b>{{$ktk->instansi}}</b>
            </address>
            <address>
            {{$ktk->address}}
            </address>
            <address>
			      {{$ktk->telepon}}   
            </address>
            <address>
			      {{$ktk->email}}  
            </address>        
            </div>
			
			<br/><br/>
          </div><!-- Col end -->
          @endforeach
        </div><!-- Row end -->
      </div><!-- Container end -->
    </div><!-- Footer main end -->
    <div id="map" style="width: 600px; height: 400px; margin-left: 700px; margin-bottom:100px"></div>

    <div class="copyright">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-md-6">
            <div class="copyright-info">
              <span>Copyright &copy; 2022 <a href="/">
              @foreach ($kontak as $ktk)
              <b>{{$ktk->instansi}}</b>
              @endforeach</a></span>
            </div>
          </div>
          
          
          <div class="col-md-6">
            <div class="footer-menu text-center text-md-right">
              
            </div>
          </div>
        </div><!-- Row end -->

        <div id="back-to-top" data-spy="affix" data-offset-top="10" class="back-to-top position-fixed">
          <button class="btn btn-primary" title="Back to Top">
            <i class="fa fa-angle-double-up"></i>
          </button>
        </div>

      </div><!-- Container end -->
    </div><!-- Copyright end -->
  </footer><!-- Footer end -->


  <!-- Javascript Files
  ================================================== -->

  <!-- initialize jQuery Library -->
    <script src="{{url('/constra/plugins/jQuery/jquery.min.js')}}"></script>
  <!-- Bootstrap jQuery -->
    <script src="{{url('/constra/plugins/bootstrap/bootstrap.min.js')}}"></script>
  <!-- Slick Carousel -->
    <script src="{{url('/constra/plugins/slick/slick.min.js')}}"></script>
    <script src="{{url('/constra/plugins/slick/slick-animation.min.js')}}"></script>
  <!-- Color box -->
    <script src="{{url('/constra/plugins/colorbox/jquery.colorbox.js')}}"></script>
  <script src="../.."></script>
  <!-- shuffle -->
    <script src="{{url('/constra/plugins/shuffle/shuffle.min.js')}}"></script>
  <script src="../.." defer></script>
<!-- init map open street -->
  {{-- <script>

    var map = L.map('map').setView([-7.77442363894478, 110.43121584077474], 17);
    
    L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);
    
    L.marker([-7.77442363894478, 110.43121584077474]).addTo(map)
        .bindPopup('Balai Pengembangan Jasa Konstruksi.')
        .openPopup();

        fetch('https://nominatim.openstreetmap.org/reverse?lat=-7.7849&lon=110.3656&format=json')
  .then(response => response.json())
  .then(data => {
    // Extract the relevant data from the response
    const result = data[0];

    // Get the HTML div element to display the result
    const resultElement = document.getElementById('result');

    // Create HTML markup with the desired data
    const html = `
      <h2>Address:</h2>
      <p>${result.display_name}</p>
      <h2>Latitude:</h2>
      <p>${result.lat}</p>
      <h2>Longitude:</h2>
      <p>${result.lon}</p>
    `;

    // Set the HTML of the result div
    resultElement.innerHTML = html;
  })
  .catch(error => {
    console.error('Error:', error);
  });
        
    </script> --}}

    <script>
      mapboxgl.accessToken = 'pk.eyJ1IjoiYW5nZ2l0b3dpY2Frc29ubyIsImEiOiJjbGoydjJzZ2YxcGxyM2dxZnFzdnltNmt3In0.Ei1dFptcnVB3VQeyw9_kIg';

      var longitude = 110.3987; // Example longitude value (e.g., San Francisco)
      var latitude = -7.7839; // Example latitude value (e.g., San Francisco)
      var map = new mapboxgl.Map({
  container: 'map',
  style: 'mapbox://styles/mapbox/dark-v11',
  center: [longitude, latitude], // Set the initial center coordinates
  zoom: 17// Set the initial zoom level
});


var zoomControl = new mapboxgl.NavigationControl();
map.addControl(zoomControl, 'top-right');


var marker = new mapboxgl.Marker()
  .setLngLat([longitude, latitude])
  .addTo(map)
  .setPopup(new mapboxgl.Popup().setHTML("<h3>Balai Pengembangan Jasa Konstruksi</h3>"))
  .togglePopup();


    </script>

  <!-- Template custom -->
    <script src="{{url('/constra/js/script.js')}}"></script>
    <script src="{{url('/constra/js/loader.js')}}"></script>
    
  </div><!-- Body inner end -->
  </body>

  </html>