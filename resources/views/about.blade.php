@extends('layouts.lay')

@section('content')
<div class="banner-carousel banner-carousel-1 mb-0">
          <?php
          $banner = DB::table('tbl_slider')
		  ->select('*')
          ->where('status_slider', '=', 'aktif')
          ->get();
          ?>
		  @foreach ($banner as $b) 
      <div class="banner-carousel-item">
              <div id="imageContainer">
                <div class="box">
                 <img class="gmb" src="{{asset('images/slider/'.$b->image_slider)}}" width='100%' alt="service-icon">

                <p class='tengah1' data-animation-in="fadeIn">
                
                  <b><font size='7vw' color='#fff' >{{$b->judul_slider}}</font></b><br/>
                  <font size='4' color='#fff'>{{$b->keterangan_slider}}</font>
                
                </p>
                
                </div>
               </div>
            </div>
          @endforeach
      </div>

    <div class="row">
      
        <img src="{{asset('constra/images/bg_aboutus.png')}}" alt="">
      
    </div> 
    <section class="call-to-action-box no-padding">
  <div class="container">
    <div class="action-style-box">
        <div class="row align-items-center">
          <div class="col-12 text-center text-md-center">
              <div class="call-to-action-text">
                <h3 class="action-title">Tentang Kami</h3>
              </div>
          </div><!-- Col end -->
        </div><!-- row end -->
    </div><!-- Action style box -->
  </div><!-- Container end -->
</section><!-- Action end -->
   

<section id="ts-service-area" class="ts-service-area pb-0 mb-5">
  <div class="container">
  
    <div class="row">
    
        <div class="col-lg-12">
          <div class="ts-service-box d-flex">
              <div class="ts-service-box-img">
                <img loading="lazy" src="{{asset('constra/images/icon-image/service-icon2.png')}}"  alt="service-icon">
              </div>
              <div class="ts-service-box-info">
                <h3 class="service-box-title"><a href="#">Klinik Konstruksi</a></h3>
                <b>
                Klinik Konstruksi untuk Mewujudkan Tertib Penyelenggaraan Jasa Konstruksi Layanan Klinik Konsultasi Konstruksi oleh Kementerian Pekerjaan Umum dan Perumahan Rakyat bertujuan untuk meminimalisir permasalahan konstruksi di tingkat nasional dan Daerah yang ditangani oleh tim ahli sesuai bidang, diantaranya pengadaan barang/jasa, kontrak konstruksi, dan keselamatan konstruksi.
                </b>
              </div>
          </div><!-- Service 3 end -->
        </div><!-- Col end -->
		
    </div><!-- Content row end -->

  </div>
  <!--/ Container end -->
</section><!-- Service end -->
<link rel="stylesheet" href="{{url('/constra/css/text1.css')}}">
    @endsection