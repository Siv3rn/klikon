@extends('layouts.lay2')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-12">
        @if(!empty(\Session::get('success')) > 0)
                    <div class="alert alert-success">     
                        <strong>Sukses!</strong> {!! \Session::get('success') !!}             
                        </div>
                    @endif
                <!-- /.card-header -->
              @if(count($errors) > 0)
              <div class="alert alert-danger" role="alert">
                @foreach ($errors->all() as $error)
                {{ $error }} <br/>
                @endforeach
              </div>
              @endif
              <script>
                window.setTimeout(function() {
                  $.noConflict();
                  $(".alert").fadeTo(500, 0).slideUp(500, function(){
                    $(this).remove(); 
                  });
                }, 5000);
              </script>
              <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
              <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
                </div>
                <div class="col-sm-6">
            <h1 class="m-0">Lokasi</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/home">Dashboard</a></li>
              <li class="breadcrumb-item"><a href="/layanan/harga">Informasi</a></li>
              <li class="breadcrumb-item active">Lokasi</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
                <!-- Small boxes (Stat box) -->
        <div class="row"> 
						
						<div class="col-12">
							<div class="card">
							<div class="card-header">
										<h3 class="card-title">Lokasi | &nbsp;</h3>
						   
              <button type="button" class="btn btn-primary btn-sm confirmation" data-toggle="modal" data-target="#myModal"><i class="fas fa-plus"></i> 
                Add
              </button>
                    <!-- Modal -->
                    <div class="modal fade" id="myModal" role="dialog">
												<div class="modal-dialog">
												
												<!-- Modal content-->
												<div class="modal-content">
													<div class="modal-header">
													<h4 class="modal-title">Input Lokasi</h4>
													</div>
													<div class="modal-body">
                          <form class="form" action="<?php echo url("/layanan/lokasi/create"); ?>" method="POST">
                            {{csrf_field()}}
                              <div class="card-body">

                              <div class="col-md-12">
                                  <div class="form-group">
                                      <label>Kabupaten/Kota *</label>
                                        <select name='id_kota' class="form-control select2bs4" style="width: 100%;">
                                        <?php
                                        $kota = DB::table('kota')
                                        ->select('*')
                                        ->get();
                                        ?>
                                        @foreach ($kota as $kc) 
                                        <option value='{{$kc->id_kota}}' >{{$kc->kota}}</option>
                                        @endforeach
                                        </select>
                                  </div>
                                  </div>
                                                                
                                  <div class="col-md-12">
                                    <div class="form-group">
                                    <label>Nama Kecamatan</label>
                                    <input type="text" class="form-control" id="nama_kecamatan" name="nama_kecamatan" required>
                                    </div>
                                  </div>
                                  <!-- /.col -->
                                                                    
                                  <div class="col-md-12">
                                    <div class="form-group">
                                      <button type="submit" class="btn btn-primary">Submit</button>     
                                    </div>
                                  </div>
                                  <!-- /.col -->
                               </div>
													</form>
													</div>
													<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													</div>
												</div>
												
												</div>
											</div>
							</div>
										
							</div>
											
							<!-- /.card-header -->
							<div class="card-body">
								<table id="next" class="table table-bordered table-hover">
								<thead>
								<tr>
									<th style="width:6%">No</th>
									<th style="width:20%">Kabupaten/Kota</th>
									<th>Kecamatan</th>
									<th style="width:15%">Aksi</th>
								</tr>
								</thead>
								<tbody>
								<?php $no=0; ?> 
							@foreach($kecamatan as $j)
							<?php $no++; ?>
								<tr>
									<td>{{$no}}</td>
									<td>
                                        {{$j->kota}}
                                    </td>
									<td>
                                        {{$j->nama_kecamatan}}
									</td>
                                    <td>
										<button type="button" class="btn btn-primary btn-sm confirmation" data-toggle="modal" data-target="#myModal{{$j->id_kecamatan}}"><i class="fas fa-edit"></i>
									  </button>
										<!-- Modal -->
										<div class="modal fade" id="myModal{{$j->id_kecamatan}}" role="dialog">
												<div class="modal-dialog">
												
												<!-- Modal content-->
												<div class="modal-content">
													<div class="modal-header">
													<h4 class="modal-title">Update Lokasi</h4>
													</div>
													<div class="modal-body">
												  <form class="form" action="<?php echo url('/layanan/lokasi/update/'.$j->id_kecamatan); ?>" method="POST">
													{{csrf_field()}}
													  <div class="card-body">

													  <div class="col-md-12">
														  <div class="form-group">
															  <label>Kabupaten/Kota *</label>
																<select name='id_kota' class="form-control select2bs4" style="width: 100%;">
																<?php
																$kota = DB::table('kota')
																->select('*')
																->get();
																?>
																@foreach ($kota as $kc) 
																<option value='{{$kc->id_kota}}' {{$kc->id_kota ==  $j->id_kota  ? 'selected' : ''}}>{{$kc->kota}}</option>
																@endforeach
																</select>
														  </div>
														  </div>
																						
														  <div class="col-md-12">
															<div class="form-group">
															<label>Nama Kecamatan</label>
															<input type="text" class="form-control" id="nama_kecamatan" name="nama_kecamatan" value='{{$j->nama_kecamatan}}'>
															</div>
														  </div>
														  <!-- /.col -->
																							
														  <div class="col-md-12">
															<div class="form-group">
															  <button type="submit" class="btn btn-primary">Submit</button>     
															</div>
														  </div>
														  <!-- /.col -->
													   </div>
													</form>
													</div>
													<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													</div>
												</div>
												
												</div>
											</div>
										
										
										<button type="button" class="btn btn-danger btn-sm confirmation" data-toggle="modal" data-target="#myModalis{{$j->id_kecamatan}}"><i class="fas fa-trash"></i></button>
										<!-- Modal -->
                                                <div class="modal fade" id="myModalis{{$j->id_kecamatan}}" role="dialog">
												<div class="modal-dialog">
												
												<!-- Modal content-->
												<div class="modal-content">
													<div class="modal-header">
													<h4 class="modal-title">Apakah Anda Yakin menghapus data Kecamatan {{$j->nama_kecamatan}}..?</h4>
													</div>
													<div class="modal-footer">
													<a href="<?php echo url('/layanan/lokasi/destroy/'.$j->id_kecamatan); ?>" class="btn btn-danger btn-sm confirmation" >Hapus Data</a>
														
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													</div>
												</div>
												
												</div>
											</div>
									</td>
								</tr>
								@endforeach
								</tbody>
								</table>
							</div>
							<!-- /.card-body -->
							</div>

						</div> 
						
                        </div>
				</div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <script>
  $(function () {
    $("#next").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": true,"pageLength": 10,"lengthChange": true,
                    "lengthMenu": [[10, 50, 100, -1], [10, 50, 100, "All"]],
    }).buttons().container().appendTo('#next_wrapper .col-md-6:eq(0)');
  });
  $(function () {
    $("#next2").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": true
    }).buttons().container().appendTo('#next2_wrapper .col-md-6:eq(0)');
  });
</script>
@endsection