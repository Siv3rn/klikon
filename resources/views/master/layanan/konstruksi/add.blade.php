@extends('layouts.lay2')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Konstruksi</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/home">Dashboard</a></li>
              <li class="breadcrumb-item"><a href="/layanan/konstruksi">layanan</a></li>
              <li class="breadcrumb-item active">Konstruksi Add</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
                <!-- Small boxes (Stat box) -->
        <div class="row">
          
        <div class="col-12">
            <div class="card">
              <div class="card-header">
                        <h3 class="card-title">Konstruksi | Add</h3>
              </div>
              
              <!-- /.card-header -->
              @if(count($errors) > 0)
              <div class="alert alert-danger" role="alert">
                @foreach ($errors->all() as $error)
                {{ $error }} <br/>
                @endforeach
              </div>
              @endif
              <script>
                window.setTimeout(function() {
                  $.noConflict();
                  $(".alert").fadeTo(500, 0).slideUp(500, function(){
                    $(this).remove(); 
                  });
                }, 5000);
              </script>
              <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
              <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
    
                
              <form class="form" action="<?php echo url("/layanan/konstruksi/create"); ?>" method="POST" enctype="multipart/form-data">
                <!-- /.card-header -->
                {{csrf_field()}}
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Judul *</label>
                        <input type="text" class="form-control" id="judul_tenaga" name="judul_tenaga" placeholder="judul" required>
                      </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-md-6">
                      <div class="form-group">
                            <label>Upload File *</label>
                            <input type="file" class="form-control" id="file" name="file" placeholder="Upload Gambar tenaga" required>                  
                        </div>
                      <!-- /.form-group -->
                    </div>
                    <!-- /.col -->

                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Deskripsi *</label>
                        <textarea id="summernote1" name='deskripsi_tenaga'>
                            Place <em>some</em> <u>text</u> <strong>here</strong>
                        </textarea>
                     </div>
                    </div>
                    <!-- /.col -->

                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Kriteria *</label>
                        <select name='kriteria_tenaga' class="form-control select2bs4" style="width: 100%;">
                          <option value='Tenaga Ahli' selected="selected">Tenaga Ahli</option>
                          <option value='Tenaga Terampil'>Tenaga Terampil</option>=
                        </select>
                        </div>
                      <!-- /.form-group -->
                    </div>
                    
					          <div class="col-12 col-sm-12">
                      <div class="form-group">
                          <button type="submit" class="btn btn-primary">Submit</button>       
                      </div>
                      <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                  </div>
                  <!-- /.row -->
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  Klinik Konstruksi 
                </div>
              </div>
              <!-- /.card -->
              </form>
              <!-- /.card-body -->
            </div>
        </div>
               
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection