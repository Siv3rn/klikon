@extends('layouts.lay')

@section('content')
<div id="banner-area" class="banner-area" style="background-image:url({{url('/constra/images/banner/banner1.jpg')}})">
  <div class="banner-text">
    <div class="container">
        <div class="row">
          <div class="col-lg-12">
              <div class="banner-heading">
              <h1 class="banner-title">Tenaga Ahli dan Terampil</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb justify-content-center">
                      <li class="breadcrumb-item"><a href="<?php echo url("/"); ?>">Beranda</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Konstruksi</li>
                      <li class="breadcrumb-item"><a href="<?php echo url("/konstruksi/all"); ?>">Konstruksi</a></li>
                    </ol>
                </nav>
              </div>
          </div><!-- Col end -->
        </div><!-- Row end -->
    </div><!-- Container end -->
  </div><!-- Banner text end -->
</div><!-- Banner area end --> 

<section id="main-container"  class="main-container">
  <div class="container">

    <div class="row">

    <div class="col-lg-4 mt-5 mt-lg-0">
    <br/>
        <div class="sidebar sidebar-right">
        <div class="widget recent-posts">
            <h3 class="widget-title">Konsultasi Terkini</h3>
            <ul class="list-unstyled">
            @foreach($konsul as $ksl)
              <li class="d-flex align-items-center">
                <div class="posts-thumb">
                  <a href="#"><img loading="lazy" alt="news-img" src="{{url('/constra/images/icon-image/ksl.png')}}"></a>
                </div>
                <div class="post-info">
                  <h4 class="entry-title">
                    <a href="<?php echo url("/konsultasi/detail/$ksl->id_konsultasi"); ?>">{{$ksl->judul_konsultasi}}</a>
                  </h4>
                </div>
              </li><!-- 2nd post end-->
              @endforeach
            </ul>
            <div class="call-to-action-btn">
                 <a class="btn btn-blue" href="<?php echo url("/konsultasi/tanya"); ?>">Kirim Pertanyaan</a>
                </div>
        </div><!-- Recent post end -->
        </div><!-- Sidebar end -->

    </div><!-- Col end -->
		<div class="col-lg-8">
        <div class="container mt-4">
        <div class="card card-primary card-tabs">
              <div class="card-header p-0 pt-1">
                <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">

                  
                  <li class="nav-item">
                    <a class="nav-link active" id="custom-tabs-one-profile-tab" data-toggle="pill" href="#custom-tabs-one-profile" role="tab" aria-controls="custom-tabs-one-profile" aria-selected="false">Tenaga Ahli</a>
                  </li>
                  
                  <li class="nav-item">
                    <a class="nav-link " id="custom-tabs-one-home-tab" data-toggle="pill" href="#custom-tabs-one-home" role="tab" aria-controls="custom-tabs-one-home" aria-selected="true">Tenaga Terampil</a>
                  </li>

                </ul>
              </div>
              <div class="card-body">
                <div class="tab-content" id="custom-tabs-one-tabContent">

                <div class="tab-pane fade" id="custom-tabs-one-profile" role="tabpanel" aria-labelledby="custom-tabs-one-profile-tab">
                <style>
                        table#next2.dataTable tr:hover {
                        background-color: #d0ecdc;
                        }
                        .table-striped>tbody>tr:nth-child(odd)>td, 
                        .table-striped>tbody>tr:nth-child(odd)>th {
                            background-color: #e8f7f0 ; /* Choose your own color here */
                        }                       
                    </style> 
                     <table id="next2" class="table table-striped table-bordered hover" bordercolor="#e0e4e4">
                     <thead>
                                   <tr>
                                       <th>No</th>
                                       <th>Judul</th>
                                       <th style="width:15%">File</th>
                                       <th>Deskripsi</th>
                                       <th>Kriteria</th>
                                   </tr>
                                   </thead>
                                   <tbody>
                                   <?php $no=0; ?> 
                               @foreach($konstruksi2 as $lk2)
                               <?php $no++; ?>
                                   <tr>
                                       <td>{{$no}}</td>
                                       <td>{{$lk2->judul_tenaga}}</td>
                                       <td><center>
                                       <a href="{{asset('storage/images/tenaga/'.$lk2->file_tenaga)}}" alt="{{$lk2->file_tenaga}}"  target='_blank'>{{$lk2->file_tenaga}}</a>
                                       </center></td>
                                       <td>{!!$lk2->deskripsi_tenaga!!}</td>
                                       <td>{{$lk2->kriteria_tenaga}}</td>
                                   </tr>
                                   @endforeach
                                   </tbody>
                                   </table>
   
                     </div>  

                  <div class="tab-pane fade show active" id="custom-tabs-one-home" role="tabpanel" aria-labelledby="custom-tabs-one-home-tab">
                  <style>
                        table#next.dataTable tr:hover {
                        background-color: #d0ecdc;
                        }
                        .table-striped>tbody>tr:nth-child(odd)>td, 
                        .table-striped>tbody>tr:nth-child(odd)>th {
                            background-color: #e8f7f0 ; /* Choose your own color here */
                        }                       
                    </style>   
                  <table id="next" class="table table-striped table-bordered hover" bordercolor="#e0e4e4">
                  <thead>
								<tr>
									<th>No</th>
									<th>Judul</th>
									<th style="width:15%">File</th>
									<th>Deskripsi</th>
									<th>Kriteria</th>
								</tr>
								</thead>
								<tbody>
								<?php $no=0; ?> 
							@foreach($konstruksi as $lk)
							<?php $no++; ?>
								<tr>
									<td>{{$no}}</td>
									<td>{{$lk->judul_tenaga}}</td>
									<td><center>
									<a href="{{asset('storage/images/tenaga/'.$lk->file_tenaga)}}" alt="{{$lk->file_tenaga}}"  target='_blank'>{{$lk->file_tenaga}}</a>
									</center></td>
									<td>{!!$lk->deskripsi_tenaga!!}</td>
									<td>{{$lk->kriteria_tenaga}}</td>
								</tr>
								@endforeach
								</tbody>
								</table>
                  
                  </div>
                  
                  
                </div>
              </div>
              <!-- /.card -->

            </div>
		</div>
        </div><!-- Content row end -->      
</section><!-- Main container end -->

<!-- bootstrap5 dataTables js cdn -->
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    
    <script src="{{url('/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    
    <script>
    var jq = $.noConflict();
    jq(function () {
        jq("#next").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
        }).buttons().container().appendTo('#next_wrapper .col-md-6:eq(0)');
    });

    jq(function () {
        jq("#next2").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
        }).buttons().container().appendTo('#next2_wrapper .col-md-6:eq(0)');
    });

    </script>

@endsection