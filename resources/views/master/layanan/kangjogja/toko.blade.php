@extends('layouts.lay3')

@section('content')
<link href="{{url('v2/assets/css/product.css')}}" rel="stylesheet" />
<link href="{{url('v2/assets/css/ribbon.css')}}" rel="stylesheet" />
<link href="{{url('v2/assets/css/ribbon2.css')}}" rel="stylesheet" />
<section id="main-container"  class="main-container">
  <div class="container">

    <div class="row">

    <div class="col-lg-2 mt-5 mt-lg-0">

        <div class="sidebar sidebar-right">
        <div class="widget recent-posts">
            
            <form class="form" action="<?php echo url("/kangjogja/toko/search"); ?>" method="post">
                <!-- /.card-header -->
                {{csrf_field()}}
            <div class="form-group">
              <label>Keyword</label>
              <input type="text" class="form-control" id="nama" name="nama">
            </div>

            <div class="form-group">
              <label class="col-xs-1 control-label">Kabupaten</label>
              <div class="col-xs-2 selectContainer">
                  <select class="form-control" name="id_kabupaten" id="id_kabupaten">
                  <option value='0'>--Kabupaten--</option>
                      @foreach($kabupaten as $j)
                        <option value="{{$j->id_kabupaten}}">{{$j->kabupaten}}</option>
                      @endforeach
                  </select>
              </div>
			</div>

            <div class="form-group"><br/>
              <button type="submit" class="btn btn-primary">Cari</button>  
            </div>
            </form>

        </div><!-- Recent post end -->
        </div><!-- Sidebar end -->

        <div class="sidebar sidebar-right">
        <div class="widget recent-posts">
          <hr/>
        <a href='<?php echo url("/kangjogja/toko/peta"); ?>'>
        <img src="{{url('/images/diy.png')}}" class="card-img img-fluid" width="30%" alt="">
        <center><b>Peta Sebaran Toko</b></center>
        </a>
        </div><!-- Recent post end -->
        </div><!-- Sidebar end -->

    </div><!-- Col end -->
		<div class="col-lg-10">
         <div class="row"> 
        <?php 
        $hit3 = count($toko); 
        ?>
        @if($hit3 <= 0)
        <div class="call-to-action-btn"><br/>
        <center>
        <h1 class="btn btn-danger">Data yang Anda Cari Tidak ada! </h1> 
        </center>
        </div>
        @else
        @foreach($toko as $k)
         <div class="col-md-3 mt-2">
            <div class="card">
                <div class="card-body">
                <div class="card-img-actions"> 
        
                @if(empty($k->file))
                <img src="{{url('/images/toko/no-images.png')}}" class="card-img img-fluid" width="50%" alt=""> </div>
                @else
                <img src="{{asset('storage/images/toko/'.$k->file)}}" class="card-img img-fluid" width="50%" alt=""> </div>
                @endif
                </div>
                <div class="card bg-light text-center">
                    <div class="mb-2">
                    <h6 class="mb-0 font-weight-semibold">
                        <div class="bg-warning color-palette">
                            <span>
                                <b>{{$k->nama}}</b>
                            </span>
                        </div>
                    </h6>

                        <h6 class="font-weight-semibold mb-2"> 
                            <p class="text" data-abc="true">
                            {{substr($k->alamat, 0, 78)}}
                            @if(strlen($k->alamat) > 78)
                            ....
                            @endif
						    <!-- Button trigger modal -->
							<br/>
							<a href='#' class='card' data-bs-toggle="modal" data-bs-target="#myModalkategori{{$k->id}}">
							<span class="text-gradient fw-bold">Detail</span>
							</a>
                            </p> 
                        </h6>
                    </div>
                    
                   

                    <!-- Modal -->
                    <div class="modal fade" id="myModalkategori{{$k->id}}" tabindex="-1" aria-labelledby="myModalkategori{{$k->id}}" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myModalkategori{{$k->id}}">Detail Data Toko</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">

                        <table  class="table table-bordered table-hover">
                        <tr><td align="left"  width='30%'><b>Toko</b></td><td align="left">{{$k->nama}}</td></tr>
                        <tr><td align="left"  width='30%'><b>Alamat</b></td><td align="left">{{$k->alamat}}</td></tr>
                        <tr><td align="left"  width='30%'><b>No Hp</b></td><td align="left">+62 {{$k->hp}}</td></tr>
                        <tr><td align="left"  width='30%'><b>Info</b></td><td align="left">{!!$k->info!!}</td></tr>
                        </table>
                        
                        </div>
                        <div class="modal-footer">
                            <button type="button" style="background-color: green;" class="btn" data-bs-dismiss="modal"><font color='white'>Close</font></button>
                        </div>
                        </div>
                    </div>
                    </div>

                </div>
            </div>
        </div>
		@endforeach
		@endif
         </div>
      <br/>
      <div class="d-flex justify-content-center">
      {!! $toko->links() !!}
      </div>				
		</div>
        </div><!-- Content row end -->      
  </section><!-- Main container end -->

@endsection