@extends('layouts.lay2')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Tukang</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/home">Dashboard</a></li>
              <li class="breadcrumb-item"><a href="/menu/kangjogja">Layanan</a></li>
              <li class="breadcrumb-item active">KangJogja Edit</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
                <!-- Small boxes (Stat box) -->
        <div class="row">
          
        <div class="col-12">
            <div class="card">
              <div class="card-header">
                        <h3 class="card-title">Tukang | Update</h3>
              </div>
              
              <!-- /.card-header -->
              @if(count($errors) > 0)
              <div class="alert alert-danger" role="alert">
                @foreach ($errors->all() as $error)
                {{ $error }} <br/>
                @endforeach
              </div>
              @endif
              <script>
                window.setTimeout(function() {
                  $.noConflict();
                  $(".alert").fadeTo(500, 0).slideUp(500, function(){
                    $(this).remove(); 
                  });
                }, 5000);
              </script>
              <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
              <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
    
               @foreach($kang as $lk) 
              <form class="form" action="<?php echo url('/menu/kangjogja/update_kangjogja/'.$lk->id_user); ?>" method="POST" enctype="multipart/form-data">
                                        <!-- /.card-header -->
                                        {{csrf_field()}}
                                        <div class="col-12 col-sm-12">
                                        <div class="form-group">
                                            <label>Nama *</label>
                                            <input type="text" class="form-control" id="name" name="name" value='{{$lk->name}}'>
                                            </div>
                                            <!-- /.form-group -->

                                            <div class="form-group">
                                            <label>email *</label>
                                            <input type="email" class="form-control" id="email" name="email" value='{{$lk->email}}'>
                                            </div>
                                            <!-- /.form-group -->
                                            
                                            <div class="form-group">
                                          <label>Sertifikasi *</label>
                                          <input type="text" class="form-control" id="nama" name="nama" value='{{$lk->nama_ser}}'>
                                            </div>
                                            <!-- /.form-group -->
                                                        
                                           <div class="form-group">
                                          <label>No Sertifikasi *</label>
                                          <input type="text" class="form-control" id="nomor" name="nomor" value='{{$lk->nomor}}'>
                                          </div>
                                          <!-- /.form-group -->

                                                        
                                          <div class="form-group">
                                          <label>Masa Berlaku *</label>
                                          <input type="date" class="form-control" id="masa_berlaku" name="masa_berlaku" value='{{$lk->masa_berlaku}}'>
                                          </div>
                                          <!-- /.form-group -->


                                            <div class="form-group">
                                          <label>Jenis Tukang</label>
                                            <select name='id_jenis' class="form-control select2bs4" style="width: 100%;" required>
                                              <?php
                                                  $jt = \DB::table('jenis_tukang')
                                                  ->select('*')
                                                  ->get();
                                                    ?>
                                                    @foreach ($jt as $jenis)
                                                      <option value='{{$jenis->id_jenis}}' {{$jenis->id_jenis ==  $lk->id_jenis  ? 'selected' : ''}}>{{$jenis->jenis}}</option>
                                                        @endforeach
                                                          </select>
                                                            </div>

                                            <div class="form-group">
                                          <label>Propinsi</label>
                                            <select name='id_propinsi' class="form-control select2bs4" style="width: 100%;" required>
                                              <?php
                                                  $p = \DB::table('m_propinsi')
                                                  ->select('*')
                                                  ->get();
                                                    ?>
                                                    @foreach ($p as $mp)
                                                      <option value='{{$mp->id_propinsi}}' {{$mp->id_propinsi ==  $lk->id_propinsi  ? 'selected' : ''}}>{{$mp->propinsi}}</option>
                                                        @endforeach
                                                          </select>
                                                            </div>

                                            <div class="form-group">
                                          <label>Kabupaten</label>
                                            <select name='id_kabupaten' class="form-control select2bs4" style="width: 100%;" required>
                                              <?php
                                                  $kb = \DB::table('m_kabupaten')
                                                  ->select('*')
                                                  ->get();
                                                    ?>
                                                    @foreach ($kb as $kbp)
                                                      <option value='{{$kbp->id_kabupaten}}' {{$kbp->id_kabupaten ==  $lk->id_kabupaten  ? 'selected' : ''}}>{{$kbp->kabupaten}}</option>
                                                        @endforeach
                                                          </select>
                                                            </div>

                                                        <div class="form-group">
                                                        <label>longtitude *</label>
                                                        <input type="text" class="form-control" id="longtitude" name="longtitude" value='{{$lk->longtitude}}'>
                                                        </div>
                                                        <!-- /.form-group -->

                                                        <div class="form-group">
                                                        <label>latitude *</label>
                                                        <input type="text" class="form-control" id="latitude" name="latitude" value='{{$lk->latitude}}'>
                                                        </div>
                                                        <!-- /.form-group -->

                                                        <div class="form-group">
                                                        <label>WA *</label>
                                                        <input type="text" class="form-control" id="wa" name="wa" value='{{$lk->wa}}'>
                                                        </div>
                                                        <!-- /.form-group -->

                                                        <div class="form-group">
                                                        <label>HP *</label>
                                                        <input type="text" class="form-control" id="hp" name="hp" value='{{$lk->hp}}'>
                                                        </div>
                                                        <!-- /.form-group -->

                                                        <div class="form-group">
                                                        <label>Tanggal Lahir *</label>
                                                        <input type="date" class="form-control" id="tgl_lahir" name="tgl_lahir" value='{{$lk->tgl_lahir}}'>
                                                        </div>
                                                        <!-- /.form-group -->

                                                        <div class="form-group">
                                                        <label>KTP *</label>
                                                        <input type="text" class="form-control" id="ktp" name="ktp" value='{{$lk->ktp}}'>
                                                        </div>
                                                        <!-- /.form-group -->

                                                        <div class="form-group">
                                                        <label>Foto *</label>
                                                        <input type="file" class="form-control" id="file" name="file">
                                                        </div>
                                                        <!-- /.form-group -->

                                                        <div class="form-group">
                                                          <label>Alamat_map *</label>
                                                          <textarea class="form-control" name='alamat_map'>{{$lk->alamat_map}}</textarea>
                                                        </div>

                                                        <div class="form-group">
                                                          <label>Alamat Tinggal *</label>
                                                          <textarea class="form-control" name='alamat'>{{$lk->alamat}}</textarea>
                                                        </div>

                                            <div class="form-group"><center>
                                                <button type="submit" class="btn btn-primary">Save</button>       
                                            </center></div>
                                            <!-- /.form-group -->
                                        </div>
                                        </form>
                                        @endforeach
              <!-- /.card-body -->
            </div>
        </div>
               
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection