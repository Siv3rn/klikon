@extends('layouts.lay3')

@section('content')

<section class="py-0" id="contact">
<br/><br/><br/>
        <div class="container">
          <div class="row">
            <div class="col-lg-7 col-xxl-5 mx-auto text-center mt-5">
              <h5 class="fw-bold fs-3 fs-lg-5 lh-sm mb-5">Tentang Kami</h5>
            </div>
          </div>
        </div>
        <!-- end of .container-->

      </section>
      <!-- <section> close ============================-->
      <!-- ============================================-->

<section class="py-5 mb-8">
        <div class="bg-holder" style="background-image:url(v2/assets/img/illustrations/contact-us-bg.png);background-position:left bottom;background-size:auto;">
        </div>
        <!--/.bg-holder-->

        <div class="bg-holder" style="background-image:url(v2/assets/img/gallery/contact-bg.png);background-position:center right ;background-size:contain;">
        </div>
        <!--/.bg-holder-->

        <div class="container">
          <div class="row flex-center">
            <div class="col-lg-5 mb-5">
              <div class="card card-bg p-2">
                <div class="card-body">
                  <form>

                    <div class="row mb-3 input-group-icon">
                      <div class="col-sm-12">
                          
                        <h2>Syarat dan Ketentuan Kang Jogja</h2>
                        <p>
                                            
                            Layanan dalam aplikasi ini dilakukan langsung oleh pengguna kepada tenaga kerja konstruksi bersertifikat.
                            Kerjasama yang terjadi antara pengguna dan tenaga kerja konstruksi bersertifikat menjadi tanggung jawab masing-masing
                            Tenaga Kerja Konstruksi yang terdaftar di aplikasi ini sudah menandatangani surat kesanggupan menjaga etika kerja dan profesionalitas.

                        </p>
                      </div>
                    </div>
                    
                  </form>
                </div>
              </div>
            </div>
            <div class="col-lg-5 offset-lg-1">
              <div class="d-flex align-items-center mb-5">
                <div class="icon icon-primary"><span data-feather="phone"></span></div>
                <div class="flex-1 ms-3">
                  <p class="fw-bold mb-0">Phone </p>
                  <p>0815 7871 8705 : FAUZAN</p>
                  <p>0897 6812 129 : IHZAN</p>
                </div>
              </div>
              
              <div class="d-flex align-items-center">
                <div class="icon icon-primary"><span data-feather="map-pin"></span></div>
                <div class="flex-1 ms-3">
                  <p class="fw-bold mb-0">Alamat</p>
                  <p>Jl. Ring Road Utara No.4, Nanggulan, Maguwoharjo, Kec. Depok, Kabupaten Sleman, Daerah Istimewa Yogyakarta 55282 </p>
                </div>
              </div>

            </div>
          </div>
        </div>
      </section>

@endsection