@extends('layouts.lay3')

@section('content')
<section id="home">
        
        <div class="bg-holder d-none d-md-block bg-size" style="background-image:url({{url('/images/all.png')}});background-position:right bottom;">
        </div>
        <!--/.bg-holder-->

        <div class="bg-holder" style="background-image:url({{url('/v2/assets/img/illustrations/heroheader-bg.png')}});background-position:center;background-size:contain;">
        </div>
        <!--/.bg-holder-->

        <div class="container">
          <div class="row align-items-center">
            <div class="col-md-7 col-lg-6 py-6 text-sm-start text-center">
            <table border=0>
                <tr>
                    <td>                    
                        <img width='100%' class="me-3 d-inline-block" src="{{asset('/images/logo_header.png')}}" alt="logo">
                    </td>
                    <td>
                        <h1 class="text-700">  
                            <span class="fw-bold"> &nbsp; </span>
                        </h1>
                    </td>
                </tr>
            </table>
              </div>
          </div>     
        </div>
        
      </section>

      @endsection