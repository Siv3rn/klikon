@extends('layouts.lay2')

@section('content')
{{-- @if (Auth::check() && Auth::user()->roles_name == 'admin data')
  <h1> you cant see this shit</h1>
@endif --}}
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-12">
        @if(!empty(\Session::get('success')) > 0)
                    <div class="alert alert-success">     
                        <strong>Sukses!</strong> {!! \Session::get('success') !!}             
                        </div>
                    @endif
                <!-- /.card-header -->
              @if(count($errors) > 0)
              <div class="alert alert-danger" role="alert">
                @foreach ($errors->all() as $error)
                {{ $error }} <br/>
                @endforeach
              </div>
              @endif
              <script>
                window.setTimeout(function() {
                  $.noConflict();
                  $(".alert").fadeTo(500, 0).slideUp(500, function(){
                    $(this).remove(); 
                  });
                }, 5000);
              </script>
              <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
              <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
                </div>
                <div class="col-sm-6">
            <h1 class="m-0">Kang Jogja</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/home">Dashboard</a></li>
              <li class="breadcrumb-item"><a href="/menu/kangjogja">Layanan</a></li>
              <li class="breadcrumb-item active">Kang Jogja</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
                <!-- Small boxes (Stat box) -->
        <div class="row">
					<div class="col-12">
							<div class="card">
							<div class="card-header">
										<h3 class="card-title">Tukang | &nbsp;</h3>
                    <button type="button" class="btn btn-success btn-sm confirmation" data-toggle="modal" data-target="#myModaladdtukang"><i class="fas fa-plus"></i> Add </button>
                            <!-- Modal -->
                                <div class="modal fade" id="myModaladdtukang" role="dialog">
                                    <div class="modal-dialog">
                                    
                                    <!-- Modal content-->
                                    Anggito Wicaksono.blend1
                                    balldimpled.png
                                    Basket-edit.jpg
                                    BasketballColor.jpg          <div class="modal-content">
                                        <div class="modal-header">
                                        <h4 class="modal-title">Create Tukang</h4>
                                        </div>
                                        <div class="modal-body">
                                        <form class="form" action="<?php echo url("/menu/kangjogja/create_kangjogja"); ?>" method="POST" enctype="multipart/form-data">
                                    <!-- /.card-header -->
                                    {{csrf_field()}}
                                    <div class="card-body">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                            <label>Nama *</label>
                                            <input type="text" class="form-control" id="name" name="name" required>
                                            </div>
                                            <!-- /.form-group -->

                                            <div class="form-group">
                                            <label>email *</label>
                                            <input type="email" class="form-control" id="email" name="email" required>
                                            </div>
                                            <!-- /.form-group -->

                                            <div class="form-group">
                                            <label>Password *</label>
                                            <input type="password" class="form-control" id="password" name="password" required>
                                            </div>
                                            <!-- /.form-group -->

                                          <div class="form-group">
                                          <label>Jenis Tukang</label>
                                            <select name='id_jenis' class="form-control select2bs4" style="width: 100%;" required>
                                              <?php
                                                  $jt = \DB::table('jenis_tukang')
                                                  ->select('*')
                                                  ->get();
                                                    ?>
                                                    @foreach ($jt as $jenis)
                                                      <option value='{{$jenis->id_jenis}}'>{{$jenis->jenis}}</option>
                                                        @endforeach
                                                          </select>
                                                            </div>

                                            <div class="form-group">
                                          <label>Propinsi</label>
                                            <select name='id_propinsi' class="form-control select2bs4" style="width: 100%;" required>
                                              <?php
                                                  $p = \DB::table('m_propinsi')
                                                  ->select('*')
                                                  ->get();
                                                    ?>
                                                    @foreach ($p as $mp)
                                                      <option value='{{$mp->id_propinsi}}'>{{$mp->propinsi}}</option>
                                                        @endforeach
                                                          </select>
                                                            </div>

                                            <div class="form-group">
                                          <label>Kabupaten</label>
                                            <select name='id_kabupaten' class="form-control select2bs4" style="width: 100%;" required>
                                              <?php
                                                  $kb = \DB::table('m_kabupaten')
                                                  ->select('*')
                                                  ->get();
                                                    ?>
                                                    @foreach ($kb as $kbp)
                                                      <option value='{{$kbp->id_kabupaten}}'>{{$kbp->kabupaten}}</option>
                                                        @endforeach
                                                          </select>
                                                            </div>

                                                        <div class="form-group">
                                                        <label>longtitude *</label>
                                                        <input type="text" class="form-control" id="longtitude" name="longtitude" required>
                                                        </div>
                                                        <!-- /.form-group -->

                                                        <div class="form-group">
                                                        <label>latitude *</label>
                                                        <input type="text" class="form-control" id="latitude" name="latitude" required>
                                                        </div>
                                                        <!-- /.form-group -->

                                                        <div class="form-group">
                                                        <label>WA *</label>
                                                        <input type="text" class="form-control" id="wa" name="wa">
                                                        </div>
                                                        <!-- /.form-group -->

                                                        <div class="form-group">
                                                        <label>HP *</label>
                                                        <input type="text" class="form-control" id="hp" name="hp">
                                                        </div>
                                                        <!-- /.form-group -->

                                                        <div class="form-group">
                                                        <label>Tanggal Lahir *</label>
                                                        <input type="date" class="form-control" id="tgl_lahir" name="tgl_lahir" required>
                                                        </div>
                                                        <!-- /.form-group -->

                                                        <div class="form-group">
                                                        <label>KTP *</label>
                                                        <input type="text" class="form-control" id="ktp" name="ktp" required>
                                                        </div>
                                                        <!-- /.form-group -->

                                                        <div class="form-group">
                                                        <label>Foto *</label>
                                                        <input type="file" class="form-control" id="file" name="file">
                                                        </div>
                                                        <!-- /.form-group -->

                                                        <div class="form-group">
                                                        <label>Sertifikasi *</label>
                                                        <input type="text" class="form-control" id="nama" name="nama" required>
                                                        </div>
                                                        <!-- /.form-group -->
                                                        
                                                        <div class="form-group">
                                                        <label>No Sertifikasi *</label>
                                                        <input type="text" class="form-control" id="nomor" name="nomor" required>
                                                        </div>
                                                        <!-- /.form-group -->

                                                        
                                                        <div class="form-group">
                                                        <label>Masa Berlaku *</label>
                                                        <input type="date" class="form-control" id="masa_berlaku" name="masa_berlaku" required>
                                                        </div>
                                                        <!-- /.form-group -->

                                                          </div>
                                                          <!-- /.col -->
                                                          <div class="col-md-12">
                                              <div class="form-group">
                                          <label>Alamat_map *</label>
                                          <textarea class="form-control" name='alamat_map'></textarea>
                                        </div>
                                        </div>
                                        <!-- /.col -->

                                        <div class="col-md-12">
                                              <div class="form-group">
                                          <label>Alamat Tinggal *</label>
                                          <textarea class="form-control" name='alamat'></textarea>
                                        </div>
                                        </div>
                                        <!-- /.col -->
                                        </div>
                                        <!-- /.row -->

                                        <div class="row">
                                        <div class="col-12 col-sm-12">
                                            <div class="form-group"><center>
                                                <button type="submit" class="btn btn-primary">Save</button>       
                                            </center></div>
                                            <!-- /.form-group -->
                                        </div>
                                        <!-- /.col -->
                                        </div>
                                        <!-- /.row -->

                                    </div>
                                    <!-- /.card-body -->
                                    </form>  
                                    <button type="button" class="btn btn-default" data-dismiss="modal"><b>X Close</b></button>
                                        
                                        </div>
                                    </div>
                                    
                                    </div>
                                </div>
							</div>
											
							<!-- /.card-header -->
							<div class="card-body">

                    <table id="next" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Tukang</th>
                        <th>Umur</th>
                        <th>Alamat</th>
                        <th>No Hp</th>
                        <th>Jenis Tukang</th>
                        <th>Sertifikasi</th>
                        <th>Masa Berlaku</th>
                        <th>Foto</th>
                        <th style="width:10%">Aksi</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $no=0; ?> 
                @foreach($kang as $lk)
                <?php 
                $no++;
                 ?>
                    <tr>
                        <td>{{$no}}</td>
                        <td>
                          {{$lk->name}}
                        </td>
                        <td>
                        @if($lk->tgl_lahir == '0000-00-00' or is_null($lk->tgl_lahir))
                            (- Thn)
                            @else
                            (
                            <?php
                            $tanggal_lahir = date('Y-m-d', strtotime($lk->tgl_lahir));
                            $birthDate = new DateTime($tanggal_lahir);
                            $today = new DateTime("today");
                            echo $y = $today->diff($birthDate)->y;
                            ?>
                             Thn)
                            @endif
                        </td>
                        <td>
                        {{$lk->alamat}}
                        </td>
                        <td>
                        {{$lk->hp}}
                        </td>
                        <td>
                        {{$lk->jenis}}
                        </td>
                        <td>
                        {{$lk->nama_ser}} - {{$lk->nomor}}
                        </td>
                        <td>
                        {{date('d M Y', strtotime($lk->masa_berlaku))}}
                        </td>
                        <td><center>
                          @if($lk->file == 'no-images.jpg' or empty($lk->file))
                          Tidak ada foto
                          @else
                          <a href="{{asset('storage/images/tukang/'.$lk->file)}}" target=_blank>
                          Cek Foto
                          </a>
                          @endif
                        <!--
                          <img src="{{asset('storage/images/tukang/'.$lk->file)}}" alt="{{$lk->file}}" width='50%' class="brand-image img-circle elevation-2" style="opacity: .8">
                        -->
                        </center>
						          </td>
                        <td>
                        <a class="btn btn-info btn-sm" href="<?php echo url('/menu/kangjogja/edit_tukang/'.$lk->id_user); ?>">
                          <i class="fas fa-edit">
                          </i>
                        </a>

                            <button type="button" class="btn btn-danger btn-sm confirmation" data-toggle="modal" data-target="#myModaltukang{{$lk->id}}"><i class="fas fa-trash"></i></button>
                            <!-- Modal -->
                                <div class="modal fade" id="myModaltukang{{$lk->id}}" role="dialog">
                                    <div class="modal-dialog">
                                    
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                        <h4 class="modal-title">Apakah Anda Yakin menghapus data Tukang {{$lk->name}}..?</h4>
                                        </div>
                                        <div class="modal-footer">
                                        <a href="<?php echo url('/menu/kangjogja/destroy_tukang/'.$lk->id_user); ?>" class="btn btn-danger btn-sm confirmation" >Hapus Data</a>
                                            
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                    
                                    </div>
                                </div>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                    </table>
                
							</div>
							<!-- /.card-body -->
							</div>
              	
							<div class="col-12">
								<hr/><br/><br/>
							<div class="card">
							<div class="card-header">
										<h3 class="card-title">Toko | &nbsp;</h3>
						   
                    <button type="button" class="btn btn-success btn-sm confirmation" data-toggle="modal" data-target="#myModaladdtoko"><i class="fas fa-plus"></i> Add </button>
                            <!-- Modal -->
                                <div class="modal fade" id="myModaladdtoko" role="dialog">
                                    <div class="modal-dialog">
                                    
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                        <h4 class="modal-title">Create Toko</h4>
                                        </div>
                                        <div class="modal-body">
                                        <form class="form" action="<?php echo url("/menu/kangjogja/create_toko"); ?>" method="POST" enctype="multipart/form-data">
                                    <!-- /.card-header -->
                                    {{csrf_field()}}
                                    <div class="card-body">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                            <label>Nama Toko*</label>
                                            <input type="text" class="form-control" id="name" name="name" required>
                                            </div>
                                            <!-- /.form-group -->

                                            <div class="form-group">
                                            <label>email *</label>
                                            <input type="email" class="form-control" id="email" name="email" required>
                                            </div>
                                            <!-- /.form-group -->

                                            <div class="form-group">
                                            <label>Password *</label>
                                            <input type="password" class="form-control" id="password" name="password" required>
                                            </div>
                                            <!-- /.form-group -->

                                            <div class="form-group">
                                          <label>Propinsi</label>
                                            <select name='id_propinsi' class="form-control select2bs4" style="width: 100%;" required>
                                              <?php
                                                  $p = \DB::table('m_propinsi')
                                                  ->select('*')
                                                  ->get();
                                                    ?>
                                                    @foreach ($p as $mp)
                                                      <option value='{{$mp->id_propinsi}}'>{{$mp->propinsi}}</option>
                                                        @endforeach
                                                          </select>
                                                            </div>

                                            <div class="form-group">
                                          <label>Kabupaten</label>
                                            <select name='id_kabupaten' class="form-control select2bs4" style="width: 100%;" required>
                                              <?php
                                                  $kb = \DB::table('m_kabupaten')
                                                  ->select('*')
                                                  ->get();
                                                    ?>
                                                    @foreach ($kb as $kbp)
                                                      <option value='{{$kbp->id_kabupaten}}'>{{$kbp->kabupaten}}</option>
                                                        @endforeach
                                                          </select>
                                                            </div>

                                                        <div class="form-group">
                                                        <label>longtitude *</label>
                                                        <input type="text" class="form-control" id="longtitude" name="longtitude" required>
                                                        </div>
                                                        <!-- /.form-group -->

                                                        <div class="form-group">
                                                        <label>latitude *</label>
                                                        <input type="text" class="form-control" id="latitude" name="latitude" required>
                                                        </div>
                                                        <!-- /.form-group -->

                                                        <div class="form-group">
                                                        <label>WA *</label>
                                                        <input type="text" class="form-control" id="wa" name="wa">
                                                        </div>
                                                        <!-- /.form-group -->

                                                        <div class="form-group">
                                                        <label>HP *</label>
                                                        <input type="text" class="form-control" id="hp" name="hp">
                                                        </div>
                                                        <!-- /.form-group -->

                                                        <div class="form-group">
                                                        <label>Foto *</label>
                                                        <input type="file" class="form-control" id="file" name="file">
                                                        </div>
                                                        <!-- /.form-group -->
                                                          </div>
                                                          <!-- /.col -->

                                                  <div class="col-md-12">
                                                        <div class="form-group">
                                                    <label>Info *</label>
                                                    <textarea class="form-control" name='info'></textarea>
                                                  </div>
                                                  </div>
                                                  <!-- /.col -->

                                                <div class="col-md-12">
                                              <div class="form-group">
                                          <label>Alamat_map *</label>
                                          <textarea class="form-control" name='alamat_map'></textarea>
                                        </div>
                                        </div>
                                        <!-- /.col -->

                                        <div class="col-md-12">
                                              <div class="form-group">
                                          <label>Alamat Tinggal *</label>
                                          <textarea class="form-control" name='alamat'></textarea>
                                        </div>
                                        </div>
                                        <!-- /.col -->
                                        </div>
                                        <!-- /.row -->

                                        <div class="row">
                                        <div class="col-12 col-sm-12">
                                            <div class="form-group"><center>
                                                <button type="submit" class="btn btn-primary">Save</button>       
                                            </center></div>
                                            <!-- /.form-group -->
                                        </div>
                                        <!-- /.col -->
                                        </div>
                                        <!-- /.row -->

                                    </div>
                                    <!-- /.card-body -->
                                    </form>  
                                    <button type="button" class="btn btn-default" data-dismiss="modal"><b>X Close</b></button>
                                        
                                        </div>
                                    </div>
                                    
                                    </div>
                                </div>
							</div>
							<!-- /.card-header -->
							<div class="card-body">
              <table id="next2" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Foto</th>
                        <th>Nama Toko</th>
                        <th>Alamat</th>
                        <th>No Hp</th>
                        <th style="width:10%">Aksi</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $no=0; ?> 
                @foreach($toko as $tk)
                <?php 
                $no++;
                 ?>
                    <tr>
                        <td>{{$no}}</td>
                        <td><center>
                        @if($tk->file == 'no-images.jpg' or empty($tk->file))                          
                          Tidak ada foto
                          @else
                          <a href="{{asset('storage/images/toko/'.$tk->file)}}" target=_blank>
                          Cek Foto
                          </a>
                          @endif
                        </center>
						</td>
                        <td>
                          {{$tk->nama}}
                        </td>
                        <td>
                          {{$tk->alamat}}
                        </td>
                        <td>
                          {{$tk->hp}}
                        </td>
                        <td>
                        <a class="btn btn-info btn-sm" href="<?php echo url('/menu/kangjogja/edit_toko/'.$tk->id_user); ?>">
                                <i class="fas fa-edit">
                                </i>
                            </a>
                            <button type="button" class="btn btn-danger btn-sm confirmation" data-toggle="modal" data-target="#myModalToko{{$tk->id_user}}"><i class="fas fa-trash"></i></button>
                            <!-- Modal -->
                                <div class="modal fade" id="myModalToko{{$tk->id_user}}" role="dialog">
                                    <div class="modal-dialog">
                                    
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                        <h4 class="modal-title">Apakah Anda Yakin menghapus data Toko {{$tk->nama}}..?</h4>
                                        </div>
                                        <div class="modal-footer">
                                        <a href="<?php echo url('/menu/kangjogja/destroy_toko/'.$tk->id_user); ?>" class="btn btn-danger btn-sm confirmation" >Hapus Data</a>
                                            
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                    
                                    </div>
                                </div>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                    </table>
							</div>
							<!-- /.card-body -->
							</div>

						</div> 
						
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <script>
  $(function () {
    $("#next").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false, "pageLength": 5
    }).buttons().container().appendTo('#next_wrapper .col-md-6:eq(0)');
  });
  $(function () {
    $("#next2").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false, "pageLength": 5
    }).buttons().container().appendTo('#next_wrapper .col-md-6:eq(0)');
  });
</script>
@endsection