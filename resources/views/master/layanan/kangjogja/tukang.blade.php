@extends('layouts.lay3')

@section('content')
<link href="{{url('v2/assets/css/product.css')}}" rel="stylesheet" />
<link href="{{url('v2/assets/css/ribbon.css')}}" rel="stylesheet" />
<link href="{{url('v2/assets/css/ribbon2.css')}}" rel="stylesheet" />
<section id="main-container"  class="main-container">
  <div class="container">

    <div class="row">

    <div class="col-lg-2 mt-5 mt-lg-0">

        <div class="sidebar sidebar-right">
        <div class="widget recent-posts">
            
            <form class="form" action="<?php echo url("/kangjogja/tukang/search"); ?>" method="post">
                <!-- /.card-header -->
                {{csrf_field()}}
            <div class="form-group">
              <label>Keyword</label>
              <input type="text" class="form-control" id="nama" name="nama">
            </div>

            <div class="form-group">
              <label class="col-xs-1 control-label">Jenis Tukang</label>
              <div class="col-xs-2 selectContainer">
                  <select class="form-control" name="jenis" id="jenis">
                  <option value='0'>--Pilih Jenis--</option>
                      @foreach($jenis as $j)
                        <option value="{{$j->id_jenis}}">{{$j->jenis}}</option>
                      @endforeach
                  </select>
              </div>
			</div>

            <div class="form-group"><br/>
              <button type="submit" class="btn btn-primary">Cari</button>  
            </div>
            </form>

        </div><!-- Recent post end -->
        </div><!-- Sidebar end -->

        <div class="sidebar sidebar-right">
        <div class="widget recent-posts">
          <hr/>
        </div><!-- Recent post end -->
        </div><!-- Sidebar end -->

    </div><!-- Col end -->
		<div class="col-lg-10">
         <div class="row"> 
        <?php 
        $hit3 = count($kang); 
        ?>
        @if($hit3 <= 0)
        <div class="call-to-action-btn"><br/>
        <center>
        <h1 class="btn btn-danger">Data yang Anda Cari Tidak ada! </h1> 
        </center>
        </div>
        @else
        @foreach($kang as $k)
         <div class="col-md-3 mt-2">
            <div class="card">
                <div class="card-body">
                <div class="card-img-actions"> 
              
                    @if($k->status == 1)
                    <div class="cover-ribbon">
                    <h6 class="mb-0 font-weight-semibold">
                    <span br="" gt="" style="background-color: green; border-radius: 10px; border-radius: 10px; border-radius: 10px; border: 10x double #fff9j99; padding: 0px; t-align: center; t-color: #CCFFFF;">
                    &nbsp;&nbsp; <font color='white'><b>Siap Bekerja</b></font> &nbsp;&nbsp;</span>
                    </h6>
                    </div>
                    @else
                    <div class="cover-ribbon">
                    <h6 class="mb-0 font-weight-semibold">
                    <span br="" gt="" style="background-color: orange; border-radius: 10px; border-radius: 10px; border-radius: 10px; border: 10x double #fff9j99; padding: 0px; t-align: center; t-color: #CCFFFF;">
                    &nbsp;<b>Sedang Bekerja</b>&nbsp;</span>
                    </h6>
                    </div>
                    @endif
              <?php
                date_default_timezone_set("Asia/Jakarta");
                $mb = date('d M Y', strtotime($k->masa_berlaku));
                $awal  = date_create($k->masa_berlaku);
                $akhir = date_create();
                /*
                ?>
                @if($awal < $akhir)
                <div class="arrow-ribbon">Habis</div>
                @else
		        <div class="arrow1-ribbon">
                  Aktif:
                <?php
                $awal  = date_create($k->masa_berlaku);
                $akhir = date_create(); // waktu sekarang
                $diff  = date_diff( $awal, $akhir );
                ?>
                {{$diff->days}}Hari
                </div>
                @endif
                */ ?>
                @if(empty($k->file))
                <img src="{{url('/images/tukang/no-image.jpg')}}" class="card-img img-fluid" width="50%" alt=""> </div>
                @else
                <img src="{{asset('storage/images/tukang/'.$k->file)}}" class="card-img img-fluid" width="50%" alt=""> </div>
                @endif
                </div>
                <div class="card bg-light text-center">
                    <div class="mb-2">
                    <h6 class="mb-0 font-weight-semibold">
                        <div class="bg-warning color-palette">
                            <span>
                                <b>{{$k->jenis}}</b>
                            </span>
                        </div>
                    </h6>

                        <h6 class="font-weight-semibold mb-2"> 
                            <p class="text" data-abc="true">
                            <b>
                            {{$k->name}}
                            </b> 
                            <br/>
                           {{$k->alamat}} 
						    <!-- Button trigger modal -->
							<br/>
							<a href='#' class='card' data-bs-toggle="modal" data-bs-target="#myModalkategori{{$k->id}}">
							<span class="text-gradient fw-bold">Detail</span>
							</a>
                            </p> 
                        </h6>
                    </div>
                    
                   

                    <!-- Modal -->
                    <div class="modal fade" id="myModalkategori{{$k->id}}" tabindex="-1" aria-labelledby="myModalkategori{{$k->id}}" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myModalkategori{{$k->id}}">Detail Data Tukang</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">

                        <table  class="table table-bordered table-hover">
                        <tr><td align="left"  width='30%'><b>Nama</b></td><td align="left">{{$k->name}}</td></tr>
                        <tr><td align="left"  width='30%'><b>Umur</b></td><td align="left">
                        @if($k->tgl_lahir == '0000-00-00' or is_null($k->tgl_lahir))
                            (- Thn)
                            @else
                            (
                            <?php
                            $tanggal_lahir = date('Y-m-d', strtotime($k->tgl_lahir));
                            $birthDate = new DateTime($tanggal_lahir);
                            $today = new DateTime("today");
                            echo $y = $today->diff($birthDate)->y;
                            ?>
                             Thn)
                            @endif
                            </td></tr>
                        <tr><td align="left"  width='30%'><b>Alamat</b></td><td align="left">{{$k->alamat}}</td></tr>
                        <tr><td align="left"  width='30%'><b>No Hp</b></td><td align="left">+62 {{$k->hp}}</td></tr>
                        <tr><td align="left"  width='30%'><b>Jenis Tukang</b></td><td align="left">{{$k->jenis}}</td></tr>
                        <tr><td align="left"  width='30%'><b>Sertifikasi</b></td><td align="left">{{$k->nama_ser}} ({{$k->nomor}})</td></tr>
                        <tr><td align="left"  width='30%'><b>Masa berlaku</b></td><td align="left">{{$mb}}</td></tr>
                        </table>
                        
                        </div>
                        <div class="modal-footer">
                            <button type="button" style="background-color: green;" class="btn" data-bs-dismiss="modal"><font color='white'>Close</font></button>
                        </div>
                        </div>
                    </div>
                    </div>

                </div>
            </div>
        </div>
		@endforeach
		@endif
         </div>
      <br/>
      <div class="d-flex justify-content-center">
      {!! $kang->links() !!}
      </div>				
		</div>
        </div><!-- Content row end -->      
  </section><!-- Main container end -->

@endsection