@extends('layouts.lay3')

@section('content')
<section id="home">

        <div class="bg-holder" style="background-image:url({{url('/v2/assets/img/illustrations/heroheader-bg.png')}});background-position:center;background-size:contain;">
        </div>
        <!--/.bg-holder-->

        <div class="container">
          <div class="row align-items-center">
            <div class="col-md-7 col-lg-6 py-6 text-sm-start text-center">

            <div class="action-style-box">
                <div class="row align-items-center">
                <div class="col-md-12">
                <iframe src="https://www.google.com/maps/d/embed?mid=1Cc5m6V2nLFUX1f9ep_T_f3AV0sqg9MSr" width="1100" height="600"></iframe>
                </div><!-- col end -->
                </div><!-- row end -->
            </div><!-- Action style box -->

            </div>
          </div>     
        </div>
        
      </section>

      @endsection