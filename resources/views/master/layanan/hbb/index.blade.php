@extends('layouts.lay2')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-12">
        @if(!empty(\Session::get('success')) > 0)
                    <div class="alert alert-success">     
                        <strong>Sukses!</strong> {!! \Session::get('success') !!}             
                        </div>
                    @endif
                <!-- /.card-header -->
              @if(count($errors) > 0)
              <div class="alert alert-danger" role="alert">
                @foreach ($errors->all() as $error)
                {{ $error }} <br/>
                @endforeach
              </div>
              @endif
              <script>
                window.setTimeout(function() {
                  $.noConflict();
                  $(".alert").fadeTo(500, 0).slideUp(500, function(){
                    $(this).remove(); 
                  });
                }, 5000);
              </script>
              <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
              <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
                </div>
                <div class="col-sm-6">
            <h1 class="m-0">Harga</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/home">Dashboard</a></li>
              <li class="breadcrumb-item"><a href="/layanan/harga">Informasi</a></li>
              <li class="breadcrumb-item active">Harga</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
                <!-- Small boxes (Stat box) -->
        <div class="row">
					<div class="col-12">
							<div class="card">
							<div class="card-header">
							<div class="row">
							<div class="col-2" style='background-color:#e8ece5;'><center>
										<h3 class="card-title"><br/>Harga V2 | &nbsp;</h3>
						   <br/>
							<a class="btn btn-info btn-sm" href="<?php echo url('layanan/hbb/add'); ?>">
											<i class="fas fa-plus"> Add
											</i>
										</a>
							</div>

              <div class="col-1">
							</div>
							<div class="col-4" style='background-color:#e8ece5;'><br/><center>
              <button type="button" class="btn btn-success btn-sm confirmation" data-toggle="modal" data-target="#myModal"><i class="fas fa-file"></i> 
                Tata Cara Upload atau Import file Harga
              </button>
              <a class="btn btn-info btn-sm" href="{{asset('file_harga/templateharganew.xlsx')}}" target='_blank'>
							Download. 
              </a>
              </center>
										<!-- Modal -->
                                                <div class="modal fade" id="myModal" role="dialog">
												<div class="modal-dialog">
												
												<!-- Modal content-->
												<div class="modal-content">
													<div class="modal-header">
													<h4 class="modal-title">Tata Cara Import Harga via .xlsx</h4>
													</div>
													<div class="modal-body">
                            <p>
                            1. Download Template Harga<br/>
                            2. perhatikan kolomnya, seperti dibawah ini<br/>
                            Penjelasan Kolom :<br/>
                            Kolom A. Di kosongkan<br/>
                            Kolom B. Berisi Kode barang(bisa cek di table Barang)<br/>
                            Kolom C. Berisi Kan Nama Jenis Banyaknya Barang(contoh : Semen = SAK)<br/>
                            Kolom D. Berisi Nominal Harga(Tanpa ada petik, koma dan Titik)<br/>
                            Kolom E. Berisi Tanggal Last Update ditambahkan tanda '(contoh : '2022-09-02)<br/>
                            Kolom F. Berisi Nama Agen<br/>
                            Kolom G. Isiskan status Stock barang(ada)<br/>
                            Kolom H. Isikan Id Lokasi(angka)<br/>
                            Kolom I. Isikan Status Kwalitas barang(ok/baik)<br/>
                            Kolom J. Isikan Keterangan(default->tidak ada)<br/>
                            3. Semua data wajib di Isi kecuali kolom A<br/>
                            4. Setelah Semua terisi masukan file di Import Harga.<br/>
                            5. Push import ke Database<br/></p>
													</div>
													<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													</div>
												</div>
												
												</div>
											</div>
							</div>

							<div class="col-1">
							</div>
							<div class="col-4" style='background-color:#e8ece5;'><br/>
										<form method="post" action="<?php echo url("/layanan/hbb/importexcelnew"); ?>" enctype="multipart/form-data">
										{{ csrf_field() }}			
											<div class="form-group">
												<input type="file" name="file" required="required">
												<button type="submit" class="btn btn-primary">Import</button>
											</div>												
										</form>
							</div>
							
							</div>
							</div>
							<!-- /.card-header -->
              <div class="col-6"><br/>
              &nbsp; <button type="button" class="btn btn-danger btn-sm confirmation" data-toggle="modal" data-target="#myModald"><i class="fas fa-trash"></i> 
                Delete Advanced
              </button> 
              &nbsp; <button type="button" class="btn btn-info btn-sm confirmation" data-toggle="modal" data-target="#myModalda"><i class="fas fa-search"></i> 
                Search Keyword
              </button>
							</div>
              
              <div class="modal fade" id="myModalda" role="dialog">
												<div class="modal-dialog">
												
												<!-- Modal content-->
												<div class="modal-content">
													<div class="modal-header">
													<h4 class="modal-title">Search</h4>
													</div>
													<div class="modal-body">
                          <form class="form" action="<?php echo url("/layanan/hbb"); ?>" method="POST" enctype="multipart/form-data">
                            <!-- /.card-header -->
                            {{csrf_field()}}
                            <div class="card-body">
                              <div class="row">

                                    <div class="col-md-12">
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                    <label>Keyword</label>
                                    <input type="text" class="form-control" id="keyword" name="keyword" required>
                                  </div>
                                  <!-- /.form-group -->
                                </div>

                                <div class="col-12 col-sm-12">
                                <div class="form-group"><center>
                                    <button type="submit" class="btn btn-primary">Submit</button>       
                                </center></div>
                                <!-- /.form-group -->
                              </div>

                                </div>
                              </div>
                          </form>
													</div>
													<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													</div>
												</div>
												
												</div>
											</div>


              <div class="modal fade" id="myModald" role="dialog">
												<div class="modal-dialog">
												
												<!-- Modal content-->
												<div class="modal-content">
													<div class="modal-header">
													<h4 class="modal-title">Delete Advanced</h4>
													</div>
													<div class="modal-body">
                          <form class="form" action="<?php echo url("/layanan/hbb/destroy_search/"); ?>" method="POST" enctype="multipart/form-data">
                            <!-- /.card-header -->
                            {{csrf_field()}}
                            <div class="card-body">
                              <div class="row">
                                <div class="col-md-12">
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                    <label>Kecamatan *</label>
                                    <select name='id_kecamatan' class="form-control select2bs4" style="width: 100%;">
                                    <?php
                                    $kcmt = DB::table('tbl_kecamatan')
                                    ->select('*')
                                    ->get();
                                    ?>
                                    @foreach ($kcmt as $kc) 
                                    <option value='{{$kc->id_kecamatan}}' >{{$kc->nama_kecamatan}}</option>
                                    @endforeach
                                    </select>
                                    </div>
                                    <!-- /.form-group -->
                                    </div>

                                    <div class="col-md-12">
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                    <label>Tanggal</label>(hanya difungsikan bulan dan tahun)
                                    <input type="date" class="form-control" id="tgl" name="tgl" required>
                                  </div>
                                  <!-- /.form-group -->
                                </div>

                                <div class="col-12 col-sm-12">
                                <div class="form-group"><center>
                                    <button type="submit" class="btn btn-primary">Submit</button>       
                                </center></div>
                                <!-- /.form-group -->
                              </div>

                                </div>
                              </div>
                          </form>
													</div>
													<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													</div>
												</div>
												
												</div>
											</div>
							<div class="card-body">
              @if(empty($keyword))  
              @else
              <center><h4>Keyword anda : {{$keyword}}</h4></center>
              @endif
								<table id="next" class="table table-bordered table-hover">
								<thead>
                <tr>
									<th>Kode</th>
									<th>Nama Barang</th>
									<th>Jenis</th>
									<th>Kota/Kabupaten</th>
									<th>Kecamatan</th>
									<th>Waktu</th>
									<th>Harga</th>
									<th style="width:10%">Aksi</th>
								</tr>
								</thead>
								<tbody>
								<?php $no=0; ?> 
							  @foreach($tbl_harga as $lka)
                <?php
                 $tbl_harga1 = \DB::table('tbl_harga')
                 ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_harga.id_kecamatan')
                 ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
                 ->join('tbl_barang', 'tbl_harga.kode_barang', '=', 'tbl_barang.kode_jenis')
                 ->join('bahan_j', 'bahan_j.id_jenis', '=', 'tbl_barang.id_jenis')
                 ->select('tbl_harga.last_update','tbl_harga.kode_barang','tbl_harga.harga','tbl_harga.id as id_harga','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan','bahan_j.jenis')
                 ->groupBy('tbl_harga.last_update','tbl_harga.kode_barang','tbl_harga.harga','tbl_harga.id','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan','bahan_j.jenis')
                 ->orderBy('tbl_harga.last_update','DESC')
                 ->where('tbl_harga.id','=', $lka->id_harga)
                 ->limit('1')
                 ->paginate(500);
                 ?>
							  @foreach($tbl_harga1 as $lk)
							  <?php $no++; ?>
								<tr>
                    <td class='up'>{{$lk->kode_barang}}</p></td>
                    <td>{{$lk->nama_barang}}</td>
                    <td>{{$lk->jenis}}</td>
                    <td>{{$lk->kota}}</td>
                    <td>{{$lk->nama_kecamatan}}</td>
                    <td>{{date('Y-m-d', strtotime($lk->last_update))}}</td>
                    <td><?php $harga_format=number_format($lk->harga,0,".","."); ?>
                    {{$harga_format}}
                    </td>
                    <td>
									<a class="btn btn-info btn-sm" href="<?php echo url('/layanan/hbb/edit/'.$lk->id_harga); ?>">
											<i class="fas fa-edit">
											</i>
										</a>
										<button type="button" class="btn btn-danger btn-sm confirmation" data-toggle="modal" data-target="#myModali{{$lk->id_harga}}"><i class="fas fa-trash"></i></button>
										<!-- Modal -->
                                                <div class="modal fade" id="myModali{{$lk->id_harga}}" role="dialog">
												<div class="modal-dialog">
												
												<!-- Modal content-->
												<div class="modal-content">
													<div class="modal-header">
													<h4 class="modal-title">Apakah Anda Yakin menghapus data harga {{$lk->nama_barang}}..?</h4>
													</div>
													<div class="modal-footer">
													<a href="<?php echo url('/layanan/hbb/destroy/'.$lk->id_harga); ?>" class="btn btn-danger btn-sm confirmation" >Hapus Data</a>
														
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													</div>
												</div>
												
												</div>
											</div>
									</td>
								</tr>
								@endforeach
								@endforeach
								</tbody>
								</table>
							</div>
							<!-- /.card-body -->
							</div>
              <div class="d-flex justify-content-center">
                  {!! $tbl_harga->links() !!}
              </div>

						</div>  
						
            </div>
				</div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <script>
  $(function () {
    $("#next").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": true,"pageLength": 10,"lengthChange": true,"info": false,"sDom": "lfrti",
                    "lengthMenu": [[10, 50, 100, -1], [10, 50, 100, "All"]],bFilter: false,
    }).buttons().container().appendTo('#next_wrapper .col-md-6:eq(0)');
  });
  $(function () {
    $("#next2").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": true,"pageLength": 4
    }).buttons().container().appendTo('#next2_wrapper .col-md-6:eq(0)');
  });

  $(function () {
    $("#next3").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": true,"pageLength": 4
    }).buttons().container().appendTo('#next3_wrapper .col-md-6:eq(0)');
  });
</script>
@endsection