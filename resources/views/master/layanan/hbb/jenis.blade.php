@extends('layouts.lay2')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-12">
        @if(!empty(\Session::get('success')) > 0)
                    <div class="alert alert-success">     
                        <strong>Sukses!</strong> {!! \Session::get('success') !!}             
                        </div>
                    @endif
                <!-- /.card-header -->
              @if(count($errors) > 0)
              <div class="alert alert-danger" role="alert">
                @foreach ($errors->all() as $error)
                {{ $error }} <br/>
                @endforeach
              </div>
              @endif
              <script>
                window.setTimeout(function() {
                  $.noConflict();
                  $(".alert").fadeTo(500, 0).slideUp(500, function(){
                    $(this).remove(); 
                  });
                }, 5000);
              </script>
              <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
              <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
                </div>
                <div class="col-sm-6">
            <h1 class="m-0">Bahan Jenis</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/home">Dashboard</a></li>
              <li class="breadcrumb-item"><a href="/layanan/harga">Informasi</a></li>
              <li class="breadcrumb-item active">Bahan Jenis</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
                <!-- Small boxes (Stat box) -->
        <div class="row"> 
						
						<div class="col-12">
							<div class="card">
							<div class="card-header">
										<h3 class="card-title">Jenis | &nbsp;</h3>
						   
							<a class="btn btn-info btn-sm" href="<?php echo url('layanan/jenis/add_jenis'); ?>">
											<i class="fas fa-plus"> Add
											</i>
										</a>
										
							</div>
											
							<!-- /.card-header -->
							<div class="card-body">
								<table id="next2" class="table table-bordered table-hover">
								<thead>
								<tr>
									<th style="width:6%">Id</th>
									<th>Kode</th>
									<th>Jenis</th>
									<th style="width:15%">Aksi</th>
								</tr>
								</thead>
								<tbody>
								<?php $no=0; ?> 
							@foreach($jenis as $j)
							<?php $no++; ?>
								<tr>
									<td>
                    {{$j->id_jenis}}
                  </td>
									<td>
                    {{$j->kode}}
                  </td>
									<td>
                    {{$j->jenis}}
                  </td>
                  <?php /*
									<td>
									@if(empty($j->file))
									<img src="{{asset('/images/bahan/no-images.png')}}" alt="{{$j->file}}" width='40%' class="brand-image img-circle elevation-2" style="opacity: .8">
                        			@else
									<img src="{{asset('storage/images/bahan/'.$j->file)}}" alt="{{$j->file}}" width='40%' class="brand-image img-circle elevation-2" style="opacity: .8">
                        			@endif
									</td>
                  */ ?>
                                    <td>
									<a class="btn btn-info btn-sm" href="<?php echo url('/layanan/jenis/edit_jenis/'.$j->id_jenis); ?>">
											<i class="fas fa-edit">
											</i>
										</a>
										<button type="button" class="btn btn-danger btn-sm confirmation" data-toggle="modal" data-target="#myModalis{{$j->id_jenis}}"><i class="fas fa-trash"></i></button>
										<!-- Modal -->
                                                <div class="modal fade" id="myModalis{{$j->id_jenis}}" role="dialog">
												<div class="modal-dialog">
												
												<!-- Modal content-->
												<div class="modal-content">
													<div class="modal-header">
													<h4 class="modal-title">Apakah Anda Yakin menghapus data Link {{$j->jenis}}..?</h4>
													</div>
													<div class="modal-footer">
													<a href="<?php echo url('/layanan/jenis/destroy_jenis/'.$j->id_jenis); ?>" class="btn btn-danger btn-sm confirmation" >Hapus Data</a>
														
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													</div>
												</div>
												
												</div>
											</div>
									</td>
								</tr>
								@endforeach
								</tbody>
								</table>
							</div>
							<!-- /.card-body -->
							</div>

						</div> 
						
                        </div>
				</div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <script>
  $(function () {
    $("#next2").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": true,"pageLength": 10,"lengthChange": true,
                    "lengthMenu": [[10, 50, 100, -1], [10, 50, 100, "All"]],
    }).buttons().container().appendTo('#next_wrapper .col-md-6:eq(0)');
  });
  $(function () {
    $("#next").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": true
    }).buttons().container().appendTo('#next2_wrapper .col-md-6:eq(0)');
  });
</script>
@endsection