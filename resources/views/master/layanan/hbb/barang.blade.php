@extends('layouts.lay2')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-12">
        @if(!empty(\Session::get('success')) > 0)
                    <div class="alert alert-success">     
                        <strong>Sukses!</strong> {!! \Session::get('success') !!}             
                        </div>
                    @endif
                <!-- /.card-header -->
              @if(count($errors) > 0)
              <div class="alert alert-danger" role="alert">
                @foreach ($errors->all() as $error)
                {{ $error }} <br/>
                @endforeach
              </div>
              @endif
              <script>
                window.setTimeout(function() {
                  $.noConflict();
                  $(".alert").fadeTo(500, 0).slideUp(500, function(){
                    $(this).remove(); 
                  });
                }, 5000);
              </script>
              <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
              <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
                </div>
                <div class="col-sm-6">
            <h1 class="m-0">Barang</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/home">Dashboard</a></li>
              <li class="breadcrumb-item"><a href="/layanan/harga">Informasi</a></li>
              <li class="breadcrumb-item active">Barang</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
                <!-- Small boxes (Stat box) -->
                <div class="row">
					<div class="col-12">
							<div class="card">
							<div class="card-header">
							<div class="row">
							<div class="col-2" style='background-color:#e8ece5;'><center>
										<h3 class="card-title"><br/>Barang V2 | &nbsp;</h3>
						   <br/>
               <a class="btn btn-info btn-sm" href="<?php echo url('layanan/hbb/add_barang'); ?>">
											<i class="fas fa-plus"> Add
											</i>
										</a>
							</div>

              <div class="col-1">
							</div>
							<div class="col-4" style='background-color:#e8ece5;'><br/><center>
              <button type="button" class="btn btn-success btn-sm confirmation" data-toggle="modal" data-target="#myModal"><i class="fas fa-file"></i> 
                Tata Cara Upload atau Import file Barang
              </button>
              <a class="btn btn-info btn-sm" href="{{asset('file_harga/tabel_barang.xlsx')}}" target='_blank'>
							Download. 
              </a>
              </center>
										<!-- Modal -->
                                                <div class="modal fade" id="myModal" role="dialog">
												<div class="modal-dialog">
												
												<!-- Modal content-->
												<div class="modal-content">
													<div class="modal-header">
													<h4 class="modal-title">Tata Cara Import Barang via .xlsx</h4>
													</div>
													<div class="modal-body">
                            <p>
                            1. Download Template Harga<br/>
                            Penjelasan Kolom :<br/>
                            Kolom A. Di kosongkan<br/>
                            Kolom B. Berisi ID barang(bisa cek di table Bahan Jenis)<br/>
                            Kolom C. Berisi KODE Jenis(bisa cek di table Bahan Jenis)<br/>
                            Kolom D. Berisi Kan Nama Barang(contoh : Semen Tiga Roda)<br/>
                            Kolom E. Berisi Keterangan Barang(Semen)<br/><br/>
                            2. Semua data wajib di Isi kecuali kolom A<br/>
                            3. Setelah Semua terisi masukan file di Import Barang.<br/>
                            4. Push import ke Database<br/></p>
													</div>
													<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													</div>
												</div>
												
												</div>
											</div>
							</div>

							<div class="col-1">
							</div>
							<div class="col-4" style='background-color:#e8ece5;'><br/>
										<form method="post" action="<?php echo url("/layanan/hbb/importexcelnewbarang"); ?>" enctype="multipart/form-data">
										{{ csrf_field() }}			
											<div class="form-group">
												<input type="file" name="file" required="required">
												<button type="submit" class="btn btn-primary">Import</button>
											</div>												
										</form>
							</div>
							
							</div>
							</div>
							<!-- /.card-header -->
							<div class="card-body">
								<table id="next3" class="table table-bordered table-hover">
								<thead>
								<tr>
									<th style="width:6%">Kode Barang</th>
									<th>Jenis</th>
									<th>Nama</th>
									<th style="width:15%">Aksi</th>
								</tr>
								</thead>
								<tbody>
								<?php $no=0; ?> 
							@foreach($jenis as $j)
							<?php $no++; ?>
								<tr>
									<td>
                    {{$j->kode_jenis}}
                  </td>
									<td>
                    {{$j->jenis}}
                  </td>
									<td>
										{{$j->nama_barang}}
									</td>
                                    <td>
									<a class="btn btn-info btn-sm" href="<?php echo url('/layanan/hbb/edit_barang/'.$j->id); ?>">
											<i class="fas fa-edit">
											</i>
										</a>
										<button type="button" class="btn btn-danger btn-sm confirmation" data-toggle="modal" data-target="#myModalis{{$j->id}}"><i class="fas fa-trash"></i></button>
										<!-- Modal -->
                                                <div class="modal fade" id="myModalis{{$j->id}}" role="dialog">
												<div class="modal-dialog">
												
												<!-- Modal content-->
												<div class="modal-content">
													<div class="modal-header">
													<h4 class="modal-title">Apakah Anda Yakin menghapus data Barang {{$j->nama_barang}}..?</h4>
													</div>
													<div class="modal-footer">
													<a href="<?php echo url('/layanan/hbb/destroy_barang/'.$j->id); ?>" class="btn btn-danger btn-sm confirmation" >Hapus Data</a>
														
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													</div>
												</div>
												
												</div>
											</div>
									</td>
								</tr>
								@endforeach
								</tbody>
								</table>
							</div>
							<!-- /.card-body -->
							</div>

						</div> 
						
                        </div>
				</div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div> <script>
  $(function () {
    $("#next3").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": true,"pageLength": 10,"lengthChange": true,
                    "lengthMenu": [[10, 50, 100, -1], [10, 50, 100, "All"]],
    }).buttons().container().appendTo('#next_wrapper .col-md-6:eq(0)');
  });
</script>
@endsection