@extends('layouts.lay')

@section('content')

<link href="{{url('v2/assets/css/pdk.css')}}" rel="stylesheet" />
<style>
#rcorners2 {
  border-radius: 5px;
  border: 1px solid #0001;
  padding: 1px;
}
<style>
table#next.dataTable tr:hover {
background-color: #d0ecdc;
}
.table-striped>tbody>tr:nth-child(odd)>td, 
.table-striped>tbody>tr:nth-child(odd)>th {
	background-color: #e8f7f0 ; /* Choose your own color here */
	}\tr.even td
	{background-color:#fff}                        
</style>
<br/>
<div><br/><br/></div>
        
	<div class="col-xl-12 col-md-12 overlay-image-card">
                              
                                    <div class="card">
                                        <ul class="nav nav-pills border-bottom justify-content-right mb-1" role="tablist">
                                            
                                        <li class="nav-item mr-1">
                                            &nbsp; &nbsp;
                                            </li>

                                        <li class="nav-item mr-1" id="rcorners2">
                                                <a class=" nav-link active" id="january-tab" data-toggle="tab" href="#january" aria-controls="january" role="tab" aria-selected="true">
                                                    Table Harga</a>
                                            </li>
                                            <li class="nav-item mr-1" id="rcorners2">
                                                <a class=" nav-link " id="february-tab" data-toggle="tab" href="#february" aria-controls="february" role="tab" aria-selected="false">
                                                    Peta Harga</a>
                                            </li>
                                        </ul>
                                        <div class="tab-content pl-0">
                                            <div class="tab-pane active" id="january" aria-labelledby="january-tab" role="tabpanel">
                                                <div class="table-responsive">
																
																		<div id="toggleCari" class="accordion-body collapse show" aria-labelledby="cari">
																		<div class="card-body">
																		<form class="form" action="<?php echo url("/harga/all"); ?>" method="get">
																			<!-- /.card-header -->
																			{{csrf_field()}}
                                                                            <div class='row'>
                                                                                <div class="col-lg-3">
																		     <label><font color='#000'><b>Nama Barang</b></font></label>
																				<div class="custom-select-1">
																				  <input type="text" class="form-control" id="nama" name="nama" placeholder='Nama Barang'>
																				</div>
																			  </div>
																			  
                                                                              <div class="col-lg-2">
                                                                              <label><font color='#000'><b>Jenis Barang</b></font></label>
																				<div class="custom-select-1">
																				  <select name='id_jenis' class="form-control">
																					  <option value="0" selected>--Pilih Jenis.--</option>
																					  @foreach ($jenis as $jns) 
																					  <option value='{{$jns->id_jenis}}' >{{$jns->jenis}}</option>
																					  @endforeach
																				   </select>
																				</div>
																			  </div>
																			  
                                                                              <div class="col-lg-2">
                                                                                <label><font color='#000'><b>Kabupaten</b></font></label>
																				<div class="custom-select-1">
																				  <select class="form-control" name="id_kota" id="kota">
																					  <option value='0' >--Pilih Kabupaten/Kota.--</option>
																						@foreach ($kota as $item)
																						<option value="{{ $item->id_kota }}">{{ $item->kota }}</option>
																						@endforeach
																				  </select>
																				</div>
																			  </div>
																			  
                                                                              <div class="col-lg-2">
                                                                              <label><font color='#000'><b>Kecamatan</b></font></label>
																				<div class="custom-select-1">
																				  <select class="form-control" name="id_kecamatan" id="id_kecamatan"></select>
																				</div>
																			  </div>
																			  
																				<input type='hidden' name='urut'>
																			  
                                                                                <div class="col-lg-1">
                                                                                    <label><font color='#000'><b>Tahun</b></font></label>
																				<div class="custom-select-1">
																				  <select class="form-control" name="tahun">
																					<option value="0" selected="">--Tahun--</option>
																					<?php
																					for($i=date('Y'); $i>=date('Y')-5; $i-=1){
																					?>
																					<option value="{{$i}}">{{$i}}</option>
																					<?php } ?>
																				  </select>
																				</div>
																			  </div>
																			  
                                                                              <div class="col-lg-2">
                                                                                <label><font color='#fff'><b>.</b></font></label>
																				<div class="custom-select-1">
																				  <button class="btn btn-primary btn-rounded font-weight-bold btn-h-2 btn-v-3" type="submit">
                                                                                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                                                                                    Cari 
                                                                                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                                                                                  </button>
																				</div>
																			  </div>
																			  
																			</div>
																		  </form>
																		  <?php
																		  if($tahun > 0){
																			$year=$tahun;
																		  }else{
																			$year=date('Y');
																		  }
																		  ?>
																		  <table id="next" class="table table-striped table-bordered hover" bordercolor="#e0e4e4">
																			<thead>
																				<tr>
																					<th colspan="14" style='background:#4c7dd1 '><font color='#fff'><center>{{$year}} (Dalam satuan rupiah)</center></font></th>
																				</tr>
																				<tr style='background:#58b474'>
                                                                                
																					<th style='background:#fff' width="305px"><font color='#000'><center>Nama Barang</center></font></th>
																					<th style='background:#fff' width="170px"><font color='#000'><center>Lokasi</center></font></th>
																					<th width="80px"><font color='#fff'><center>Jan</center></font></th>
																					<th width="80px" ><font color='#fff'><center>Feb</center></font></th>
																					<th width="80px"><font color='#fff'><center>Mar</center></font></th>
																					<th width="80px"><font color='#fff'><center>Apr</center></font></th>
																					<th width="80px"><font color='#fff'><center>Mei</center></font></th>
																					<th width="80px"><font color='#fff'><center>Jun</center></font></th>
																					<th width="80px"><font color='#fff'><center>Jul</center></font></th>
																					<th width="80px"><font color='#fff'><center>Ags</center></font></th>
																					<th width="80px"><font color='#fff'><center>Sep</center></font></th>
																					<th width="80px"><font color='#fff'><center>Okt</center></font></th>
																					<th width="80px"><font color='#fff'><center>Nov</center></font></th>
																					<th width="80px"><font color='#fff'><center>Des</center></font></th>
																				</tr>
																			</thead>
																			<?php 
																			$hit3 = count($harga); 
																			?>
																			@if($hit3 <= 0)
																			<tbody>
																			<div class="call-to-action-btn"><br/>
																
																			</div> 
																			</tbody>
																			@else
																			<?php $no=0; ?>
																			<tbody>
																			@foreach($harga as $h)
																				   <?php $no++; ?>
																				<tr>
																					<td>
																						{{$h->nama_barang}}
																					</td>
																					<td>
																						{{$h->nama_kecamatan}}, {{$h->kota}}
																					</td>
																					<td>
																					<?php
																						 
																						 $jan = \DB::table('tbl_harga')
																						 ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_harga.id_kecamatan')
																						 ->join('tbl_barang', 'tbl_harga.kode_barang', '=', 'tbl_barang.kode_jenis')
																						 ->select('tbl_harga.harga')
																						 ->where('tbl_barang.nama_barang', '=', $h->nama_barang)
																						 ->where('tbl_kecamatan.nama_kecamatan', '=', $h->nama_kecamatan)
																						 ->where(\DB::raw('DATE_FORMAT(tbl_harga.last_update, "%m-%Y")'), '01-'.$year)
																						 ->orderBy('tbl_harga.last_update','DESC')
																						 ->limit('1')
																						 ->get();
																					?>
																						<?php 
																						$hit5 = count($jan); 
																						?>
																						@if($hit5 <= 0)
																						-
																						@else
																						@foreach ($jan as $j)
																						 <?php echo $harga_format=number_format($j->harga,0,".","."); ?>
																						@endforeach 
																						@endif
																					</td>
																					<td>
																					<?php
																						 
																						 $feb = \DB::table('tbl_harga')
																						 ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_harga.id_kecamatan')
																						 ->join('tbl_barang', 'tbl_harga.kode_barang', '=', 'tbl_barang.kode_jenis')
																						 ->select('tbl_harga.harga')
																						 ->where('tbl_barang.nama_barang', '=', $h->nama_barang)
																						 ->where('tbl_kecamatan.nama_kecamatan', '=', $h->nama_kecamatan)
																						 ->where(\DB::raw('DATE_FORMAT(tbl_harga.last_update, "%m-%Y")'), '02-'.$year)
																						 ->orderBy('tbl_harga.last_update','DESC')
																						 ->limit('1')
																						 ->get();
																					?>
																						<?php 
																						$hit6 = count($feb); 
																						?>
																						@if($hit6 <= 0)
																						-
																						@else
																						@foreach ($feb as $f)
																						 <?php echo $harga_format=number_format($f->harga,0,".","."); ?>
																						@endforeach 
																						@endif
																					</td>
																					<td>
																					<?php
																						 
																						 $mar = \DB::table('tbl_harga')
																						 ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_harga.id_kecamatan')
																						 ->join('tbl_barang', 'tbl_harga.kode_barang', '=', 'tbl_barang.kode_jenis')
																						 ->select('tbl_harga.harga')
																						 ->where('tbl_barang.nama_barang', '=', $h->nama_barang)
																						 ->where('tbl_kecamatan.nama_kecamatan', '=', $h->nama_kecamatan)
																						 ->where(\DB::raw('DATE_FORMAT(tbl_harga.last_update, "%m-%Y")'), '03-'.$year)
																						 ->orderBy('tbl_harga.last_update','DESC')
																						 ->limit('1')
																						 ->get();
																					?>
																						<?php 
																						$hit7 = count($mar); 
																						?>
																						@if($hit7 <= 0)
																						-
																						@else
																						@foreach ($mar as $mr)
																						 <?php echo $harga_format=number_format($mr->harga,0,".","."); ?>
																						@endforeach 
																						@endif
																					</td>
																					<td>
																					<?php
																						 
																						 $apr = \DB::table('tbl_harga')
																						 ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_harga.id_kecamatan')
																						 ->join('tbl_barang', 'tbl_harga.kode_barang', '=', 'tbl_barang.kode_jenis')
																						 ->select('tbl_harga.harga')
																						 ->where('tbl_barang.nama_barang', '=', $h->nama_barang)
																						 ->where('tbl_kecamatan.nama_kecamatan', '=', $h->nama_kecamatan)
																						 ->where(\DB::raw('DATE_FORMAT(tbl_harga.last_update, "%m-%Y")'), '04-'.$year)
																						 ->orderBy('tbl_harga.last_update','DESC')
																						 ->limit('1')
																						 ->get();
																					?>
																						<?php 
																						$hit8 = count($apr); 
																						?>
																						@if($hit8 <= 0)
																						-
																						@else
																						@foreach ($apr as $a)
																						 <?php echo $harga_format=number_format($a->harga,0,".","."); ?>
																						@endforeach 
																						@endif
																					</td>
																					<td>
																					<?php
																						 
																						 $mei = \DB::table('tbl_harga')
																						 ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_harga.id_kecamatan')
																						 ->join('tbl_barang', 'tbl_harga.kode_barang', '=', 'tbl_barang.kode_jenis')
																						 ->select('tbl_harga.harga')
																						 ->where('tbl_barang.nama_barang', '=', $h->nama_barang)
																						 ->where('tbl_kecamatan.nama_kecamatan', '=', $h->nama_kecamatan)
																						 ->where(\DB::raw('DATE_FORMAT(tbl_harga.last_update, "%m-%Y")'), '05-'.$year)
																						 ->orderBy('tbl_harga.last_update','DESC')
																						 ->limit('1')
																						 ->get();
																					?>
																						<?php 
																						$hit9 = count($mei); 
																						?>
																						@if($hit9 <= 0)
																						-
																						@else
																						@foreach ($mei as $m)
																						 <?php echo $harga_format=number_format($m->harga,0,".","."); ?>
																						@endforeach 
																						@endif
																					</td>
																					<td>
																					<?php
																						 
																						 $jun = \DB::table('tbl_harga')
																						 ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_harga.id_kecamatan')
																						 ->join('tbl_barang', 'tbl_harga.kode_barang', '=', 'tbl_barang.kode_jenis')
																						 ->select('tbl_harga.harga')
																						 ->where('tbl_barang.nama_barang', '=', $h->nama_barang)
																						 ->where('tbl_kecamatan.nama_kecamatan', '=', $h->nama_kecamatan)
																						 ->where(\DB::raw('DATE_FORMAT(tbl_harga.last_update, "%m-%Y")'), '06-'.$year)
																						 ->orderBy('tbl_harga.last_update','DESC')
																						 ->limit('1')
																						 ->get();
																					?>
																						<?php 
																						$hit10 = count($jun); 
																						?>
																						@if($hit10 <= 0)
																						-
																						@else
																						@foreach ($jun as $ju)
																						 <?php echo $harga_format=number_format($ju->harga,0,".","."); ?>
																						@endforeach 
																						@endif
																					</td>
																					<td>
																					<?php
																						 
																						 $jul = \DB::table('tbl_harga')
																						 ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_harga.id_kecamatan')
																						 ->join('tbl_barang', 'tbl_harga.kode_barang', '=', 'tbl_barang.kode_jenis')
																						 ->select('tbl_harga.harga')
																						 ->where('tbl_barang.nama_barang', '=', $h->nama_barang)
																						 ->where('tbl_kecamatan.nama_kecamatan', '=', $h->nama_kecamatan)
																						 ->where(\DB::raw('DATE_FORMAT(tbl_harga.last_update, "%m-%Y")'), '07-'.$year)
																						 ->orderBy('tbl_harga.last_update','DESC')
																						 ->limit('1')
																						 ->get();
																					?>
																						<?php 
																						$hit11 = count($jul); 
																						?>
																						@if($hit11 <= 0)
																						-
																						@else
																						@foreach ($jul as $jl)
																						 <?php echo $harga_format=number_format($jl->harga,0,".","."); ?>
																						@endforeach 
																						@endif
																					</td>
																					<td>
																					<?php
																						 
																						 $ags = \DB::table('tbl_harga')
																						 ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_harga.id_kecamatan')
																						 ->join('tbl_barang', 'tbl_harga.kode_barang', '=', 'tbl_barang.kode_jenis')
																						 ->select('tbl_harga.harga')
																						 ->where('tbl_barang.nama_barang', '=', $h->nama_barang)
																						 ->where('tbl_kecamatan.nama_kecamatan', '=', $h->nama_kecamatan)
																						 ->where(\DB::raw('DATE_FORMAT(tbl_harga.last_update, "%m-%Y")'), '08-'.$year)
																						 ->orderBy('tbl_harga.last_update','DESC')
																						 ->limit('1')
																						 ->get();
																					?>
																						<?php 
																						$hit12 = count($ags); 
																						?>
																						@if($hit12 <= 0)
																						-
																						@else
																						@foreach ($ags as $ag)
																						 <?php echo $harga_format=number_format($ag->harga,0,".","."); ?>
																						@endforeach 
																						@endif
																					</td>
																					<td>
																					<?php
																						 
																						 $sep = \DB::table('tbl_harga')
																						 ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_harga.id_kecamatan')
																						 ->join('tbl_barang', 'tbl_harga.kode_barang', '=', 'tbl_barang.kode_jenis')
																						 ->select('tbl_harga.harga')
																						 ->where('tbl_barang.nama_barang', '=', $h->nama_barang)
																						 ->where('tbl_kecamatan.nama_kecamatan', '=', $h->nama_kecamatan)
																						 ->where(\DB::raw('DATE_FORMAT(tbl_harga.last_update, "%m-%Y")'), '09-'.$year)
																						 ->orderBy('tbl_harga.last_update','DESC')
																						 ->limit('1')
																						 ->get();
																					?>
																						<?php 
																						$hit13 = count($sep); 
																						?>
																						@if($hit13 <= 0)
																						-
																						@else
																						@foreach ($sep as $s)
																						 <?php echo $harga_format=number_format($s->harga,0,".","."); ?>
																						@endforeach 
																						@endif
																					</td>
																					<td>
																					<?php
																						 
																						 $okt = \DB::table('tbl_harga')
																						 ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_harga.id_kecamatan')
																						 ->join('tbl_barang', 'tbl_harga.kode_barang', '=', 'tbl_barang.kode_jenis')
																						 ->select('tbl_harga.harga')
																						 ->where('tbl_barang.nama_barang', '=', $h->nama_barang)
																						 ->where('tbl_kecamatan.nama_kecamatan', '=', $h->nama_kecamatan)
																						 ->where(\DB::raw('DATE_FORMAT(tbl_harga.last_update, "%m-%Y")'), '10-'.$year)
																						 ->orderBy('tbl_harga.last_update','DESC')
																						 ->limit('1')
																						 ->get();
																					?>
																						<?php 
																						$hit14 = count($okt); 
																						?>
																						@if($hit14 <= 0)
																						-
																						@else
																						@foreach ($okt as $o)
																						 <?php echo $harga_format=number_format($o->harga,0,".","."); ?>
																						@endforeach 
																						@endif
																					</td>
																					<td>
																					<?php
																						 
																						 $nov = \DB::table('tbl_harga')
																						 ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_harga.id_kecamatan')
																						 ->join('tbl_barang', 'tbl_harga.kode_barang', '=', 'tbl_barang.kode_jenis')
																						 ->select('tbl_harga.harga')
																						 ->where('tbl_barang.nama_barang', '=', $h->nama_barang)
																						 ->where('tbl_kecamatan.nama_kecamatan', '=', $h->nama_kecamatan)
																						 ->where(\DB::raw('DATE_FORMAT(tbl_harga.last_update, "%m-%Y")'), '11-'.$year)
																						 ->orderBy('tbl_harga.last_update','DESC')
																						 ->limit('1')
																						 ->get();
																					?>
																						<?php 
																						$hit15 = count($nov); 
																						?>
																						@if($hit15 <= 0)
																						-
																						@else
																						@foreach ($nov as $n)
																						 <?php echo $harga_format=number_format($n->harga,0,".","."); ?>
																						@endforeach 
																						@endif
																					</td>
																					<td>
																					<?php
																						 
																						 $des = \DB::table('tbl_harga')
																						 ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_harga.id_kecamatan')
																						 ->join('tbl_barang', 'tbl_harga.kode_barang', '=', 'tbl_barang.kode_jenis')
																						 ->select('tbl_harga.harga')
																						 ->where('tbl_barang.nama_barang', '=', $h->nama_barang)
																						 ->where('tbl_kecamatan.nama_kecamatan', '=', $h->nama_kecamatan)
																						 ->where(\DB::raw('DATE_FORMAT(tbl_harga.last_update, "%m-%Y")'), '12-'.$year)
																						 ->orderBy('tbl_harga.last_update','DESC')
																						 ->limit('1')
																						 ->get();
																					?>
																						<?php 
																						$hit16 = count($des); 
																						?>
																						@if($hit16 <= 0)
																						-
																						@else
																						@foreach ($des as $d)
																						 <?php echo $harga_format=number_format($d->harga,0,".","."); ?>
																						@endforeach 
																						@endif
																					</td>
																				</tr> 
																				@endforeach      
																			</tbody>
																			@endif
																		</table>
																		<div class="d-flex justify-content-center">
																		{{ $harga->appends(['nama' => $_GET['nama'],'id_jenis'=>$_GET['id_jenis'],'id_kota'=>$_GET['id_kota'],'tahun'=>$_GET['tahun']])->links() }}
                                                                        </div>
																		</div>
																	  </div>
																		
                                                                        <br/>
                                                </div>
                                            </div>
                                            <div class="tab-pane pl-0" id="february" aria-labelledby="february-tab" role="tabpanel">
                                                <div class="table-responsive">
                                                    <div class="col-lg-12">
														<div class="row">
														  <div class="col-lg-6">
														  <?php
															$map = DB::table('peta')
															->select('*')
															->where('id_jenis','=',1)
															->limit(1)
															->get();
															?>
															@foreach ($map as $mp)
															<div class="card">
															{!!$mp->iframe_peta!!}  
															</div>
															@endforeach
														  </div><!-- Col end -->
														  <div class="col-lg-6">
														  <?php
															$map2 = DB::table('peta')
															->select('*')
															->where('id_jenis','=',2)
															->limit(1)
															->get();
															?>
															@foreach ($map2 as $mp2)
															<div class="card">
															{!!$mp2->iframe_peta!!}  
															</div>
															@endforeach
														  </div><!-- Col end -->
														</div><!-- Row end -->
													</div>
													<br/>
                                                </div>
                                            </div>
                                           
                                        </div>
                                    </div>
                        </div>
                        <!-- User Widget with Overlay Image Ends -->

  <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
        <script>
            $(document).ready(function() {
            $('#kota').on('change', function() {
               var kotaID = $(this).val();
               if(kotaID) {
                   $.ajax({
                       url: '/kecamatan/'+kotaID,
                       type: "post",
                       data : {"_token":"{{ csrf_token() }}"},
                       dataType: "json",
                       success:function(data)
                       {
                         if(data){
                            $('#id_kecamatan').empty();
                            $('#id_kecamatan').append('<option value = "0" >--Pilih Kecamatan.--</option>'); 
                            $.each(data, function(key, id_kecamatan){
                                $('select[name="id_kecamatan"]').append('<option value="'+ id_kecamatan.id_kecamatan +'">' + id_kecamatan.nama_kecamatan+ '</option>');
                            });
                        }else{
                            $('#id_kecamatan').empty();
                        }
                     }
                   });
               }else{
                 $('#id_kecamatan').empty();
               }
            });
            });
        </script>
        

        <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
        <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.11.5/js/dataTables.bootstrap.min.js"></script>
        <script src="https://cdn.datatables.net/fixedcolumns/4.0.2/js/dataTables.fixedColumns.min.js"></script>
        
        <script src="{{url('/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
        <script>
        var jq = $.noConflict();
        jq(function () {
            jq("#next").DataTable({
            'scrollX':true,"lengthChange": false, bFilter: false,'info': false,
            'scrollCollapse': true, 'paging':false,"ordering": false,
            });
        });
        </script>
@endsection