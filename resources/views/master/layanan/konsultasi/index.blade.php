@extends('layouts.lay2')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-12">
        @if(!empty(\Session::get('success')) > 0)
                    <div class="alert alert-success">     
                        <strong>Sukses!</strong> {!! \Session::get('success') !!}             
                        </div>
                    @endif
                <script>
                    window.setTimeout(function() {
                    $.noConflict();
                    $(".alert").fadeTo(500, 0).slideUp(500, function(){
                        $(this).remove(); 
                    });
                    }, 5000);
                </script>
                <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
                <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
                </div>
                <div class="col-sm-6">
            <h1 class="m-0">Konsultasi</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/home">Dashboard</a></li>
              <li class="breadcrumb-item"><a href="/layanan/konsultasi">layanan</a></li>
              <li class="breadcrumb-item active">Konsultasi</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
                <!-- Small boxes (Stat box) -->
        <div class="row">
                <div class="col-12 col-sm-12">
                    <div class="card card-primary card-outline card-outline-tabs">
                    <div class="card-header p-0 border-bottom-0">
                        <ul class="nav nav-tabs" id="custom-tabs-four-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="custom-tabs-four-home-tab" data-toggle="pill" href="#custom-tabs-four-home" role="tab" aria-controls="custom-tabs-four-home" aria-selected="true">Konsultasi Terbaru
                                &nbsp; 
                                <span class="badge badge-danger right">
                                    @foreach($tbl_count as $c){{$c->hit}}@endforeach
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="custom-tabs-four-profile-tab" data-toggle="pill" href="#custom-tabs-four-profile" role="tab" aria-controls="custom-tabs-four-profile" aria-selected="false">Konsultasi Terjawab
                            &nbsp; 
                                <span class="badge badge-info right">
                                    @foreach($tbl_count2 as $c2){{$c2->hit}}@endforeach
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="custom-tabs-four-messages-tab" data-toggle="pill" href="#custom-tabs-four-messages" role="tab" aria-controls="custom-tabs-four-messages" aria-selected="false">Kategori Konsultasi</a>
                        </li>
                        </ul>
                    </div>
                    <div class="card-body">
                        <div class="tab-content" id="custom-tabs-four-tabContent">
                        <div class="tab-pane fade show active" id="custom-tabs-four-home" role="tabpanel" aria-labelledby="custom-tabs-four-home-tab">
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <table id="next" class="table table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Tanggal</th>
                                            <th>Nama</th>
                                            <th>Judul</th>
                                            <th>Pertanyaan</th>
                                            <th>Aksi</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $no=0; ?> 
                                    @foreach($tbl_konsultasi as $lk)
                                    <?php $no++; ?>
                                        <tr>
                                            <td>{{$no}}</td>
                                            <td>{{$lk->tgl_konsultasi}}</td>
                                            <td>
                                                <p>
                                                Nama : {{$lk->nama_konsultasi}}<br/>
                                                HP   : {{$lk->hp_konsultasi}}<br/>
                                                Email: {{$lk->email_konsultasi}}
                                                </p>
                                            </td>
                                            <td>{{$lk->judul_konsultasi}}</td>
									        <td>{!!$lk->pertanyaan_konsultasi!!}</td>
                                            <td>
                                            <a class="btn btn-info btn-sm" href="<?php echo url('/layanan/konsultasi/edit/'.$lk->id_konsultasi); ?>">
                                                    <i class="fas fa-edit">
                                                    </i>
                                                </a>
                                                <button type="button" class="btn btn-danger btn-sm confirmation" data-toggle="modal" data-target="#myModal{{$lk->id_konsultasi}}"><i class="fas fa-trash"></i></button>
                                                <!-- Modal -->
                                                        <div class="modal fade" id="myModal{{$lk->id_konsultasi}}" role="dialog">
                                                        <div class="modal-dialog">
                                                        
                                                        <!-- Modal content-->
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                            <h4 class="modal-title">Apakah Anda Yakin menghapus data Link {{$lk->judul_konsultasi}}..?</h4>
                                                            </div>
                                                            <div class="modal-footer">
                                                            <a href="<?php echo url('/layanan/konsultasi/destroy/'.$lk->id_konsultasi); ?>" class="btn btn-danger btn-sm confirmation" >Hapus Data</a>
                                                                
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                        
                                                        </div>
                                                    </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                        </table>
                                    </div>
                                    <!-- /.card-body -->
                        </div>

                        <!-- bagian terjawab -->
                        <div class="tab-pane fade" id="custom-tabs-four-profile" role="tabpanel" aria-labelledby="custom-tabs-four-profile-tab">
                          
                            <!-- /.card-header -->
							<div class="card-body">
								<table id="next2" class="table table-bordered table-hover">
								<thead>
								<tr>
									<th>No</th>
									<th>Tanggal</th>
									<th>Nama</th>
									<th>Judul | Kategori</th>
									<th>Pertanyaan | Jawaban</th>
									<th>Aksi</th>
								</tr>
								</thead>
								<tbody>
								<?php $no=0; ?> 
							@foreach($tbl_konsultasi2 as $lk2)
							<?php $no++; ?>
								<tr>
									<td>{{$no}}</td>
									<td>{{$lk2->tgl_konsultasi}}</td>
									<td>
                                        <p>
                                        Nama : {{$lk2->nama_konsultasi}}<br/>
                                        HP   : {{$lk2->hp_konsultasi}}<br/>
                                        Email: {{$lk2->email_konsultasi}}
                                        </p>
                                    </td>
									<td>
                                        
                                        Judul : <br/> {{$lk2->judul_konsultasi}} <br/><br/>
                                        Kategori : <br/> {{$lk2->kategori_konsultasi}}
                                    </td>
									<td>
                                        Pertanyaan : <br/>
                                        {!!$lk2->pertanyaan_konsultasi!!}<br/>
                                        Jawaban : <br/>
                                        {!!$lk2->jawaban_konsultasi!!}
                                    </td>
									<td>
									<a class="btn btn-info btn-sm" href="<?php echo url('/layanan/konsultasi/edit/'.$lk2->id_konsultasi); ?>">
											<i class="fas fa-edit">
											</i>
										</a>
										<button type="button" class="btn btn-danger btn-sm confirmation" data-toggle="modal" data-target="#myModaly{{$lk2->id_konsultasi}}"><i class="fas fa-trash"></i></button>
										<!-- Modal -->
                                                <div class="modal fade" id="myModaly{{$lk2->id_konsultasi}}" role="dialog">
												<div class="modal-dialog">
												
												<!-- Modal content-->
												<div class="modal-content">
													<div class="modal-header">
													<h4 class="modal-title">Apakah Anda Yakin menghapus data Link {{$lk2->judul_konsultasi}}..?</h4>
													</div>
													<div class="modal-footer">
													<a href="<?php echo url('/layanan/konsultasi/destroy/'.$lk2->id_konsultasi); ?>" class="btn btn-danger btn-sm confirmation" >Hapus Data</a>
														
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													</div>
												</div>
												
												</div>
											</div>
									</td>
								</tr>
								@endforeach
								</tbody>
								</table>
							</div>
							<!-- /.card-body -->
                    
                        </div>
                        <div class="tab-pane fade" id="custom-tabs-four-messages" role="tabpanel" aria-labelledby="custom-tabs-four-messages-tab">
                        <a class="btn btn-info btn-sm" href="<?php echo url('/layanan/konsultasi/add'); ?>">
                                                    <i class="fas fa-plus"> Add
                                                    </i>
                                                </a>
                            <!-- /.card-header -->
							<div class="card-body">
								<table id="next3" class="table table-bordered table-hover">
								<thead>
								<tr>
									<th>No</th>
									<th>Kategori</th>
									<th>Aksi</th>
								</tr>
								</thead>
								<tbody>
								<?php $no=0; ?> 
							@foreach($tbl_kat_konsul as $ksl)
							<?php $no++; ?>
								<tr>
									<td>{{$no}}</td>
									<td>{{$ksl->nama_kat_konsul}}</td>
									<td>
									<a class="btn btn-info btn-sm" href="<?php echo url('/layanan/konsultasi/edit_kategori/'.$ksl->id_kat_konsul); ?>">
											<i class="fas fa-edit">
											</i>
										</a>
										<button type="button" class="btn btn-danger btn-sm confirmation" data-toggle="modal" data-target="#myModaloo{{$ksl->id_kat_konsul}}"><i class="fas fa-trash"></i></button>
										<!-- Modal -->
                                                <div class="modal fade" id="myModaloo{{$ksl->id_kat_konsul}}" role="dialog">
												<div class="modal-dialog">
												
												<!-- Modal content-->
												<div class="modal-content">
													<div class="modal-header">
													<h4 class="modal-title">Apakah Anda Yakin menghapus data Link {{$ksl->nama_kat_konsul}}..?</h4>
													</div>
													<div class="modal-footer">
													<a href="<?php echo url('/layanan/konsultasi/destroy_kategori/'.$ksl->id_kat_konsul); ?>" class="btn btn-danger btn-sm confirmation" >Hapus Data</a>
														
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													</div>
												</div>
												
												</div>
											</div>
									</td>
								</tr>
								@endforeach
								</tbody>
								</table>
							</div>
							<!-- /.card-body -->
                        </div>
                        </div>
                    </div>
                    <!-- /.card -->
                    </div>
                </div>
                </div>                     
			
			</div>
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <script>
  $(function () {
    $("#next").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
    }).buttons().container().appendTo('#next_wrapper .col-md-6:eq(0)');
  });
  $(function () {
    $("#next2").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
    }).buttons().container().appendTo('#next2_wrapper .col-md-6:eq(0)');
  });
  $(function () {
    $("#next3").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
    }).buttons().container().appendTo('#next3_wrapper .col-md-6:eq(0)');
  });
</script>
@endsection