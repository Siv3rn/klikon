@extends('layouts.lay2')

@section('content')
@foreach($tbl_konsultasi as $g)
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Konsultasi</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/home">Dashboard</a></li>
              <li class="breadcrumb-item"><a href="/layanan/konsultasi">layanan</a></li>
              <li class="breadcrumb-item active">Konsultasi Update</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
                <!-- Small boxes (Stat box) -->
        <div class="row">
          
        <div class="col-12">
            <div class="card">
              <div class="card-header">
                        <h3 class="card-title">Konsultasi | Update</h3>
              </div>
              
              <!-- /.card-header -->
              @if(count($errors) > 0)
              <div class="alert alert-danger" role="alert">
                @foreach ($errors->all() as $error)
                {{ $error }} <br/>
                @endforeach
              </div>
              @endif
              <script>
                window.setTimeout(function() {
                  $.noConflict();
                  $(".alert").fadeTo(500, 0).slideUp(500, function(){
                    $(this).remove(); 
                  });
                }, 5000);
              </script>
              <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
              <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
    
                
              <form class="form" action="<?php echo url("/layanan/konsultasi/update/$g->id_konsultasi"); ?>" method="POST" enctype="multipart/form-data">
                <!-- /.card-header -->
                {{csrf_field()}}
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Judul *</label>
                        <input type="text" class="form-control" id="judul_konsultasi" name="judul_konsultasi" value="{{$g->judul_konsultasi}}" required>
                      <input type="hidden" class="form-control" id="tgl_jawaban" name="tgl_jawaban" value="{{date('Y-m-d')}}" required>
                      </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Kategori *</label>
                        <select name='id_kat_konsul' class="form-control select2bs4" style="width: 100%;">
                          @foreach($id_kat_konsul as $ik)
                            <option value='{{$ik->id_kat_konsul}}' {{$g->id_kat_konsul ==  $ik->id_kat_konsul  ? 'selected' : ''}}>{{$ik->nama_kat_konsul}}</option>
                          @endforeach
                        </select>
                        </div>
                      <!-- /.form-group -->
                    </div>
                    <!-- /.col -->  

                        <div class="col-md-6">
                        <div class="form-group">
                            <label>Pertanyaan *</label>
                            <textarea id="summernote1" name='pertanyaan_konsultasi'>{{$g->pertanyaan_konsultasi}}</textarea>
                        </div>
                        </div>
                        <!-- /.col -->

                        <div class="col-md-6">
                        <div class="form-group">
                            <label>Jawaban *</label>
                            <textarea id="summernote" name='jawaban_konsultasi'>{{$g->jawaban_konsultasi}}</textarea>
                        </div>
                        </div>
                        <!-- /.col -->
                    
					<div class="col-12 col-sm-12">
                      <div class="form-group">
                          <button type="submit" class="btn btn-primary">Submit</button>       
                      </div>
                      <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                  </div>
                  <!-- /.row -->
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  Klinik konsultasi 
                </div>
              </div>
              <!-- /.card -->
              </form>
              <!-- /.card-body -->
            </div>
        </div>
               
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  @endforeach
@endsection