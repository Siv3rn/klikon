@extends('layouts.lay')

@section('content')
<style>
  input::-webkit-outer-spin-button,
  input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }
  </style>
<section id="tanya" class="main-container">
  <div class="container">

    <div class="row">
    <div class="col-lg-2">
    </div>
      <div class="col-lg-8">
      <div class="row text-center">
        <div class="col-12">
          <br/><br/>
          <font size='6'><b>Form Konsultasi</b></font><br/><br>
          <font size='3'>Silakan mengisi form di bawah ini untuk mengirimkan pertanyaan kepada kami</b></font>
          <br/><br/>
        </div>
    </div>
      <div class="card">
              
              <!-- /.card-header -->
              @if(count($errors) > 0)
              <div class="alert alert-danger" role="alert">
                @foreach ($errors->all() as $error)
                {{ $error }} <br/>
                @endforeach
              </div>
              @endif
              <script>
                window.setTimeout(function() {
                  $.noConflict();
                  $(".alert").fadeTo(500, 0).slideUp(500, function(){
                    $(this).remove(); 
                  });
                }, 5000);
              </script>
              <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
              <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
    
                
              <form class="form" action="<?php echo url("/konsultasi/create"); ?>" method="POST" enctype="multipart/form-data">
                <!-- /.card-header -->
                {{csrf_field()}}
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Nama *</label>
                        <input type="text" class="form-control" id="nama_konsultasi" name="nama_konsultasi" placeholder="Nama" required>
                      </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>No Hp *</label>
                        <input type="number" class="form-control" id="hp_konsultasi" name="hp_konsultasi" placeholder="+62" required>
                      </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Email *</label>
                        <input type="email" class="form-control" id="email_konsultasi" name="email_konsultasi" placeholder="abc123@gmail.com" required>
                      </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Kategori *</label>
                        <select name='id_kat_konsul' class="form-control select2bs4" style="width: 100%;">
                          @foreach($id_kat_konsul as $ik)
                            <option value='{{$ik->id_kat_konsul}}' >{{$ik->nama_kat_konsul}}</option>
                          @endforeach
                        </select>
                        </div>
                      <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-12">
                        <div class="form-group">
                        <label>Judul *</label>
                          <input type="text" class="form-control" id="judul_konsultasi" name="judul_konsultasi" placeholder="Judul" required>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Pertanyaan *</label>
                            <textarea class="form-control" name='pertanyaan_konsultasi'></textarea>
                        </div>
                    </div>
                    <!-- /.col -->
					          <div align="right" class="col-12 col-sm-12">
                      <div class="form-group">
                          <button type="submit" class="btn btn-blue">Kirim</button>       
                      </div>
                      <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                  </div>
                  <!-- /.row -->
                </div>
                <!-- /.card-body -->
              <!-- /.card -->
              </form>
              </div>
              <!-- /.card-body -->
			  
			  
            </div>
			<div class="col-lg-2">
		</div>

    </div><!-- Content row end -->

  </div><!-- Container end -->
</section><!-- Main container end -->
<link rel="stylesheet" href="{{url('/constra/css/text1.css')}}">
@endsection