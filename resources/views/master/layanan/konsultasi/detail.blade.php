@extends('layouts.lay')

@section('content')
<div id="banner-area" class="banner-area" style="background-image:url({{url('/constra/images/banner/banner1.jpg')}})">
  <div class="banner-text">
    <div class="container">
        <div class="row">
          <div class="col-lg-12">
              <div class="banner-heading">
                <h1 class="banner-title">Konsultasi</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb justify-content-center">
                      <li class="breadcrumb-item"><a href="<?php echo url("/"); ?>">Beranda</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Konsultasi</li>
                      <li class="breadcrumb-item"><a href="<?php echo url("/konsultasi/tanya"); ?>">Kirim Pertanyaan ..?</a></li>
                    </ol>
                </nav>
              </div>
          </div><!-- Col end -->
        </div><!-- Row end -->
    </div><!-- Container end -->
  </div><!-- Banner text end -->
</div><!-- Banner area end --> 

<section id="main-container"  class="main-container">
  <div class="container">

    <div class="row">

   <div class="col-lg-9">

	@foreach($tbl_konsultasi as $kon)      
    <div class="post-content post-single">
          <div class="post-body">
            <div class="entry-header">
              <div class="post-meta">
                <span class="post-author">
                  <i class="far fa-user"></i><a href="#"> {{$kon->nama_konsultasi}}</a>
                </span>
                <span class="post-cat">
                  <i class="fa fa-tags"></i><a href="#"> {{$kon->kategori_konsultasi}}</a>
                </span>
                <span class="post-meta-date"><i class="far fa-calendar"></i> {{$kon->tgl_konsultasi}}</span>
              </div>
              <h2 class="entry-title">
              {{$kon->judul_konsultasi}}
              </h2>
            </div><!-- header end -->

            <div class="entry-content">
            {!!$kon->pertanyaan_konsultasi!!}
              <blockquote>Jawaban : 
              @if($kon->jawaban_konsultasi <> '')
              {!!$kon->jawaban_konsultasi!!}
              @else
              "Dalam Proses Review"
              @endif
              </blockquote>
            </div>

            <div class="tags-area d-flex align-items-center justify-content-between">
              
              <div class="share-items">
                <ul class="post-social-icons list-unstyled">
                  <li class="social-icons-head">Share:</li>
                  <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                  <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                  <li><a href="#"><i class="fab fa-google-plus"></i></a></li>
                  <li><a href="#"><i class="fab fa-linkedin"></i></a></li>
                </ul>
              </div>
            </div>

          </div><!-- post-body end -->
        </div><!-- post content end -->
        @endforeach   
        <br/><br/>
        <section class="call-to-action-box no-padding">
        <div class="container">
          <div class="action-style-box">
              <div class="row align-items-center">
                <div class="col-md-8 text-center text-md-left">
                    <div class="call-to-action-text">
                      <h3 class="action-title">Apa yang ingin anda konsultasikan ?</h3>
                    </div>
                </div><!-- Col end -->
                <div class="col-md-4 text-center text-md-right mt-3 mt-md-0">
                    <div class="call-to-action-btn">
                      <a class="btn btn-blue" href="<?php echo url('/konsultasi/tanya'); ?>">Kirim Pertanyaan</a>
                    </div>
                </div><!-- col end -->
              </div><!-- row end -->
          </div><!-- Action style box -->
        </div><!-- Container end -->
        </section>
	
    </div><!-- Col end -->
		<div class="col-lg-3 mt-5 mt-lg-0">
		<div class="sidebar sidebar-left">
        <div class="widget recent-posts">
            <h3 class="widget-title">Konsultasi Terkini</h3>
            <ul class="list-unstyled">
            @foreach($id_kat_konsul as $ksl)
              <li class="d-flex align-items-center">
                <div class="posts-thumb">
                  <a href="#"><img loading="lazy" alt="news-img" src="{{url('/constra/images/icon-image/ksl.png')}}"></a>
                </div>
                <div class="post-info">
                  <h4 class="entry-title">
                    <a href="<?php echo url("/konsultasi/detail/$ksl->id_konsultasi"); ?>">{{$ksl->judul_konsultasi}}</a>
                  </h4>
                </div>
              </li><!-- 2nd post end-->
              @endforeach
            </ul>
            <div class="col-md-4 text-center text-md-right mt-3 mt-md-0">
                <div class="call-to-action-btn">
                 <a class="btn btn-primary" href="<?php echo url("/konsultasi/all"); ?>">Lainnya..</a>
                </div>
            </div><!-- col end -->
        </div><!-- Recent post end -->
        </div><!-- Sidebar end -->

        <div class="sidebar sidebar-right">
        <div class="widget recent-posts">
          <hr/>
        </div><!-- Recent post end -->
        </div><!-- Sidebar end -->
		   
		</div>
    
        </div><!-- Content row end -->      
	</section><!-- Main container end -->

<!-- bootstrap5 dataTables js cdn -->
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    
    <script src="{{url('/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    
    <script>
    var jq = $.noConflict();
    jq(function () {
        jq("#next").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
        }).buttons().container().appendTo('#next_wrapper .col-md-6:eq(0)');
    });
    </script>

@endsection