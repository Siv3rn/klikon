@extends('layouts.lay')

@section('content')
<div id="banner-area" class="banner-area" style="background-image:url({{url('/constra/images/banner/banner1.jpg')}})">
  <div class="banner-text">
    <div class="container">
        <div class="row">
          <div class="col-lg-12">
              <div class="banner-heading">
                <h1 class="banner-title">Konsultasi</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb justify-content-center">
                      <li class="breadcrumb-item"><a href="<?php echo url("/"); ?>">Beranda</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Konsultasi</li>
                      <li class="breadcrumb-item"><a href="<?php echo url("/konsultasi/tanya"); ?>">Kirim Pertanyaan ..?</a></li>
                    </ol>
                </nav>
              </div>
          </div><!-- Col end -->
        </div><!-- Row end -->
    </div><!-- Container end -->
  </div><!-- Banner text end -->
</div><!-- Banner area end --> 

<section id="main-container"  class="facts-area">
  <div class="container">

    <div class="row">
    
    <div class="col-lg-4 mt-5 mt-lg-0">
    <br/>
        <div class="sidebar sidebar-right">
        <div class="widget recent-posts">
            <h3 class="widget-title">Konsultasi Terkini</h3>
            <ul class="list-unstyled">
            @foreach($konsul as $ksl)
              <li class="d-flex align-items-center">
                <div class="posts-thumb">
                  <a href="#"><img loading="lazy" alt="news-img" src="{{url('/constra/images/icon-image/ksl.png')}}"></a>
                </div>
                <div class="post-info">
                  <h4 class="entry-title">
                    <a href="<?php echo url("/konsultasi/detail/$ksl->id_konsultasi"); ?>">{{$ksl->judul_konsultasi}}</a>
                  </h4>
                </div>
              </li><!-- 2nd post end-->
              @endforeach
            </ul>
            
        </div><!-- Recent post end -->
        </div><!-- Sidebar end -->

        <div class="sidebar sidebar-right">
        <div class="widget recent-posts">
          <hr/>
        </div><!-- Recent post end -->
        </div><!-- Sidebar end -->

        <div class="sidebar sidebar-right">
        <div class="widget recent-posts">
            <h3 class="widget-title">Video</h3>
            <ul class="list-unstyled">
            @foreach($video as $vdo)
              <li class="d-flex align-items-center">
                <div class="post-info">
                  {!!$vdo->iframe_video!!}
                </div>
              </li><!-- 2nd post end-->
              @endforeach
            </ul>
        </div><!-- Recent post end -->
        </div><!-- Sidebar end -->

    </div><!-- Col end -->
		<div class="col-lg-8">
      <style>
        table#next.dataTable tr:hover {
          background-color: #c2e3dd;
        }
        .table-striped>tbody>tr:nth-child(odd)>td, 
        .table-striped>tbody>tr:nth-child(odd)>th {
            background-color: #d9e8e5; /* Choose your own color here */
        }
      </style>
        <div class="container mt-4">
        <b>Filter :</b> <input style="width:40%;height:35px;" type="text" id="myInput" onkeyup="myFunction()" placeholder="Filter By Keyword">
        <div id="test" style="float:right">
          <a href="<?php echo url("/konsultasi/tanya"); ?>" class="btn btn-blue">Kirim Pertanyaan</a>
        </div><br/><br/>
            <table id="next" class='table table-striped table-hover'>
                <tbody>
                    @foreach($tbl_konsultasi as $ksl)
                <tr>
                    <td>
                    <div class='post_commentbox'>
                        <a href='#'><i class='fa fa-user'></i> {{$ksl->nama_konsultasi}} | </a>
                        <span><i class='fa fa-calendar'></i> {{$ksl->tgl_konsultasi}} | </span>
                        <a href='#'><i class='fa fa-tags'></i> {{$ksl->kategori_konsultasi}} </a> 
                    </div>
                    <a href='<?php echo url("/konsultasi/detail/$ksl->id_konsultasi"); ?>'>
                        <h5>{!!$ksl->judul_konsultasi!!}</h5>
                    </a>
                    {!!substr($ksl->pertanyaan_konsultasi, 0, 150)!!}...<a href='<?php echo url("/konsultasi/detail/$ksl->id_konsultasi"); ?>'><b> >> Lihat Konsultasi </b></a><br/>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
            </div>
    <div class="d-flex justify-content-center">
        {!! $tbl_konsultasi->links() !!}
      </div>
		</div>
        </div><!-- Content row end -->      
</section><!-- Main container end -->

<!-- bootstrap5 dataTables js cdn -->
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    
    <script src="{{url('/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
    <script type="text/javascript">
        $('#next').DataTable({
          "responsive": true, "lengthChange": false, "autoWidth": false,"filtering":false,
            dom: 'lrtip',
            buttons: [
              {
                  text: '<b>&nbsp; &nbsp; &nbsp; Daftar</b>',
                  className: 'btn btn-blue btnAddJob',
                  titleAttr: 'Form Pertanyaan',
                  action: function ( e, dt, button, config ) {
                  window.location = '/konsultasi/tanya'
                  }
              }
            ]
        }).buttons().container().appendTo('#test');
        function myFunction() {
        var input, filter, table, tr, td, i, txtValue;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        table = document.getElementById("next");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
          td = tr[i].getElementsByTagName("td")[0];
          if (td) {
            txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
              tr[i].style.display = "";
            } else {
              tr[i].style.display = "none";
            }
          }       
        }
      }
    </script>

@endsection