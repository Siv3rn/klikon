@extends('layouts.lay')

@section('content')

<link href="{{url('v2/assets/css/pdk.css')}}" rel="stylesheet" />
<br/><br/>
<div><br/><br/></div>
        <div class="col-lg-12">
          <?php
            $map = DB::table('peta')
            ->select('*')
            ->where('id_jenis','=',3)
            ->limit(1)
            ->get();
            ?>
            @foreach ($map as $mp)
            <div class="sidebar sidebar-right">
            <h3 class="widget-title">{{$mp->judul_peta}}</h3>
            </div>
            <div class="card">
        <div class="row">
			
          <div class="col-lg-2">
		  &nbsp;
		  </div>
		  
          <div class="col-lg-6">
            {!!$mp->iframe_peta!!}  
		  </div>
		  
          <div class="col-lg-3">
		  &nbsp;
		  </div>
		 
        </div><!-- Row end -->
		  
            </div>
            @endforeach
        </div><!-- Col end -->
	<br/>
  <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
        <script>
            $(document).ready(function() {
            $('#kota').on('change', function() {
               var kotaID = $(this).val();
               if(kotaID) {
                   $.ajax({
                       url: '/kecamatan/'+kotaID,
                       type: "post",
                       data : {"_token":"{{ csrf_token() }}"},
                       dataType: "json",
                       success:function(data)
                       {
                         if(data){
                            $('#id_kecamatan').empty();
                            $('#id_kecamatan').append('<option value = "0" >--Pilih Kecamatan.--</option>'); 
                            $.each(data, function(key, id_kecamatan){
                                $('select[name="id_kecamatan"]').append('<option value="'+ id_kecamatan.id_kecamatan +'">' + id_kecamatan.nama_kecamatan+ '</option>');
                            });
                        }else{
                            $('#id_kecamatan').empty();
                        }
                     }
                   });
               }else{
                 $('#id_kecamatan').empty();
               }
            });
            });
        </script>
        

        <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
        <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.11.5/js/dataTables.bootstrap.min.js"></script>
        <script src="https://cdn.datatables.net/fixedcolumns/4.0.2/js/dataTables.fixedColumns.min.js"></script>
        
        <script src="{{url('/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
        <script>
        var jq = $.noConflict();
        jq(function () {
            jq("#next").DataTable({
            'scrollX':true,"lengthChange": false, bFilter: false,'pageLength': 10,
            'scrollCollapse': true, 'paging':true,"ordering": false,
            fixedColumns:   {
            left: 3
        },
            });
        });
        </script>
@endsection