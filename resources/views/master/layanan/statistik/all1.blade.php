@extends('layouts.lay')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
@section('content')
<br/>
<div><br/><br/></div>
        <div class="col-lg-12">
        
        </div><!-- Col end -->
    </div><!-- Row end -->
</div>

<section id="main-container"  class="facts-area">
		<div class="col-lg-12">
        <div class="sidebar sidebar-right">
					<h3 class="widget-title">Statistik Harga Bahan Bangunan</h3>
        </div>
        <div class='card'>
        <div class="col-lg-12">
            <br/>
            <style>
                table#next.dataTable tr:hover {
                background-color: #d0ecdc;
                }
                .table-striped>tbody>tr:nth-child(odd)>td, 
                .table-striped>tbody>tr:nth-child(odd)>th {
                background-color: #e8f7f0 ; /* Choose your own color here */
                }                   
                tr.even td
                {background-color:#fff}                        
                </style>
            
			<form method="post" action="<?php echo url("/harga/statistik"); ?>">          
          {{csrf_field()}}
            <div class="form-row">
              <div class="form-group col-2">
                <div class="custom-select-1">
                  <select class="form-control" name="jenis"  required>
                    <option value="">--Pilih Jenis Bahan Bangunan--</option>
                    <?php
                    $bhn = \DB::table('bahan_j')
                    ->select('*')
                    ->get();
                    ?>
                    @foreach($bhn as $bhan)
                    <option value="{{$bhan->id_jenis}}">{{$bhan->jenis}}</option>
                    @endforeach               
                  </select>
                </div>
              </div>
			  <div class="form-group col-2">
                <div class="custom-select-1">
                  <button class="btn btn-blue btn-rounded font-weight-bold btn-h-2 btn-v-3" type="submit">Cari</button>
                </div>
              </div>
            </div>
          </form>
            @foreach($bahan as $data2)
            <?php
            if ($data2->id_jenis=='6'){
            ?>	  
            <h2><center>Rerata Harga {{$data2->jenis}} (per kg) bulan {{$bulan3}} {{$tahun}} - bulan {{$bulan}} {{$tahun}}</center></h2>
            <?php
            } else {
            ?>
            <h2><center>Rerata Harga {{$data2->jenis}} bulan {{$bulan3}} {{$tahun}} - bulan {{$bulan}} {{$tahun}}</center></h2>
            <?php
            }
            ?>  
            @endforeach

            <div class="chart-container" style="position: relative; height:100%; width:100%">
    <canvas id="myChart"></canvas>
	</div>
	<br><br>
	<script>
		var ctx = document.getElementById("myChart").getContext('2d');
		var myChart = new Chart(ctx, {
			type: 'horizontalBar',
			data: {
				labels: ["Bantul {{$bulan3}}","Bantul {{$bulan2}}","Bantul {{$bulan}}", 
				"Gunung Kidul {{$bulan3}}","Gunung Kidul {{$bulan2}}","Gunung Kidul {{$bulan}}", 
				"Kota Yogyakarta {{$bulan3}}","Kota Yogyakarta {{$bulan2}}","Kota Yogyakarta {{$bulan}}",
				"Kulon Progo {{$bulan3}}","Kulon Progo {{$bulan2}}","Kulon Progo {{$bulan}}",
				"Sleman {{$bulan3}}","Sleman {{$bulan2}}","Sleman {{$bulan}}"],
				datasets: [{
					label: 'harga rata-rata {{$data2->jenis}}',
					data: [
                    <?php    
					foreach($bantul3 as $btl3){ 
                    echo $btl3->harga ;
                    }
                    ?>
                    ,
                    <?php    
					foreach($bantul2 as $btl2){ 
                    echo $btl2->harga ;
                    }
                    ?>
                    ,
                    <?php    
					foreach($bantul1 as $btl1){ 
                    echo $btl1->harga ;
                    }
                    ?>
                    ,
                    <?php    
					foreach($gk3 as $g3){ 
                    echo $g3->harga ;
                    }
                    ?>
                    ,
                    <?php    
					foreach($gk2 as $gk2){ 
                    echo $gk2->harga ;
                    }
                    ?>
                    ,
                    <?php    
					foreach($gk1 as $g1){ 
                    echo $g1->harga ;
                    }
                    ?>
                    ,
                    <?php    
					foreach($ky3 as $k3){ 
                    echo $k3->harga ;
                    }
                    ?>
                    ,
                    <?php    
					foreach($ky2 as $k2){ 
                    echo $k2->harga ;
                    }
                    ?>
                    ,
                    <?php    
					foreach($ky1 as $k1){ 
                    echo $k1->harga ;
                    }
                    ?>
                    ,
                    <?php    
					foreach($kp3 as $kpp3){ 
                    echo $kpp3->harga ;
                    }
                    ?>
                    ,
                    <?php    
					foreach($kp2 as $kpp2){ 
                    echo $kpp2->harga ;
                    }
                    ?>
                    ,
                    <?php    
					foreach($kp1 as $kpp1){ 
                    echo $kpp1->harga ;
                    }
                    ?>
                    ,
                    <?php    
					foreach($slm3 as $sl3){ 
                    echo $sl3->harga ;
                    }
                    ?>
                    ,
                    <?php    
					foreach($slm2 as $sl2){ 
                    echo $sl2->harga ;
                    }
                    ?>
                    ,
                    <?php    
					foreach($slm1 as $sl1){ 
                    echo $sl1->harga ;
                    }
                    ?>
                    ,
					],
					backgroundColor: [
					'rgba(255, 99, 132, 0.2)',
					'rgba(255, 99, 132, 0.2)',
					'rgba(255, 99, 132, 0.2)',
					'rgba(54, 162, 235, 0.2)',
					'rgba(54, 162, 235, 0.2)',
					'rgba(54, 162, 235, 0.2)',
					'rgba(255, 206, 86, 0.2)',
					'rgba(255, 206, 86, 0.2)',
					'rgba(255, 206, 86, 0.2)',
					'rgba(75, 192, 192, 0.2)',
					'rgba(75, 192, 192, 0.2)',
					'rgba(75, 192, 192, 0.2)',
					'rgba(153, 102, 255, 0.2)'
					],
					borderColor: [
					'rgba(255,99,132,1)',
					'rgba(255,99,132,1)',
					'rgba(255,99,132,1)',
					'rgba(54, 162, 235, 1)',
					'rgba(54, 162, 235, 1)',
					'rgba(54, 162, 235, 1)',
					'rgba(255, 206, 86, 1)',
					'rgba(255, 206, 86, 1)',
					'rgba(255, 206, 86, 1)',
					'rgba(75, 192, 192, 1)',
					'rgba(75, 192, 192, 1)',
					'rgba(75, 192, 192, 1)',
					'rgba(153, 102, 255, 1)',
					'rgba(153, 102, 255, 1)',
					'rgba(153, 102, 255, 1)'
					],
					borderWidth: 1
				}]
			},
			options: {
				scales: {
					yAxes: [{
						ticks: {
							beginAtZero:true
						}
					}]
				}
			}
		});
	</script>

		</div>     
		</div>    
		</div>    
</section><!-- Main container end -->

@endsection