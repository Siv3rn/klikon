@extends('layouts.lay')

@section('content')
<link rel="stylesheet" href="{{url('constra/css/menu.css')}}">
<div id="banner-area" class="banner-area" style="background-image:url({{url('/constra/images/banner/banner2.jpg')}})">
  <div class="banner-text">
    <div class="container">
        <div class="row">
          <div class="col-lg-12">
              <div class="banner-heading">
                <h1 class="banner-title">NPSM</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb justify-content-center">
                      <li class="breadcrumb-item"><a href="<?php echo url("/"); ?>">Beranda</a></li>
                      <li class="breadcrumb-item active" aria-current="page">NPSM</li>
                    </ol>
                </nav>
              </div>
          </div><!-- Col end -->
        </div><!-- Row end -->
    </div><!-- Container end -->
  </div><!-- Banner text end -->
</div><!-- Banner area end --> 

<section id="main-container"  class="facts-area">
  <div class="container">

    <div class="row">
    <!--
    <div class="col-lg-3 mt-5 mt-lg-0">
    <br/><br/><br/><br/><br/><br/><br/><br/><br/>
    <img  src="{{url('/constra/images/icon-image/gifnew.gif')}}" width="100%">
    -->           
        
        <?php /*
        <div class="sidebar sidebar-right">
        <div class="widget recent-posts">
            <h3 class="widget-title">Video</h3>
            <ul class="list-unstyled">
            @foreach($video as $vdo)
              <li class="d-flex align-items-center">
                <div class="post-info">
                  {!!$vdo->iframe_video!!}
                </div>
              </li><!-- 2nd post end-->
              @endforeach
            </ul>
        </div><!-- Recent post end -->
        </div><!-- Sidebar end -->
      */ ?>

    </div><!-- Col end -->
		<div class="col-lg-12">
        <div class="container mt-4">
          
        <div class="shuffle-btn-group">
          <label class="active" for="all">
            <input type="radio" name="shuffle-filter" id="all" value="all" checked="checked">NPSM Kriteria
          </label>
        </div><!-- project filter end -->
        <!-- Menu -->
        <div class='row'>
        @foreach($tbl_nspm_kriteria as $nspm)
        
        @if($nspm->id_nspm_kriteria == 1)
            <div class="col-4">
            <a class='item' href="<?php echo url("/nspm/detail/$nspm->id_nspm_kriteria"); ?>">
                <center>
                <img class='item' src="{{url('images/1.png')}}" width="85%" style="border-radius: 50%; border: 1px solid rgb(255, 123, 0);">
                <h5>UMUM</h5><br/>
                </center>
            </a>
            </div>
				@endif
        @if($nspm->id_nspm_kriteria == 2)
              <div class="col-4">
              <a class='item' href="<?php echo url("/nspm/detail/$nspm->id_nspm_kriteria"); ?>">
                <center>
                <img class='item' src="{{url('images/2.png')}}" width="80%" style="border-radius: 50%; border: 1px solid rgb(255, 123, 0);">
                <h5>CIPTA KARYA</h5><br/>
                </center>
               </a>
              </div>
				@endif
        @if($nspm->id_nspm_kriteria == 3)
              <div class="col-4">
              <a class='item' href="<?php echo url("/nspm/detail/$nspm->id_nspm_kriteria"); ?>">
                <center>
                <img class='item' src="{{url('images/3.png')}}" width="80%" style="border-radius: 50%; border: 1px solid rgb(255, 123, 0);">
                <h5>PERUMAHAN PERMUKIMAN</h5><br/>
                </center>
               </a>
              </div>
				@endif
        @if($nspm->id_nspm_kriteria == 4)
              <div class="col-4">
              <a class='item' href="<?php echo url("/nspm/detail/$nspm->id_nspm_kriteria"); ?>">
                <center>
                <img class='item' src="{{url('images/4.png')}}" width="82%" style="border-radius: 50%; border: 1px solid rgb(255, 123, 0);">
                <h5>BINA MARGA</h5><br/>
                </center>
              </a>
              </div>
				@endif
        @if($nspm->id_nspm_kriteria == 5)
              <div class="col-4">
                <a class='item' href="<?php echo url("/nspm/detail/$nspm->id_nspm_kriteria"); ?>">
                <center>
                <img class='item' src="{{url('images/5.png')}}" width="80%" style="border-radius: 50%; border: 1px solid rgb(255, 123, 0);">
                <h5>SUMBER DAYA AIR</h5><br/>
                </center>
                </a>
              </div>
				@endif
        @if($nspm->id_nspm_kriteria == 6)
              <div class="col-4">
               <a class='item' href="<?php echo url("/nspm/detail/$nspm->id_nspm_kriteria"); ?>">
                <center>
                <img class='item' src="{{url('images/6.png')}}" width="80%" style="border-radius: 50%; border: 1px solid rgb(255, 123, 0);">
                <h5>ESDM</h5><br/>
                </center>
                </a>
              </div>
				@endif
        @endforeach
        </div>
      </div>
		<?php /*
        <br/><br/>
		  <div class="shuffle-btn-group">
          <label class="active" for="all">
            <input type="radio" name="shuffle-filter" id="all" value="all" checked="checked">Poster
          </label>
        </div><!-- project filter end -->

            <div class="row shuffle-wrapper">
          <div class="col-1 shuffle-sizer"></div>
			@foreach($poster as $pst) 
          <div class="col-lg-3 col-md-6 shuffle-item" data-groups="[&quot;poster&quot;,&quot;poster&quot;]">
            <div class="project-img-container">
              <a class="gallery-popup" width='100%' href="{{asset('storage/images/poster/'.$pst->file_poster)}}">
                <img class="img-fluid" src="{{asset('storage/images/poster/'.$pst->file_poster)}}" alt="project-image">
                <span class="gallery-icon"><i class="fa fa-plus"></i></span>
              </a>
              <div class="project-item-info">
                <div class="project-item-info-content">
                  <h3 class="project-item-title">
                    <a href="#">{{$pst->judul_poster}}</a>
                  </h3>
                </div>
              </div>
            </div>
          </div><!-- shuffle item 1 end -->
			@endforeach
        </div><!-- shuffle end -->
		*/ ?>
	    </div>
    </div><!-- Content row end -->  
    </div><!-- Content row end -->      
</section><!-- Main container end -->

<!-- bootstrap5 dataTables js cdn -->
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    
    <script src="{{url('/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    
    <script>
    var jq = $.noConflict();
    jq(function () {
        jq("#next").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
        }).buttons().container().appendTo('#next_wrapper .col-md-6:eq(0)');
    });
    </script>

@endsection