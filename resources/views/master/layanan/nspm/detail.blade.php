@extends('layouts.lay')

@section('content')

<link rel="stylesheet" href="{{url('constra/css/menu.css')}}">
<div id="banner-area" class="banner-area" style="background-image:url({{url('/constra/images/banner/banner2.jpg')}})">
  <div class="banner-text">
    <div class="container">
        <div class="row">
          <div class="col-lg-12">
              <div class="banner-heading">
                <h1 class="banner-title">NSPM</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb justify-content-center">
                      <li class="breadcrumb-item"><a href="<?php echo url("/"); ?>">Beranda</a></li>
                      <li class="breadcrumb-item active" aria-current="page">NSPM Detail</li>
                    </ol>
                </nav>
              </div>
          </div><!-- Col end -->
        </div><!-- Row end -->
    </div><!-- Container end -->
  </div><!-- Banner text end -->
</div><!-- Banner area end --> 

<?php 
   $cut = Request::getRequestUri();
   $poin = preg_replace("/[^0-9]/", "",$cut);
?>
<section id="main-container"  class="facts-area">
  <div class="container">

    <div class="row">

    <div class="col-lg-12">
    <?php
    $kriteriaa = \DB::table('tbl_nspm_kriteria')
                      ->select('*')
                      ->where('id_nspm_kriteria','=',$poin)
                      ->get();
                      ?>
      @foreach($kriteriaa as $ktra)
      <center><font size='4'><b>Data NSPM Kriteria {{$ktra->nama_nspm_kriteria}}</b></font></center>
        @endforeach
        <br/>
        <div class="widget recent-posts">

            <div class="form-group">
              <div class="col-xs-2 selectContainer">
                  <select class="form-control" onchange="location = this.value;">
                      <option value='0' hidden>Pencarian By Kriteria </option>
                      <?php
                      $kriteria = \DB::table('tbl_nspm_kriteria1')
                      ->select('*')
                      ->where('id_nspm_kriteria1','=',$poin)
                      ->get();
                      ?>
                        @foreach($kriteria as $ktr)
                      <?php
                      $domain = \DB::table('tbl_nspm_kriteria1')
                        ->select('*')
                        ->where('id_nspm_kriteria','=',$ktr->id_nspm_kriteria)
                        ->get();
                       ?>
                        @foreach ($domain as $item)
                        <option value="<?php echo url("/nspm/detail/kriteria/"); ?>/{{$item->id_nspm_kriteria1}}">{{ $item->nama_nspm_kriteria1 }}</option>
                        @endforeach
                        @endforeach
                  </select>
              </div>
			</div>

        </div><!-- Recent post end -->

        <div>

		</div><!-- Col end -->
        
        <table id="next" class="table table-hover">
								<thead>
								<tr>
									<th>No</th>
									<th>Judul</th>
									<th>Uraian</th>
									<th>No_seri</th>
									<th>Tahun</th>
									<th>Kriteria</th>
									<th>Kategori</th>
								</tr>
								</thead>
								<tbody>
								<?php $no=0; ?> 
							@foreach($detail_nspm as $lk2)
							<?php $no++; ?>
								<tr>
									<td>{{$no}}</td>
									<td>{{$lk2->judul}}</td>
									<td>{!!$lk2->uraian!!}</td>
									<td>{{$lk2->no_seri}}</td>
									<td>{{$lk2->tahun}}</td>
									<td>{{$lk2->nama_kriteria1}}</td>
									<td>{{$lk2->kategori}}</td>
								</tr>
								@endforeach
								</tbody>
								</table>


	    </div>
    </div><!-- Content row end -->      
</section><!-- Main container end -->

<!-- bootstrap5 dataTables js cdn -->
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    
    <script src="{{url('/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    
    <script>
    var jq = $.noConflict();
    jq(function () {
        jq("#next").DataTable({
        "responsive": true, "lengthChange": true, "autoWidth": false,
        }).buttons().container().appendTo('#next_wrapper .col-md-6:eq(0)');
    });
    </script>

@endsection