@extends('layouts.lay2')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-12">
        @if(!empty(\Session::get('success')) > 0)
                    <div class="alert alert-success">     
                        <strong>Sukses!</strong> {!! \Session::get('success') !!}             
                        </div>
                    @endif
                <!-- /.card-header -->
              @if(count($errors) > 0)
              <div class="alert alert-danger" role="alert">
                @foreach ($errors->all() as $error)
                {{ $error }} <br/>
                @endforeach
              </div>
              @endif
              <script>
                window.setTimeout(function() {
                  $.noConflict();
                  $(".alert").fadeTo(500, 0).slideUp(500, function(){
                    $(this).remove(); 
                  });
                }, 5000);
              </script>
              <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
              <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
                </div>
                <div class="col-sm-6">
            <h1 class="m-0">NSPM</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/home">Dashboard</a></li>
              <li class="breadcrumb-item"><a href="/menu/nspm">Master</a></li>
              <li class="breadcrumb-item active">NSPM</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
                <!-- Small boxes (Stat box) -->
        <div class="row">
		
			<div class="col-4">
				<div class="card">
							
					<div class="card-body">
                        <center>
                            NSPM KATEGORI
                            <button type="button" class="btn btn-primary btn-sm confirmation" data-toggle="modal" data-target="#myModalkategori"><i class="fas fa-plus"></i></button>
                        </center> 
                        				
										<!-- Modal -->
                                                <div class="modal fade" id="myModalkategori" role="dialog">
												<div class="modal-dialog">
												
												<!-- Modal content-->
												<div class="modal-content">
                                                <div class="modal-header">
													<h4 class="modal-title">Tambah Data Kategori</h4>
													</div>
                                                    <div class="modal-body">
                                                        <!-- /.modal update file pendukung -->
                                                        <form class="form" action="<?php echo url("/menu/nspm_kategori/add_kategori"); ?>" method="POST">
                                                        {{csrf_field()}}
                                                                <div class="card-body">
                                                                
                                                                    <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label>Nama Kategori</label>
                                                                        <input type="text" class="form-control" id="nama_nspm_kategori" name="nama_nspm_kategori" required>
                                                                    </div>
                                                                    </div>
                                                                    <!-- /.col -->
                                                                    
                                                                    <div class="col-md-12">
                                                                    <div class="form-group">
                                                                    <button type="submit" class="btn btn-primary">Submit</button>     
                                                                    </div>
                                                                    </div>
                                                                    <!-- /.col -->
                                                                </div>
																</form>
                                                            </div>

															<div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													</div>
												</div>
												
												</div>
											</div>
                    <table id="next" class="table table-bordered table-hover">
								<thead>
								<tr>
									<th>No</th>
									<th>Nama Kategori</th>
									<th style="width:10%">Aksi</th>
								</tr>
								</thead>
								<tbody>
								<?php $no=0; ?> 
							@foreach($kategori as $lk)
							<?php $no++; ?>
								<tr>
									<td>{{$no}}</td>
									<td>{{$lk->nama_nspm_kategori}}</td>
									<td>
                                    <button type="button" class="btn btn-primary btn-sm confirmation" data-toggle="modal" data-target="#myModalkategoriupdate{{$lk->id_nspm_kategori}}"><i class="fas fa-edit"></i></button>
                      	
										<!-- Modal update -->
                                                <div class="modal fade" id="myModalkategoriupdate{{$lk->id_nspm_kategori}}" role="dialog">
												<div class="modal-dialog">
												
												<!-- Modal content-->
												<div class="modal-content">
                                                <div class="modal-header">
													<h4 class="modal-title">Update Data Kategori</h4>
													</div>
                                                    <div class="modal-body">
                                                        <!-- /.modal update file pendukung -->
                                                        <form class="form" action="<?php echo url('/menu/nspm_kategori/update/'.$lk->id_nspm_kategori); ?>" method="POST">
                                                        {{csrf_field()}}
                                                                <div class="card-body">
                                                                
                                                                    <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label>Nama Kategori</label>
                                                                        <input type="text" class="form-control" id="nama_nspm_kategori" name="nama_nspm_kategori" value='{{$lk->nama_nspm_kategori}}'>
                                                                    </div>
                                                                    </div>
                                                                    <!-- /.col -->
                                                                    
                                                                    <div class="col-md-12">
                                                                    <div class="form-group">
                                                                    <button type="submit" class="btn btn-primary">Update</button>     
                                                                    </div>
                                                                    </div>
                                                                    <!-- /.col -->
                                                                </div>
																</form>
                                                            </div>

															<div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													</div>
												</div>
												
												</div>
											</div>

                                            
										<button type="button" class="btn btn-danger btn-sm confirmation" data-toggle="modal" data-target="#myModal{{$lk->id_nspm_kategori}}"><i class="fas fa-trash"></i></button>
										<!-- Modal delete -->
                                                <div class="modal fade" id="myModal{{$lk->id_nspm_kategori}}" role="dialog">
												<div class="modal-dialog">
												
												<!-- Modal content-->
												<div class="modal-content">
													<div class="modal-header">
													<h4 class="modal-title">Apakah Anda Yakin menghapus data Kategori {{$lk->nama_nspm_kategori}}..?</h4>
													</div>
													<div class="modal-footer">
													<a href="<?php echo url('/menu/nspm_kategori/destroy_kategori/'.$lk->id_nspm_kategori); ?>" class="btn btn-danger btn-sm confirmation" >Hapus Data</a>
														
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													</div>
												</div>
												
												</div>
											</div>
									</td>
								</tr>
								@endforeach
								</tbody>
								</table>
					</div>
					<!-- /.card-body -->
				</div>
			</div> 
			
			<div class="col-4">
				<div class="card">
							
					<div class="card-body">
                    <center>NSPM kriteria1 
                            <button type="button" class="btn btn-primary btn-sm confirmation" data-toggle="modal" data-target="#myModalkriteria1"><i class="fas fa-plus"></i></button>
                        </center> 
                        				
										<!-- Modal -->
                                                <div class="modal fade" id="myModalkriteria1" role="dialog">
												<div class="modal-dialog">
												
												<!-- Modal content-->
												<div class="modal-content">
                                                <div class="modal-header">
													<h4 class="modal-title">Tambah Data Kriteria1</h4>
													</div>
                                                    <div class="modal-body">
                                                        <!-- /.modal update file pendukung -->
                                                        <form class="form" action="<?php echo url("/menu/nspm_kriteria1/add_kriteria1"); ?>" method="POST">
                                                        {{csrf_field()}}
                                                                <div class="card-body">
                                                                
                                                                    <div class="col-md-12">
                                                                        <!-- /.form-group -->
                                                                        <div class="form-group">
                                                                            <label>Kriteria *</label>
                                                                            <select name='id_nspm_kriteria' class="form-control select2bs4" style="width: 100%;">
                                                                            <?php
                                                                                $fe = DB::table('tbl_nspm_kriteria')
                                                                                ->select('*')
                                                                                ->get();
                                                                                ?>
                                                                                @foreach ($fe as $fee) 
                                                                                <option value='{{$fee->id_nspm_kriteria}}' >{{$fee->nama_nspm_kriteria}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                        <!-- /.form-group -->
                                                                    </div>

                                                                    <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label>Nama Kriteria</label>
                                                                        <input type="text" class="form-control" id="nama_nspm_kriteria1" name="nama_nspm_kriteria1" required>
                                                                    </div>
                                                                    </div>
                                                                    <!-- /.col -->
                                                                    
                                                                    <div class="col-md-12">
                                                                    <div class="form-group">
                                                                    <button type="submit" class="btn btn-primary">Submit</button>     
                                                                    </div>
                                                                    </div>
                                                                    <!-- /.col -->
                                                                </div>
																</form>
                                                            </div>

															<div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													</div>
												</div>
												
												</div>
											</div>
                    <table id="next2" class="table table-bordered table-hover">
								<thead>
								<tr>
									<th>No</th>
									<th>kriteria</th>
									<th>kriteria 1</th>
									<th style="width:10%">Aksi</th>
								</tr>
								</thead>
								<tbody>
								<?php $no=0; ?> 
							@foreach($kriteria1 as $lk)
							<?php $no++; ?>
								<tr>
									<td>{{$no}}</td>
									<td>{{$lk->nama_nspm_kriteria}}</td>
									<td>{{$lk->nama_nspm_kriteria1}}</td>
									<td>
									
                                    <button type="button" class="btn btn-primary btn-sm confirmation" data-toggle="modal" data-target="#myModalkriteria1update{{$lk->id_nspm_kriteria1}}"><i class="fas fa-edit"></i></button>
                      	
										<!-- Modal update -->
                                                <div class="modal fade" id="myModalkriteria1update{{$lk->id_nspm_kriteria1}}" role="dialog">
												<div class="modal-dialog">
												
												<!-- Modal content-->
												<div class="modal-content">
                                                <div class="modal-header">
													<h4 class="modal-title">Update Data Kriteria1</h4>
													</div>
                                                    <div class="modal-body">
                                                        <!-- /.modal update file pendukung -->
                                                        <form class="form" action="<?php echo url('/menu/nspm_kriteria1/update/'.$lk->id_nspm_kriteria1); ?>" method="POST">
                                                        {{csrf_field()}}
                                                                <div class="card-body">

                                                                <div class="col-md-12">
                                                                        <!-- /.form-group -->
                                                                        <div class="form-group">
                                                                            <label>Kriteria *</label>
                                                                            <select name='id_nspm_kriteria' class="form-control select2bs4" style="width: 100%;">
                                                                            <?php
                                                                                $fe = DB::table('tbl_nspm_kriteria')
                                                                                ->select('*')
                                                                                ->get();
                                                                                ?>
                                                                                @foreach ($fe as $fee) 
                                                                                <option value='{{$fee->id_nspm_kriteria}}' {{$fee->id_nspm_kriteria ==  $lk->id_nspm_kriteria  ? 'selected' : ''}} >{{$fee->nama_nspm_kriteria}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                        <!-- /.form-group -->
                                                                    </div>
                                                                
                                                                    <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label>Nama Kriteria 1</label>
                                                                        <input type="text" class="form-control" id="nama_nspm_kriteria1" name="nama_nspm_kriteria1" value='{{$lk->nama_nspm_kriteria1}}'>
                                                                    </div>
                                                                    </div>
                                                                    <!-- /.col -->
                                                                    
                                                                    <div class="col-md-12">
                                                                    <div class="form-group">
                                                                    <button type="submit" class="btn btn-primary">Update</button>     
                                                                    </div>
                                                                    </div>
                                                                    <!-- /.col -->
                                                                </div>
																</form>
                                                            </div>

															<div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													</div>
												</div>
												
												</div>
											</div>

										<button type="button" class="btn btn-danger btn-sm confirmation" data-toggle="modal" data-target="#myModal{{$lk->id_nspm_kriteria1}}"><i class="fas fa-trash"></i></button>
										<!-- Modal -->
                                                <div class="modal fade" id="myModal{{$lk->id_nspm_kriteria1}}" role="dialog">
												<div class="modal-dialog">
												
												<!-- Modal content-->
												<div class="modal-content">
													<div class="modal-header">
													<h4 class="modal-title">Apakah Anda Yakin menghapus data kriteria1 {{$lk->nama_nspm_kriteria1}}..?</h4>
													</div>
													<div class="modal-footer">
													<a href="<?php echo url('/menu/nspm_kriteria1/destroy_kriteria1/'.$lk->id_nspm_kriteria1); ?>" class="btn btn-danger btn-sm confirmation" >Hapus Data</a>
														
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													</div>
												</div>
												
												</div>
											</div>
									</td>
								</tr>
								@endforeach
								</tbody>
								</table>
					</div>
					<!-- /.card-body -->
				</div>
			</div> 
			
			<div class="col-4">
				<div class="card">
							
					<div class="card-body">
                    <center>NSPM KRITERIA 2 <button type="button" class="btn btn-primary btn-sm confirmation" data-toggle="modal" data-target="#myModalkriteria"><i class="fas fa-plus"></i></button>
                        </center> 
                        				
										<!-- Modal -->
                                                <div class="modal fade" id="myModalkriteria" role="dialog">
												<div class="modal-dialog">
												
												<!-- Modal content-->
												<div class="modal-content">
                                                <div class="modal-header">
													<h4 class="modal-title">Tambah Data Kriteria</h4>
													</div>
                                                    <div class="modal-body">
                                                        <!-- /.modal update file pendukung -->
                                                        <form class="form" action="<?php echo url("/menu/nspm_kriteria/add_kriteria"); ?>" method="POST">
                                                        {{csrf_field()}}
                                                                <div class="card-body">
                                                                
                                                                    <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label>Nama Kriteria</label>
                                                                        <input type="text" class="form-control" id="nama_nspm_kriteria" name="nama_nspm_kriteria" required>
                                                                    </div>
                                                                    </div>
                                                                    <!-- /.col -->
                                                                    
                                                                    <div class="col-md-12">
                                                                    <div class="form-group">
                                                                    <button type="submit" class="btn btn-primary">Submit</button>     
                                                                    </div>
                                                                    </div>
                                                                    <!-- /.col -->
                                                                </div>
																</form>
                                                            </div>

															<div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													</div>
												</div>
												
												</div>
											</div>
                    <table id="next3" class="table table-bordered table-hover">
								<thead>
								<tr>
									<th>No</th>
									<th>Nama Kriteria</th>
									<th style="width:10%">Aksi</th>
								</tr>
								</thead>
								<tbody>
								<?php $no=0; ?> 
							@foreach($kriteria as $lk)
							<?php $no++; ?>
								<tr>
									<td>{{$no}}</td>
									<td>{{$lk->nama_nspm_kriteria}}</td>
									<td>
									
                                    <button type="button" class="btn btn-primary btn-sm confirmation" data-toggle="modal" data-target="#myModalkriteriaupdate{{$lk->id_nspm_kriteria}}"><i class="fas fa-edit"></i></button>
                      	
										<!-- Modal update -->
                                                <div class="modal fade" id="myModalkriteriaupdate{{$lk->id_nspm_kriteria}}" role="dialog">
												<div class="modal-dialog">
												
												<!-- Modal content-->
												<div class="modal-content">
                                                <div class="modal-header">
													<h4 class="modal-title">Update Data Kriteria</h4>
													</div>
                                                    <div class="modal-body">
                                                        <!-- /.modal update file pendukung -->
                                                        <form class="form" action="<?php echo url('/menu/nspm_kriteria/update/'.$lk->id_nspm_kriteria); ?>" method="POST">
                                                        {{csrf_field()}}
                                                                <div class="card-body">

                                                                    <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label>Nama Kriteria</label>
                                                                        <input type="text" class="form-control" id="nama_nspm_kriteria" name="nama_nspm_kriteria" value='{{$lk->nama_nspm_kriteria}}'>
                                                                    </div>
                                                                    </div>
                                                                    <!-- /.col -->
                                                                    
                                                                    <div class="col-md-12">
                                                                    <div class="form-group">
                                                                    <button type="submit" class="btn btn-primary">Update</button>     
                                                                    </div>
                                                                    </div>
                                                                    <!-- /.col -->
                                                                </div>
																</form>
                                                            </div>

															<div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													</div>
												</div>
												
												</div>
											</div>

										<button type="button" class="btn btn-danger btn-sm confirmation" data-toggle="modal" data-target="#myModal{{$lk->id_nspm_kriteria}}"><i class="fas fa-trash"></i></button>
										<!-- Modal -->
                                                <div class="modal fade" id="myModal{{$lk->id_nspm_kriteria}}" role="dialog">
												<div class="modal-dialog">
												
												<!-- Modal content-->
												<div class="modal-content">
													<div class="modal-header">
													<h4 class="modal-title">Apakah Anda Yakin menghapus data Kriteria {{$lk->nama_nspm_kriteria}}..?</h4>
													</div>
													<div class="modal-footer">
													<a href="<?php echo url('/menu/nspm_kriteria/destroy_kriteria/'.$lk->id_nspm_kriteria); ?>" class="btn btn-danger btn-sm confirmation" >Hapus Data</a>
														
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													</div>
												</div>
												
												</div>
											</div>
									</td>
								</tr>
								@endforeach
								</tbody>
								</table>
					</div>
					<!-- /.card-body -->
				</div>
			</div> 
			
			<div class="col-12">
				<div class="card">
							
					<div class="card-body">
                    <center>Data NSPM
                         <button type="button" class="btn btn-primary btn-sm confirmation" data-toggle="modal" data-target="#myModalnspm"><i class="fas fa-plus"></i></button>
                        </center> 
                        				
										<!-- Modal -->
                                                <div class="modal fade" id="myModalnspm" role="dialog">
												<div class="modal-dialog">
												
												<!-- Modal content-->
												<div class="modal-content">
                                                <div class="modal-header">
													<h4 class="modal-title">Tambah Data NSPM</h4>
													</div>
                                                    <div class="modal-body">
                                                        <!-- /.modal update file pendukung -->
                                                        <form class="form" action="<?php echo url("/menu/nspm/add_nspm"); ?>" method="POST" enctype="multipart/form-data">
                                                        {{csrf_field()}}
                                                                <div class="card-body">
                                                                    <div class='row'>

                                                                    <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label>Judul NSPM</label>
                                                                        <input type="text" class="form-control" id="judul_data_nspm" name="judul_data_nspm" required>
                                                                    </div>
                                                                    </div>
                                                                    <!-- /.col -->

                                                                    <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label>No Seri NSPM</label>
                                                                        <input type="text" class="form-control" id="no_seri_nspm" name="no_seri_nspm" required>
                                                                    </div>
                                                                    </div>
                                                                    <!-- /.col -->

                                                                    <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label>File NSPM</label>
                                                                        <input type="file" class="form-control" id="file" name="file">
                                                                    </div>
                                                                    </div>
                                                                    <!-- /.col -->

                                                                    <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label>Tahun NSPM</label>
                                                                        <input type="number" class="form-control" id="tahun_data_nspm" name="tahun_data_nspm" required>
                                                                    </div>
                                                                    </div>
                                                                    <!-- /.col -->

                                                                    <div class="col-md-6">
                                                                        <!-- /.form-group -->
                                                                        <div class="form-group">
                                                                            <label>Kategori *</label>
                                                                            <select name='id_nspm_kategori' class="form-control select2bs4" style="width: 100%;" required>
                                                                            <?php
                                                                                $fae = DB::table('tbl_nspm_kategori')
                                                                                ->select('*')
                                                                                ->get();
                                                                                ?>
                                                                                @foreach ($fae as $faee) 
                                                                                <option value='{{$faee->id_nspm_kategori}}' >{{$faee->nama_nspm_kategori}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                        <!-- /.form-group -->
                                                                    </div>

                                                                    <div class="col-md-6">
                                                                        <!-- /.form-group -->
                                                                        <div class="form-group">
                                                                            <label>Kriteria 1 *</label>
                                                                            <select name='id_nspm_kriteria1' class="form-control select2bs4" style="width: 100%;" required>
                                                                            <?php
                                                                                $fea = DB::table('tbl_nspm_kriteria1')
                                                                                ->select('*')
                                                                                ->get();
                                                                                ?>
                                                                                @foreach ($fea as $feea) 
                                                                                <option value='{{$feea->id_nspm_kriteria1}}' >{{$feea->nama_nspm_kriteria1}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                        <!-- /.form-group -->
                                                                    </div>

                                                                    <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label>Uraian NSPM</label>
                                                                        <textarea class="form-control" id="uraian_data_nspm" name="uraian_data_nspm" rows="4" cols="10"></textarea>
                                                                    </div>
                                                                    </div>
                                                                    <!-- /.col -->

                                                                    </div>
                                                                    
                                                                    
                                                                    <div class="col-md-12">
                                                                    <div class="form-group">
                                                                    <button type="submit" class="btn btn-primary">Submit</button>     
                                                                    </div>
                                                                    </div>
                                                                    <!-- /.col -->
                                                                </div>
																</form>
                                                            </div>

															<div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													</div>
												</div>
												
												</div>
											</div>
                    <table id="next4" class="table table-bordered table-hover">
								<thead>
								<tr>
									<th>No</th>
									<th>Judul</th>
									<th>No Seri</th>
									<th>Thn</th>
									<th>Uraian</th>
									<th>Kategori</th>
									<th>Kriteria1</th>
									<th>Kriteria2</th>
									<th style="width:5%">Aksi</th>
								</tr>
								</thead>
								<tbody>
								<?php $no=0; ?> 
							@foreach($nspm as $lk)
							<?php $no++; ?>
								<tr>
									<td>{{$no}}</td>
									<td>{{$lk->judul_data_nspm}}</td>
									<td>
                                    @if(empty($lk->file_data_nspm))
                                    {{$lk->no_seri_nspm}} | Tidak Ada File
                                    @else
                                    <a href="{{asset('storage/images/nspm/'.$lk->file_data_nspm)}}" alt="{{$lk->no_seri_nspm}}"  target='_blank'>
                                    {{$lk->no_seri_nspm}}
                                    </a>
                                    @endif
                                    </td>
									<td>{{$lk->tahun_data_nspm}}</td>
									<td>{!!$lk->uraian_data_nspm!!}</td>
									<td>{{$lk->nama_nspm_kategori}}</td>
									<td>{{$lk->nama_nspm_kriteria}}</td>
									<td>{{$lk->nama_nspm_kriteria1}}</td>
									<td>
									
                                    <button type="button" class="btn btn-primary btn-sm confirmation" data-toggle="modal" data-target="#myModalnspm{{$lk->id_data_nspm}}"><i class="fas fa-edit"></i></button>
                        				
										<!-- Modal -->
                                                <div class="modal fade" id="myModalnspm{{$lk->id_data_nspm}}" role="dialog">
												<div class="modal-dialog">
												
												<!-- Modal content-->
												<div class="modal-content">
                                                <div class="modal-header">
													<h4 class="modal-title">Update Data NSPM {{$lk->judul_data_nspm}}</h4>
													</div>
                                                    <div class="modal-body">
                                                        <!-- /.modal update file pendukung -->
                                                        <form class="form" action="<?php echo url('/menu/nspm/update_nspm/'.$lk->id_data_nspm); ?>" method="POST" enctype="multipart/form-data">
                                                        {{csrf_field()}}
                                                                <div class="card-body">
                                                                    <div class='row'>

                                                                    <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label>Judul NSPM</label>
                                                                        <input type="text" class="form-control" id="judul_data_nspm" name="judul_data_nspm" value='{{$lk->judul_data_nspm}}'>
                                                                    </div>
                                                                    </div>
                                                                    <!-- /.col -->

                                                                    <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label>No Seri NSPM</label>
                                                                        <input type="text" class="form-control" id="no_seri_nspm" name="no_seri_nspm" value='{{$lk->no_seri_nspm}}'>
                                                                    </div>
                                                                    </div>
                                                                    <!-- /.col -->

                                                                    <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label>File NSPM</label>
                                                                        <input type="file" class="form-control" id="file" name="file">
                                                                    </div>
                                                                    </div>
                                                                    <!-- /.col -->

                                                                    <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label>Tahun NSPM</label>
                                                                        <input type="number" class="form-control" id="tahun_data_nspm" name="tahun_data_nspm" value='{{$lk->tahun_data_nspm}}'>
                                                                    </div>
                                                                    </div>
                                                                    <!-- /.col -->

                                                                    <div class="col-md-6">
                                                                        <!-- /.form-group -->
                                                                        <div class="form-group">
                                                                            <label>Kategori *</label>
                                                                            <select name='id_nspm_kategori' class="form-control select2bs4" style="width: 100%;" required>
                                                                            <?php
                                                                                $fae = DB::table('tbl_nspm_kategori')
                                                                                ->select('*')
                                                                                ->get();
                                                                                ?>
                                                                                @foreach ($fae as $faee) 
                                                                                <option value='{{$faee->id_nspm_kategori}}' {{$faee->id_nspm_kategori ==  $lk->id_nspm_kategori  ? 'selected' : ''}}>{{$faee->nama_nspm_kategori}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                        <!-- /.form-group -->
                                                                    </div>

                                                                    <div class="col-md-6">
                                                                        <!-- /.form-group -->
                                                                        <div class="form-group">
                                                                            <label>Kriteria 1 *</label>
                                                                            <select name='id_nspm_kriteria1' class="form-control select2bs4" style="width: 100%;" required>
                                                                            <?php
                                                                                $fea = DB::table('tbl_nspm_kriteria1')
                                                                                ->select('*')
                                                                                ->get();
                                                                                ?>
                                                                                @foreach ($fea as $feea) 
                                                                                <option value='{{$feea->id_nspm_kriteria1}}' {{$feea->id_nspm_kriteria1 ==  $lk->id_nspm_kriteria1  ? 'selected' : ''}} >{{$feea->nama_nspm_kriteria1}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                        <!-- /.form-group -->
                                                                    </div>

                                                                    <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label>Uraian NSPM</label>
                                                                        <textarea class="form-control" id="uraian_data_nspm" name="uraian_data_nspm" rows="4" cols="10">{{$lk->uraian_data_nspm}}</textarea>
                                                                    </div>
                                                                    </div>
                                                                    <!-- /.col -->

                                                                    </div>
                                                                    
                                                                    
                                                                    <div class="col-md-12">
                                                                    <div class="form-group">
                                                                    <button type="submit" class="btn btn-primary">Submit</button>     
                                                                    </div>
                                                                    </div>
                                                                    <!-- /.col -->
                                                                </div>
																</form>
                                                            </div>

															<div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													</div>
												</div>
												
												</div>
											</div>


										<button type="button" class="btn btn-danger btn-sm confirmation" data-toggle="modal" data-target="#myModal{{$lk->id_data_nspm}}"><i class="fas fa-trash"></i></button>
										<!-- Modal -->
                                                <div class="modal fade" id="myModal{{$lk->id_data_nspm}}" role="dialog">
												<div class="modal-dialog">
												
												<!-- Modal content-->
												<div class="modal-content">
													<div class="modal-header">
													<h4 class="modal-title">Apakah Anda Yakin menghapus data Link {{$lk->judul_data_nspm}}..?</h4>
													</div>
													<div class="modal-footer">
													<a href="<?php echo url('/menu/nspm/destroy_nspm/'.$lk->id_data_nspm); ?>" class="btn btn-danger btn-sm confirmation" >Hapus Data</a>
														
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													</div>
												</div>
												
												</div>
											</div>
									</td>
								</tr>
								@endforeach
								</tbody>
								</table>
					</div>
					<!-- /.card-body -->
				</div>
			</div> 
			
		</div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <script>
  $(function () {
    $("#next").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": true, "searching":false,"pageLength": 4
    }).buttons().container().appendTo('#next_wrapper .col-md-6:eq(0)');
  });
  $(function () {
    $("#next2").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": true, "searching":false,"pageLength": 4
    }).buttons().container().appendTo('#next2_wrapper .col-md-6:eq(0)');
  });
  $(function () {
    $("#next3").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": true, "searching":false,"pageLength": 4
    }).buttons().container().appendTo('#next3_wrapper .col-md-6:eq(0)');
  });
  $(function () {
    $("#next4").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": true, "searching":true,"pageLength": 5
    }).buttons().container().appendTo('#next4_wrapper .col-md-6:eq(0)');
  });
</script>
@endsection