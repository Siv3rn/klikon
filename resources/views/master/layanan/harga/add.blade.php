@extends('layouts.lay2')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Harga</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/home">Dashboard</a></li>
              <li class="breadcrumb-item"><a href="/layanan/harga">Layanan</a></li>
              <li class="breadcrumb-item active">Harga Add</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
                <!-- Small boxes (Stat box) -->
        <div class="row">
          
        <div class="col-12">
            <div class="card">
              <div class="card-header">
                        <h3 class="card-title">Harga | Add</h3>
              </div>
              
              <!-- /.card-header -->
              @if(count($errors) > 0)
              <div class="alert alert-danger" role="alert">
                @foreach ($errors->all() as $error)
                {{ $error }} <br/>
                @endforeach
              </div>
              @endif
              <script>
                window.setTimeout(function() {
                  $.noConflict();
                  $(".alert").fadeTo(500, 0).slideUp(500, function(){
                    $(this).remove(); 
                  });
                }, 5000);
              </script>
              <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
              <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
    
                
              <form class="form" action="<?php echo url("/layanan/harga/create"); ?>" method="POST" enctype="multipart/form-data">
                <!-- /.card-header -->
                {{csrf_field()}}
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-4">
                    <div class="form-group">
                        <label>Jenis *</label>
                           <select name='id_jenis' class="form-control select2bs4" style="width: 100%;">
                           <?php
                           $jenis = DB::table('bahan_j')
                           ->select('*')
                           ->get();
                           ?>
                           @foreach ($jenis as $jns) 
                           <option value='{{$jns->id_jenis}}' >{{$jns->jenis}}</option>
                           @endforeach
                           </select>
                    </div>
                    <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Nama Barang</label>
                        <input type="text" class="form-control" id="nama" name="nama" required>
                        <input type="hidden" class="form-control" id="stock" name="stock" value='ada'>
                       </div>
                      <!-- /.form-group -->
                    </div>
                    <!-- /.col -->

                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Agen</label>
                        <input type="text" class="form-control" id="agen" name="agen" required>
                       </div>
                      <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                    
                    <div class="col-md-4">
                        <!-- /.form-group -->
                        <div class="form-group">
                        <label>Ukuran</label>
                        <input type="text" class="form-control" id="ukuran" name="ukuran" required>
                       </div>
                      <!-- /.form-group -->
                    </div>
                    <div class="col-md-4">
                    <div class="form-group">
                        <label>Harga</label>
                        <input type="text" class="form-control" id="harga" name="harga" required onkeypress="return hanyaAngka(event)">
                        <script>
                            function hanyaAngka(evt) {
                                var charCode = (evt.which) ? evt.which : event.keyCode
                                if (charCode > 31 && (charCode < 48 || charCode > 57))

                                return false;
                                return true;
                            }
                        </script>   
                    </div>
                      <!-- /.form-group -->
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Kecamatan *</label>
                           <select name='id_kecamatan' class="form-control select2bs4" style="width: 100%;">
                           <?php
                           $kcmt = DB::table('tbl_kecamatan')
                           ->select('*')
                           ->get();
                           ?>
                           @foreach ($kcmt as $kc) 
                           <option value='{{$kc->id_kecamatan}}' >{{$kc->nama_kecamatan}}</option>
                           @endforeach
                           </select>
                    </div>
                    </div>
                    <!-- /.col -->
					          <div class="col-12 col-sm-12">
                      <div class="form-group"><center>
                          <button type="submit" class="btn btn-primary">Submit</button>       
                      </center></div>
                      <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                  </div>
                  <!-- /.row -->
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  Klinik Konstruksi 
                </div>
              </div>
              <!-- /.card -->
              </form>
              <!-- /.card-body -->
            </div>
        </div>
               
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection