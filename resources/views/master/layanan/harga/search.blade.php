@extends('layouts.lay')

@section('content')

<div id="banner-area" class="banner-area" style="background-image:url({{url('/constra/images/banner/banner3.jpg')}})">
  <div class="banner-text">
    <div class="container">
        <div class="row">
          <div class="col-lg-12">
              <div class="banner-heading">
                <h1 class="banner-title">Harga Bahan Bangunan</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb justify-content-center">
                      <li class="breadcrumb-item"><a href="<?php echo url("/"); ?>">Beranda</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Harga Bahan Bangunan</li>
                    </ol>
                </nav>
              </div>
          </div><!-- Col end -->
        </div><!-- Row end -->
    </div><!-- Container end -->
  </div><!-- Banner text end -->
</div><!-- Banner area end --> 

<section id="main-container"  class="main-container">
  <div class="container">

    <div class="row">

    <div class="col-lg-3 mt-5 mt-lg-0">

        <div class="sidebar sidebar-right">
        <div class="widget recent-posts">
            <h3 class="widget-title">Percarian Berdasarkan :</h3>
            <form class="form" action="<?php echo url("/hbb/search"); ?>" method="post">
                <!-- /.card-header -->
                {{csrf_field()}}
            <div class="form-group">
              <label>Nama Barang</label>
              <input type="text" class="form-control" id="nama" name="nama">
            </div>

            <div class="form-group">
              <label>Jenis *</label>
                <select name='id_jenis' class="form-control select2bs4" style="width: 100%;">
                  <option value="0" selected>--Pilih Jenis.--</option>
                  @foreach ($jenis as $jns) 
                  <option value='{{$jns->id_jenis}}' >{{$jns->jenis}}</option>
                  @endforeach
                </select>
            </div>

            <div class="form-group">
              <label class="col-xs-1 control-label">Kabupaten/Kota</label>
              <div class="col-xs-2 selectContainer">
                  <select class="form-control" name="id_kota" id="kota">
                      <option value='0' hidden>Kabupaten/Kota</option>
                        @foreach ($kota as $item)
                        <option value="{{ $item->id_kota }}">{{ $item->kota }}</option>
                        @endforeach
                  </select>
              </div>
						</div>
            
            <div class="form-group">
              <label for="id_kecamatan" class="form-label">kecamatan</label>
              <select class="form-control" name="id_kecamatan" id="id_kecamatan"></select>
						</div>
            
            <div class="form-group">
              <label class="col-xs-1 control-label">Urutkan</label>
              <div class="col-xs-2 selectContainer">
                <select name="urut" class="form-control">
                  <option value=''>--Pilih Urutan--</option>
                    echo "<option value='ASC'>Harga Terendah</option>
                    echo "<option value='DESC'>Harga Tertinggi</option>
                </select>
						  </div>
						</div>
            <div class="form-group">
              <button type="submit" class="btn btn-primary">Cari</button>  
            </div>
            </form>

        </div><!-- Recent post end -->
        </div><!-- Sidebar end -->

        <div class="sidebar sidebar-right">
        <div class="widget recent-posts">
          <hr/>
        </div><!-- Recent post end -->
        </div><!-- Sidebar end -->

        <div class="sidebar sidebar-right">
        <div class="widget recent-posts">
            <h3 class="widget-title">Konsultasi Terkini</h3>
            <ul class="list-unstyled">
            @foreach($konsul as $ksl)
              <li class="d-flex align-items-center">
                <div class="posts-thumb">
                  <a href="#"><img loading="lazy" alt="news-img" src="{{url('/constra/images/icon-image/ksl.png')}}"></a>
                </div>
                <div class="post-info">
                  <h4 class="entry-title">
                    <a href="<?php echo url("/konsultasi/detail/$ksl->id_konsultasi"); ?>">{{$ksl->judul_konsultasi}}</a>
                  </h4>
                </div>
              </li><!-- 2nd post end-->
              @endforeach
            </ul>
            <div class="col-md-4 text-center text-md-right mt-3 mt-md-0">
                <div class="call-to-action-btn">
                 <a class="btn btn-primary" href="<?php echo url("/konsultasi/tanya"); ?>">Kirim Pertanyaan..?</a>
                </div>
            </div><!-- col end -->
        </div><!-- Recent post end -->
        </div><!-- Sidebar end -->

    </div><!-- Col end -->
		<div class="col-lg-9">
      <div class="row">    
      <?php 
        $hit3 = count($harga); 
      ?>
      @if($hit3 <= 0)
      <div class="call-to-action-btn">
      <center>
       <h3 class="btn btn-primary">Data yang Anda Cari Tidak ada! </h3> 
      </center>
      </div>
      @else
      @foreach($harga as $hrg) 
        <div class="col-md-4">
                <div class='div1'>
                <div class="card mt-3 item">
                  <span class="text">
                  {{substr($hrg->barang, 0, 30)}}
                  @if(strlen($hrg->barang) > 30)
                  ....
                  @endif
                  </span>
                  <span class="text-dark">{{$hrg->kota}} , {{$hrg->nama_kecamatan}}
                  </span><br/>
                  <span class="text-dark">
                   {{date('M Y', strtotime($hrg->last_update))}} 
                </span><br/>
                <span class="h6 font-weight-bold mb-0">
                   Rp. <?php echo $harga_format=number_format($hrg->harga,0,".","."); ?>
                </span><br/>
                 
                </div>
                </div>
        </div>
        @endforeach
      @endif
    </div>
    <br/>
      <div class="d-flex justify-content-center">
        {!! $harga->links() !!}
      </div>				
		</div>
        </div><!-- Content row end -->      
  </section><!-- Main container end -->
  <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
        <script>
            $(document).ready(function() {
            $('#kota').on('change', function() {
               var kotaID = $(this).val();
               if(kotaID) {
                   $.ajax({
                       url: '/kecamatan/'+kotaID,
                       type: "post",
                       data : {"_token":"{{ csrf_token() }}"},
                       dataType: "json",
                       success:function(data)
                       {
                         if(data){
                            $('#id_kecamatan').empty();
                            $('#id_kecamatan').append('<option hidden>Pilih Kecamatan</option>'); 
                            $.each(data, function(key, id_kecamatan){
                                $('select[name="id_kecamatan"]').append('<option value="'+ key +'">' + id_kecamatan.nama_kecamatan+ '</option>');
                            });
                        }else{
                            $('#id_kecamatan').empty();
                        }
                     }
                   });
               }else{
                 $('#id_kecamatan').empty();
               }
            });
            });
        </script>
@endsection