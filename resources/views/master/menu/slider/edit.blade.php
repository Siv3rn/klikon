@extends('layouts.lay2')

@section('content')
@foreach($slider as $g)
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Menu Management</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/home">Dashboard</a></li>
              <li class="breadcrumb-item"><a href="/menu">Menu</a></li>
              <li class="breadcrumb-item active">Slider Update</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
                <!-- Small boxes (Stat box) -->
        <div class="row">
          
        <div class="col-12">
            <div class="card">
              <div class="card-header">
                        <h3 class="card-title">Slider | Add</h3>
              </div>
              
              <!-- /.card-header -->
              @if(count($errors) > 0)
              <div class="alert alert-danger" role="alert">
                @foreach ($errors->all() as $error)
                {{ $error }} <br/>
                @endforeach
              </div>
              @endif
              <script>
                window.setTimeout(function() {
                  $.noConflict();
                  $(".alert").fadeTo(500, 0).slideUp(500, function(){
                    $(this).remove(); 
                  });
                }, 5000);
              </script>
              <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
              <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
    
                
              <form class="form" action="<?php echo url("/menu/update/$g->id_slider"); ?>" method="POST" enctype="multipart/form-data">
                <!-- /.card-header -->
                {{csrf_field()}}
                <div class="card-body">
                    <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                        <label>Judul *</label>
                        <input type="text" class="form-control" id="judul_slider" name="judul_slider" value="{{$g->judul_slider}}" required>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group">
                        <label>Status</label>
                        <select name='status_slider' class="form-control select2bs4" style="width: 100%;">
                            <option value='aktif' {{'aktif' ==  $g->status_slider  ? 'selected' : ''}}>Aktif</option>
                            <option value='pasif' {{'pasif' ==  $g->status_slider  ? 'selected' : ''}}>Non Aktif</option>
                        </select>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-6">
                        <div class="form-group">
                        <label>Link</label>
                        <input type="text" class="form-control" id="url_slider" name="url_slider" value='{{$g->url_slider}}'>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group">
                        <label>Upload Gambar *</label>
                        <input type="file" class="form-control" id="file" name="file" placeholder="upload Banner" >                  
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                    </div>
                    <!-- /.row -->

                    <div class="row">
                    <div class="col-12 col-sm-12">
                        <div class="form-group">
                        <label>Deskripsi *</label>
                        <textarea class="form-control" name='keterangan_slider' rows="3" required>{{$g->keterangan_slider}}</textarea>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                    </div>
                    <!-- /.row -->

                    <div class="row">
                    <div class="col-12 col-sm-12">
                        <div class="form-group"><center>
                            <button type="submit" class="btn btn-primary">Update</button>       
                        </center></div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                    </div>
                    <!-- /.row -->

                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    Klinik Konstruksi 
                </div>
                </div>
                <!-- /.card -->
                </form>         
              <!-- /.card-body -->
            </div>
        </div>
               
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  @endforeach
@endsection