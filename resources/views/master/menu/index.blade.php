@extends('layouts.lay2')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-12">
        @if(!empty(\Session::get('success')) > 0)
                    <div class="alert alert-success">     
                        <strong>Sukses!</strong> {!! \Session::get('success') !!}             
                        </div>
                    @endif
                <script>
                    window.setTimeout(function() {
                    $.noConflict();
                    $(".alert").fadeTo(500, 0).slideUp(500, function(){
                        $(this).remove(); 
                    });
                    }, 5000);
                </script>
                <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
                <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
                </div>
                <div class="col-sm-6">
            <h1 class="m-0">Menu Management</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/home">Dashboard</a></li>
              <li class="breadcrumb-item"><a href="/menu">Menu</a></li>
              <li class="breadcrumb-item active">Menu</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
                <!-- Small boxes (Stat box) -->
        <div class="row">
		<div class="col-md-12">
				 <div class="card">
					<div class="card-header">
								<h3 class="card-title">PETA | &nbsp; 
                                <button type="button" class="btn btn-success btn-sm confirmation" data-toggle="modal" data-target="#myModaladd"><i class="fas fa-plus"></i> Add </button>
                            <!-- Modal -->
                                <div class="modal fade" id="myModaladd" role="dialog">
                                    <div class="modal-dialog">
                                    
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                        <h4 class="modal-title">New Peta By Bahan Jenis</h4>
                                        </div>
                                        <div class="modal-body">
                                        <form class="form" action="<?php echo url("/menu/kontak/create_peta"); ?>" method="POST" enctype="multipart/form-data">
                                    <!-- /.card-header -->
                                    {{csrf_field()}}
                                    <div class="card-body">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                            <label>Judul *</label>
                                            <input type="text" class="form-control" id="judul_peta" name="judul_peta" required>
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
												<div class="form-group">
												<label>Jenis Peta</label>
													<select name='id_jenis' class="form-control select2bs4" style="width: 100%;" required>
                                                      <option value='1'>Peta Sebaran Harga</option>
													  <option value='2'>Peta Sebaran Rantai Pasok</option>
													  <option value='3'>Grafik Sebaran Harga Material/Statistik</option>
													</select>
												</div>
                                            </div>

                                            <div class="form-group">
												<div class="form-group">
												<label>Status</label>
													<select name='status_peta' class="form-control select2bs4" style="width: 100%;">
													  <option value='aktif' selected="selected">Aktif</option>
													  <option value='pasif'>Non Aktif</option>
													</select>
												</div>
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <!-- /.col -->
                                        <div class="col-md-12">
                                            <div class="form-group">
												<label>Iframe *</label>
												<textarea class='form-control' name='iframe_peta'></textarea>
											 </div>
                                        </div>
                                        <!-- /.col -->
                                        </div>
                                        <!-- /.row -->

                                        <div class="row">
                                        <div class="col-12 col-sm-12">
                                            <div class="form-group"><center>
                                                <button type="submit" class="btn btn-primary">Save</button>       
                                            </center></div>
                                            <!-- /.form-group -->
                                        </div>
                                        <!-- /.col -->
                                        </div>
                                        <!-- /.row -->

                                    </div>
                                    <!-- /.card-body -->
                                    </form>  
                                    <button type="button" class="btn btn-default" data-dismiss="modal"><b>X Close</b></button>
                                        
                                        </div>
                                    </div>
                                    
                                    </div>
                                </div>
                                </h3>
                    
					</div>
                        <div class="card-body">
                            <!-- /.card-header -->
                            <table id="next" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Peta</th>
                        <th>Jenis</th>
                        <th>Status</th>
                        <th style="width:10%">Aksi</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $no=0; ?> 
                @foreach($peta as $lka)
                <?php $no++; ?>
                    <tr>
                        <td>{{$no}}</td>
                        <td>{{$lka->judul_peta}}</td>
                        <td>
                            @if($lka->id_jenis == 1)
                            Peta Sebaran Harga
                            @elseif($lka->id_jenis == 2)
                            Peta Sebaran Rantai Pasok
                            @else
                            Grafik Sebaran Harga Material
                            @endif
                        </td>
                        <td>{{$lka->status_peta}}</td>
                        <td>
                        <button type="button" class="btn btn-info btn-sm confirmation" data-toggle="modal" data-target="#myModalupdate{{$lka->id}}"><i class="fas fa-edit"></i></button>
                            <!-- Modal -->
                                <div class="modal fade" id="myModalupdate{{$lka->id}}" role="dialog">
                                    <div class="modal-dialog">
                                    
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                        <h4 class="modal-title">Edit Data Peta : {{$lka->judul_peta}}..?</h4>
                                        </div>
                                        <div class="modal-body">
                                        <form class="form" action="<?php echo url("/menu/kontak/update_peta/$lka->id"); ?>" method="POST" enctype="multipart/form-data">
                                    <!-- /.card-header -->
                                    {{csrf_field()}}
                                    <div class="card-body">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                            <label>Judul *</label>
                                            <input type="text" class="form-control" id="judul_peta" name="judul_peta" value='{{$lka->judul_peta}}'>
                                            </div>

                                            <div class="form-group">
												<div class="form-group">
												<label>Bahan</label>
                                                    <select name='id_jenis' class="form-control select2bs4" style="width: 100%;" required>
                                                      <option value='1' {{$lka->id_jenis ==  1  ? 'selected' : ''}}>Peta Sebaran Harga</option>
													  <option value='2' {{$lka->id_jenis ==  2  ? 'selected' : ''}}>Peta Sebaran Rantai Pasok</option>
													  <option value='3' {{$lka->id_jenis ==  3  ? 'selected' : ''}}>Grafik Sebaran Harga/Statistik</option>
													</select>
												</div>
                                            </div>
                                            <!-- /.form-group -->

                                            <div class="form-group">
												<div class="form-group">
												<label>Status</label>
													<select name='status_peta' class="form-control select2bs4" style="width: 100%;">
													  <option value='aktif' {{$lka->status_peta ==  'aktif'  ? 'selected' : ''}}>Aktif</option>
													  <option value='pasif' {{$lka->status_peta ==  'pasif'  ? 'selected' : ''}}>Non Aktif</option>
													</select>
												</div>
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <!-- /.col -->
                                        <div class="col-md-12">
                                            <div class="form-group">
												<label>Iframe *</label>
												<textarea class='form-control' name='iframe_peta'>{{$lka->iframe_peta}}</textarea>
											 </div>
                                        </div>
                                        <!-- /.col -->

                                        <div class="row">
                                        <div class="col-12 col-sm-12">
                                            <div class="form-group"><center>
                                                <button type="submit" class="btn btn-primary">Save</button>       
                                            </center></div>
                                            <!-- /.form-group -->
                                        </div>
                                        <!-- /.col -->
                                        </div>
                                        <!-- /.row -->

                                    </div>
                                    </form>
                                    <button type="button" class="btn btn-default" data-dismiss="modal"><b>X Close</b></button>
                                       
                                        </div>
                                    </div>
                                    
                                    </div>
                                </div>


                            <button type="button" class="btn btn-danger btn-sm confirmation" data-toggle="modal" data-target="#myModal{{$lka->id}}"><i class="fas fa-trash"></i></button>
                            <!-- Modal -->
                                <div class="modal fade" id="myModal{{$lka->id}}" role="dialog">
                                    <div class="modal-dialog">
                                    
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                        <h4 class="modal-title">Apakah Anda Yakin menghapus data Link {{$lka->judul_peta}}..?</h4>
                                        </div>
                                        <div class="modal-footer">
                                        <a href="<?php echo url('/menu/kontak/destroy_peta/'.$lka->id); ?>" class="btn btn-danger btn-sm confirmation" >Hapus Data</a>
                                            
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                    
                                    </div>
                                </div>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                    </table>					
						
                        </div>
        <div class="col-12">
                <div class="card">
                <div class="card-header">
                            <h3 class="card-title">Link | &nbsp;</h3>
               
                <a class="btn btn-info btn-sm" href="<?php echo url('menu/link/add'); ?>">
                                <i class="fas fa-plus"> Add
                                </i>
                            </a>
                </div>
                                
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="next2" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Link</th>
                        <th style="width:15%">Gambar</th>
                        <th>Url</th>
                        <th style="width:10%">Aksi</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $no=0; ?> 
                @foreach($link as $lk)
                <?php $no++; ?>
                    <tr>
                        <td>{{$no}}</td>
                        <td>{{$lk->nama_link}}</td>
                        <td><center>
                        <img src="{{asset('storage/images/link/'.$lk->image_link)}}" alt="{{$lk->image_link}}" width='40%' class="brand-image img-circle elevation-2" style="opacity: .8">
                        </center></td>
                        <td>{{$lk->url_link}}</td>
                        <td>
                        <a class="btn btn-info btn-sm" href="<?php echo url('/menu/link/edit/'.$lk->id_link); ?>">
                                <i class="fas fa-edit">
                                </i>
                            </a>
                            <button type="button" class="btn btn-danger btn-sm confirmation" data-toggle="modal" data-target="#myModal{{$lk->id_link}}"><i class="fas fa-trash"></i></button>
                            <!-- Modal -->
                                <div class="modal fade" id="myModal{{$lk->id_link}}" role="dialog">
                                    <div class="modal-dialog">
                                    
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                        <h4 class="modal-title">Apakah Anda Yakin menghapus data Link {{$lk->nama_link}}..?</h4>
                                        </div>
                                        <div class="modal-footer">
                                        <a href="<?php echo url('/menu/link/destroy_link/'.$lk->id_link); ?>" class="btn btn-danger btn-sm confirmation" >Hapus Data</a>
                                            
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                    
                                    </div>
                                </div>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
                </div>

            </div>

            <div class="col-12">
                <div class="card">
                <div class="card-header">
                            <h3 class="card-title">Popup | &nbsp;
                                <button type="button" class="btn btn-success btn-sm confirmation" data-toggle="modal" data-target="#myModaladdpop"><i class="fas fa-plus"></i> Add </button>
                            <!-- Modal -->
                                <div class="modal fade" id="myModaladdpop" role="dialog">
                                    <div class="modal-dialog">
                                    
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                        <h4 class="modal-title">New Peta By Bahan Jenis</h4>
                                        </div>
                                        <div class="modal-body">
                                        <form class="form" action="<?php echo url("/menu/popup/create_popup"); ?>" method="POST" enctype="multipart/form-data">
                                    <!-- /.card-header -->
                                    {{csrf_field()}}
                                    <div class="card-body">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                            <label>Judul *</label>
                                            <input type="text" class="form-control" id="judul_popup" name="judul_popup" required>
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
												<div class="form-group">
												<label>Url</label>
													<select name='url_popup' class="form-control select2bs4" style="width: 100%;" required>
													  <?php
                                                      $bahan = \DB::table('berita')
                                                      ->select('*')
                                                      ->get();
                                                      ?>
                                                      @foreach ($bahan as $bj)
                                                        <option value='/berita/detail/{{$bj->id_berita}}'>{{$bj->judul}}</option>
                                                      @endforeach
                                                    </select>
												</div>
                                            </div>

                                            <div class="form-group">
												<div class="form-group">
												<label>Status</label>
													<select name='status_popup' class="form-control select2bs4" style="width: 100%;">
													  <option value='aktif' selected="selected">Aktif</option>
													  <option value='pasif'>Non Aktif</option>
													</select>
												</div>
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <!-- /.col -->
                                        <div class="col-md-12">
                                            <div class="form-group">
												<label>Iframe *</label>												
                                            <input type="file" class="form-control" id="file" name="file" required>
											 </div>
                                        </div>
                                        <!-- /.col -->
                                        </div>
                                        <!-- /.row -->

                                        <div class="row">
                                        <div class="col-12 col-sm-12">
                                            <div class="form-group"><center>
                                                <button type="submit" class="btn btn-primary">Save</button>       
                                            </center></div>
                                            <!-- /.form-group -->
                                        </div>
                                        <!-- /.col -->
                                        </div>
                                        <!-- /.row -->

                                    </div>
                                    <!-- /.card-body -->
                                    </form>  
                                    <button type="button" class="btn btn-default" data-dismiss="modal"><b>X Close</b></button>
                                        
                                        </div>
                                    </div>
                                    
                                    </div>
                                </div>
                                </h3>
                </div>
                                
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="next5" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Judul Popup</th>
                        <th style="width:15%">image_popup</th>
                        <th>url_popup</th>
                        <th>Status_popup</th>
                        <th style="width:10%">Aksi</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $no=0; ?> 
                @foreach($popup as $pp)
                <?php $no++; ?>
                    <tr>
                        <td>{{$no}}</td>
                        <td>{{$pp->judul_popup}}</td>
                        <td>
                            <img src="{{asset('storage/images/popup/'.$pp->image_popup)}}" alt="{{$pp->image_popup}}" width='40%' class="brand-image img-circle elevation-2" style="opacity: .8">
                        </td>
                        <td>{{$pp->url_popup}}</td>
                        <td>{{$pp->status_popup}}</td>
                        <td>
                        <button type="button" class="btn btn-info btn-sm confirmation" data-toggle="modal" data-target="#myModaladdpop{{$pp->id_popup}}"><i class="fas fa-edit"></i></button>
                            <!-- Modal -->
                                <div class="modal fade" id="myModaladdpop{{$pp->id_popup}}" role="dialog">
                                    <div class="modal-dialog">
                                    
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                        <h4 class="modal-title">New Peta By Bahan Jenis</h4>
                                        </div>
                                        <div class="modal-body">
                                        <form class="form" action="<?php echo url("/menu/popup/update_popup/$pp->id_popup"); ?>" method="POST" enctype="multipart/form-data">
                                    <!-- /.card-header -->
                                    {{csrf_field()}}
                                    <div class="card-body">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                            <label>Judul *</label>
                                            <input type="text" class="form-control" id="judul_popup" name="judul_popup" value='{{$pp->judul_popup}}'>
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
												<div class="form-group">
												<label>Url</label>
													<select name='url_popup' class="form-control select2bs4" style="width: 100%;" required>
													  <?php
                                                      $bahan = \DB::table('berita')
                                                      ->select('*')
                                                      ->get();
                                                      ?>
                                                      @foreach ($bahan as $bj)
                                                        <option value='/berita/detail/{{$bj->id_berita}}' {{$pp->url_popup ==  '/berita/detail/'.$bj->id_berita  ? 'selected' : ''}}>{{$bj->judul}}</option>
                                                      @endforeach
                                                    </select>
												</div>
                                            </div>

                                            <div class="form-group">
												<div class="form-group">
												<label>Status</label>
													<select name='status_popup' class="form-control select2bs4" style="width: 100%;">
													  <option value='aktif' {{$pp->status_popup ==  'aktif'  ? 'selected' : ''}}>Aktif</option>
													  <option value='pasif' {{$pp->status_popup ==  'pasif'  ? 'selected' : ''}}>Non Aktif</option>
													</select>
												</div>
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <!-- /.col -->
                                        <div class="col-md-12">
                                            <div class="form-group">
												<label>Iframe *</label>												
                                            <input type="file" class="form-control" id="file" name="file">
											 </div>
                                        </div>
                                        <!-- /.col -->
                                        </div>
                                        <!-- /.row -->

                                        <div class="row">
                                        <div class="col-12 col-sm-12">
                                            <div class="form-group"><center>
                                                <button type="submit" class="btn btn-primary">Save</button>       
                                            </center></div>
                                            <!-- /.form-group -->
                                        </div>
                                        <!-- /.col -->
                                        </div>
                                        <!-- /.row -->

                                    </div>
                                    <!-- /.card-body -->
                                    </form>  
                                    <button type="button" class="btn btn-default" data-dismiss="modal"><b>X Close</b></button>
                                        
                                        </div>
                                    </div>
                                    
                                    </div>
                                </div>

                            <button type="button" class="btn btn-danger btn-sm confirmation" data-toggle="modal" data-target="#myModalp{{$pp->id_popup}}"><i class="fas fa-trash"></i></button>
                            <!-- Modal -->
                                <div class="modal fade" id="myModalp{{$pp->id_popup}}" role="dialog">
                                    <div class="modal-dialog">
                                    
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                        <h4 class="modal-title">Apakah Anda Yakin menghapus data Popup {{$pp->judul_popup}}..?</h4>
                                        </div>
                                        <div class="modal-footer">
                                        <a href="<?php echo url('/menu/popup/destroy_popup/'.$pp->id_popup); ?>" class="btn btn-danger btn-sm confirmation" >Hapus Data</a>
                                            
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                    
                                    </div>
                                </div>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
                </div>

            </div>
			
			<div class="col-12">
                <div class="card">
                <div class="card-header">
                            <h3 class="card-title">Video | &nbsp;</h3>
               
                <a class="btn btn-info btn-sm" href="<?php echo url('menu/video/add_video'); ?>">
                                <i class="fas fa-plus"> Add
                                </i>
                            </a>
                </div>
                                
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="next3" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Judul Video</th>
                        <th style="width:15%">Iframe</th>
                        <th>Status</th>
                        <th style="width:10%">Aksi</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $no=0; ?> 
                @foreach($video as $vdo)
                <?php $no++; ?>
                    <tr>
                        <td>{{$no}}</td>
                        <td>{{$vdo->judul_video}}</td>
                        <td><center>
                        {!!$vdo->iframe_video!!}
                        </center></td>
                        <td>{{$vdo->status_video}}</td>
                        <td>
                        <a class="btn btn-info btn-sm" href="<?php echo url('/menu/video/edit_video/'.$vdo->id_video); ?>">
                                <i class="fas fa-edit">
                                </i>
                            </a>
                            <button type="button" class="btn btn-danger btn-sm confirmation" data-toggle="modal" data-target="#myModalv{{$vdo->id_video}}"><i class="fas fa-trash"></i></button>
                            <!-- Modal -->
                                <div class="modal fade" id="myModalv{{$vdo->id_video}}" role="dialog">
                                    <div class="modal-dialog">
                                    
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                        <h4 class="modal-title">Apakah Anda Yakin menghapus data Video {{$vdo->judul_video}}..?</h4>
                                        </div>
                                        <div class="modal-footer">
                                        <a href="<?php echo url('/menu/video/destroy_video/'.$vdo->id_video); ?>" class="btn btn-danger btn-sm confirmation" >Hapus Data</a>
                                            
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                    
                                    </div>
                                </div>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
                </div>

            </div>

            <div class="col-12">
                <div class="card">
                <div class="card-header">
                            <h3 class="card-title">Slider | &nbsp;</h3>
                <?php
                    $banner = DB::table('tbl_slider')
                    ->select(DB::raw('COUNT(id_slider) as total'))
                    ->where('status_slider', '=', 'aktif')
                    ->get();
                    ?>
                    @foreach ($banner as $b) 
                    @if($b->total <= 4)
                <a class="btn btn-info btn-sm" href="<?php echo url('menu/add'); ?>">
                                <i class="fas fa-plus"> Add
                                </i>
                            </a>
                    @else
                    @endif
                    @endforeach
                </div>
                                
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="next4" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Judul</th>
                        <th>Keterangan</th>
                        <th style="width:15%">Gambar</th>
                        <th>Url</th>
                        <th>Status</th>
                        <th style="width:5%">Aksi</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $no=0; ?> 
                @foreach($slider as $g)
                <?php $no++; ?>
                    <tr>
                        <td>{{$no}}</td>
                        <td>{{$g->judul_slider}}</td>
                        <td>{{$g->keterangan_slider}}</td>
                        <td><center>
                        <img src="{{asset('storage/images/slider/'.$g->image_slider)}}" alt="{{$g->image_slider}}" width='40%' class="brand-image img-circle elevation-2" style="opacity: .8">
                        </center></td>
                        <td>{{$g->url_slider}}</td>
                        <td>
                        @if($g->status_slider == 'aktif')
                            <span class="badge badge-success">Aktif</span>
                        @else
                            <span class="badge badge-danger">Non Aktif</span>
                        @endif
                        </td>
                        <td>
                        <a class="btn btn-info btn-sm" href="<?php echo url('/menu/edit/'.$g->id_slider); ?>">
                                <i class="fas fa-edit">
                                </i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
                </div>

            </div>
			<div class="col-12">
				 <div class="card">
					<div class="card-header">
								<h3 class="card-title">Kontak Kami | &nbsp;</h3>
                    
					</div>
                        <div class="card-body">
                            <!-- /.card-header -->
                        
                                <?php
                                $kontak = DB::table('kontak')
                                ->select(DB::raw('COUNT(id) as total'))
                                ->get();
                                ?>
                                @foreach ($kontak as $k)                                 
                                @if($k->total <= 0)

                                <form class="form" action="<?php echo url("/menu/kontak/create"); ?>" method="POST" enctype="multipart/form-data">
                                    <!-- /.card-header -->
                                    {{csrf_field()}}
                                    <div class="card-body">
                                        <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                            <label>Telepon *</label>
                                            <input type="text" class="form-control" id="telepon" name="telepon" required>
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                            <label>Fax</label>
                                            <input type="text" class="form-control" id="fax" name="fax" >
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <!-- /.col -->
                                        <div class="col-md-12">
                                            <div class="form-group">
                                            <label>email</label>
                                            <input type="email" class="form-control" id="email" name="email">
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                            <label>Upload Logo *</label>
                                            <input type="file" class="form-control" id="file" name="file" placeholder="upload Logo">                  
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <!-- /.col -->
                                        </div>
                                        <!-- /.row -->

                                        <div class="row">
                                        <div class="col-12">
                                        <div class="form-group">
                                            <label>Alamat *</label>
                                            <textarea class="form-control" name='address' rows="3" required></textarea>
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        
                                        <div class="col-12">
                                            <div class="form-group">
                                            <label>Deskripsi *</label>
                                            <textarea class="form-control" name='deskripsi' rows="3"></textarea>
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <!-- /.col -->
                                        </div>
                                        <!-- /.row -->

                                        <div class="row">
                                        <div class="col-12 col-sm-12">
                                            <div class="form-group"><center>
                                                <button type="submit" class="btn btn-primary">Save</button>       
                                            </center></div>
                                            <!-- /.form-group -->
                                        </div>
                                        <!-- /.col -->
                                        </div>
                                        <!-- /.row -->

                                    </div>
                                    <!-- /.card-body -->
                                    <div class="card-footer">
                                        Klinik Konstruksi 
                                    </div>
                                    </div>
                                    <!-- /.card -->
                                    </form>   

                                    @else
                                    <?php
                                $ktak = DB::table('kontak')
                                ->select('*')
                                ->get();
                                ?>
                                @foreach ($ktak as $ktk)
                                <form class="form" action="<?php echo url("/menu/kontak/update/$ktk->id"); ?>" method="POST" enctype="multipart/form-data">
                                    <!-- /.card-header -->
                                    {{csrf_field()}}
                                    <div class="card-body">
                                        <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                            <label>Telepon *</label>
                                            <input type="text" class="form-control" id="telepon" name="telepon" value="{{$ktk->telepon}}" required>
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                            <label>Fax</label>
                                            <input type="text" class="form-control" id="fax" name="fax" value="{{$ktk->fax}}">
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <!-- /.col -->
                                        <div class="col-md-12">
                                            <div class="form-group">
                                            <label>email</label>
                                            <input type="email" class="form-control" id="email" name="email" value='{{$ktk->email}}'>
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                            <label>Upload Logo *</label>
                                            <input type="file" class="form-control" id="file" name="file" placeholder="upload Banner">                  
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <!-- /.col -->
                                        </div>
                                        <!-- /.row -->

                                        <div class="row">
                                        <div class="col-12">
                                        <div class="form-group">
                                            <label>Instansi</label>
                                            <input type="text" class="form-control" id="instansi" name="instansi" value='{{$ktk->instansi}}'>
                                            </div>
                                        <div class="form-group">
                                            <label>Alamat *</label>
                                            <textarea class="form-control" name='address' rows="3" required>{{$ktk->address}}</textarea>
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        
                                        <div class="col-12">
                                            <div class="form-group">
                                            <label>Deskripsi *</label>
                                            <textarea class="form-control" name='deskripsi' rows="3">{{$ktk->deskripsi}}</textarea>
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <!-- /.col -->
                                        </div>
                                        <!-- /.row -->

                                        <div class="row">
                                        <div class="col-12 col-sm-12">
                                            <div class="form-group"><center>
                                                <button type="submit" class="btn btn-primary">Update</button>       
                                            </center></div>
                                            <!-- /.form-group -->
                                        </div>
                                        <!-- /.col -->
                                        </div>
                                        <!-- /.row -->

                                    </div>
                                    <!-- /.card-body -->
                                    <div class="card-footer">
                                        Klinik Konstruksi 
                                    </div>
                                    <!-- /.card -->
                                    </form>   
                                    @endforeach
                                @endif
                                    @endforeach
									</div>

                        </div>
				</div>
			
			</div>
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  
  <script>
  $(function () {
    $("#next").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false, "pageLength": 5
    }).buttons().container().appendTo('#next_wrapper .col-md-6:eq(0)');
  });
  $(function () {
    $("#next2").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false, "pageLength": 5
    }).buttons().container().appendTo('#next_wrapper .col-md-6:eq(0)');
  });
  $(function () {
    $("#next3").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false, "pageLength": 5
    }).buttons().container().appendTo('#next_wrapper .col-md-6:eq(0)');
  });
  $(function () {
    $("#next4").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false, "pageLength": 5
    }).buttons().container().appendTo('#next_wrapper .col-md-6:eq(0)');
  });
  $(function () {
    $("#next5").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false, "pageLength": 5
    }).buttons().container().appendTo('#next_wrapper .col-md-6:eq(0)');
  });
</script>
@endsection