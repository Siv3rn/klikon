@extends('layouts.lay')

@section('content')
<div id="banner-area" class="banner-area" style="background-image:url({{url('/constra/images/banner/banner1.jpg')}})">
  <div class="banner-text">
    <div class="container">
        <div class="row">
          <div class="col-lg-12">
              <div class="banner-heading">
                <h1 class="banner-title">Artikel</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb justify-content-center">
                      <li class="breadcrumb-item"><a href="<?php echo url("/"); ?>">Beranda</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Artikel</li>
                    </ol>
                </nav>
              </div>
          </div><!-- Col end -->
        </div><!-- Row end -->
    </div><!-- Container end -->
  </div><!-- Banner text end -->
</div><!-- Banner area end --> 

<section id="main-container"  class="facts-area">
  <div class="container">

    <div class="row">

	<div class="col-lg-8">

		<div class="container mt-4">
            @foreach($news as $kon)      
    <div class="post-content post-single">
        <div class="post-media post-image">
            @if(empty($kon->file_image))

            @else
            <img loading="lazy" src="{{asset('storage/images/artikel/'.$kon->file_image)}}" class="img-fluid" alt="post-image">
            @endif
        </div>
          <div class="post-body">
            <div class="entry-header">
              <div class="post-meta">
                <span class="post-author">
                  <i class="far fa-user"></i><a href="#"> {{$kon->petugas}}</a>
                </span>
                <span class="post-cat">
                  <i class="fa fa-tags"></i><a href="#"> {{$kon->pengarang}}</a>
                </span>
                <span class="post-meta-date"><i class="far fa-calendar"></i> {{$kon->tanggal}}</span>
              </div>
              <h2 class="entry-title">
              {{$kon->judul}}
              </h2>
            </div><!-- header end -->

            <div class="entry-content">
            @if(empty($kon->isi))
            {!!$kon->ringkasan!!}
            @else
            {!!$kon->isi!!}
            @endif
            </div>

            <div class="tags-area d-flex align-items-center justify-content-between">
              
              <div class="share-items">
                <ul class="post-social-icons list-unstyled">
                  <li class="social-icons-head">Share:</li>
                  <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                  <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                  <li><a href="#"><i class="fab fa-google-plus"></i></a></li>
                  <li><a href="#"><i class="fab fa-linkedin"></i></a></li>
                </ul>
              </div>
            </div>

          </div><!-- post-body end -->
        </div><!-- post content end -->
        @endforeach 
            </div>
    </div><!-- Col end -->
    
    <div class="col-lg-4 mt-5 mt-lg-0">
    <br/>
    <div class="accordion accordion-default accordion-toggle accordion-style-1 mb-5" data-plugin-sticky data-plugin-options="{'offset_top': 100}" role="tablist">
    <div class="card">
  <div class="card-header accordion-header" role="tab" id="popularPosts">
    <h3 class="text-3 mb-0">
      <a href="#" data-toggle="collapse" data-target="#togglePopularPosts" aria-expanded="false" aria-controls="togglePopularPosts">artikel Terkini</a>
    </h3>
  </div>
  <div id="togglePopularPosts" class="accordion-body collapse show" role="tabpanel" aria-labelledby="popularPosts">
 <div class="card-body">
  <ul class="list-unstyled">
  @foreach($konsul as $ksl)
  <li class="d-flex align-items-center">
      <div class="posts-thumb">
        <a href="#">
        @if(empty($ksl->file_image))
        <img loading="lazy" src="{{url('/constra/images/icon-image/ksl1.png')}}" style="height:70px;width:80px;">
        @else
        <img loading="lazy" style="height:70px;width:80px;" alt="news-img" src="{{asset('storage/images/artikel/'.$ksl->file_image)}}">
        @endif
      </a>
      </div>
      <div>&nbsp;</div>
      <div class="post-info">
          <a href="<?php echo url("/artikel/detail/$ksl->id_artikel"); ?>">
          {{substr($ksl->judul, 0, 50)}}<b>..Selengkapnya</b></a>
      </div>
    </li><!-- 2nd post end-->
    <hr/>
    @endforeach
  </ul>
  <div class="card">
    <h3 class="text-3 mb-0">
      <a href="<?php echo url("/artikel/all"); ?>" >artikel Lainnya</a>
    </h3>
  </div>
    </div>
  </div>
</div>

<div class="card">
  <div class="card-header accordion-header" role="tab" id="tags">
  <h5 class="text-5 mb-0">
      <a href="#" data-toggle="collapse" data-target="#toggleTags" aria-expanded="false" aria-controls="toggleTags">Video</a>
  </h5>
  </div>
  <div id="toggleTags" class="accordion-body collapse show" role="tabpanel" aria-labelledby="tags">
    <div class="card-body">
            <ul class="list-inline">
            @foreach($video as $vdo)
              <li class="list-inline-item">
                <div class="post-info">
                  {!!$vdo->iframe_video!!}
                </div>
              </li><!-- 2nd post end-->
              @endforeach
            </ul>
    </div>
  </div>
</div>

</div>
        
        
		</div>
        </div><!-- Content row end -->      
</section><!-- Main container end -->


<!-- bootstrap5 dataTables js cdn -->
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    
    <script src="{{url('/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    
    <script>
    var jq = $.noConflict();
    jq(function () {
        jq("#next").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false, 'pageLength': 7,
        }).buttons().container().appendTo('#next_wrapper .col-md-6:eq(0)');
    });
    </script>

@endsection