@extends('layouts.lay')

@section('content')
<div id="banner-area" class="banner-area" style="background-image:url({{url('/constra/images/banner/banner2.jpg')}})">
  <div class="banner-text">
    <div class="container">
        <div class="row">
          <div class="col-lg-12">
              <div class="banner-heading">
                <h1 class="banner-title">Event</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb justify-content-center">
                      <li class="breadcrumb-item"><a href="<?php echo url("/"); ?>">Beranda</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Event</li>
                    </ol>
                </nav>
              </div>
          </div><!-- Col end -->
        </div><!-- Row end -->
    </div><!-- Container end -->
  </div><!-- Banner text end -->
</div><!-- Banner area end --> 

<section id="main-container"  class="facts-area">
  <div class="container">

    <div class="row">

    <div class="col-lg-4 mt-5 mt-lg-0">
      <br/>
        <div class="sidebar sidebar-right">
        <div class="widget recent-posts">
            <h3 class="widget-title">Konsultasi Terkini</h3>
            <ul class="list-unstyled">
            @foreach($konsul as $ksl)
              <li class="d-flex align-items-center">
                <div class="posts-thumb">
                  <a href="#"><img loading="lazy" alt="news-img" src="{{url('/constra/images/icon-image/ksl.png')}}"></a>
                </div>
                <div class="post-info">
                  <h4 class="entry-title">
                    <a href="<?php echo url("/konsultasi/detail/$ksl->id_konsultasi"); ?>">{{$ksl->judul_konsultasi}}</a>
                  </h4>
                </div>
              </li><!-- 2nd post end-->
              @endforeach
            </ul>
                <div class="call-to-action-btn">
                 <a class="btn btn-blue" href="<?php echo url("/konsultasi/tanya"); ?>">Kirim Pertanyaan</a>
                </div>
        </div><!-- Recent post end -->
        </div><!-- Sidebar end -->

    </div><!-- Col end -->
		<div class="col-lg-8">
        <div class="container mt-4">
        <style>
                        table#next.dataTable tr:hover {
                        background-color: #d0ecdc;
                        }
                        .table-striped>tbody>tr:nth-child(odd)>td, 
                        .table-striped>tbody>tr:nth-child(odd)>th {
                            background-color: #e8f7f0 ; /* Choose your own color here */
                        }                       
                    </style>      
        <table id="next" class="table table-striped table-bordered hover" bordercolor="#e0e4e4">
								<thead>
								<tr>
									<th>No</th>
									<th style="width:20%">Event</th>
									<th>File Pendukung</th>
								</tr>
								</thead>
								<tbody>
								<?php $no=0; ?> 
							@foreach($event as $lk)
							<?php $no++; ?>
								<tr>
									<td>{{$no}}
                                    </td>
									<td>
                                    <?php
                                        $tbl_event = DB::table('tbl_event')
                                        ->select('*')
                                        ->where('id_event','=',$lk->id_event)
                                        ->get();
                                        ?>
                                        @foreach ($tbl_event as $te) 
                                        {{$te->judul_event}}
                                        @endforeach
                                    </td>
									<td>
                                    <?php
                                        $file = DB::table('tbl_file_pendukung')
                                        ->select('*')
                                        ->where('id_event','=',$lk->id_event)
                                        ->get();
                                        ?>
                                        @foreach ($file as $f) 
                                        <a href="{{asset('storage/images/event/'.$f->file)}}" target='_blank'>
                                        - {{$f->judul_file}} 
                                        <i class="fas fa-download">
											                  </i>
                                        </a> <br/>
                                        @endforeach
                                    </td>
								</tr>
								@endforeach
								</tbody>
								</table>
        
            </div>
            <br/><br/>
		 
		</div>
        </div><!-- Content row end -->      
</section><!-- Main container end -->

<!-- bootstrap5 dataTables js cdn -->
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    
    <script src="{{url('/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    
    <script>
    var jq = $.noConflict();
    jq(function () {
        jq("#next").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false, 'pageLength': 7,
        }).buttons().container().appendTo('#next_wrapper .col-md-6:eq(0)');
    });
    </script>

@endsection