@extends('layouts.lay2')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-12">
        @if(!empty(\Session::get('success')) > 0)
                    <div class="alert alert-success">     
                        <strong>Sukses!</strong> {!! \Session::get('success') !!}             
                        </div>
                    @endif
                <script>
                    window.setTimeout(function() {
                    $.noConflict();
                    $(".alert").fadeTo(500, 0).slideUp(500, function(){
                        $(this).remove(); 
                    });
                    }, 5000);
                </script>
                <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
                <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
                </div>
                <div class="col-sm-6">
            <h1 class="m-0">Event</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/home">Dashboard</a></li>
              <li class="breadcrumb-item"><a href="/informasi/event">Informasi</a></li>
              <li class="breadcrumb-item active">Event</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
                <!-- Small boxes (Stat box) -->
        <div class="row">
					<div class="col-12">
							<div class="card">
							<div class="card-header">
										<h3 class="card-title">Event | &nbsp;</h3>
                                        
                                        <button type="button" class="btn btn-success btn-sm confirmation" data-toggle="modal" data-target="#event"><i class="fas fa-plus"></i>Add</button>
										<!-- Modal -->
                                                <div class="modal fade" id="event" role="dialog">
												<div class="modal-dialog">
												
												<!-- Modal content-->
												<div class="modal-content">
													<div class="modal-header">
													<h4 class="modal-title">Add New Event</h4>
													</div>
                                                    <div class="modal-body">
                                                        
                                                        <form class="form" action="<?php echo url("/informasi/event/create_event"); ?>" method="POST">
                                                        {{csrf_field()}}
                                                        <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>Nama Event *</label>
                                                            <input type="text" class="form-control" id="judul_event" name="judul_event" placeholder="Event" required>
                                                            <br/>
                                                            <div class="form-group"><center>
                                                                <button type="submit" class="btn btn-primary">Submit</button>       
                                                            </center></div>
                                                        </div>
                                                        </div>
                                                        </form>
                                                    </div>

													<div class="modal-footer">														
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													</div>
												</div>
												
												</div>
											</div>

							</div>
											
							<!-- /.card-header -->
							<div class="card-body">
                            <form class="form" action="<?php echo url("/informasi/event/create"); ?>" method="POST" enctype="multipart/form-data">
                                    <!-- /.card-header -->
                                    {{csrf_field()}}
                                    <div class="card-body">
                                    <div class="row">
                                    <div class="col-md-6">
                                        <!-- /.form-group -->
                                        <div class="form-group">
                                            <label>Event *</label>
                                            <select name='id_event' class="form-control select2bs4" style="width: 100%;">
                                            <?php
                                                $fe = DB::table('tbl_event')
                                                ->select('*')
                                                ->where('status_event','=','on')
                                                ->get();
                                                ?>
                                                @foreach ($fe as $fee) 
                                                <option value='{{$fee->id_event}}' >{{$fee->judul_event}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <!-- /.form-group -->
                                        </div>
                                        <!-- /.col -->
                                        <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Judul File *</label>
                                            <input type="text" class="form-control" id="judul_file" name="judul_file" placeholder="judul File" required>
                                        </div>
                                        </div>
                                        <!-- /.col -->
                                        <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Upload File *</label>
                                            <input type="file" class="form-control" id="file" name="file" placeholder="Upload " required>                  
                                        </div>
                                        </div>
                                        <!-- /.col -->
                                        <div class="col-md-12">
                                        <div class="form-group">
                                        <button type="submit" class="btn btn-primary">Upload File</button>     
                                        </div>
                                        </div>
                                        <!-- /.col -->
                                    </div>
                                    <!-- /.row -->
                                    </div>
                                </div>
                                <!-- /.card -->
                                </form>
								<table id="next" class="table table-bordered table-hover">
								<thead>
								<tr>
									<th>No</th>
									<th style="width:20%">Event</th>
									<th>File Pendukung</th>
									<th>Unduhan</th>
									<th style="width:10%">Aksi</th>
								</tr>
								</thead>
								<tbody>
								<?php $no=0; ?> 
							@foreach($event as $lk)
							<?php $no++; ?>
								<tr>
									<td>{{$no}} | 
                                        @if($lk->status_event == 'off')
                                    <span class="btn btn-danger">Off</span>
                                        @endif
                                        @if($lk->status_event == 'on')
                                    <span class="btn btn-success">On</span>
                                       @endif
                                    </td>
									<td>
                                    <?php
                                        $tbl_event = DB::table('tbl_event')
                                        ->select('*')
                                        ->where('id_event','=',$lk->id_event)
                                        ->get();
                                        ?>
                                        @foreach ($tbl_event as $te) 
                                        {{$te->judul_event}}
                                        @endforeach
                                    </td>
									<td>
                                    <?php
                                        $file = DB::table('tbl_file_pendukung')
                                        ->select('*')
                                        ->where('id_event','=',$lk->id_event)
                                        ->get();
                                        ?>
                                        @foreach ($file as $f) 
                                        - {{$f->judul_file}} <br/>
                                        @endforeach
                                    </td>
									<td>
                                    <?php
                                        $files = DB::table('tbl_file_pendukung')
                                        ->select('*')
                                        ->where('id_event','=',$lk->id_event)
                                        ->get();
                                        ?>
                                        @foreach ($files as $fs) 
                                        - <a href="{{asset('storage/images/event/'.$fs->file)}}" target='_blank'> {{$fs->file}} </a> | 
									
                                        <button type="button" class="btn btn-primary btn-sm confirmation" data-toggle="modal" data-target="#myModalya{{$fs->id_file}}"><i class="fas fa-edit"></i></button>
										<!-- Modal -->
                                                <div class="modal fade" id="myModalya{{$fs->id_file}}" role="dialog">
												<div class="modal-dialog">
												
												<!-- Modal content-->
												<div class="modal-content">
													<div class="modal-header">
													<h4 class="modal-title">Update File Event {{$fs->judul_file}}</h4>
													</div>
                                                    <div class="modal-body">
                                                        <!-- /.modal update file pendukung -->
                                                        <form class="form" action="<?php echo url("/informasi/event/update/$fs->id_file"); ?>" method="POST">
                                                        {{csrf_field()}}
                                                                <div class="card-body">
                                                                <div class="col-md-12">
                                                                    <!-- /.form-group -->
                                                                    <div class="form-group">
                                                                        <label>Event *</label>
                                                                        <select name='id_event' class="form-control select2bs4" style="width: 100%;">
                                                                        <?php
                                                                            $fes = DB::table('tbl_event')
                                                                            ->select('*')
                                                                            ->get();
                                                                            ?>
                                                                            @foreach ($fes as $fee) 
                                                                            <option value='{{$fee->id_event}}' {{$fee->id_event ==  $lk->id_event  ? 'selected' : ''}}>{{$fee->judul_event}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                    <!-- /.form-group -->
                                                                    </div>
                                                                    <!-- /.col -->
                                                                    <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label>Judul File *</label>
                                                                        <input type="text" class="form-control" id="judul_file" name="judul_file" value="{{$fs->judul_file}}" required>
                                                                    </div>
                                                                    </div>
                                                                    <!-- /.col -->
                                                                    <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label>Upload File *</label>
                                                                        <input type="file" class="form-control" id="file" name="file" placeholder="Upload ">                  
                                                                    </div>
                                                                    </div>
                                                                    <!-- /.col -->
                                                                    <div class="col-md-12">
                                                                    <div class="form-group">
                                                                    <button type="submit" class="btn btn-primary">Upload File</button>     
                                                                    </div>
                                                                    </div>
                                                                    <!-- /.col -->
                                                                </div>
																</form>
                                                            </div>
															<div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													</div>
                                                    </div>
													
												</div>
												
												</div>
                                    </br> 
                                        @endforeach
                                    </td>
									<td>
                                    <button type="button" class="btn btn-primary btn-sm confirmation" data-toggle="modal" data-target="#myModaly{{$lk->id_event}}"><i class="fas fa-edit"></i></button>
										<!-- Modal -->
                                                <div class="modal fade" id="myModaly{{$lk->id_event}}" role="dialog">
												<div class="modal-dialog">
												
												<!-- Modal content-->
												<div class="modal-content">
													<div class="modal-header">
													<h4 class="modal-title">Update Event {{$lk->judul_event}}</h4>
													</div>
                                                    <div class="modal-body">
                                                        
                                                        <form class="form" action="<?php echo url("/informasi/event/update_event/$lk->id_event"); ?>" method="POST">
                                                        {{csrf_field()}}
                                                        <div class="col-md-12">

                                                        <div class="form-group">
                                                            <label>Nama Event *</label>
                                                            <textarea id="form-control" name='judul_event' style="width: 100%;">{{$lk->judul_event}}</textarea>
                                                        </div>

                                                        <div class="form-group">
                                                            <label>Status</label>
                                                            <select name='status_event' class="form-control select2bs4" style="width: 100%;">
                                                                <option value='on' {{'on' ==  $lk->status_event  ? 'selected' : ''}}>On</option>
                                                                <option value='off' {{'off' ==  $lk->status_event  ? 'selected' : ''}}>Off</option>
                                                            </select>
                                                            </div>
                                                            
                                                        <div class="form-group">
                                                            <div class="form-group"><center>
                                                                <button type="submit" class="btn btn-primary">Submit</button>       
                                                            </center></div>
                                                        </div>
                                                        </div>
                                                        </form>
                                                    </div>
													<div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													</div>
												</div>
												
												</div>
											</div>


										<button type="button" class="btn btn-danger btn-sm confirmation" data-toggle="modal" data-target="#myModal{{$lk->id_event}}"><i class="fas fa-trash"></i></button>
										<!-- Modal -->
                                                <div class="modal fade" id="myModal{{$lk->id_event}}" role="dialog">
												<div class="modal-dialog">
												
												<!-- Modal content-->
												<div class="modal-content">
													<div class="modal-header">
													<h4 class="modal-title">Apakah Anda Yakin menghapus data Event {{$lk->judul_event}}..?</h4>
													</div>
													<div class="modal-footer">
													<a href="<?php echo url('/informasi/event/destroy/'.$lk->id_event); ?>" class="btn btn-danger btn-sm confirmation" >Hapus Data</a>
														
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													</div>
												</div>
												
												</div>
											</div>
									</td>
								</tr>
								@endforeach
								</tbody>
								</table>
							</div>
							<!-- /.card-body -->
							</div>

						</div> 
                        </div>
				</div>
			
			</div>
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <script>
  $(function () {
    $("#next").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false
    }).buttons().container().appendTo('#next_wrapper .col-md-6:eq(0)');
  });
</script>
@endsection