@extends('layouts.lay')

@section('content')
<div id="banner-area" class="banner-area" style="background-image:url({{url('/constra/images/banner/banner1.jpg')}})">
  <div class="banner-text">
    <div class="container">
        <div class="row">
          <div class="col-lg-12">
              <div class="banner-heading">
                <h1 class="banner-title">Berita</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb justify-content-center">
                      <li class="breadcrumb-item"><a href="<?php echo url("/"); ?>">Beranda</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Berita</li>
                    </ol>
                </nav>
              </div>
          </div><!-- Col end -->
        </div><!-- Row end -->
    </div><!-- Container end -->
  </div><!-- Banner text end -->
</div><!-- Banner area end --> 

<section id="main-container"  class="facts-area">
  <div class="container">

    <div class="row">

	<div class="col-lg-8">

		<div class="container mt-4">
            
            <table id="x" border='0'>
                <thead>
                <tr>
                    <th>&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                <?php 
                $hit3 = count($news); 
                ?>
                @if($hit3 <= 0)
                <div class="call-to-action-btn"><br/>
                <center>
                <h1 class="btn btn-danger">Berita yang Anda Cari Tidak ada! </h1> 
                </center>
                </div>
                @else
                    @foreach($news as $b)
                <tr>
                    <td>
                    <a href='<?php echo url("/berita/detail/$b->id_berita"); ?>'>
                        <h4>{{$b->judul}}</h4>
                    </a>
                    <div class="cropped">
                        @if(empty($b->image_berita))
                        <img loading="lazy" src="{{url('/constra/images/berita.jpg')}}" style="height:480px;width:100%;" class="img-fluid" alt="post-image">
                        @else
                        <img loading="lazy" src="{{asset('storage/images/berita/'.$b->image_berita)}}" style="height:480px;width:100%;" class="img-fluid" alt="post-image">
                        @endif
                    </div>
                    <div class='post_commentbox'>
                        <a href='#'><i class='fa fa-user'></i> {{$b->petugas}} | </a>
                        <span><i class='fa fa-calendar'></i> {{$b->tanggal}} | </span>
                        <a href='#'><i class='fa fa-tags'></i> Berita </a> 
                    </div>
                    
                    {!!$b->ringkasan!!}
                    <h5><a href='<?php echo url("/berita/detail/$b->id_berita"); ?>'> >> Lihat Selengkapnya </a></h5><br/>
                    </td>
                </tr>
                @endforeach
                @endif
                </tbody>
            </table>
            </div>
            <br/>
      <div class="d-flex justify-content-center">
        {!! $news->links() !!}
      </div>
    </div><!-- Col end -->
    <div class="col-lg-4 mt-5 mt-lg-0">
      
    <div class="accordion accordion-default accordion-toggle accordion-style-1 mb-5" data-plugin-sticky data-plugin-options="{'offset_top': 100}" role="tablist">
<br/><br/>
<div class="card">
      <div class="card-header accordion-header" role="tab" id="cari">
      <h5 class="text-5 mb-0">
          <a href="#" data-toggle="collapse" data-target="#toggleCari" aria-expanded="false" aria-controls="toggleCategories">Filter Berita  </a>
      </h5>
      </div>
      <div id="toggleCari" class="accordion-body collapse show" aria-labelledby="cari">
        <div class="card-body">
          <form method="post" action="<?php echo url("/berita/all"); ?>">          
          {{csrf_field()}}
            <div class="form-row">
              <div class="form-group col-4">
                <div class="custom-select-1">
                  <select class="form-control" name="bulan"  required>
                    <option value="0" selected="" disabled="">Bulan</option>
                    <option value="1">Januari</option>
                    <option value="2">Februari</option>
                    <option value="3">Maret</option>
                    <option value="4">April</option>
                    <option value="5">Mei</option>
                    <option value="6">Juni</option>
                    <option value="7">Juli</option>
                    <option value="8">Agustus</option>
                    <option value="9">September</option>
                    <option value="10">Oktober</option>
                    <option value="11">November</option>
                    <option value="12">Desember</option>
                  </select>
                </div>
              </div>
              <div class="form-group col-4">
                <div class="custom-select-1">
                  <select class="form-control" name="tahun"  required>
                    <option value="0" selected="" disabled="">Tahun</option>
                    <?php
                    for($i=date('Y'); $i>=date('Y')-5; $i-=1){
                    ?>
                    <option value="{{$i}}">{{$i}}</option>
                    <?php } ?>
                  </select>
                </div>
              </div>
			  <div class="form-group col-4">
                <div class="custom-select-1">
                  <button class="btn btn-primary btn-rounded font-weight-bold btn-h-2 btn-v-3" type="submit">Submit</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
<div class="card">
  <div class="card-header accordion-header" role="tab" id="popularPosts">
  <h5 class="text-5 mb-0">
      <a href="#" data-toggle="collapse" data-target="#togglePopularPosts" aria-expanded="false" aria-controls="togglePopularPosts">Berita Terkini</a>
  </h5>
  </div>
  <div id="togglePopularPosts" class="accordion-body collapse show" role="tabpanel" aria-labelledby="popularPosts">
    <div class="card-body">
                      <ul class="list-unstyled">
  @foreach($konsul as $ksl)
    <li class="d-flex align-items-center">
      <div class="posts-thumb">
        <a href="#">
        @if(empty($ksl->image_berita))
        <img loading="lazy" src="{{url('/constra/images/berita.jpg')}}" style="height:70px;width:80px;">
        @else
        <img loading="lazy" style="height:70px;width:80px;" alt="news-img" src="{{asset('storage/images/berita/'.$ksl->image_berita)}}">
        @endif
      </a>
      </div>
      <div>&nbsp;</div>
      <div class="post-info">
          <a href="<?php echo url("/berita/detail/$ksl->id_berita"); ?>">
          {{substr($ksl->judul, 0, 50)}}<b>..Selengkapnya</b></a>
      </div>
    </li><!-- 2nd post end-->
    <hr/>
    @endforeach
  </ul>
    </div>
  </div>
</div>
<?php /*
<div class="card">
  <div class="card-header accordion-header" role="tab" id="tags">
  <h5 class="text-5 mb-0">
      <a href="#" data-toggle="collapse" data-target="#toggleTags" aria-expanded="false" aria-controls="toggleTags">Tag Populer</a>
  </h5>
  </div>
  <div id="toggleTags" class="accordion-body collapse show" role="tabpanel" aria-labelledby="tags">
    <div class="card-body">
                      <ul class="list-inline">
                        
                        @foreach($konsul as $ksl)
                          <li class="list-inline-item">
                            <a href="<?php echo url("/berita/detail/$ksl->id_berita"); ?>" class="badge badge-dark btn-primary badge-sm badge-pill px-3 py-2 mb-2">
                            <?php
                            $arr = explode(' ',trim($ksl->judul));
                            echo $arr[1]; 
                            ?>
                            </a>
                          </li>
                        @endforeach
                        </ul>
    </div>
  </div>
</div>
*/ ?>
<div class="card">
  <div class="card-header accordion-header" role="tab" id="tags">
  <h5 class="text-5 mb-0">
      <a href="#" data-toggle="collapse" data-target="#toggleTags" aria-expanded="false" aria-controls="toggleTags">Video</a>
  </h5>
  </div>
  <div id="toggleTags" class="accordion-body collapse show" role="tabpanel" aria-labelledby="tags">
    <div class="card-body">
            <ul class="list-inline">
            @foreach($video as $vdo)
              <li class="list-inline-item">
                <div class="post-info">
                  {!!$vdo->iframe_video!!}
                </div>
              </li><!-- 2nd post end-->
              @endforeach
            </ul>
    </div>
  </div>
</div>
</div>
        
        
		</div>
        </div><!-- Content row end -->      
</section><!-- Main container end -->


<!-- bootstrap5 dataTables js cdn -->
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    
    <script src="{{url('/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    
    <script>
    var jq = $.noConflict();
    jq(function () {
        jq("#next").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false, 'pageLength': 7,
        }).buttons().container().appendTo('#next_wrapper .col-md-6:eq(0)');
    });
    </script>

@endsection