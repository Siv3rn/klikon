@extends('layouts.lay')

@section('content')
<div id="banner-area" class="banner-area" style="background-image:url({{url('/constra/images/banner/banner2.jpg')}})">
  <div class="banner-text">
    <div class="container">
          <div class="col-lg-12">
              <div class="banner-heading">
                <h1 class="banner-title">Agenda</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb justify-content-center">
                      <li class="breadcrumb-item"><a href="<?php echo url("/"); ?>">Beranda</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Agenda</li>
                    </ol>
                </nav>
              </div>
          </div><!-- Col end -->
    </div><!-- Container end -->
  </div><!-- Banner text end -->
</div><!-- Banner area end --> 

<section id="main-container"  class="facts-area">
		<div class="col-lg-12">
        <div class='card'>
        <div class="col-lg-12">
            <br/>
            <style>
                table#next.dataTable tr:hover {
                background-color: #d0ecdc;
                }
                .table-striped>tbody>tr:nth-child(odd)>td, 
                .table-striped>tbody>tr:nth-child(odd)>th {
                background-color: #e8f7f0 ; /* Choose your own color here */
                }                   
                tr.even td
                {background-color:#fff}                        
                </style>
            <form method="post" action="<?php echo url("/agenda/schedulle"); ?>">          
          {{csrf_field()}}
            <div class="form-row">
              <div class="form-group col-2">
                <div class="custom-select-1">
                  <select class="form-control" name="tahun"  required>
                    <option value="0" selected="" disabled="">Tahun</option>
                    <?php
                    for($i=date('Y'); $i>=date('Y')-4; $i-=1){
                    ?>
                    <option value="{{$i}}">{{$i}}</option>
                    <?php } ?>
                  </select>
                </div>
              </div>
			  <div class="form-group col-2">
                <div class="custom-select-1">
                  <button class="btn btn-blue btn-rounded font-weight-bold btn-h-2 btn-v-3" type="submit">Cari</button>
                </div>
              </div>
            </div>
          </form>
				<table id="next" class="table table-striped table-bordered hover" bordercolor="#e0e4e4" width='100%'>
                <thead>
                    <tr>
                        <th rowspan="2" width="30px" style='background:#fff'><center>No</center></th>
                        <th rowspan="2" width="380px" style='background:#fff'><center>Nama Agenda</center></th>
                        <th rowspan="2" width="100px" style='background:#fff'><center>Tanggal</center></th>
                        <th colspan="12"><center>Pelaksanaan</center></th>
                    </tr>
                    <tr style='background:#58b474'>
                    <th ><font color='#fff'><center>Jan</center></font></th>
                                <th ><font color='#fff'><center>Feb</center></font></th>
                                <th ><font color='#fff'><center>Mar</center></font></th>
                                <th ><font color='#fff'><center>Apr</center></font></th>
                                <th ><font color='#fff'><center>Mei</center></font></th>
                                <th ><font color='#fff'><center>Jun</center></font></th>
                                <th ><font color='#fff'><center>Jul</center></font></th>
                                <th ><font color='#fff'><center>Ags</center></font></th>
                                <th ><font color='#fff'><center>Sep</center></font></th>
                                <th ><font color='#fff'><center>Okt</center></font></th>
                                <th ><font color='#fff'><center>Nov</center></font></th>
                                <th ><font color='#fff'><center>Des</center></font></th>
                    </tr>
                </thead>
                <tbody>
                    <?php $no=0; ?>
                    @foreach($tbl_agenda as $ksl)
                    <?php $no++; ?>
                    <tr>
                        <td>
                            {{$no}}
                        </td>
                        <td>
                        <a type="button" data-toggle="modal" data-target="#event{{$ksl->id_agenda}}">
                        @if(strlen($ksl->judul_agenda) > 20)
                        {{substr($ksl->judul_agenda,0,50)}} ...
                        @else
                        {{substr($ksl->judul_agenda,0,50)}}
                        @endif
						<div class="modal fade" id="event{{$ksl->id_agenda}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                            <div class="modal-header">
                            <center>
                            <h4 class="modal-title" id="exampleModalLabel">{{$ksl->judul_agenda}}</h4>
                            </center>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                {!!$ksl->isi_agenda!!}
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                            </div>
                        </div>
                        </div>
                        </td>
                        <td>
                        {{date("d M Y", strtotime($ksl->tgl_agenda))}}
                        </td>
                        <?php
                            $tgl=date("m", strtotime($ksl->tgl_agenda));
                            ?>
                            @if($tgl == '01')
                            <td style='background:orange'>
                            </td>
                            @else
                            <td>
                            </td>
                            @endif
                            @if($tgl == '02')
                            <td style='background:orange'>
                            </td>
                            @else
                            <td>
                            </td>
                            @endif
                            @if($tgl == '03')
                            <td style='background:orange'>
                            </td>
                            @else
                            <td>
                            </td>
                            @endif
                            @if($tgl == '04')
                            <td style='background:orange'>
                            </td>
                            @else
                            <td>
                            </td>
                            @endif
                            @if($tgl == '05')
                            <td style='background:orange'>
                            </td>
                            @else
                            <td>
                            </td>
                            @endif
                            @if($tgl == '06')
                            <td style='background:orange'>
                            </td>
                            @else
                            <td>
                            </td>
                            @endif
                            @if($tgl == '07')
                            <td style='background:orange'>
                            </td>
                            @else
                            <td>
                            </td>
                            @endif
                            @if($tgl == '08')
                            <td style='background:orange'>
                            </td>
                            @else
                            <td>
                            </td>
                            @endif
                            @if($tgl == '09')
                            <td style='background:orange'>
                            </td>
                            @else
                            <td>
                            </td>
                            @endif
                            @if($tgl == '10')
                            <td style='background:orange'>
                            </td>
                            @else
                            <td>
                            </td>
                            @endif
                            @if($tgl == '11')
                            <td style='background:orange'>
                            </td>
                            @else
                            <td>
                            </td>
                            @endif
                            @if($tgl == '12')
                            <td style='background:orange'>
                            </td>
                            @else
                            <td>
                            </td>
                            @endif
                    </tr>
                    @endforeach
                </tbody>
            </table>
		</div>     
		</div>    
		</div>    
</section><!-- Main container end -->

<!-- bootstrap5 dataTables js cdn -->
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
        <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.11.5/js/dataTables.bootstrap.min.js"></script>
        
        <script src="{{url('/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
        <script>
        var jq = $.noConflict();
        jq(function () {
            jq("#next").DataTable({
            'scrollX':true,"lengthChange": false, bFilter: false,'pageLength': 10,
            'scrollCollapse': true, 'paging':true,"ordering": false,
            });
        });
        </script>

@endsection