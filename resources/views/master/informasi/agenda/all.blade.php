@extends('layouts.lay')

@section('content')
<div id="banner-area" class="banner-area" style="background-image:url({{url('/constra/images/banner/banner2.jpg')}})">
  <div class="banner-text">
    <div class="container">
        <div class="row">
          <div class="col-lg-12">
              <div class="banner-heading">
                <h1 class="banner-title">Agenda</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb justify-content-center">
                      <li class="breadcrumb-item"><a href="<?php echo url("/"); ?>">Beranda</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Agenda</li>
                    </ol>
                </nav>
              </div>
          </div><!-- Col end -->
        </div><!-- Row end -->
    </div><!-- Container end -->
  </div><!-- Banner text end -->
</div><!-- Banner area end --> 

<section id="main-container"  class="facts-area">
  <div class="container">

    <div class="row">

    <div class="col-lg-4 mt-5 mt-lg-0">
      <br/>
        <div class="sidebar sidebar-right">
        <div class="widget recent-posts">
            <h3 class="widget-title">Konsultasi Terkini</h3>
            <br/>
            <ul class="list-unstyled">
            @foreach($konsul as $ksl)
              <li class="d-flex align-items-center">
                <div class="posts-thumb">
                  <a href="#"><img loading="lazy" alt="news-img" src="{{url('/constra/images/icon-image/ksl.png')}}"></a>
                </div>
                <div class="post-info">
                  <h4 class="entry-title">
                    <a href="<?php echo url("/konsultasi/detail/$ksl->id_konsultasi"); ?>">{{$ksl->judul_konsultasi}}</a>
                  </h4>
                </div>
              </li><!-- 2nd post end-->
              @endforeach
            </ul>
                <div class="call-to-action-btn">
                 <a class="btn btn-blue" href="<?php echo url("/konsultasi/tanya"); ?>">Kirim Pertanyaan</a>
                </div>
        </div><!-- Recent post end -->
        </div><!-- Sidebar end -->

        <div class="sidebar sidebar-right">
        <div class="widget recent-posts">
          <hr/>
        </div><!-- Recent post end -->
        </div><!-- Sidebar end -->

        <div class="sidebar sidebar-right">
        <div class="widget recent-posts">
            <h3 class="widget-title">Video</h3>
            <ul class="list-unstyled">
            @foreach($video as $vdo)
              <li class="d-flex align-items-center">
                <div class="post-info">
                  {!!$vdo->iframe_video!!}
                </div>
              </li><!-- 2nd post end-->
              @endforeach
            </ul>
        </div><!-- Recent post end -->
        </div><!-- Sidebar end -->

    </div><!-- Col end -->
		<div class="col-lg-8">
        <div class="container mt-4">
            
            <table id="next" border='0'>
                <thead>
                <tr>
                    <th>&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($tbl_agenda as $ksl)
                <tr>
                    <td>
                    <div class='post_commentbox'>
                        <span><i class='fa fa-calendar'></i> {{$ksl->tgl_agenda}} | </span>
                        <a href='#'><i class='fa fa-tags'></i> Agenda </a> 
                    </div>
                    <a href='<?php echo url("/agenda/detail/$ksl->id_agenda"); ?>'>
                        <h5>{{$ksl->judul_agenda}}</h5>
                    </a>
                    {!!$ksl->ringkasan_agenda!!}
                    <h5><a href='<?php echo url("/agenda/detail/$ksl->id_agenda"); ?>'> >> Lihat Selengkapnya </a></h5><br/>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
            </div>
		</div>
        </div><!-- Content row end -->      
</section><!-- Main container end -->

<!-- bootstrap5 dataTables js cdn -->
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    
    <script src="{{url('/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    
    <script>
    var jq = $.noConflict();
    jq(function () {
        jq("#next").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false, 'pageLength': 7,
        }).buttons().container().appendTo('#next_wrapper .col-md-6:eq(0)');
    });
    </script>

@endsection