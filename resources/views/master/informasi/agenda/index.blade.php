@extends('layouts.lay2')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-12">
        @if(!empty(\Session::get('success')) > 0)
                    <div class="alert alert-success">     
                        <strong>Sukses!</strong> {!! \Session::get('success') !!}             
                        </div>
                    @endif
                <script>
                    window.setTimeout(function() {
                    $.noConflict();
                    $(".alert").fadeTo(500, 0).slideUp(500, function(){
                        $(this).remove(); 
                    });
                    }, 5000);
                </script>
                <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
                <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
                </div>
                <div class="col-sm-6">
            <h1 class="m-0">Agenda</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/home">Dashboard</a></li>
              <li class="breadcrumb-item"><a href="/informasi/agenda">Informasi</a></li>
              <li class="breadcrumb-item active">Agenda</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
                <!-- Small boxes (Stat box) -->
        <div class="row">
					<div class="col-12">
							<div class="card">
							<div class="card-header">
										<h3 class="card-title">Agenda | &nbsp;</h3>
						   
							<a class="btn btn-info btn-sm" href="<?php echo url('informasi/agenda/add'); ?>">
											<i class="fas fa-plus"> Add
											</i>
										</a>
							</div>
											
							<!-- /.card-header -->
							<div class="card-body">
								<table id="next" class="table table-bordered table-hover">
								<thead>
								<tr>
									<th>No</th>
									<th>Tanggal</th>
									<th>Judul</th>
									<th>Ringkasan</th>
									<th>Deskripsi</th>
									<th style="width:10%">Aksi</th>
								</tr>
								</thead>
								<tbody>
								<?php $no=0; ?> 
							@foreach($agenda as $lk)
							<?php $no++; ?>
								<tr>
									<td>{{$no}}</td>
									<td>{{$lk->tgl_agenda}}</td>
									<td>{{$lk->judul_agenda}}</td>
									<td>{!!$lk->ringkasan_agenda!!}</td>
									<td>{!!$lk->isi_agenda!!}</td>
									<td>
									<a class="btn btn-info btn-sm" href="<?php echo url('/informasi/agenda/edit/'.$lk->id_agenda); ?>">
											<i class="fas fa-edit">
											</i>
										</a>
										<button type="button" class="btn btn-danger btn-sm confirmation" data-toggle="modal" data-target="#myModal{{$lk->id_agenda}}"><i class="fas fa-trash"></i></button>
										<!-- Modal -->
                                                <div class="modal fade" id="myModal{{$lk->id_agenda}}" role="dialog">
												<div class="modal-dialog">
												
												<!-- Modal content-->
												<div class="modal-content">
													<div class="modal-header">
													<h4 class="modal-title">Apakah Anda Yakin menghapus data Link {{$lk->judul_agenda}}..?</h4>
													</div>
													<div class="modal-footer">
													<a href="<?php echo url('/informasi/agenda/destroy/'.$lk->id_agenda); ?>" class="btn btn-danger btn-sm confirmation" >Hapus Data</a>
														
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													</div>
												</div>
												
												</div>
											</div>
									</td>
								</tr>
								@endforeach
								</tbody>
								</table>
							</div>
							<!-- /.card-body -->
							</div>

						</div> 
                        </div>
				</div>
			
			</div>
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <script>
  $(function () {
    $("#next").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false
    }).buttons().container().appendTo('#next_wrapper .col-md-6:eq(0)');
  });
</script>
@endsection