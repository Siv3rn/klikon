@extends('layouts.lay')

@section('content')

<link href="{{url('v2/assets/css/pdk.css')}}" rel="stylesheet" />
<br/><br/>	
<section id="main-container"  class="banner-area">
    <div class="row">
          <div class="col-lg-6">
            @foreach ($map as $mp)
              <center><h4>{{$mp->judul_peta}}</h4></center>
            <div class="card">
            {!!$mp->iframe_peta!!}  
            </div>
            @endforeach
          </div><!-- Col end -->
		  <div class="col-lg-6">
            @foreach ($map2 as $mp2)
              <center><h4>{{$mp2->judul_peta}}</h4></center>
            <div class="card">
            {!!$mp2->iframe_peta!!}  
            </div>
            @endforeach
          </div><!-- Col end -->
        </div><!-- Row end -->     
  </section><!-- Main container end -->
  
@endsection