@extends('layouts.lay')

@section('content')
<br/>

<section id="main-container"  class="facts-area">
  <div class="container">

    <div class="row">

    <div class="col-lg-3 mt-5 mt-lg-0">
      <br/>
        <div class="sidebar sidebar-right">
        <div class="widget recent-posts">
            <h3 class="widget-title">Unduh</h3>
            <ul class="list-unstyled">
			
			<a href="<?php echo url("/unduh/poster/all"); ?>">
              <li class="d-flex align-items-center">
				<div class="posts-thumb">
                  <img src="{{url('images/poster.png')}}" width="80px">
                </div>
				<div class="info-thumb">
                  <h5>Poster</h5>
                </div>
              </li><!-- 2nd post end-->
			  </a>
			  <hr/>
			  <a href="<?php echo url("/unduh/buku/all"); ?>">
              <li class="d-flex align-items-center">
				<div class="posts-thumb">
                  <img src="{{url('images/book.png')}}" width="80px">
                </div>
				<div class="info-thumb">
                  <h5>Buku</h5>
                </div>
              </li><!-- 2nd post end-->
			  </a>
			  <hr/>
			  <a href="<?php echo url("/unduh/peraturan/all"); ?>">
              <li class="d-flex align-items-center">
				<div class="posts-thumb">
                  <img src="{{url('images/uud.png')}}" width="80px">
                </div>
				<div class="info-thumb">
                  <h5>Peraturan</h5>
                </div>
              </li><!-- 2nd post end-->
			  </a>
			  <hr/>
			  <a href="<?php echo url("/unduh/spesifikasi/all"); ?>">
              <li class="d-flex align-items-center">
				<div class="posts-thumb">
                  <img src="{{url('images/st.png')}}" width="80px">
                </div>
				<div class="info-thumb">
                  <h5>Spesifikasi</h5>
                </div>
              </li><!-- 2nd post end-->
			  </a>
			  <hr/>
			  <a href="<?php echo url("/unduh/sk/all"); ?>">
              <li class="d-flex align-items-center">
				<div class="posts-thumb">
                  <img src="{{url('images/sk.png')}}" width="80px">
                </div>
				<div class="info-thumb">
                  <h5>SK HBB</h5>
                </div>
              </li><!-- 2nd post end-->
			  </a>
			  <hr/>
			  <a href="<?php echo url("/unduh/covid/all"); ?>">
              <li class="d-flex align-items-center">
				<div class="posts-thumb">
                  <img src="{{url('images/covid.png')}}" width="80px">
                </div>
				<div class="info-thumb">
                  <h5>Covid</h5>
                </div>
              </li><!-- 2nd post end-->
			  </a>
			  
			  
            </ul>
        </div><!-- Recent post end -->
        </div><!-- Sidebar end -->

    </div><!-- Col end -->
		
      <div class="col-lg-9">
      <div class="row">  
      <div class="shuffle-btn-group">
          <label class="active" for="all">
            <input type="radio" name="shuffle-filter" id="all" value="all" checked="checked">Spesifikasi
          </label>
        </div><!-- project filter end -->
        <style>
                        table#next.dataTable tr:hover {
                        background-color: #d0ecdc;
                        }
                        .table-striped>tbody>tr:nth-child(odd)>td, 
                        .table-striped>tbody>tr:nth-child(odd)>th {
                            background-color: #e8f7f0 ; /* Choose your own color here */
                        }                       
                    </style>      
        <table id="next" class="table table-striped table-bordered hover" bordercolor="#e0e4e4">
								<thead>
								<tr>
									<th>No</th>
									<th>Judul</th>
									<th>File</th>
								</tr>
								</thead>
								<tbody>
								<?php $no=0; ?> 
							@foreach($peraturan as $lk)
							<?php $no++; ?>
								<tr>
									<td>{{$no}}</td>
									<td>{{$lk->judul_peraturan}}</td>
									<td>
									<a type='button' class='btn btn-primary' href="{{asset('storage/images/peraturan/'.$lk->file_peraturan)}}" alt="{{$lk->file_peraturan}}"  target='_blank'>Download</a>
									</td>
								</tr>
								@endforeach
								</tbody>
								<tfoot>
								<tr>
									<th>No</th>
									<th>Judul</th>
									<th>Deskripsi</th>
									<th>File</th>
								</tr>
								</tfoot>
								</table>

		</div>
        </div><!-- Content row end --> 
	</div>
	</div>
</section><!-- Main container end -->

<!-- bootstrap5 dataTables js cdn -->
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    
    <script src="{{url('/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    
    <script>
    var jq = $.noConflict();
    jq(function () {
        jq("#next").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false, 'pageLength': 5,
        }).buttons().container().appendTo('#next_wrapper .col-md-6:eq(0)');
    });
    </script>

@endsection