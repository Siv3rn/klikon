@extends('layouts.lay')

@section('content')
<br/>
<section id="main-container"  class="facts-area">
  <div class="container">

    <div class="row">

    <div class="col-lg-3 mt-5 mt-lg-0">
      <br/>
        <div class="sidebar sidebar-right">
        <div class="widget recent-posts">
            <h3 class="widget-title">Unduh</h3>
            <ul class="list-unstyled">
			
			<a href="<?php echo url("/unduh/poster/all"); ?>">
              <li class="d-flex align-items-center">
				<div class="posts-thumb">
                  <img src="{{url('images/poster.png')}}" width="80px">
                </div>
				<div class="info-thumb">
                  <h5>Poster</h5>
                </div>
              </li><!-- 2nd post end-->
			  </a>
			  <hr/>
			  <a href="<?php echo url("/unduh/buku/all"); ?>">
              <li class="d-flex align-items-center">
				<div class="posts-thumb">
                  <img src="{{url('images/book.png')}}" width="80px">
                </div>
				<div class="info-thumb">
                  <h5>Buku</h5>
                </div>
              </li><!-- 2nd post end-->
			  </a>
			  <hr/>
			  <a href="<?php echo url("/unduh/peraturan/all"); ?>">
              <li class="d-flex align-items-center">
				<div class="posts-thumb">
                  <img src="{{url('images/uud.png')}}" width="80px">
                </div>
				<div class="info-thumb">
                  <h5>Peraturan</h5>
                </div>
              </li><!-- 2nd post end-->
			  </a>
			  <hr/>
			  <a href="<?php echo url("/unduh/spesifikasi/all"); ?>">
              <li class="d-flex align-items-center">
				<div class="posts-thumb">
                  <img src="{{url('images/st.png')}}" width="80px">
                </div>
				<div class="info-thumb">
                  <h5>Spesifikasi</h5>
                </div>
              </li><!-- 2nd post end-->
			  </a>
			  <hr/>
			  <a href="<?php echo url("/unduh/sk/all"); ?>">
              <li class="d-flex align-items-center">
				<div class="posts-thumb">
                  <img src="{{url('images/sk.png')}}" width="80px">
                </div>
				<div class="info-thumb">
                  <h5>SK HBB</h5>
                </div>
              </li><!-- 2nd post end-->
			  </a>
			  <hr/>
			  <a href="<?php echo url("/unduh/covid/all"); ?>">
              <li class="d-flex align-items-center">
				<div class="posts-thumb">
                  <img src="{{url('images/covid.png')}}" width="80px">
                </div>
				<div class="info-thumb">
                  <h5>Covid</h5>
                </div>
              </li><!-- 2nd post end-->
			  </a>
			  
			  
            </ul>
        </div><!-- Recent post end -->
        </div><!-- Sidebar end -->

    </div><!-- Col end -->
		
        <div class="col-lg-9">
      <div class="row">  
      <div class="shuffle-btn-group">
          <label class="active" for="all">
            <input type="radio" name="shuffle-filter" id="all" value="all" checked="checked">Buku
          </label>
        </div><!-- project filter end -->

        @foreach($buku as $lk)
        @if(empty($buku))
        <h1>Mohon maaf buku telum tersedia untuk saat ini</h1>
        @else

        <div class="col-2">
                <div class="mt-3 item">
                <center>
                
                <?php
                    $file = DB::table('buku')
                    ->select('*')
                    ->where('id','=',$lk->id)
                    ->get();
                    ?>
                    @foreach ($file as $f) 
                    <a href="{{asset('storage/images/buku/'.$f->file_buku)}}" target='_blank'>
                    <img src="{{url('images/buku.png')}}" width="70px"><br/>
                    <span class="h6 font-weight-bold mb-0">
                    {{$f->judul_buku}} 
                    </span>
                    </a>
                    @endforeach
                  
                </center>               
                </div>
        </div>
        @endif
        @endforeach

		</div>
        </div><!-- Content row end --> 
	</div>
	</div>
</section><!-- Main container end -->

<!-- bootstrap5 dataTables js cdn -->
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    
    <script src="{{url('/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    
    <script>
    var jq = $.noConflict();
    jq(function () {
        jq("#next").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false, 'pageLength': 5,
        }).buttons().container().appendTo('#next_wrapper .col-md-6:eq(0)');
    });
    </script>

@endsection