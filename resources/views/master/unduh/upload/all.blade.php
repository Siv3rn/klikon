@extends('layouts.lay')

@section('content')
<br/><br/>
<section id="main-container"  class="facts-area">
    <div class="row">
      <div class="col-lg-12">
  <div class="card-body">
  <div class="card">
  <div class="card-body">
      <div class="shuffle-btn-group">
          <label class="active" for="all">
            <input type="radio" name="shuffle-filter" id="all" value="all" checked="checked">Unduh HBB
          </label>
        </div><!-- project filter end -->
        <style>
                        table#next.dataTable tr:hover {
                        background-color: #d0ecdc;
                        }
                        .table-striped>tbody>tr:nth-child(odd)>td, 
                        .table-striped>tbody>tr:nth-child(odd)>th {
                            background-color: #e8f7f0 ; /* Choose your own color here */
                        }                       
                    </style>      
        <table id="next" class="table table-striped table-bordered hover" bordercolor="#e0e4e4">
								<thead>
								<tr>
									<th style="width:5%">No</th>
									<th>Toko</th>
									<th>Alamat</th>
									<th>Kabupaten</th>
									<th>Tanggal Update</th>
									<th style="width:25%;  text-align:left">File</th>
								</tr>
								</thead>
								<tbody>
								<?php $no=0; ?> 
							@foreach($unduh as $lk)
							<?php $no++; ?>
                            <tr>
									<td>{{$no}}</td>
									<td>{{$lk->nama_toko}}</td>
									<td>{{$lk->alamat_toko}}</td>
									<td>{{$lk->kabupaten}}</td>
									<td>{{$lk->update_harga}}</td>
                                    <td>
									<a href="{{asset('storage/images/harga/'.$lk->file_harga)}}" alt="{{$lk->file_harga}}"  target='_blank'>{{$lk->file_harga}}</a>
									</td>
                            </tr>
								@endforeach
								</tbody>
								</table>
	</div>
	</div>
	</div>
        </div><!-- Content row end --> 
	</div>
</section><!-- Main container end -->

<!-- bootstrap5 dataTables js cdn -->
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    
    <script src="{{url('/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    
    <script>
    var jq = $.noConflict();
    jq(function () {
        jq("#next").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false, 'pageLength': 10,
        }).buttons().container().appendTo('#next_wrapper .col-md-6:eq(0)');
    });
    </script>

@endsection