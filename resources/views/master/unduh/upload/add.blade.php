@extends('layouts.lay2')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Upload Harga</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/home">Dashboard</a></li>
              <li class="breadcrumb-item"><a href="/unduh/buku">Unduh</a></li>
              <li class="breadcrumb-item active">Upload Harga Add</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
                <!-- Small boxes (Stat box) -->
        <div class="row">
          
        <div class="col-12">
            <div class="card">
              <div class="card-header">
                        <h3 class="card-title">Upload Harga | Add</h3>
              </div>
              
              <!-- /.card-header -->
              @if(count($errors) > 0)
              <div class="alert alert-danger" role="alert">
                @foreach ($errors->all() as $error)
                {{ $error }} <br/>
                @endforeach
              </div>
              @endif
              <script>
                window.setTimeout(function() {
                  $.noConflict();
                  $(".alert").fadeTo(500, 0).slideUp(500, function(){
                    $(this).remove(); 
                  });
                }, 5000);
              </script>
              <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
              <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
    
                
              <form class="form" action="<?php echo url("/layanan/unduh/create"); ?>" method="POST" enctype="multipart/form-data">
                <!-- /.card-header -->
                {{csrf_field()}}
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Nama Toko *</label>
                        <input type="text" class="form-control" id="nama_toko" name="nama_toko" placeholder="Nama Toko" required>
                      </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Update Upload*</label>
                        <input type="text" class="form-control" id="update_harga" name="update_harga" placeholder='cth : April 2019' required>
                      </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-md-3">
                      <div class="form-group">
                            <label>Upload Harga *</label>
                            <input type="file" class="form-control" id="file" name="file" placeholder="Upload File Harga" required>                  
                        </div>
                      <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                    <div class="col-lg-6">
					<div class="form-group">
                    <label><font color='#000'><b>Kabupaten/Kota</b></font></label>
					  <div class="col-xs-2 selectContainer">
						  <select class="form-control" name="id_kota" id="kota">
							  <option value='0' >--Pilih Kabupaten/Kota.--</option>
								@foreach ($kota as $item)
								<option value="{{ $item->id_kota }}">{{ $item->kota }}</option>
								@endforeach
						  </select>
					  </div>
					  </div>
					  </div>
					
					<div class="col-lg-6">
					<div class="form-group">
                    <label><font color='#000'><b>Kecamatan</b></font></label>
					  <select class="form-control" name="id_kecamatan" id="id_kecamatan"></select>
					</div>
					</div>
                    
					<div class="col-12 col-sm-12">
                      <div class="form-group">
                          <button type="submit" class="btn btn-primary">Submit</button>       
                      </div>
                      <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                  </div>
                  <!-- /.row -->
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  Klinik Konstruksi 
                </div>
              </div>
              <!-- /.card -->
              </form>
              <!-- /.card-body -->
            </div>
        </div>
               
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
        <script>
            $(document).ready(function() {
            $('#kota').on('change', function() {
               var kotaID = $(this).val();
               if(kotaID) {
                   $.ajax({
                       url: '/kecamatan/'+kotaID,
                       type: "post",
                       data : {"_token":"{{ csrf_token() }}"},
                       dataType: "json",
                       success:function(data)
                       {
                         if(data){
                            $('#id_kecamatan').empty();
                            $('#id_kecamatan').append('<option value = "0" >--Pilih Kecamatan.--</option>'); 
                            $.each(data, function(key, id_kecamatan){
                                $('select[name="id_kecamatan"]').append('<option value="'+ id_kecamatan.nama_kecamatan +'">' + id_kecamatan.nama_kecamatan+ '</option>');
                            });
                        }else{
                            $('#id_kecamatan').empty();
                        }
                     }
                   });
               }else{
                 $('#id_kecamatan').empty();
               }
            });
            });
        </script>
@endsection