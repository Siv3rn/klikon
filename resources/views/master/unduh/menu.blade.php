@extends('layouts.lay')

@section('content')

<link rel="stylesheet" href="{{url('constra/css/menu.css')}}">
<div id="banner-area" class="banner-area" style="background-image:url({{url('/constra/images/banner/banner2.jpg')}})">
  <div class="banner-text">
    <div class="container">
        <div class="row">
          <div class="col-lg-12">
              <div class="banner-heading">
                <h1 class="banner-title">Unduh</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb justify-content-center">
                      <li class="breadcrumb-item"><a href="<?php echo url("/"); ?>">Beranda</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Unduh</li>
                    </ol>
                </nav>
              </div>
          </div><!-- Col end -->
        </div><!-- Row end -->
    </div><!-- Container end -->
  </div><!-- Banner text end -->
</div><!-- Banner area end --> 

<section id="main-container"  class="facts-area">
  <div class="container">
		<div class="col-lg-12">
				  
				<div class="shuffle-btn-group">
				  <label class="active" for="all">
					<input type="radio" name="shuffle-filter" id="all" value="all" checked="checked">Unduh
				  </label>
				</div><!-- project filter end -->
				<!-- Menu -->
				<table border='0'>
					<tr>
						<td>
						<div class='col-2 item'>
								<a class='' href="<?php echo url("/unduh/poster/all"); ?>">
								<img src="{{url('images/poster.png')}}" width="70px">
								<center>
								&nbsp;&nbsp;&nbsp;<b>Poster</b>
								</center>
								</a>
						</div>
						</td>
						<td>
						<div class='col-2 item'>
								<a class='' href="<?php echo url("/unduh/buku/all"); ?>">
								<img src="{{url('images/book.png')}}" width="70px">
								<center>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Buku</b>
								</center>
								</a>
						</div>
						</td>
						<td>
						<div class='col-2 item'>
								<a class='' href="<?php echo url("/unduh/peraturan/all"); ?>">
								<img src="{{url('images/uud.png')}}" width="72px">
								<center>
								&nbsp;&nbsp;<b>Peraturan</b>
								</center>
								</a>
						</div>
						</td>
						<td>
						<div class='col-2 item'>
								<a class='' href="<?php echo url("/unduh/spesifikasi/all"); ?>">
								<img src="{{url('images/st.png')}}" width="66px">
								<center>
								<b>Spesifikasi</b>
								</center>
								</a>
						</div>
						</td>
						<td>
						<div class='col-2 item'>
								<a class='' href="<?php echo url("/unduh/sk/all"); ?>">
								<img src="{{url('images/sk.png')}}" width="66px">
								<center>
								&nbsp;&nbsp;<b>SK_HBB</b>
								</center>
								</a>
						</div>
						</td>
						<td>
						<div class='col-2 item'>
								<a class='' href="<?php echo url("/unduh/covid/all"); ?>">
								<img src="{{url('images/covid.png')}}" width="80px">
								</a>
						</div>
						</td>
					</tr>
				</table>
				
	    </div>
    </div><!-- Content row end -->      
</section><!-- Main container end -->

<!-- bootstrap5 dataTables js cdn -->
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    
    <script src="{{url('/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    
    <script>
    var jq = $.noConflict();
    jq(function () {
        jq("#next").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
        }).buttons().container().appendTo('#next_wrapper .col-md-6:eq(0)');
    });
    </script>

@endsection