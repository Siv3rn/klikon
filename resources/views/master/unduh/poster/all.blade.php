@extends('layouts.lay')
<style> 

</style>
@section('content')
<style> 
.gallery-block.grid-gallery{
  padding-bottom: 60px;
  padding-top: 60px;
}

.gallery-block.grid-gallery .heading{
    margin-bottom: 50px;
    text-align: center;
}

.gallery-block.grid-gallery .heading h2{
    font-weight: bold;
    font-size: 1.4rem;
    text-transform: uppercase;
}

.gallery-block.grid-gallery a:hover{
  opacity: 0.8;
}

.gallery-block.grid-gallery .item img{
  box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.15);
  transition: 0.4s;
}

.gallery-block.grid-gallery .item{
  margin-bottom: 20px;
}

@media (min-width: 576px) {

  .gallery-block.grid-gallery .scale-on-hover:hover{
    transform: scale(1.05);
    box-shadow: 0px 10px 10px rgba(0, 0, 0, 0.15) !important;
  }
}
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css" />


<section id="main-container"  class="facts-area">
  <div class="container">

    <div class="row">

    <div class="col-lg-3 mt-5 mt-lg-0">
      <br/>
        <div class="sidebar sidebar-right">
        <div class="widget recent-posts">
            <h3 class="widget-title">Unduh</h3>
            <ul class="list-unstyled">
			
			<a href="<?php echo url("/unduh/poster/all"); ?>">
              <li class="d-flex align-items-center">
				<div class="posts-thumb">
                  <img src="{{url('images/poster.png')}}" width="80px">
                </div>
				<div class="info-thumb">
                  <h5>Poster</h5>
                </div>
              </li><!-- 2nd post end-->
			  </a>
			  <hr/>
			  <a href="<?php echo url("/unduh/buku/all"); ?>">
              <li class="d-flex align-items-center">
				<div class="posts-thumb">
                  <img src="{{url('images/book.png')}}" width="80px">
                </div>
				<div class="info-thumb">
                  <h5>Buku</h5>
                </div>
              </li><!-- 2nd post end-->
			  </a>
			  <hr/>
			  <a href="<?php echo url("/unduh/peraturan/all"); ?>">
              <li class="d-flex align-items-center">
				<div class="posts-thumb">
                  <img src="{{url('images/uud.png')}}" width="80px">
                </div>
				<div class="info-thumb">
                  <h5>Peraturan</h5>
                </div>
              </li><!-- 2nd post end-->
			  </a>
			  <hr/>
			  <a href="<?php echo url("/unduh/spesifikasi/all"); ?>">
              <li class="d-flex align-items-center">
				<div class="posts-thumb">
                  <img src="{{url('images/st.png')}}" width="80px">
                </div>
				<div class="info-thumb">
                  <h5>Spesifikasi</h5>
                </div>
              </li><!-- 2nd post end-->
			  </a>
			  <hr/>
			  <a href="<?php echo url("/unduh/sk/all"); ?>">
              <li class="d-flex align-items-center">
				<div class="posts-thumb">
                  <img src="{{url('images/sk.png')}}" width="80px">
                </div>
				<div class="info-thumb">
                  <h5>SK HBB</h5>
                </div>
              </li><!-- 2nd post end-->
			  </a>
			  <hr/>
			  <a href="<?php echo url("/unduh/covid/all"); ?>">
              <li class="d-flex align-items-center">
				<div class="posts-thumb">
                  <img src="{{url('images/covid.png')}}" width="80px">
                </div>
				<div class="info-thumb">
                  <h5>Covid</h5>
                </div>
              </li><!-- 2nd post end-->
			  </a>
			  
			  
            </ul>
        </div><!-- Recent post end -->
        </div><!-- Sidebar end -->

    </div><!-- Col end -->
		
      <div class="col-lg-9">
		  <div class="shuffle-btn-group">
          <label class="active" for="all">
            <input type="radio" name="shuffle-filter" id="all" value="all" checked="checked">Poster
          </label>
		  </div><!-- project filter end -->


      <div class="col-lg-12">
        <div class="row shuffle-wrapper">

        <div class="col-1 shuffle-sizer"></div>
			@foreach($poster as $pst) 
          <div class="col-lg-3 col-md-12 shuffle-item" data-groups="[&quot;poster&quot;,&quot;poster&quot;]">
            <div class="project-img-container card-body">
              
             <a class="gallery-popup" width='100%' href="{{asset('storage/images/poster/'.$pst->file_poster)}}">
                <img class="img-fluid" src="{{asset('storage/images/poster/'.$pst->file_poster)}}" alt="project-image">
                <span class="gallery-icon"><i class="fa fa-plus"></i></span>
              </a>
              
              <div class="project-item-info">
                <div class="project-item-info-content">
                  <h3 class="project-item-title">
                    <a href="#">{{$pst->judul_poster}}</a>
                  </h3>
                </div>
              </div>
              
            </div>
          </div><!-- shuffle item 1 end -->
			@endforeach
</div>
        </div><!-- shuffle end --><br/>
        <div class="d-flex justify-content-center">
          {!! $poster->links() !!}
        </div>
		</div> 
	</div>
	</div>
</section><!-- Main container end -->

        <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
        <script>
            baguetteBox.run('.grid-gallery', { animation: 'slideIn'});
        </script>

@endsection