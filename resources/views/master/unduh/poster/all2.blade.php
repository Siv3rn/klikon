@extends('layouts.lay')

@section('content')
<div id="banner-area" class="banner-area" style="background-image:url({{url('/constra/images/banner/banner2.jpg')}})">
  <div class="banner-text">
    <div class="container">
        <div class="row">
          <div class="col-lg-12">
              <div class="banner-heading">
                <h1 class="banner-title">Poster</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb justify-content-center">
                      <li class="breadcrumb-item"><a href="<?php echo url("/"); ?>">Beranda</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Poster</li>
                    </ol>
                </nav>
              </div>
          </div><!-- Col end -->
        </div><!-- Row end -->
    </div><!-- Container end -->
  </div><!-- Banner text end -->
</div><!-- Banner area end --> 

<section id="main-container"  class="facts-area">
  <div class="container">

    <div class="row">

    <div class="col-lg-3 mt-5 mt-lg-0">
      <br/>
        <div class="sidebar sidebar-right">
        <div class="widget recent-posts">
            <h3 class="widget-title">Unduh</h3>
            <ul class="list-unstyled">
			
			<a href="<?php echo url("/unduh/poster/all"); ?>">
              <li class="d-flex align-items-center">
				<div class="posts-thumb">
                  <img src="{{url('images/poster.png')}}" width="80px">
                </div>
				<div class="info-thumb">
                  <h5>Poster</h5>
                </div>
              </li><!-- 2nd post end-->
			  </a>
			  <hr/>
			  <a href="<?php echo url("/unduh/buku/all"); ?>">
              <li class="d-flex align-items-center">
				<div class="posts-thumb">
                  <img src="{{url('images/book.png')}}" width="80px">
                </div>
				<div class="info-thumb">
                  <h5>Buku</h5>
                </div>
              </li><!-- 2nd post end-->
			  </a>
			  <hr/>
			  <a href="<?php echo url("/unduh/peraturan/all"); ?>">
              <li class="d-flex align-items-center">
				<div class="posts-thumb">
                  <img src="{{url('images/uud.png')}}" width="80px">
                </div>
				<div class="info-thumb">
                  <h5>Peraturan</h5>
                </div>
              </li><!-- 2nd post end-->
			  </a>
			  <hr/>
			  <a href="<?php echo url("/unduh/spesifikasi/all"); ?>">
              <li class="d-flex align-items-center">
				<div class="posts-thumb">
                  <img src="{{url('images/st.png')}}" width="80px">
                </div>
				<div class="info-thumb">
                  <h5>Spesifikasi</h5>
                </div>
              </li><!-- 2nd post end-->
			  </a>
			  <hr/>
			  <a href="<?php echo url("/unduh/sk/all"); ?>">
              <li class="d-flex align-items-center">
				<div class="posts-thumb">
                  <img src="{{url('images/sk.png')}}" width="80px">
                </div>
				<div class="info-thumb">
                  <h5>SK HBB</h5>
                </div>
              </li><!-- 2nd post end-->
			  </a>
			  <hr/>
			  <a href="<?php echo url("/unduh/covid/all"); ?>">
              <li class="d-flex align-items-center">
				<div class="posts-thumb">
                  <img src="{{url('images/covid.png')}}" width="80px">
                </div>
				<div class="info-thumb">
                  <h5>Covid</h5>
                </div>
              </li><!-- 2nd post end-->
			  </a>
			  
			  
            </ul>
        </div><!-- Recent post end -->
        </div><!-- Sidebar end -->

    </div><!-- Col end -->
		
      <div class="col-lg-8">
		  <div class="shuffle-btn-group">
          <label class="active" for="all">
            <input type="radio" name="shuffle-filter" id="all" value="all" checked="checked">Poster
          </label>
		  </div><!-- project filter end -->


        <div class="row shuffle-wrapper">
          <div class="col-1 shuffle-sizer"></div>
			@foreach($poster as $pst) 
          <div class="col-lg-3 col-md-6 shuffle-item" data-groups="[&quot;poster&quot;,&quot;poster&quot;]">
            <div class="project-img-container">
              <a class="gallery-popup" width='100%' href="{{asset('storage/images/poster/'.$pst->file_poster)}}">
                <img class="img-fluid" src="{{asset('storage/images/poster/'.$pst->file_poster)}}" alt="project-image">
                <span class="gallery-icon"><i class="fa fa-plus"></i></span>
              </a>
              <div class="project-item-info">
                <div class="project-item-info-content">
                  <h3 class="project-item-title">
                    <a href="#">{{$pst->judul_poster}}</a>
                  </h3>
                </div>
              </div>
            </div>
          </div><!-- shuffle item 1 end -->
			@endforeach
        </div><!-- shuffle end -->

		</div> 
	</div>
	</div>
</section><!-- Main container end -->

<!-- bootstrap5 dataTables js cdn -->
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    
    <script src="{{url('/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    
    <script>
    var jq = $.noConflict();
    jq(function () {
        jq("#next").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false, 'pageLength': 5,
        }).buttons().container().appendTo('#next_wrapper .col-md-6:eq(0)');
    });
    </script>

@endsection