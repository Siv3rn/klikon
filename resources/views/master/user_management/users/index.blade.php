@extends('layouts.lay2')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-12">
        @if(!empty(\Session::get('success')) > 0)
                    <div class="alert alert-success">     
                        <strong>Sukses!</strong> {!! \Session::get('success') !!}             
                        </div>
                    @endif
                <script>
                    window.setTimeout(function() {
                    $.noConflict();
                    $(".alert").fadeTo(500, 0).slideUp(500, function(){
                        $(this).remove(); 
                    });
                    }, 5000);
                </script>
                <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
                <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
                </div>
                <div class="col-sm-6">
            <h1 class="m-0">Users Management</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/home">Dashboard</a></li>
              <li class="breadcrumb-item"><a href="/usersmanagement/users">User Management</a></li>
              <li class="breadcrumb-item active">Users</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
                <!-- Small boxes (Stat box) -->
        <div class="row">
        <div class="col-12 col-sm-12">
            <div class="card card-primary card-outline card-outline-tabs">
              <div class="card-header p-0 pt-1">
                <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">

                  <li class="nav-item">
                    <a class="nav-link active" id="custom-tabs-one-home-tab" data-toggle="pill" href="#custom-tabs-one-home" role="tab" aria-controls="custom-tabs-one-home" aria-selected="true">User</a>
                  </li>

                  <li class="nav-item">
                    <a class="nav-link" id="custom-tabs-one-profile-tab" data-toggle="pill" href="#custom-tabs-one-profile" role="tab" aria-controls="custom-tabs-one-profile" aria-selected="false">Roles</a>
                  </li>
                  
                  
                </ul>
              </div>
              <div class="card-body">
                <div class="tab-content" id="custom-tabs-one-tabContent">

                  <div class="tab-pane fade show active" id="custom-tabs-one-home" role="tabpanel" aria-labelledby="custom-tabs-one-home-tab">
                  <div class="card">
							<div class="card-header">
										<h3 class="card-title">Users | &nbsp;</h3>
						   
							<a class="btn btn-info btn-sm" href="<?php echo url('usersmanagement/users/register'); ?>">
											<i class="fas fa-plus"> Add
											</i>
										</a>

							</div>

							
											
							<!-- /.card-header -->
							<div class="card-body">
								<table id="next" class="table table-bordered table-hover">
								<thead>
								<tr>
									<th>No</th>
									<th>Role</th>
									<th>Name</th>
									<th>Email</th>
									<th style="width:10%">Aksi</th>
								</tr>
								</thead>
								<tbody>
								<?php $no=0; ?> 
							@foreach($users as $lk)
							<?php $no++; ?>
								<tr>
									<td>{{$no}}</td>
									<td>{{$lk->roles_name}}</td>
									<td>{{$lk->name}}</td>
									<td>{{$lk->email}}</td>
									<td>
									<a class="btn btn-info btn-sm" href="<?php echo url('/usersmanagement/users/edit_users/'.$lk->id); ?>">
											<i class="fas fa-edit">
											</i>
										</a>
                                        @if($lk->name == Auth::user()->name)

                                        @else
										<button type="button" class="btn btn-danger btn-sm confirmation" data-toggle="modal" data-target="#myModaluser{{$lk->id}}"><i class="fas fa-trash"></i></button>
										<!-- Modal -->
                                                <div class="modal fade" id="myModaluser{{$lk->id}}" role="dialog">
												<div class="modal-dialog">
												
												<!-- Modal content-->
												<div class="modal-content">
													<div class="modal-header">
													<h4 class="modal-title">Apakah Anda Yakin menghapus data Link {{$lk->name}}..?</h4>
													</div>
													<div class="modal-footer">
													<a href="<?php echo url('/usersmanagement/users/destroy_users/'.$lk->id); ?>" class="btn btn-danger btn-sm confirmation" >Hapus Data</a>
														
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													</div>
												</div>
												
												</div>
											</div>
                                            @endif
									</td>
								</tr>
								@endforeach
								</tbody>
								</table>
							</div>
							<!-- /.card-body -->
							</div>
                  </div>

                  <div class="tab-pane fade" id="custom-tabs-one-profile" role="tabpanel" aria-labelledby="custom-tabs-one-profile-tab">
                  
                  <div class="card">
							<div class="card-header">
										<h3 class="card-title">Users | &nbsp;</h3>
						   
							<a class="btn btn-info btn-sm" href="<?php echo url('usersmanagement/roles/add'); ?>">
											<i class="fas fa-plus"> Add
											</i>
										</a>
							</div>
											
							<!-- /.card-header -->
							<div class="card-body">
								<table id="next2" class="table table-bordered table-hover">
								<thead>
								<tr>
									<th>No</th>
									<th>Roles</th>
									<th style="width:10%">Aksi</th>
								</tr>
								</thead>
								<tbody>
								<?php $no=0; ?> 
							@foreach($roles as $r)
							<?php $no++; ?>
								<tr>
									<td>{{$no}}</td>
									<td>{{$r->role_user}}</td>
									<td>
									<a class="btn btn-info btn-sm" href="<?php echo url('/usersmanagement/roles/edit/'.$r->id); ?>">
											<i class="fas fa-edit">
											</i>
										</a>
                                        @if($r->id > 4)
                                        <button type="button" class="btn btn-danger btn-sm confirmation" data-toggle="modal" data-target="#myModalroles{{$r->id}}"><i class="fas fa-trash"></i></button>
										<!-- Modal -->
                                                <div class="modal fade" id="myModalroles{{$r->id}}" role="dialog">
												<div class="modal-dialog">
												
												<!-- Modal content-->
												<div class="modal-content">
													<div class="modal-header">
													<h4 class="modal-title">Apakah Anda Yakin menghapus data Link {{$r->role_user}}..?</h4>
													</div>
													<div class="modal-footer">
													<a href="<?php echo url('/usersmanagement/roles/destroy/'.$r->id); ?>" class="btn btn-danger btn-sm confirmation" >Hapus Data</a>
														
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													</div>
												</div>
												
												</div>
											</div>
                                        @endif									
									</td>
								</tr>
								@endforeach
								</tbody>
								</table>
							</div>
							<!-- /.card-body -->
							</div>
                
                  </div>
                 
                </div>
              </div>
              <!-- /.card -->
            </div>
          </div>

                        </div>
				</div>
			
			</div>
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <script>
   $(function () {
    $("#next").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
    }).buttons().container().appendTo('#next_wrapper .col-md-6:eq(0)');
  });
  $(function () {
    $("#next2").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
    }).buttons().container().appendTo('#next_wrapper .col-md-6:eq(0)');
  });
</script>
@endsection