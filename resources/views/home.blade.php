@extends('layouts.lay2')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v1</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
                <!-- Small boxes (Stat box) -->
        <div class="row">
        
        <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3><?php 
                    $count = \DB::table('tbl_konsultasi')
                    ->select(\DB::raw('COUNT(id_konsultasi) as hit'))
                    ->where('tgl_jawaban', '=', '')
                    ->orWhereNull('tgl_jawaban')
                    ->get();
                    ?>
                    @foreach($count as $cc){{$cc->hit}}@endforeach
                   </h3>

                <p>Konsultasi Baru</p>
              </div>
              <div class="icon">
                <i class="fas fa-volume-up nav-icon"></i>
              </div>
              <a href="<?php echo url('/layanan/konsultasi'); ?>" class="small-box-footer">Details <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>

          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3><?php 
                    $counta = \DB::table('tbl_barang')
                    ->select(\DB::raw('COUNT(id) as hit'))
                    ->get();
                    ?>
                    @foreach($counta as $cca){{$cca->hit}}@endforeach</h3>

                <p>Barang Dan Bahan Terkini</p>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
              <a href="<?php echo url('/layanan/barang'); ?>" class="small-box-footer">Details <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>

          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
              <h3><?php 
                    $counts = \DB::table('tbl_agenda')
                    ->select(\DB::raw('COUNT(id_agenda) as hit'))
                    ->where(\DB::raw('DATE_FORMAT(tgl_agenda, "%Y")'), '=',date("Y"))
                    ->orWhereNull('tgl_agenda')
                    ->get();
                    ?>
                    @foreach($counts as $ccs){{$ccs->hit}}@endforeach
                   </h3>

                <p>Agenda Tahun {{date('Y')}}</p>
              </div>
              <div class="icon">
                <i class="ion ion-calendar"></i>
              </div>
              <a href="<?php echo url('/informasi/agenda'); ?>" class="small-box-footer">Details <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>

          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-danger">
              <div class="inner">
              <h3><?php 
                    $counts = \DB::table('tbl_harga')
                    ->select(\DB::raw('COUNT(id) as hit'))
                    ->where(\DB::raw('DATE_FORMAT(last_update, "%Y")'), '=',date("Y"))
                    ->orWhereNull('last_update')
                    ->get();
                    ?>
                    @foreach($counts as $ccs){{$ccs->hit}}@endforeach
                   </h3>

                <p>Total List Harga</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              <a href="<?php echo url('/layanan/hbb'); ?>" class="small-box-footer">Details <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>

          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-danger">
              <div class="inner">
              
          <!-- Histats.com  (div with counter) --><div id="histats_counter"></div>
          <!-- Histats.com  START  (aync)-->
          <script type="text/javascript">var _Hasync= _Hasync|| [];
          _Hasync.push(['Histats.start', '1,4711313,4,4006,112,61,00011111']);
          _Hasync.push(['Histats.fasi', '1']);
          _Hasync.push(['Histats.track_hits', '']);
          (function() {
          var hs = document.createElement('script'); hs.type = 'text/javascript'; hs.async = true;
          hs.src = ('//s10.histats.com/js15_as.js');
          (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
          })();</script>
          <noscript><a href="/" target="_blank"><img  src="//sstatic1.histats.com/0.gif?4711313&101" alt="free web tracker" border="0"></a></noscript>
          <!-- Histats.com  END  --> 
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              </div>
          </div>
               
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection
