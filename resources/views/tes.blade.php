<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap CSS -->
   <title>Laravel 8: Dynamic Dependent Dropdown</title>
    </head>
    <body>
        <div class="container my-5">
            <h1 class="fs-5 fw-bold my-4 text-center">How to Create Dependent Dropdown in Laravel</h1>
            <div class="row">
                <form action="">
                    <div class="mb-3">
                        <label for="kota" class="form-label">Kabupaten/Kota</label>
                        <select class="form-control" name="id_kota" id="kota">
                            <option value='0' hidden>Kabpupaten/kota</option>
                            @foreach ($kota as $item)
                            <option value="{{ $item->id_kota }}">{{ $item->kota }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="mb-3">
                        <label for="id_kecamatan" class="form-label">id_kecamatan</label>
                        <select class="form-control" name="id_kecamatan" id="id_kecamatan"></select>
                    </div>
                </form>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
        <script>
            $(document).ready(function() {
            $('#kota').on('change', function() {
               var kotaID = $(this).val();
               if(kotaID) {
                   $.ajax({
                       url: '/kecamatan/'+kotaID,
                       type: "GET",
                       data : {"_token":"{{ csrf_token() }}"},
                       dataType: "json",
                       success:function(data)
                       {
                         if(data){
                            $('#id_kecamatan').empty();
                            $('#id_kecamatan').append('<option hidden>Pilih Kecamatan</option>'); 
                            $.each(data, function(key, id_kecamatan){
                                $('select[name="id_kecamatan"]').append('<option value="'+ key +'">' + id_kecamatan.nama_kecamatan+ '</option>');
                            });
                        }else{
                            $('#id_kecamatan').empty();
                        }
                     }
                   });
               }else{
                 $('#id_kecamatan').empty();
               }
            });
            });
        </script>
    </body>
</html>