<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class bahan_j extends Model
{
    protected $table = "bahan_j";
    protected $fillable =['id_jenis','kode','jenis','file'];
}
