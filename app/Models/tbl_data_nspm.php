<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tbl_data_nspm extends Model
{
    protected $table = "tbl_data_nspm";
    protected $fillable =['id_data_nspm','judul_data_nspm',
                          'uraian_data_nspm','no_seri_nspm',
                          'file_data_nspm', 'tahun_data_nspm',
                          'hit','id_nspm_kriteria','id_nspm_kriteria1',
                          'id_nspm_kategori'];
}
