<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tbl_file_pendukung extends Model
{
    protected $table = "tbl_file_pendukung";
    protected $fillable =['id_file','judul_file','file','id_event'];
}
