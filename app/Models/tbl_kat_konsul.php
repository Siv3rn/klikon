<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tbl_kat_konsul extends Model
{
    protected $table = "tbl_kat_konsul";
    protected $fillable =['id_kat_konsul','nama_kat_konsul'];
}
