<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tbl_link extends Model
{
    protected $table = "tbl_link";
    protected $fillable =['id_link','nama_link','image_link','url_link'];
}
