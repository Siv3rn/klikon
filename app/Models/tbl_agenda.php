<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tbl_agenda extends Model
{
    protected $table = "tbl_agenda";
    protected $fillable =['id_agenda','judul_agenda','ringkasan_agenda','isi_agenda','tgl_agenda'];
}
