<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tbl_sk extends Model
{
    protected $table = "tbl_sk";
    protected $fillable =['id_poster','judul_sk','file_sk'];
}