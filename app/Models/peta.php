<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class peta extends Model
{
    protected $table = "peta";
    protected $fillable =['judul_peta','iframe_peta','id_jenis','status_peta'];
}
