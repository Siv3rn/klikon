<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tukang_sertifikasi extends Model
{
    protected $table = "tukang_sertifikasi";
    protected $fillable =['id_sertifikasi', 'id_tukang','nama','nomor','masa_berlaku'];
}
