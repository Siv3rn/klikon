<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tbl_event extends Model
{
    protected $table = "tbl_event";
    protected $fillable =['id_event','judul_event','status_event'];
}
