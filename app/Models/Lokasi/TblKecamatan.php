<?php

namespace App\Models\Lokasi;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TblKecamatan extends Model
{
    protected $table = "tbl_kecamatan";
    protected $fillable =['id_kecamatan','nama_kecamatan','id_kota'];
}
