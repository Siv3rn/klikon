<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tbl_popup extends Model
{
    protected $table = "tbl_popup";
    protected $fillable =['judul_popup', 'image_popup','url_popup','status_popup'];
}
