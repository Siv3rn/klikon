<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tbl_nspm_kategori extends Model
{
    protected $table = "tbl_nspm_kategori";
    protected $fillable =['id_nspm_kategori',
                          'nama_nspm_kategori'];
}
