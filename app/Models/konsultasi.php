<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class konsultasi extends Model
{
    protected $table = "tbl_konsultasi";
    protected $fillable =['id_konsultasi','nama_konsultasi','hp_konsultasi','email_konsultasi',
                          'judul_konsultasi','pertanyaan_konsultasi','id_kat_konsul','jawaban_konsultasi',
                          'id_konsultan','tgl_konsultasi','tgl_jawaban'];
}
