<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class artikel extends Model
{
    protected $table = "artikel";
    protected $fillable =['id_artikel','tanggal','judul','pengarang','ringkasan','isi',
                            'dibaca','petugas','ip','file_image','status','kategori'];
}
