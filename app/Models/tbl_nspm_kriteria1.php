<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tbl_nspm_kriteria1 extends Model
{
    protected $table = "tbl_nspm_kriteria1";
    protected $fillable =['id_nspm_kriteria11','nama_nspm_kriteria1','id_nspm_kriteria'];
}
