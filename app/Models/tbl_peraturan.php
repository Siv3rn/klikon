<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tbl_peraturan extends Model
{
    protected $table = "tbl_peraturan";
    protected $fillable =['id_peraturan','judul_peraturan','deskripsi_peraturan','file_peraturan','kriteria_peraturan'];
}
