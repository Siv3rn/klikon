<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tbl_buku extends Model
{
    protected $table = "buku";
    protected $fillable =['id','judul_buku','file_buku','status_buku'];
}
