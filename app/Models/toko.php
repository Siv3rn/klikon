<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class toko extends Model
{
    protected $table = "toko";
    protected $fillable =['id_user','id_kabupaten','id_propinsi',
                          'alamat_map','longtitude','latitude','wa','hp',
                          'alamat','file','status','info'];
}
