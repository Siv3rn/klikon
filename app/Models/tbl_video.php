<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tbl_video extends Model
{
    protected $table = "tbl_video";
    protected $fillable =['id_video', 'judul_video','iframe_video','status_video'];
}
