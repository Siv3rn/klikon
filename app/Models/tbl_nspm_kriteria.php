<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tbl_nspm_kriteria extends Model
{
    protected $table = "tbl_nspm_kriteria";
    protected $fillable =['id_nspm_kriteria','nama_nspm_kriteria'];
}
