<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tbl_tenaga extends Model
{
    protected $table = "tbl_tenaga";
    protected $fillable =['id_tenaga','judul_tenaga','deskripsi_tenaga','file_tenaga','kriteria_tenaga'];
}
