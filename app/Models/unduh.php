<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class unduh extends Model
{
    protected $table = "tbl_upload_harga";
    protected $fillable =['id_upload','nama_toko','alamat_toko','kabupaten','file_harga','update_harga'];
}
