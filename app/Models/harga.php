<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class harga extends Model
{
    protected $table = "harga";
    protected $fillable =['id_harga','id_jenis','kode_jenis','nama','ukuran','harga','last_update','agen','stock','id_kecamatan','kwalitas','keterangan'];
}
