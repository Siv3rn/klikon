<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tukang extends Model
{
    protected $table = "tukang";
    protected $fillable =['id_user', 'id_jenis','id_kabupaten','id_propinsi',
                          'alamat_map','longtitude','latitude','wa','hp',
                          'tgl_lahir','usia','ktp','alamat','file','status'];
}
