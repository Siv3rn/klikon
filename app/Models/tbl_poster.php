<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tbl_poster extends Model
{
    protected $table = "tbl_poster";
    protected $fillable =['id_poster','judul_poster','file_poster','status_poster'];
}
