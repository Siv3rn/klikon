<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tbl_slider extends Model
{
    protected $table = "tbl_slider";
    protected $fillable =['id_slider','judul_slider','keterangan_slider','images_slider','url_slider','status_slider'];
}