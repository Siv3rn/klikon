<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class berita extends Model
{
    protected $table = "berita";
    protected $fillable =['id_berita','tanggal','judul','ringkasan','isi','dibaca','petugas','ip','image_berita'];
}
