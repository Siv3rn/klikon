<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class jenis_tukang extends Model
{
    protected $table = "jenis_tukang";
    protected $fillable =['id_jenis', 'jenis'];
}
