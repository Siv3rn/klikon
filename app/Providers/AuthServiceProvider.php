<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Illuminate\Auth\Access\Response;
use DB;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('isSuperUser', function($user) {
            $role = DB::table('user_roles')
            ->where('id_user', $user->id)
            ->value('id_role');
            return $role == 1
            ? Response::allow()
            : Response::deny('Anda harus Log-in sebagai SuperUser.');
            
         });
         Gate::define('isAdminKonten', function($user){
            $role = DB::table('user_roles')
            ->where('id_user', $user->id)
            ->value('id_role');
            return $role == 2            
            ? response::allow()
            :Response::deny('Anda harus Log-in sebagai SuperUser atau Admin Konten');
            
         });

 

    }
}
