<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Models\tbl_agenda;

class agendaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
		$agenda = \DB::table('tbl_agenda')
            ->select('*')
            ->orderBy('tgl_agenda','DESC')
            ->get();
			
		return view('master.informasi.agenda.index',['agenda'=>$agenda]);
    }    

    public function add()
    {
        return view('master.informasi.agenda.add');
    }

    public function edit(Request $request, $id)
    {
		$agenda = \DB::table('tbl_agenda')
            ->select('*')
            ->where('id_agenda','=', $id)
            ->get();
			
		return view('master.informasi.agenda.edit',['agenda'=>$agenda]);
    }    

    public function destroy($id)
    {
        $agenda= \DB::table('tbl_agenda')->where('id_agenda', $id)->delete();
        return redirect()->back()->withSuccess('Deleted Success.');
    }

    public function create(Request $request)
    {         

        $ip=\request()->ip();
           date_default_timezone_set("Asia/Jakarta");
        $agenda = new \App\Models\tbl_agenda();
        $agenda->judul_agenda = $request->judul;         
        $agenda->tgl_agenda = date('Y-m-d', strtotime($request->tgl_agenda));
        $agenda->ringkasan_agenda = $request->ringkasan_agenda;
        $agenda->isi_agenda = $request->isi_agenda;
        $agenda->save();

        
        return redirect('/informasi/agenda')->with('Agenda Berhasil Di Buat','Success');
    }

    public function update(Request $request, $id_agenda)
    {
            \DB::table('tbl_agenda')->where('id_agenda',$id_agenda)->update([
                'judul_agenda' => $request->judul_agenda,
                'isi_agenda' => $request->isi_agenda,
                'ringkasan_agenda' => $request->ringkasan_agenda,
                'tgl_agenda' => date('Y-m-d', strtotime($request->tgl_agenda))
                ]);
               
                return redirect('/informasi/agenda')->withSuccess('Agenda Berhasil Di Update','Success');

        }
}
