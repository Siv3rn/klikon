<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Models\artikel;

class artikelpublikController extends Controller
{
    public function all(Request $request)
    {
        
        $bulan = $request->bulan;
        $tahun = $request->tahun;
        if(!empty($bulan) && empty($tahun)){
            $news = \DB::table('artikel')
        ->select('*')
        ->whereMonth('tanggal', $bulan)
        ->orderBy('tanggal','DESC')
        ->paginate(10);

        $konsul = \DB::table('artikel')
            ->select('*')
            ->orderBy('tanggal','DESC')
            ->limit(3)
            ->get();
        
        $video = \DB::table('tbl_video')
            ->select('*')
            ->where('status_video', '=', 'aktif')
            ->get();
        

		return view('master.informasi.artikel.all',['konsul'=>$konsul,'news'=>$news,'video'=>$video]);
        
        }else if(empty($bulan) && !empty($tahun)){
        
        $news = \DB::table('artikel')
        ->select('*')
        ->whereYear('tanggal', $tahun)
        ->orderBy('tanggal','DESC')
        ->paginate(10);

        $konsul = \DB::table('artikel')
            ->select('*')
            ->orderBy('tanggal','DESC')
            ->limit(3)
            ->get();

        $video = \DB::table('tbl_video')
            ->select('*')
            ->where('status_video', '=', 'aktif')
            ->get();

		return view('master.informasi.artikel.all',['konsul'=>$konsul,'news'=>$news,'video'=>$video]);
        
        }else if(!empty($bulan) && !empty($tahun)){
            
            $news = \DB::table('artikel')
            ->select('*')
            ->whereYear('tanggal', $tahun)
            ->whereMonth('tanggal', $bulan)
            ->orderBy('tanggal','DESC')
            ->paginate(10);
    
            $konsul = \DB::table('artikel')
                ->select('*')
                ->orderBy('tanggal','DESC')
                ->limit(3)
                ->get();
                
            $video = \DB::table('tbl_video')
                ->select('*')
                ->where('status_video', '=', 'aktif')
                ->get();
    
            return view('master.informasi.artikel.all',['konsul'=>$konsul,'news'=>$news,'video'=>$video]);
                
        }else{
        $news = \DB::table('artikel')
        ->select('*')
        ->where('tanggal', '<>', '')
        ->orWhereNotNull('tanggal')
        ->orderBy('tanggal','DESC')
        ->paginate(10);

        $konsul = \DB::table('artikel')
            ->select('*')
            ->orderBy('tanggal','DESC')
            ->limit(3)
            ->get();

        $video = \DB::table('tbl_video')
            ->select('*')
            ->where('status_video', '=', 'aktif')
            ->get();

		return view('master.informasi.artikel.all',['konsul'=>$konsul,'news'=>$news,'video'=>$video]);
        }
        
    } 

    public function detail(Request $request, $id)
    {

        $news = \DB::table('artikel')
        ->select('*')
        ->where('id_artikel','=', $id)
        ->get();

        $konsul = \DB::table('artikel')
            ->select('*')
            ->WhereNotNull('kategori')
            ->orderBy('tanggal','DESC')
            ->limit(3)
            ->get();

        $video = \DB::table('tbl_video')
            ->select('*')
            ->where('status_video', '=', 'aktif')
            ->get();

		return view('master.informasi.artikel.detail',['konsul'=>$konsul,'news'=>$news,'video'=>$video]);
    } 
}
