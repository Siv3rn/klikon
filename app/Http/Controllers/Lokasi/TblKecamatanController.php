<?php

namespace App\Http\Controllers\Lokasi;

use App\Http\Controllers\Controller;
use App\Models\Lokasi\TblKecamatan;
use Illuminate\Http\Request;

class TblKecamatanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
		$kecamatan = \DB::table('tbl_kecamatan')
            ->join('kota', 'tbl_kecamatan.id_kota', '=', 'kota.id_kota')
            ->select('*')
            ->orderBy('tbl_kecamatan.id_kota','asc')
            ->get();
			
		return view('master.layanan.lokasi.index',['kecamatan'=>$kecamatan]);
    }

    public function create(Request $request)
    {         

        TblKecamatan::updateOrCreate([
            'id_kota'=> $request->id_kota,
            'nama_kecamatan' => $request->nama_kecamatan
        ],
        [
            'id_kota'=> $request->id_kota,
            'nama_kecamatan' => $request->nama_kecamatan
        ]);

        return redirect('/layanan/lokasi')->with('Data Kecamatan Berhasil Di Buat','Success');
    }

    public function update(Request $request, $id)
    {
            \DB::table('tbl_kecamatan')->where('id_kecamatan',$id)->update([
                'id_kota' => $request->id_kota,
                'nama_kecamatan' => $request->nama_kecamatan
                ]);
               
                return redirect('/layanan/lokasi')->withSuccess('Lokasi Berhasil Di Update','Success');

    }

    public function destroy($id)
    {
        $kecamatan= \DB::table('tbl_kecamatan')->where('id_kecamatan', $id)->delete();
        return redirect()->back()->withSuccess('Deleted Success.');
    }

}
