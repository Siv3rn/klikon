<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Models\tukang;
use App\Models\harga;

class kangjogjaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

    }

    public function admin()
    {
        $this->Authorize('isSuperUser');

        $kang = \DB::table('tukang')
        ->join('jenis_tukang','tukang.id_jenis','=','jenis_tukang.id_jenis')
        ->join('users','tukang.id_user','=','users.id')
        ->join('tukang_sertifikasi','tukang.id_user','=','tukang_sertifikasi.id_tukang')
        ->select('users.name as name','tukang.alamat as alamat','jenis_tukang.jenis as jenis','tukang.id_user as id_user','tukang.ktp'
                    ,'tukang.file as file','tukang.usia as usia','tukang.id as id','tukang_sertifikasi.nama as nama_ser','tukang.latitude'
                    ,'tukang_sertifikasi.nomor as nomor','tukang_sertifikasi.masa_berlaku as masa_berlaku','email','tukang.longtitude'
                    ,'tukang.wa as wa','tukang.hp as hp','tukang.tgl_lahir as tgl_lahir','tukang.status as status','tukang.alamat_map',
                    'tukang.id_jenis','tukang.id_propinsi','tukang.id_kabupaten')
        ->orderBy('tukang.id', 'DESC')
        ->get();

        $toko = \DB::table('toko')
        ->join('users','toko.id_user','=','users.id')
        ->join('m_kabupaten','toko.id_kabupaten','=','m_kabupaten.id_kabupaten')
        ->join('m_propinsi','toko.id_propinsi','=','m_propinsi.id_propinsi')
        ->select('toko.id_user','users.name as nama','toko.id_kabupaten','toko.id_propinsi',
                 'toko.alamat_map','toko.longtitude','toko.latitude','toko.wa','toko.hp',
                 'toko.alamat','toko.file','toko.status','toko.info','m_propinsi.propinsi',
                 'm_kabupaten.kabupaten','toko.id as id')
        ->orderBy('toko.id', 'DESC')
        ->get();
    
        $kabupaten = \DB::table('m_kabupaten')
        ->select('*')
        ->get();

		return view('master.layanan.kangjogja.admin',['toko'=>$toko,'kabupaten'=>$kabupaten,'kang'=>$kang]);
    } 

    public function edit_tukang(Request $request, $id)
    {
        $kang = \DB::table('tukang')
        ->join('jenis_tukang','tukang.id_jenis','=','jenis_tukang.id_jenis')
        ->join('users','tukang.id_user','=','users.id')
        ->join('tukang_sertifikasi','tukang.id_user','=','tukang_sertifikasi.id_tukang')
        ->select('users.name as name','tukang.alamat as alamat','jenis_tukang.jenis as jenis','tukang.id_user as id_user','tukang.ktp'
                    ,'tukang.file as file','tukang.usia as usia','tukang.id as id','tukang_sertifikasi.nama as nama_ser','tukang.latitude'
                    ,'tukang_sertifikasi.nomor as nomor','tukang_sertifikasi.masa_berlaku as masa_berlaku','email','tukang.longtitude'
                    ,'tukang.wa as wa','tukang.hp as hp','tukang.tgl_lahir as tgl_lahir','tukang.status as status','tukang.alamat_map',
                    'tukang.id_jenis','tukang.id_propinsi','tukang.id_kabupaten')
        ->where('tukang.id_user','=', $id)
        ->orderBy('tukang.id', 'DESC')
        ->get();
    
        $kabupaten = \DB::table('m_kabupaten')
        ->select('*')
        ->get();
			
            return view('master.layanan.kangjogja.edit_tukang',['kabupaten'=>$kabupaten,'kang'=>$kang]);
    }    

    public function edit_toko(Request $request, $id)
    {
        $toko = \DB::table('toko')
        ->join('users','toko.id_user','=','users.id')
        ->join('m_kabupaten','toko.id_kabupaten','=','m_kabupaten.id_kabupaten')
        ->join('m_propinsi','toko.id_propinsi','=','m_propinsi.id_propinsi')
        ->select('toko.id_user','users.email as email','users.name as name','toko.id_kabupaten','toko.id_propinsi',
                 'toko.alamat_map','toko.longtitude','toko.latitude','toko.wa','toko.hp',
                 'toko.alamat','toko.file','toko.status','toko.info','m_propinsi.propinsi',
                 'm_kabupaten.kabupaten','toko.id as id')
        ->where('toko.id_user','=', $id)
        ->orderBy('toko.id', 'DESC')
        ->get();
    
        $kabupaten = \DB::table('m_kabupaten')
        ->select('*')
        ->get();
			
            return view('master.layanan.kangjogja.edit_toko',['kabupaten'=>$kabupaten,'toko'=>$toko]);
    }    


    public function create_kangjogja(Request $request)
    {    
        $hit = \DB::table('users')->where('email', '=', $request->email)->get();
        $hit = $hit->count();

        if($hit > 0){

          return redirect()->back()->withErrors(['Error', 'Email Sudah Terdaftar']);

        }else{

            if(empty($request->file('file'))){

                $user = new \App\Models\users_new();
                $user->name = $request->name;
                $user->password = bcrypt($request->password);
                $user->email = $request->email;
                $user->remember_token = bcrypt('1234');
                $user->save();
        
                $rolesa = new \App\Models\user_roles;
                $rolesa->id_user = $user->id;
                $rolesa->id_role = 5;
                $rolesa->save();
    
                $tukang = new \App\Models\tukang();
                $tukang->id_user = $user->id;
                $tukang->id_jenis = $request->id_jenis;
                $tukang->id_propinsi = $request->id_propinsi;
                $tukang->id_kabupaten = $request->id_kabupaten;
                $tukang->alamat_map = $request->alamat_map;
                $tukang->longtitude = $request->longtitude;
                $tukang->latitude = $request->latitude;
                $tukang->wa = $request->wa;
                $tukang->hp = $request->hp;
                $tukang->tgl_lahir = date('Y-m-d', strtotime($request->tgl_lahir));
                $tukang->usia = '-';
                $tukang->ktp = $request->ktp;
                $tukang->alamat = $request->alamat;
                $tukang->file = 'no-images.jpg';
                $tukang->status = 1;
                $tukang->save();

                $sertifikasi = new \App\Models\tukang_sertifikasi();
                $sertifikasi->id_tukang = $user->id;
                $sertifikasi->nama = $request->nama;
                $sertifikasi->nomor = $request->nomor;
                $sertifikasi->masa_berlaku = date('Y-m-d', strtotime($request->masa_berlaku));
                $sertifikasi->save();
        
                return redirect('/menu/kangjogja/')->with('KangJogja Berhasil Di Buat','Success');
            }else{
                $validator = Validator::make($request->all(), [
                    'file' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
                ]);
        
                if ($validator->fails()) {
                    return redirect()->back()->withErrors(['Error', 'Upload gagal Karena file yang diupload bukan jpg,png,jpeg,gif,svg atau images ukuran melebihi 2048']);  
                }
                
                $file = $request->file('file');
                
                $filenameWithExt = $request->file('file')->getClientOriginalName();
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                $extension = $request->file('file')->getClientOriginalExtension();
                $filenameSimpan = $filename.'_'.time().'.'.$extension;
                $path = $request->file('file')->storeAs('/public/images/tukang', $filenameSimpan);
        
                $user = new \App\Models\users_new();
                $user->name = $request->name;
                $user->password = bcrypt($request->password);
                $user->email = $request->email;
                $user->remember_token = bcrypt('1234');
                $user->save();
        
                $rolesa = new \App\Models\user_roles;
                $rolesa->id_user = $user->id;
                $rolesa->id_role = 5;
                $rolesa->save();
    
                $tukang = new \App\Models\tukang();
                $tukang->id_user = $user->id;
                $tukang->id_jenis = $request->id_jenis;
                $tukang->id_propinsi = $request->id_propinsi;
                $tukang->id_kabupaten = $request->id_kabupaten;
                $tukang->alamat_map = $request->alamat_map;
                $tukang->longtitude = $request->longtitude;
                $tukang->latitude = $request->latitude;
                $tukang->wa = $request->wa;
                $tukang->hp = $request->hp;
                $tukang->tgl_lahir = date('Y-m-d', strtotime($request->tgl_lahir));
                $tukang->usia = '-';
                $tukang->ktp = $request->ktp;
                $tukang->alamat = $request->alamat;
                $tukang->file = $filenameSimpan;
                $tukang->status = 1;
                $tukang->save();

                $sertifikasi = new \App\Models\tukang_sertifikasi();
                $sertifikasi->id_tukang = $user->id;
                $sertifikasi->nama = $request->nama;
                $sertifikasi->nomor = $request->nomor;
                $sertifikasi->masa_berlaku = date('Y-m-d', strtotime($request->masa_berlaku));
                $sertifikasi->save();
        
                return redirect('/menu/kangjogja/')->with('Tukang Berhasil Di Buat','Success');
            }

        }
                
    }

    public function create_toko(Request $request)
    {    
        $hit = \DB::table('users')->where('email', '=', $request->email)->get();
        $hit = $hit->count();

        if($hit > 0){

          return redirect()->back()->withErrors(['Error', 'Email Sudah Terdaftar']);

        }else{

            if(empty($request->file('file'))){

                $user = new \App\Models\users_new();
                $user->name = $request->name;
                $user->password = bcrypt($request->password);
                $user->email = $request->email;
                $user->remember_token = bcrypt('1234');
                $user->save();
        
                $rolesa = new \App\Models\user_roles;
                $rolesa->id_user = $user->id;
                $rolesa->id_role = 6;
                $rolesa->save();
    
                $tukang = new \App\Models\toko();
                $tukang->id_user = $user->id;
                $tukang->id_propinsi = $request->id_propinsi;
                $tukang->id_kabupaten = $request->id_kabupaten;
                $tukang->alamat_map = $request->alamat_map;
                $tukang->longtitude = $request->longtitude;
                $tukang->latitude = $request->latitude;
                $tukang->wa = $request->wa;
                $tukang->hp = $request->hp;
                $tukang->info = $request->info;
                $tukang->alamat = $request->alamat;
                $tukang->file = 'no-images.jpg';
                $tukang->status = 1;
                $tukang->save();
        
                return redirect('/menu/kangjogja/')->with('Toko Berhasil Di Buat','Success');
            }else{
                $validator = Validator::make($request->all(), [
                    'file' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
                ]);
        
                if ($validator->fails()) {
                    return redirect()->back()->withErrors(['Error', 'Upload gagal Karena file yang diupload bukan jpg,png,jpeg,gif,svg atau images ukuran melebihi 2048']);  
                }
                
                $file = $request->file('file');
                
                $filenameWithExt = $request->file('file')->getClientOriginalName();
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                $extension = $request->file('file')->getClientOriginalExtension();
                $filenameSimpan = $filename.'_'.time().'.'.$extension;
                $path = $request->file('file')->storeAs('/public/images/toko', $filenameSimpan);
        
                $user = new \App\Models\users_new();
                $user->name = $request->name;
                $user->password = bcrypt($request->password);
                $user->email = $request->email;
                $user->remember_token = bcrypt('1234');
                $user->save();
        
                $rolesa = new \App\Models\user_roles;
                $rolesa->id_user = $user->id;
                $rolesa->id_role = 6;
                $rolesa->save();
    
                $tukang = new \App\Models\toko();
                $tukang->id_user = $user->id;
                $tukang->id_propinsi = $request->id_propinsi;
                $tukang->id_kabupaten = $request->id_kabupaten;
                $tukang->alamat_map = $request->alamat_map;
                $tukang->longtitude = $request->longtitude;
                $tukang->latitude = $request->latitude;
                $tukang->wa = $request->wa;
                $tukang->hp = $request->hp;
                $tukang->alamat = $request->alamat;
                $tukang->info = $request->info;
                $tukang->file = $filenameSimpan;
                $tukang->status = 1;
                $tukang->save();
        
                return redirect('/menu/kangjogja/')->with('Toko Berhasil Di Buat','Success');
            }

        }
                
    }

    public function update_toko(Request $request, $id)
    {    
            if(empty($request->file)){

                   
                    \DB::table('users')->where('id',$id)->update([
                        'email' => $request->email,
                        'name' => $request->name
                        ]);

                    \DB::table('tukang_sertifikasi')->where('id_tukang',$id)->update([
                        'nama' => $request->nama,
                        'nomor' => $request->nomor,
                        'masa_berlaku' => date('Y-m-d', strtotime($request->masa_berlaku))
                        ]);

                    \DB::table('toko')->where('id_user',$id)->update([
                        'id_kabupaten' => $request->id_kabupaten,
                        'alamat_map' => $request->alamat_map,
                        'longtitude' => $request->longtitude,
                        'latitude' => $request->latitude,
                        'wa' => $request->wa,
                        'hp' => $request->hp,
                        'info' => $request->info,
                        'alamat' => $request->alamat,
                        'status' => 1
                        ]);

                        return redirect('/menu/kangjogja/')->with('KangJogja Berhasil Di Update','Success');
                
                
            }else{
                $validator = Validator::make($request->all(), [
                    'file' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
                ]);
        
                if ($validator->fails()) {
                    return redirect()->back()->withErrors(['Error', 'Upload gagal Karena file yang diupload bukan jpg,png,jpeg,gif,svg atau images ukuran melebihi 2048']);  
                }
                
                $file = $request->file('file');
                
                $filenameWithExt = $request->file('file')->getClientOriginalName();
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                $extension = $request->file('file')->getClientOriginalExtension();
                $filenameSimpan = $filename.'_'.time().'.'.$extension;
                $path = $request->file('file')->storeAs('/public/images/toko', $filenameSimpan);
        
                
                    \DB::table('users')->where('id',$id)->update([
                        'email' => $request->email,
                        'name' => $request->name
                        ]);

                    \DB::table('toko')->where('id_user',$id)->update([
                        'id_kabupaten' => $request->id_kabupaten,
                        'alamat_map' => $request->alamat_map,
                        'longtitude' => $request->longtitude,
                        'latitude' => $request->latitude,
                        'wa' => $request->wa,
                        'hp' => $request->hp,
                        'info' => $request->info,
                        'alamat' => $request->alamat,
                        'file' => $filenameSimpan,
                        'status' => 1
                        ]);

                        return redirect('/menu/kangjogja/')->with('toko Berhasil Di Update','Success');
                }
                
    }

    public function update_kangjogja(Request $request, $id)
    {    
            if(empty($request->file)){

                   
                    \DB::table('users')->where('id',$id)->update([
                        'email' => $request->email,
                        'name' => $request->name
                        ]);

                    \DB::table('tukang_sertifikasi')->where('id_tukang',$id)->update([
                        'nama' => $request->nama,
                        'nomor' => $request->nomor,
                        'masa_berlaku' => date('Y-m-d', strtotime($request->masa_berlaku))
                        ]);

                    \DB::table('tukang')->where('id_user',$id)->update([
                        'id_jenis' => $request->id_jenis,
                        'id_kabupaten' => $request->id_kabupaten,
                        'alamat_map' => $request->alamat_map,
                        'longtitude' => $request->longtitude,
                        'latitude' => $request->latitude,
                        'wa' => $request->wa,
                        'hp' => $request->hp,
                        'tgl_lahir' => date('Y-m-d', strtotime($request->tgl_lahir)),
                        'usia' => '-',
                        'ktp' => $request->ktp,
                        'alamat' => $request->alamat,
                        'status' => 1
                        ]);

                        return redirect('/menu/kangjogja/')->with('KangJogja Berhasil Di Update','Success');
                
                
            }else{
                $validator = Validator::make($request->all(), [
                    'file' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
                ]);
        
                if ($validator->fails()) {
                    return redirect()->back()->withErrors(['Error', 'Upload gagal Karena file yang diupload bukan jpg,png,jpeg,gif,svg atau images ukuran melebihi 2048']);  
                }
                
                $file = $request->file('file');
                
                $filenameWithExt = $request->file('file')->getClientOriginalName();
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                $extension = $request->file('file')->getClientOriginalExtension();
                $filenameSimpan = $filename.'_'.time().'.'.$extension;
                $path = $request->file('file')->storeAs('/public/images/toko', $filenameSimpan);
        
                
                    \DB::table('users')->where('id',$id)->update([
                        'email' => $request->email,
                        'name' => $request->name
                        ]);

                    \DB::table('tukang')->where('id_user',$id)->update([
                        'id_jenis' => $request->id_jenis,
                        'id_kabupaten' => $request->id_kabupaten,
                        'alamat_map' => $request->alamat_map,
                        'longtitude' => $request->longtitude,
                        'latitude' => $request->latitude,
                        'wa' => $request->wa,
                        'hp' => $request->hp,
                        'tgl_lahir' => date('Y-m-d', strtotime($request->tgl_lahir)),
                        'usia' => '-',
                        'ktp' => $request->ktp,
                        'alamat' => $request->alamat,
                        'file' => $filenameSimpan,
                        'status' => 1
                        ]);

                        return redirect('/menu/kangjogja/')->with('Toko Berhasil Di Update','Success');
                }
                
    }

    public function destroy_tukang($id)
    {
        
        $agenda= \DB::table('user_roles')->where('id_user', $id)->delete();
        
        $agenda= \DB::table('users')->where('id', $id)->delete();

        $agenda= \DB::table('tukang')->where('id_user', $id)->delete();

        $agenda= \DB::table('tukang_sertifikasi')->where('id_tukang', $id)->delete();
        return redirect()->back()->withSuccess('Deleted Success.');
        

    }

    public function destroy_toko($id)
    {
        
        $agenda= \DB::table('user_roles')->where('id_user', $id)->delete();
        
        $agenda= \DB::table('users')->where('id', $id)->delete();

        $agenda= \DB::table('toko')->where('id_user', $id)->delete();

        return redirect()->back()->withSuccess('Deleted Success.');
        

    }

}