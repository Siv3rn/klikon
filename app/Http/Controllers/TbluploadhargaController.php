<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\unduh;
use Illuminate\Support\Facades\Storage;
use Validator;
use Illuminate\Http\Request;

class TbluploadhargaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
		$unduh = \DB::table('tbl_upload_harga')
            ->select('*')
            ->orderBy('update_harga','DESC')
            ->get();
			
		return view('master.unduh.upload.index',['unduh'=>$unduh]);
    } 

    public function add()
    {
        $kota = \DB::table('kota')
            ->select('*')
            ->get();

        return view('master.unduh.upload.add',['kota'=>$kota]);
    }

    public function edit(Request $request,$id)
    {
        $unduh = \DB::table('tbl_upload_harga')
            ->select('*')
            ->where('id_upload',$id)
            ->get();
        
        $kota = \DB::table('kota')
            ->select('*')
            ->get();

        return view('master.unduh.upload.edit',['unduh'=>$unduh,'kota'=>$kota]);
    }

    public function create(Request $request)
    {   
        $validator = Validator::make($request->all(), [
            'file' => 'required|mimes:pdf',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors(['Error', 'Upload gagal Karena file yang diupload bukan pdf']);  
        }

        $toko = $request->nama_toko; 
        $kecamatan = $request->id_kecamatan;
        $update_harga = $request->update_harga;
        $file = $request->file('file');
        
        $filenameWithExt = $request->file('file')->getClientOriginalName();
        $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        $extension = $request->file('file')->getClientOriginalExtension();
        $filenameSimpan = $filename.'_'.time().'.'.$extension;
        $path = $request->file('file')->storeAs('/public/images/harga', $filenameSimpan);

        $kota = \DB::table('kota')
            ->select('kota')
            ->where('id_kota',$request->id_kota)
            ->get();
        foreach($kota as $k){
            
            $upload = new \App\Models\unduh();
            $upload->nama_toko = $toko;   
            $upload->file_harga = $filenameSimpan;
            $upload->alamat_toko = $kecamatan;
            $upload->kabupaten = $k->kota;
            $upload->update_harga = $update_harga;
            $upload->save();
            
        return redirect('/layanan/unduh')->with('Harga Berhasil Di Buat','Success');

        }

    }

    public function update(Request $request, $id)
   {
       if(empty($request->file('file'))){

           \DB::table('tbl_upload_harga')->where('id_upload',$id)->update([
               'nama_toko' => $request->nama_toko,
               'alamat_toko' => $request->alamat_toko,
               'update_harga' => $request->update_harga,
               'alamat_toko' => $request->id_kecamatan,
               'kabupaten' => $request->id_kota
               ]);
              
               return redirect('/layanan/unduh')->withSuccess('Upload Harga Berhasil Di Update','Success');

       }else{

            $validator = Validator::make($request->all(), [
                'file' => 'required|mimes:pdf',
            ]);

           if ($validator->fails()) {
               return redirect()->back()->withErrors(['Error', 'Upload gagal Karena file yang diupload bukan jpg,png,jpeg,gif,svg.']);  
           }

           $file = $request->file('file');
       
           $filenameWithExt = $request->file('file')->getClientOriginalName();
           $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
           $extension = $request->file('file')->getClientOriginalExtension();
           $filenameSimpan = $filename.'_'.time().'.'.$extension;
           $path = $request->file('file')->storeAs('/public/images/harga', $filenameSimpan);

           \DB::table('tbl_upload_harga')->where('id_upload',$id)->update([
            'nama_toko' => $request->nama_toko,
            'alamat_toko' => $request->alamat_toko,
            'update_harga' => $request->update_harga,
            'alamat_toko' => $request->id_kecamatan,
            'kabupaten' => $request->id_kota,
            'file_harga' => $filenameSimpan
            ]);
              
               return redirect('/layanan/unduh')->withSuccess('success','Upload Harga Berhasil Di Update');

       }

   }

    public function destroy($id)
    {
    $buku= \DB::table('tbl_upload_harga')->where('id_upload', $id)->delete();
    return redirect()->back()->withSuccess('Deleted Success.');
    }

}
