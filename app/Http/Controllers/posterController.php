<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Models\tbl_poster;

class posterController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
		$poster = \DB::table('tbl_poster')
            ->select('*')
            ->orderBy('id_poster','DESC')
            ->get();
			
		return view('master.unduh.poster.index',['poster'=>$poster]);
    }    

    public function add()
    {
        return view('master.unduh.poster.add');
    }

    public function edit(Request $request, $id)
    {
		$poster = \DB::table('tbl_poster')
            ->select('*')
            ->where('id_poster','=', $id)
            ->get();
			
		return view('master.unduh.poster.edit',['poster'=>$poster]);
    }    

    public function create(Request $request)
    {         
       $validator = Validator::make($request->all(), [
           'file' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
       ]);

       if ($validator->fails()) {
           return redirect()->back()->withErrors(['Error', 'Upload gagal Karena file yang diupload bukan jpg,png,jpeg,gif,svg atau images ukuran melebihi 2048']);  
       }

        $file = $request->file('file');
        
        $filenameWithExt = $request->file('file')->getClientOriginalName();
        $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        $extension = $request->file('file')->getClientOriginalExtension();
        $filenameSimpan = $filename.'_'.time().'.'.$extension;
        $path = $request->file('file')->storeAs('/public/images/poster', $filenameSimpan);

        $ip=\request()->ip();
           date_default_timezone_set("Asia/Jakarta");
        $banner = new \App\Models\tbl_poster();
        $banner->judul_poster = $request->judul_poster;   
        $banner->file_poster = $filenameSimpan;
        $banner->status_poster = 'tampil';
        $banner->save();

        
        return redirect('/unduh/poster')->with('Poster Berhasil Di Buat','Success');
    }

    public function update(Request $request, $id_poster)
   {
       $ip=\request()->ip();
           date_default_timezone_set("Asia/Jakarta");
       $validator = Validator::make($request->all(), [
           'judul_poster' => 'required',
           'file' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
       ]);

       if(empty($request->file('file'))){

           \DB::table('tbl_poster')->where('id_poster',$id_poster)->update([
               'judul_poster' => $request->judul_poster,
               'status_poster' => $request->status_poster
               ]);
              
               return redirect('/unduh/poster')->withSuccess('Poster Berhasil Di Update','Success');

       }else{

           if ($validator->fails()) {
               return redirect()->back()->withErrors(['Error', 'Upload gagal Karena file yang diupload bukan jpg,png,jpeg,gif,svg atau images ukuran melebihi 2048']);  
           }

           $file = $request->file('file');
       
           $filenameWithExt = $request->file('file')->getClientOriginalName();
           $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
           $extension = $request->file('file')->getClientOriginalExtension();
           $filenameSimpan = $filename.'_'.time().'.'.$extension;
           $path = $request->file('file')->storeAs('/public/images/poster', $filenameSimpan);

           \DB::table('tbl_poster')->where('id_poster',$id_poster)->update([
               'judul_poster' => $request->judul_poster,
               'status_poster' => $request->status_poster,
               'file_poster' => $filenameSimpan
               ]);
              
               return redirect('/unduh/poster')->withSuccess('success','Poster Berhasil Di Update');

       }

   }

   public function destroy($id)
   {
       $poster= \DB::table('tbl_poster')->where('id_poster', $id)->delete();
   return redirect()->back()->withSuccess('Deleted Success.');
   }
}
