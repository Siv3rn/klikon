<?php

namespace App\Http\Controllers;

use App\Imports\HargaImport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Models\harga;
use App\Models\bahan_j;
use App\Models\tbl_kecamatan;
use App\Models\kota;
use App\Models\importharga;
use Maatwebsite\Excel\Facades\Excel;

class hargaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
		    $harga = \DB::table('harga')
            ->join('bahan_j', 'bahan_j.id_jenis', '=', 'harga.id_jenis')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'harga.id_kecamatan')
            ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
            ->select('harga.id_harga as id_harga','harga.nama as barang','bahan_j.jenis as jenis','harga.harga as harga',
            'harga.last_update as last_update','tbl_kecamatan.nama_kecamatan as nama_kecamatan','kota.kota as kota')
            ->orderBy('harga.id_harga','DESC')
            ->get();

            $jenis = \DB::table('bahan_j')
            ->select('id_jenis','jenis','file')
            ->get();
			
		return view('master.layanan.harga.index',['harga'=>$harga,'jenis'=>$jenis]);
    }
    
    public function jenis()
    {
		    $harga = \DB::table('harga')
            ->join('bahan_j', 'bahan_j.id_jenis', '=', 'harga.id_jenis')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'harga.id_kecamatan')
            ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
            ->select('harga.id_harga as id_harga','harga.nama as barang','bahan_j.jenis as jenis','harga.harga as harga',
            'harga.last_update as last_update','tbl_kecamatan.nama_kecamatan as nama_kecamatan','kota.kota as kota')
            ->orderBy('harga.id_harga','DESC')
            ->get();

            $jenis = \DB::table('bahan_j')
            ->select('id_jenis','kode','jenis','file')
            ->get();
			
		return view('master.layanan.hbb.jenis',['harga'=>$harga,'jenis'=>$jenis]);
    }
    
    public function add()
    {
        return view('master.layanan.harga.add');
    }

    public function add_jenis()
    {
        return view('master.layanan.jenis.add');
    }

    public function create_jenis(Request $request)
    {         
        if(empty($request->file)){

            $count = \DB::table('bahan_j')
            ->select('kode')
            ->where('kode',$request->kode)
            ->count();

            $count2 = \DB::table('bahan_j')
            ->select('jenis')
            ->where('jenis',$request->jenis)
            ->count();
            if($count > 0 || $count2 > 0){
                return redirect()->back()->withErrors(['Error', 'Kode Barang atau Jenis Barang sudah digunakan']);
            }else{
                $ip=\request()->ip();
                date_default_timezone_set("Asia/Jakarta");
                $jenis = new \App\Models\bahan_j();
                $jenis->kode = $request->kode;  
                $jenis->jenis = $request->jenis;  
                $jenis->save();

                
                return redirect('/layanan/jenis')->with('Jenis Berhasil Di Buat','Success');
            }

    }else{
        $validator = Validator::make($request->all(), [
            'file' => 'required',
            'file' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
        ]);

       if ($validator->fails()) {
           return redirect()->back()->withErrors(['Error', 'Upload gagal Karena file yang diupload bukan jpg,png,jpeg,gif,svg atau pdf']);  
       }

       $count = \DB::table('bahan_j')
            ->select('kode')
            ->where('kode',$request->kode)
            ->count();

            $count2 = \DB::table('bahan_j')
            ->select('jenis')
            ->where('jenis',$request->jenis)
            ->count();
            if($count > 0 || $count2 > 0){
                return redirect()->back()->withErrors(['Error', 'Kode Barang atau Jenis Barang sudah digunakan']);
            }else{
                $file = $request->file('file');
        
                $filenameWithExt = $request->file('file')->getClientOriginalName();
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                $extension = $request->file('file')->getClientOriginalExtension();
                $filenameSimpan = $filename.'_'.time().'.'.$extension;
                $path = $request->file('file')->storeAs('/public/images/bahan', $filenameSimpan);

                $ip=\request()->ip();
                date_default_timezone_set("Asia/Jakarta");
                $jenis = new \App\Models\bahan_j();
                $jenis->kode = $request->kode; 
                $jenis->jenis = $request->jenis;  
                $jenis->file = $filenameSimpan;
                $jenis->save();

                
                return redirect('/layanan/jenis')->with('Harga Berhasil Di Buat','Success');
            }
        
    }
    }


    public function edit(Request $request, $id)
    {
		$harga = \DB::table('harga')
            ->select('*')
            ->where('id_harga','=', $id)
            ->get();
			
		return view('master.layanan.harga.edit',['harga'=>$harga]);
    }    

    public function edit_jenis(Request $request, $id)
    {
		$jenis = \DB::table('bahan_j')
            ->select('*')
            ->where('id_jenis','=', $id)
            ->get();
			
		return view('master.layanan.jenis.edit',['jenis'=>$jenis]);
    }    

    public function create(Request $request)
    {         

        $ip=\request()->ip();
           date_default_timezone_set("Asia/Jakarta");
        $agenda = new \App\Models\harga();
        $agenda->id_jenis = $request->id_jenis;         
        $agenda->nama = $request->nama;
        $agenda->harga = $request->harga;
        $agenda->ukuran = $request->ukuran;
        $agenda->id_kecamatan = $request->id_kecamatan;
        $agenda->last_update = date('Y-m-d');
        $agenda->agen = $request->agen;
        $agenda->stock = $request->stock;
        $agenda->kwalitas = '';
        $agenda->keterangan = '';
        $agenda->save();

        
        return redirect('/layanan/harga')->with('Harga Berhasil Di Buat','Success');
    }

    public function update(Request $request, $id)
    {
            \DB::table('harga')->where('id_harga',$id)->update([
                'id_jenis' => $request->id_jenis,
                'nama' => $request->nama,
                'harga' => $request->harga,
                'ukuran' => $request->ukuran,
                'agen' => $request->agen,
                'last_update' => date('Y-m-d'),
                'id_kecamatan' => $request->id_kecamatan,
                'updated_at' => date('Y-m-d')
                ]);
               
                return redirect('/layanan/harga')->withSuccess('Harga Berhasil Di Update','Success');

        }

        public function update_jenis(Request $request, $id)
    {
        if(empty($request->file)){

            \DB::table('bahan_j')->where('id_jenis',$id)->update([
                'kode' => $request->kode,
                'jenis' => $request->jenis
                ]);
               
                return redirect('/layanan/jenis')->withSuccess('Harga Berhasil Di Update','Success');
            }else{
                $validator = Validator::make($request->all(), [
                    'file' => 'required',
                    'file' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
                ]);
        
               if ($validator->fails()) {
                   return redirect()->back()->withErrors(['Error', 'Upload gagal Karena file yang diupload bukan jpg,png,jpeg,gif,svg atau pdf']);  
               }
        
                $file = $request->file('file');
                
                $filenameWithExt = $request->file('file')->getClientOriginalName();
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                $extension = $request->file('file')->getClientOriginalExtension();
                $filenameSimpan = $filename.'_'.time().'.'.$extension;
                $path = $request->file('file')->storeAs('/public/images/bahan', $filenameSimpan);
        
                \DB::table('bahan_j')->where('id_jenis',$id)->update([
                    'kode' => $request->kode,
                    'jenis' => $request->jenis,
                    'file' => $filenameSimpan
                    ]);
        
                
                return redirect('/layanan/jenis')->with('Harga Berhasil Di Buat','Success');
            }

        }

    public function destroy($id)
    {
        $agenda= \DB::table('harga')->where('id_harga', $id)->delete();
        return redirect()->back()->withSuccess('Deleted Success.');
    }

    public function destroy_jenis($id)
    {
        $agenda= \DB::table('bahan_j')->where('id_jenis', $id)->delete();
        return redirect()->back()->withSuccess('Deleted Success.');
    }

    public function importexcel(Request $request) 
	{
		// validasi
        $validator = Validator::make($request->all(), [
            'file' => 'required|mimes:csv,xls,xlsx',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors(['Error', 'Upload gagal karena file bukan, csv,xls,xlsx']);  
        }else{
            // menangkap file excel
		$file = $request->file('file');
 
		// membuat nama file unik
		$nama_file = rand().$file->getClientOriginalName();
 
		// upload ke folder file/harga di dalam folder public
		$file->move('file_harga',$nama_file);
 
		// import data
		Excel::import(new HargaImport, public_path('/file_harga/'.$nama_file));
 
		// alihkan halaman kembali
        return redirect('/layanan/harga')->withSuccess('Harga Berhasil Di Import','Success');
        }
 
	}

}
