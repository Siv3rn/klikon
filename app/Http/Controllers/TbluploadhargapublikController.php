<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\unduh;
use Illuminate\Support\Facades\Storage;
use Validator;
use Illuminate\Http\Request;

class TbluploadhargapublikController extends Controller
{
    public function all()
    {
		$unduh = \DB::table('tbl_upload_harga')
            ->select('*')
            ->orderBy('update_harga','DESC')
            ->get();
			
		return view('master.unduh.upload.all',['unduh'=>$unduh]);
    } 
}
