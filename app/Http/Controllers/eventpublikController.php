<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Models\tbl_event;
use App\Models\tbl_file_pendukung;

class eventpublikController extends Controller
{
    public function all()
    {
		$event = \DB::table('tbl_event')
            ->select('*')
            ->orderBy('id_event','DESC')
            ->get();
			
            $id_kat_konsul = \DB::table('tbl_konsultasi')
            ->select('*')
            ->limit(5)
            ->orderBy('id_konsultasi','DESC')
            ->get();

            $video = \DB::table('tbl_video')
            ->select('*')
            ->where('status_video', '=', 'aktif')
            ->get();

            $poster = \DB::table('tbl_poster')
            ->select('*')
            ->where('status_poster', '=', 'tampil')
            ->get();

            $konsul = \DB::table('tbl_konsultasi')
            ->select('*')
            ->where('tgl_jawaban', '<>', '')
            ->orWhereNotNull('tgl_jawaban')
            ->orderBy('tgl_jawaban','DESC')
            ->limit(5)
            ->get();
			
		return view('master.informasi.event.all',['event'=>$event,'konsul'=>$konsul,
                    'id_kat_konsul'=>$id_kat_konsul,'video'=>$video,'poster'=>$poster]);
    }
}
