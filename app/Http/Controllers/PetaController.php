<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PetaController extends Controller
{
    public function all(Request $request)
    {
        $map = \DB::table('peta')
            ->select('*')
            ->where('id_jenis','=',1)
            ->get();

        $map2 = \DB::table('peta')
            ->select('*')
            ->where('id_jenis','=',2)
            ->get();

        return view('master.informasi.peta.all',['map'=>$map,'map2'=>$map2]);
    }
}
