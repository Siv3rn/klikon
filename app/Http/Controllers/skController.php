<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Models\tbl_sk;

class skController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
		$sk = \DB::table('tbl_sk')
            ->select('*')
            ->orderBy('id_sk','DESC')
            ->get();
			
		return view('master.unduh.sk.index',['sk'=>$sk]);
    }    

    public function add()
    {
        return view('master.unduh.sk.add');
    }

    public function edit(Request $request, $id)
    {
		$sk = \DB::table('tbl_sk')
            ->select('*')
            ->where('id_sk','=', $id)
            ->get();
			
		return view('master.unduh.sk.edit',['sk'=>$sk]);
    }    

    public function create(Request $request)
     {         
        $validator = Validator::make($request->all(), [
            'file' => 'required|mimes:jpg,jpeg,png,bmp,gif,svg,webp,pdf,docx',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors(['Error', 'Upload gagal Karena file yang diupload ukuran melebihi 5mb']);  
        }

         $file = $request->file('file');
         
         $filenameWithExt = $request->file('file')->getClientOriginalName();
         $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
         $extension = $request->file('file')->getClientOriginalExtension();
         $filenameSimpan = $filename.'_'.time().'.'.$extension;
         $path = $request->file('file')->storeAs('/public/images/sk', $filenameSimpan);
 
         date_default_timezone_set("Asia/Jakarta");
         $sk_file = new \App\Models\tbl_sk();
         $sk_file->judul_sk = $request->judul_sk;   
         $sk_file->file_sk = $filenameSimpan;
         $sk_file->save();
 
         
         return redirect('/unduh/sk')->with('sk Berhasil Di Buat','Success');
     }

    public function destroy($id)
    {
        
        $sk1= \DB::table('tbl_sk')->where('id_sk', $id)->delete();
    return redirect()->back()->withSuccess('Deleted Success.');
    }

        public function update(Request $request, $id_sk)
    {
            date_default_timezone_set("Asia/Jakarta");
		$validator = Validator::make($request->all(), [
            'file' => 'required|mimes:jpg,jpeg,png,bmp,gif,svg,webp,pdf,docx',
        ]);

        if(empty($request->file('file'))){

            \DB::table('tbl_sk')->where('id_sk',$id_sk)->update([
                'judul_sk' => $request->judul_sk
                ]);
               
                return redirect('/unduh/sk')->withSuccess('File Pendukung Berhasil Di Update','Success');

        }else{

            if ($validator->fails()) {
                return redirect()->back()->withErrors(['Error', 'Upload gagal Karena file yang diupload bukan jpg,jpeg,png,bmp,gif,svg,webp,pdf,docx']);  
            }

            $file = $request->file('file');
		
            $filenameWithExt = $request->file('file')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('file')->getClientOriginalExtension();
            $filenameSimpan = $filename.'_'.time().'.'.$extension;
            $path = $request->file('file')->storeAs('/public/images/sk', $filenameSimpan);

            \DB::table('tbl_sk')->where('id_sk',$id_sk)->update([
                'judul_sk' => $request->judul_sk,
                'file_sk' => $filenameSimpan
                ]);
               
                return redirect('/unduh/sk')->withSuccess('success','File sk Berhasil Di Update');

        }

	}
}
