<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Models\bahan_j;
use App\Models\tbl_barang;
use App\Models\tbl_harga;

class TblhargapublikController extends Controller
{
    //search by
    public function all(Request $request)
    {         
        
        $ip=\request()->ip();
        $nama = $request->nama;
        $id_jenis = $request->id_jenis;
        $id_kota = $request->id_kota;
        $id_kecamatan = $request->id_kecamatan; 
        $urut = $request->urut;
        $year = $request->tahun;

        if($year > 0){

            if(!empty($nama) && $id_jenis == 0 && $id_kota == 0 && $id_kecamatan == null && $urut == null && $year > 0){

        
                $harga = \DB::table('tbl_harga')
                    ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_harga.id_kecamatan')
                    ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
                    ->join('tbl_barang', 'tbl_harga.kode_barang', '=', 'tbl_barang.kode_jenis')
                    ->join('bahan_j', 'bahan_j.id_jenis', '=', 'tbl_barang.id_jenis')
                    ->select('tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
                    ->groupBy('tbl_harga.last_update','tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
                    ->Where('tbl_barang.nama_barang', 'like', '%'.$request->nama.'%')
                    ->where(\DB::raw('DATE_FORMAT(tbl_harga.last_update, "%Y")'), '=',$year)
                    ->orderBy('tbl_harga.last_update','DESC')
                    ->paginate(10);
                
                $jenis = \DB::table('bahan_j')
                    ->select('*')
                    ->get();
        
                $kecamatan = \DB::table('tbl_kecamatan')
                    ->join('kota','kota.id_kota','=', 'kota.id_kota')
                    ->select('tbl_kecamatan.nama_kecamatan as kecamatan','kota.kota as kota'
                            ,'tbl_kecamatan.id_kecamatan as id_kecamatan')
                    ->get();
        
                $kota = \DB::table('kota')
                    ->select('*')
                    ->get();
        
                $tahun = $request->tahun;
        
                return view('master.layanan.hbb.all2',['harga'=>$harga,'jenis'=>$jenis,'kecamatan'=>$kecamatan,'kota'=>$kota,'tahun'=>$tahun]);

        }elseif($nama == null && $id_jenis > 0 && $id_kota == 0 && $id_kecamatan == null && $urut == null && $year > 0){
         
            $harga = \DB::table('tbl_harga')
                ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_harga.id_kecamatan')
                ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
                ->join('tbl_barang', 'tbl_harga.kode_barang', '=', 'tbl_barang.kode_jenis')
                ->join('bahan_j', 'bahan_j.id_jenis', '=', 'tbl_barang.id_jenis')
                ->select('tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
                ->groupBy('tbl_harga.last_update','tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
                ->Where('tbl_barang.id_jenis', '=', $id_jenis)
                ->where(\DB::raw('DATE_FORMAT(tbl_harga.last_update, "%Y")'), '=',$year)
                ->orderBy('tbl_harga.last_update','DESC')
                ->paginate(10);
    
            $jenis = \DB::table('bahan_j')
                ->select('*')
                ->get();
    
            $kecamatan = \DB::table('tbl_kecamatan')
                ->join('kota','kota.id_kota','=', 'kota.id_kota')
                ->select('tbl_kecamatan.nama_kecamatan as kecamatan','kota.kota as kota'
                        ,'tbl_kecamatan.id_kecamatan as id_kecamatan')
                ->get();
    
            $kota = \DB::table('kota')
                ->select('*')
                ->get();
    
            $tahun = $request->tahun;
    
            return view('master.layanan.hbb.all2',['harga'=>$harga,'jenis'=>$jenis,'kecamatan'=>$kecamatan,'kota'=>$kota,'tahun'=>$tahun]);
    
            }elseif(!empty($nama) && $id_jenis > 0 && $id_kota == 0 && $id_kecamatan == null && $urut == null && $year > 0){
                
            $harga = \DB::table('tbl_harga')
                ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_harga.id_kecamatan')
                ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
                ->join('tbl_barang', 'tbl_harga.kode_barang', '=', 'tbl_barang.kode_jenis')
                ->join('bahan_j', 'bahan_j.id_jenis', '=', 'tbl_barang.id_jenis')
                ->select('tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
                ->groupBy('tbl_harga.last_update','tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
                ->Where('tbl_barang.id_jenis', '=', $id_jenis)
                ->Where('tbl_barang.nama_barang', 'like', '%'.$request->nama.'%')
                ->where(\DB::raw('DATE_FORMAT(tbl_harga.last_update, "%Y")'), '=',$year)
                ->orderBy('tbl_harga.last_update','DESC')
                ->paginate(10);
    
            $jenis = \DB::table('bahan_j')
                ->select('*')
                ->get();
    
            $kecamatan = \DB::table('tbl_kecamatan')
                ->join('kota','kota.id_kota','=', 'kota.id_kota')
                ->select('tbl_kecamatan.nama_kecamatan as kecamatan','kota.kota as kota'
                        ,'tbl_kecamatan.id_kecamatan as id_kecamatan')
                ->get();
    
            $kota = \DB::table('kota')
                ->select('*')
                ->get();
    
            $tahun = $request->tahun;
    
            return view('master.layanan.hbb.all2',['harga'=>$harga,'jenis'=>$jenis,'kecamatan'=>$kecamatan,'kota'=>$kota,'tahun'=>$tahun]);
                    
            }elseif($nama == null && $id_jenis == 0 && $id_kota > 0 && $id_kecamatan == 0 && $urut == null && $year > 0){
                
            $harga = \DB::table('tbl_harga')
                ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_harga.id_kecamatan')
                ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
                ->join('tbl_barang', 'tbl_harga.kode_barang', '=', 'tbl_barang.kode_jenis')
                ->join('bahan_j', 'bahan_j.id_jenis', '=', 'tbl_barang.id_jenis')
                ->select('tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
                ->groupBy('tbl_harga.last_update','tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
                ->Where('kota.id_kota', '=', $id_kota)
                ->where(\DB::raw('DATE_FORMAT(tbl_harga.last_update, "%Y")'), '=',$year)
                ->orderBy('tbl_harga.last_update','DESC')
                ->paginate(10);
    
            $jenis = \DB::table('bahan_j')
                ->select('*')
                ->get();
    
            $kecamatan = \DB::table('tbl_kecamatan')
                ->join('kota','kota.id_kota','=', 'kota.id_kota')
                ->select('tbl_kecamatan.nama_kecamatan as kecamatan','kota.kota as kota'
                        ,'tbl_kecamatan.id_kecamatan as id_kecamatan')
                ->get();
    
            $kota = \DB::table('kota')
                ->select('*')
                ->get();
    
            $tahun = $request->tahun;
    
            return view('master.layanan.hbb.all2',['harga'=>$harga,'jenis'=>$jenis,'kecamatan'=>$kecamatan,'kota'=>$kota,'tahun'=>$tahun]);
                    
            }elseif($nama == null && $id_jenis == 0 && $id_kota > 0 && $id_kecamatan > 0 && $urut == null && $year > 0){
                
    
            $harga = \DB::table('tbl_harga')
                ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_harga.id_kecamatan')
                ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
                ->join('tbl_barang', 'tbl_harga.kode_barang', '=', 'tbl_barang.kode_jenis')
                ->join('bahan_j', 'bahan_j.id_jenis', '=', 'tbl_barang.id_jenis')
                ->select('tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
                ->Where('tbl_kecamatan.id_kecamatan', '=', $id_kecamatan)
                ->where(\DB::raw('DATE_FORMAT(tbl_harga.last_update, "%Y")'), '=',$year)
                ->groupBy('tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
                ->paginate(10);
    
            $jenis = \DB::table('bahan_j')
                ->select('*')
                ->get();
    
            $kecamatan = \DB::table('tbl_kecamatan')
                ->join('kota','kota.id_kota','=', 'kota.id_kota')
                ->select('tbl_kecamatan.nama_kecamatan as kecamatan','kota.kota as kota'
                        ,'tbl_kecamatan.id_kecamatan as id_kecamatan')
                ->get();
    
            $kota = \DB::table('kota')
                ->select('*')
                ->get();
    
            $tahun = $request->tahun;
    
            return view('master.layanan.hbb.all2',['harga'=>$harga,'jenis'=>$jenis,'kecamatan'=>$kecamatan,'kota'=>$kota,'tahun'=>$tahun]);
                    
            }elseif($nama == null && $id_jenis == 0 && $id_kota == 0 && $id_kecamatan == null && !empty($urut) && $year > 0){
                
    
                $harga = \DB::table('tbl_harga')
                ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_harga.id_kecamatan')
                ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
                ->join('tbl_barang', 'tbl_harga.kode_barang', '=', 'tbl_barang.kode_jenis')
                ->join('bahan_j', 'bahan_j.id_jenis', '=', 'tbl_barang.id_jenis')
                ->select('tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
                ->where(\DB::raw('DATE_FORMAT(tbl_harga.last_update, "%Y")'), '=',$year)
                ->groupBy('tbl_harga.last_update','tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
                ->orderBy('tbl_harga.last_update','DESC')
                ->paginate(10);
    
            $jenis = \DB::table('bahan_j')
                ->select('*')
                ->get();
    
            $kecamatan = \DB::table('tbl_kecamatan')
                ->join('kota','kota.id_kota','=', 'kota.id_kota')
                ->select('tbl_kecamatan.nama_kecamatan as kecamatan','kota.kota as kota'
                        ,'tbl_kecamatan.id_kecamatan as id_kecamatan')
                ->get();
    
            $kota = \DB::table('kota')
                ->select('*')
                ->get();
    
            $tahun = $request->tahun;
    
            return view('master.layanan.hbb.all2',['harga'=>$harga,'jenis'=>$jenis,'kecamatan'=>$kecamatan,'kota'=>$kota,'tahun'=>$tahun]);
                    
            }elseif(!empty($nama) && $id_jenis == 0 && $id_kota == 0 && $id_kecamatan == null && !empty($urut) && $year > 0){
                
    
            $harga = \DB::table('tbl_harga')
                ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_harga.id_kecamatan')
                ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
                ->join('tbl_barang', 'tbl_harga.kode_barang', '=', 'tbl_barang.kode_jenis')
                ->join('bahan_j', 'bahan_j.id_jenis', '=', 'tbl_barang.id_jenis')
                ->select('tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
                ->groupBy('tbl_harga.last_update','tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
                ->Where('tbl_barang.nama_barang', 'like', '%'.$request->nama.'%')
                ->where(\DB::raw('DATE_FORMAT(tbl_harga.last_update, "%Y")'), '=',$year)
                ->orderBy('tbl_harga.last_update','DESC')
                ->paginate(10);
    
            $jenis = \DB::table('bahan_j')
                ->select('*')
                ->get();
    
            $kecamatan = \DB::table('tbl_kecamatan')
                ->join('kota','kota.id_kota','=', 'kota.id_kota')
                ->select('tbl_kecamatan.nama_kecamatan as kecamatan','kota.kota as kota'
                        ,'tbl_kecamatan.id_kecamatan as id_kecamatan')
                ->get();
    
            $kota = \DB::table('kota')
                ->select('*')
                ->get();
    
            $tahun = $request->tahun;
    
            return view('master.layanan.hbb.all2',['harga'=>$harga,'jenis'=>$jenis,'kecamatan'=>$kecamatan,'kota'=>$kota,'tahun'=>$tahun]);
                    
            }elseif($nama == null && $id_jenis > 0 && $id_kota == 0 && $id_kecamatan == null && !empty($urut) && $year > 0){
                
    
            $harga = \DB::table('tbl_harga')
                ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_harga.id_kecamatan')
                ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
                ->join('tbl_barang', 'tbl_harga.kode_barang', '=', 'tbl_barang.kode_jenis')
                ->join('bahan_j', 'bahan_j.id_jenis', '=', 'tbl_barang.id_jenis')
                ->select('tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
                ->groupBy('tbl_harga.last_update','tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
                ->Where('tbl_barang.nama_barang', 'like', '%'.$request->nama.'%')
                ->where(\DB::raw('DATE_FORMAT(tbl_harga.last_update, "%Y")'), '=',$year)
                ->orderBy('tbl_harga.last_update','DESC')
                ->paginate(10);
    
            $jenis = \DB::table('bahan_j')
                ->select('*')
                ->get();
    
            $kecamatan = \DB::table('tbl_kecamatan')
                ->join('kota','kota.id_kota','=', 'kota.id_kota')
                ->select('tbl_kecamatan.nama_kecamatan as kecamatan','kota.kota as kota'
                        ,'tbl_kecamatan.id_kecamatan as id_kecamatan')
                ->get();
    
            $kota = \DB::table('kota')
                ->select('*')
                ->get();
    
            $tahun = $request->tahun;
    
            return view('master.layanan.hbb.all2',['harga'=>$harga,'jenis'=>$jenis,'kecamatan'=>$kecamatan,'kota'=>$kota,'tahun'=>$tahun]);
                    
            }elseif($nama == null && $id_jenis == 0 && $id_kota > 0 && $id_kecamatan == null && !empty($urut) && $year > 0){
                
    
            $harga = \DB::table('tbl_harga')
                ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_harga.id_kecamatan')
                ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
                ->join('tbl_barang', 'tbl_harga.kode_barang', '=', 'tbl_barang.kode_jenis')
                ->join('bahan_j', 'bahan_j.id_jenis', '=', 'tbl_barang.id_jenis')
                ->select('tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
                ->groupBy('tbl_harga.last_update','tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
                ->Where('tbl_barang.nama_barang', 'like', '%'.$request->nama.'%')
                ->where(\DB::raw('DATE_FORMAT(tbl_harga.last_update, "%Y")'), '=',$year)
                ->orderBy('tbl_harga.last_update','DESC')
                ->paginate(10);
    
            $jenis = \DB::table('bahan_j')
                ->select('*')
                ->get();
    
            $kecamatan = \DB::table('tbl_kecamatan')
                ->join('kota','kota.id_kota','=', 'kota.id_kota')
                ->select('tbl_kecamatan.nama_kecamatan as kecamatan','kota.kota as kota'
                        ,'tbl_kecamatan.id_kecamatan as id_kecamatan')
                ->get();
    
            $kota = \DB::table('kota')
                ->select('*')
                ->get();
    
            $tahun = $request->tahun;
    
            return view('master.layanan.hbb.all2',['harga'=>$harga,'jenis'=>$jenis,'kecamatan'=>$kecamatan,'kota'=>$kota,'tahun'=>$tahun]);
                    
            }elseif($nama == null && $id_jenis == 0 && $id_kota > 0 && $id_kecamatan > 0 && !empty($urut) && $year > 0){
                
    
            $harga = \DB::table('tbl_harga')
                ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_harga.id_kecamatan')
                ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
                ->join('tbl_barang', 'tbl_harga.kode_barang', '=', 'tbl_barang.kode_jenis')
                ->join('bahan_j', 'bahan_j.id_jenis', '=', 'tbl_barang.id_jenis')
                ->select('tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
                ->groupBy('tbl_harga.last_update','tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
                ->Where('tbl_barang.nama_barang', 'like', '%'.$request->nama.'%')
                ->where(\DB::raw('DATE_FORMAT(tbl_harga.last_update, "%Y")'), '=',$year)
                ->orderBy('tbl_harga.last_update','DESC')
                ->paginate(10);
    
            $jenis = \DB::table('bahan_j')
                ->select('*')
                ->get();
    
            $kecamatan = \DB::table('tbl_kecamatan')
                ->join('kota','kota.id_kota','=', 'kota.id_kota')
                ->select('tbl_kecamatan.nama_kecamatan as kecamatan','kota.kota as kota'
                        ,'tbl_kecamatan.id_kecamatan as id_kecamatan')
                ->get();
    
            $kota = \DB::table('kota')
                ->select('*')
                ->get();
    
            $tahun = $request->tahun;
    
            return view('master.layanan.hbb.all2',['harga'=>$harga,'jenis'=>$jenis,'kecamatan'=>$kecamatan,'kota'=>$kota,'tahun'=>$tahun]);
                    
            }elseif(!empty($nama) && $id_jenis == 0 && $id_kota > 0 && $id_kecamatan == 0 && $urut == null && $year > 0){
    
            $harga = \DB::table('tbl_harga')
                ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_harga.id_kecamatan')
                ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
                ->join('tbl_barang', 'tbl_harga.kode_barang', '=', 'tbl_barang.kode_jenis')
                ->join('bahan_j', 'bahan_j.id_jenis', '=', 'tbl_barang.id_jenis')
                ->select('tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
                ->groupBy('tbl_harga.last_update','tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
                ->Where('tbl_barang.nama_barang', 'like', '%'.$request->nama.'%')
                ->Where('kota.id_kota', '=', $id_kota)
                ->where(\DB::raw('DATE_FORMAT(tbl_harga.last_update, "%Y")'), '=',$year)
                ->orderBy('tbl_harga.last_update','DESC')
                ->paginate(10);
    
            $jenis = \DB::table('bahan_j')
                ->select('*')
                ->get();
    
            $kecamatan = \DB::table('tbl_kecamatan')
                ->join('kota','kota.id_kota','=', 'kota.id_kota')
                ->select('tbl_kecamatan.nama_kecamatan as kecamatan','kota.kota as kota'
                        ,'tbl_kecamatan.id_kecamatan as id_kecamatan')
                ->get();
    
            $kota = \DB::table('kota')
                ->select('*')
                ->get();
    
            $tahun = $request->tahun;
    
            return view('master.layanan.hbb.all2',['harga'=>$harga,'jenis'=>$jenis,'kecamatan'=>$kecamatan,'kota'=>$kota,'tahun'=>$tahun]);
                    
            }elseif(!empty($nama) && $id_jenis == 0 && $id_kota > 0 && $id_kecamatan > 0 && $urut == null && $year > 0){
                
    
            $harga = \DB::table('tbl_harga')
                ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_harga.id_kecamatan')
                ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
                ->join('tbl_barang', 'tbl_harga.kode_barang', '=', 'tbl_barang.kode_jenis')
                ->join('bahan_j', 'bahan_j.id_jenis', '=', 'tbl_barang.id_jenis')
                ->select('tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
                ->groupBy('tbl_harga.last_update','tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
                ->Where('tbl_barang.nama_barang', 'like', '%'.$request->nama.'%')
                ->Where('kota.id_kota', '=', $id_kota)
                ->Where('tbl_harga.id_kecamatan', '=', $id_kecamatan)
                ->where(\DB::raw('DATE_FORMAT(tbl_harga.last_update, "%Y")'), '=',$year)
                ->orderBy('tbl_harga.last_update','DESC')
                ->paginate(10);
    
            $jenis = \DB::table('bahan_j')
                ->select('*')
                ->get();
    
            $kecamatan = \DB::table('tbl_kecamatan')
                ->join('kota','kota.id_kota','=', 'kota.id_kota')
                ->select('tbl_kecamatan.nama_kecamatan as kecamatan','kota.kota as kota'
                        ,'tbl_kecamatan.id_kecamatan as id_kecamatan')
                ->get();
    
            $kota = \DB::table('kota')
                ->select('*')
                ->get();
    
            $tahun = $request->tahun;
    
            return view('master.layanan.hbb.all2',['harga'=>$harga,'jenis'=>$jenis,'kecamatan'=>$kecamatan,'kota'=>$kota,'tahun'=>$tahun]);
                    
        }elseif($nama == null && $id_jenis > 0 && $id_kota > 0 && $id_kecamatan > 0 && $urut == null && $year > 0){
                
    
            $harga = \DB::table('tbl_harga')
                ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_harga.id_kecamatan')
                ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
                ->join('tbl_barang', 'tbl_harga.kode_barang', '=', 'tbl_barang.kode_jenis')
                ->join('bahan_j', 'bahan_j.id_jenis', '=', 'tbl_barang.id_jenis')
                ->select('tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
                ->groupBy('tbl_harga.last_update','tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
                ->Where('tbl_barang.id_jenis', '=', $id_jenis)
                ->Where('kota.id_kota', '=', $id_kota)
                ->Where('tbl_harga.id_kecamatan', '=', $id_kecamatan)
                ->where(\DB::raw('DATE_FORMAT(tbl_harga.last_update, "%Y")'), '=',$year)
                ->orderBy('tbl_harga.last_update','DESC')
                ->paginate(10);
    
            $jenis = \DB::table('bahan_j')
                ->select('*')
                ->get();
    
            $kecamatan = \DB::table('tbl_kecamatan')
                ->join('kota','kota.id_kota','=', 'kota.id_kota')
                ->select('tbl_kecamatan.nama_kecamatan as kecamatan','kota.kota as kota'
                        ,'tbl_kecamatan.id_kecamatan as id_kecamatan')
                ->get();
    
            $kota = \DB::table('kota')
                ->select('*')
                ->get();
    
            $tahun = $request->tahun;
    
            return view('master.layanan.hbb.all2',['harga'=>$harga,'jenis'=>$jenis,'kecamatan'=>$kecamatan,'kota'=>$kota,'tahun'=>$tahun]);
                    
        }elseif($nama == null && $id_jenis > 0 && $id_kota > 0 && $id_kecamatan == 0 && $urut == null && $year > 0){
                
    
            $harga = \DB::table('tbl_harga')
                ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_harga.id_kecamatan')
                ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
                ->join('tbl_barang', 'tbl_harga.kode_barang', '=', 'tbl_barang.kode_jenis')
                ->join('bahan_j', 'bahan_j.id_jenis', '=', 'tbl_barang.id_jenis')
                ->select('tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
                ->groupBy('tbl_harga.last_update','tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
                ->Where('tbl_barang.id_jenis', '=', $id_jenis)
                ->Where('kota.id_kota', '=', $id_kota)
                ->where(\DB::raw('DATE_FORMAT(tbl_harga.last_update, "%Y")'), '=',$year)
                ->orderBy('tbl_harga.last_update','DESC')
                ->paginate(10);
    
            $jenis = \DB::table('bahan_j')
                ->select('*')
                ->get();
    
            $kecamatan = \DB::table('tbl_kecamatan')
                ->join('kota','kota.id_kota','=', 'kota.id_kota')
                ->select('tbl_kecamatan.nama_kecamatan as kecamatan','kota.kota as kota'
                        ,'tbl_kecamatan.id_kecamatan as id_kecamatan')
                ->get();
    
            $kota = \DB::table('kota')
                ->select('*')
                ->get();
    
            $tahun = $request->tahun;
    
            return view('master.layanan.hbb.all2',['harga'=>$harga,'jenis'=>$jenis,'kecamatan'=>$kecamatan,'kota'=>$kota,'tahun'=>$tahun]);
                    
            }else{
                
            $harga = \DB::table('tbl_harga')
                ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_harga.id_kecamatan')
                ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
                ->join('tbl_barang', 'tbl_harga.kode_barang', '=', 'tbl_barang.kode_jenis')
                ->join('bahan_j', 'bahan_j.id_jenis', '=', 'tbl_barang.id_jenis')
                ->select('tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
                ->where(\DB::raw('DATE_FORMAT(tbl_harga.last_update, "%Y")'), '=',$year)
                ->groupBy('tbl_harga.last_update','tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
                ->orderBy('tbl_harga.last_update','DESC')
                ->paginate(10);
    
            $jenis = \DB::table('bahan_j')
                ->select('*')
                ->get();
    
            $kecamatan = \DB::table('tbl_kecamatan')
                ->join('kota','kota.id_kota','=', 'kota.id_kota')
                ->select('tbl_kecamatan.nama_kecamatan as kecamatan','kota.kota as kota'
                        ,'tbl_kecamatan.id_kecamatan as id_kecamatan')
                ->get();
    
            $kota = \DB::table('kota')
                ->select('*')
                ->get();
            $tahun = $request->tahun;
    
            return view('master.layanan.hbb.all2',['harga'=>$harga,'jenis'=>$jenis,'kecamatan'=>$kecamatan,'kota'=>$kota,'tahun'=>$tahun]);
    
            }
		}else{
            //bagian tanpa tahun dibawah
        if(!empty($nama) && $id_jenis == 0 && $id_kota == 0 && $id_kecamatan == null && $urut == null){

        
        $harga = \DB::table('tbl_harga')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_harga.id_kecamatan')
            ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
            ->join('tbl_barang', 'tbl_harga.kode_barang', '=', 'tbl_barang.kode_jenis')
            ->join('bahan_j', 'bahan_j.id_jenis', '=', 'tbl_barang.id_jenis')
            ->select('tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
            ->groupBy('tbl_harga.last_update','tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
            ->Where('tbl_barang.nama_barang', 'like', '%'.$request->nama.'%')
            ->orderBy('tbl_harga.last_update','DESC')
            ->paginate(10);
        
        $jenis = \DB::table('bahan_j')
            ->select('*')
            ->get();

        $kecamatan = \DB::table('tbl_kecamatan')
            ->join('kota','kota.id_kota','=', 'kota.id_kota')
            ->select('tbl_kecamatan.nama_kecamatan as kecamatan','kota.kota as kota'
                    ,'tbl_kecamatan.id_kecamatan as id_kecamatan')
            ->get();

        $kota = \DB::table('kota')
            ->select('*')
            ->get();

        $tahun = $request->tahun;

        return view('master.layanan.hbb.all2',['harga'=>$harga,'jenis'=>$jenis,'kecamatan'=>$kecamatan,'kota'=>$kota,'tahun'=>$tahun]);
            
            
        }elseif($nama == null && $id_jenis > 0 && $id_kota == 0 && $id_kecamatan == null && $urut == null){
         
        $harga = \DB::table('tbl_harga')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_harga.id_kecamatan')
            ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
            ->join('tbl_barang', 'tbl_harga.kode_barang', '=', 'tbl_barang.kode_jenis')
            ->join('bahan_j', 'bahan_j.id_jenis', '=', 'tbl_barang.id_jenis')
            ->select('tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
            ->groupBy('tbl_harga.last_update','tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
            ->Where('tbl_barang.id_jenis', '=', $id_jenis)
            ->orderBy('tbl_harga.last_update','DESC')
            ->paginate(10);

        $jenis = \DB::table('bahan_j')
            ->select('*')
            ->get();

        $kecamatan = \DB::table('tbl_kecamatan')
            ->join('kota','kota.id_kota','=', 'kota.id_kota')
            ->select('tbl_kecamatan.nama_kecamatan as kecamatan','kota.kota as kota'
                    ,'tbl_kecamatan.id_kecamatan as id_kecamatan')
            ->get();

        $kota = \DB::table('kota')
            ->select('*')
            ->get();

        $tahun = $request->tahun;

        return view('master.layanan.hbb.all2',['harga'=>$harga,'jenis'=>$jenis,'kecamatan'=>$kecamatan,'kota'=>$kota,'tahun'=>$tahun]);

        }elseif(!empty($nama) && $id_jenis > 0 && $id_kota == 0 && $id_kecamatan == null && $urut == null){
            
        $harga = \DB::table('tbl_harga')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_harga.id_kecamatan')
            ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
            ->join('tbl_barang', 'tbl_harga.kode_barang', '=', 'tbl_barang.kode_jenis')
            ->join('bahan_j', 'bahan_j.id_jenis', '=', 'tbl_barang.id_jenis')
            ->select('tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
            ->groupBy('tbl_harga.last_update','tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
            ->Where('tbl_barang.id_jenis', '=', $id_jenis)
            ->Where('tbl_barang.nama_barang', 'like', '%'.$request->nama.'%')
            ->orderBy('tbl_harga.last_update','DESC')
            ->paginate(10);

        $jenis = \DB::table('bahan_j')
            ->select('*')
            ->get();

        $kecamatan = \DB::table('tbl_kecamatan')
            ->join('kota','kota.id_kota','=', 'kota.id_kota')
            ->select('tbl_kecamatan.nama_kecamatan as kecamatan','kota.kota as kota'
                    ,'tbl_kecamatan.id_kecamatan as id_kecamatan')
            ->get();

        $kota = \DB::table('kota')
            ->select('*')
            ->get();

        $tahun = $request->tahun;

        return view('master.layanan.hbb.all2',['harga'=>$harga,'jenis'=>$jenis,'kecamatan'=>$kecamatan,'kota'=>$kota,'tahun'=>$tahun]);
                
        }elseif($nama == null && $id_jenis == 0 && $id_kota > 0 && $id_kecamatan == 0 && $urut == null){
            
        $harga = \DB::table('tbl_harga')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_harga.id_kecamatan')
            ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
            ->join('tbl_barang', 'tbl_harga.kode_barang', '=', 'tbl_barang.kode_jenis')
            ->join('bahan_j', 'bahan_j.id_jenis', '=', 'tbl_barang.id_jenis')
            ->select('tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
            ->groupBy('tbl_harga.last_update','tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
            ->Where('kota.id_kota', '=', $id_kota)
            ->orderBy('tbl_harga.last_update','DESC')
            ->paginate(10);

        $jenis = \DB::table('bahan_j')
            ->select('*')
            ->get();

        $kecamatan = \DB::table('tbl_kecamatan')
            ->join('kota','kota.id_kota','=', 'kota.id_kota')
            ->select('tbl_kecamatan.nama_kecamatan as kecamatan','kota.kota as kota'
                    ,'tbl_kecamatan.id_kecamatan as id_kecamatan')
            ->get();

        $kota = \DB::table('kota')
            ->select('*')
            ->get();

        $tahun = $request->tahun;

        return view('master.layanan.hbb.all2',['harga'=>$harga,'jenis'=>$jenis,'kecamatan'=>$kecamatan,'kota'=>$kota,'tahun'=>$tahun]);
                
        }elseif($nama == null && $id_jenis == 0 && $id_kota > 0 && $id_kecamatan > 0 && $urut == null){
            

        $harga = \DB::table('tbl_harga')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_harga.id_kecamatan')
            ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
            ->join('tbl_barang', 'tbl_harga.kode_barang', '=', 'tbl_barang.kode_jenis')
            ->join('bahan_j', 'bahan_j.id_jenis', '=', 'tbl_barang.id_jenis')
            ->select('tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
            ->Where('tbl_kecamatan.id_kecamatan', '=', $id_kecamatan)
            ->groupBy('tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
            ->paginate(10);

        $jenis = \DB::table('bahan_j')
            ->select('*')
            ->get();

        $kecamatan = \DB::table('tbl_kecamatan')
            ->join('kota','kota.id_kota','=', 'kota.id_kota')
            ->select('tbl_kecamatan.nama_kecamatan as kecamatan','kota.kota as kota'
                    ,'tbl_kecamatan.id_kecamatan as id_kecamatan')
            ->get();

        $kota = \DB::table('kota')
            ->select('*')
            ->get();

        $tahun = $request->tahun;

        return view('master.layanan.hbb.all2',['harga'=>$harga,'jenis'=>$jenis,'kecamatan'=>$kecamatan,'kota'=>$kota,'tahun'=>$tahun]);
                
        }elseif($nama == null && $id_jenis == 0 && $id_kota == 0 && $id_kecamatan == null && !empty($urut)){
            

            $harga = \DB::table('tbl_harga')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_harga.id_kecamatan')
            ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
            ->join('tbl_barang', 'tbl_harga.kode_barang', '=', 'tbl_barang.kode_jenis')
            ->join('bahan_j', 'bahan_j.id_jenis', '=', 'tbl_barang.id_jenis')
            ->select('tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
            ->groupBy('tbl_harga.last_update','tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
            ->orderBy('tbl_harga.last_update','DESC')
            ->paginate(10);

        $jenis = \DB::table('bahan_j')
            ->select('*')
            ->get();

        $kecamatan = \DB::table('tbl_kecamatan')
            ->join('kota','kota.id_kota','=', 'kota.id_kota')
            ->select('tbl_kecamatan.nama_kecamatan as kecamatan','kota.kota as kota'
                    ,'tbl_kecamatan.id_kecamatan as id_kecamatan')
            ->get();

        $kota = \DB::table('kota')
            ->select('*')
            ->get();

        $tahun = $request->tahun;

        return view('master.layanan.hbb.all2',['harga'=>$harga,'jenis'=>$jenis,'kecamatan'=>$kecamatan,'kota'=>$kota,'tahun'=>$tahun]);
                
        }elseif(!empty($nama) && $id_jenis == 0 && $id_kota == 0 && $id_kecamatan == null && !empty($urut)){
            

        $harga = \DB::table('tbl_harga')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_harga.id_kecamatan')
            ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
            ->join('tbl_barang', 'tbl_harga.kode_barang', '=', 'tbl_barang.kode_jenis')
            ->join('bahan_j', 'bahan_j.id_jenis', '=', 'tbl_barang.id_jenis')
            ->select('tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
            ->groupBy('tbl_harga.last_update','tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
            ->Where('tbl_barang.nama_barang', 'like', '%'.$request->nama.'%')
            ->orderBy('tbl_harga.last_update','DESC')
            ->paginate(10);

        $jenis = \DB::table('bahan_j')
            ->select('*')
            ->get();

        $kecamatan = \DB::table('tbl_kecamatan')
            ->join('kota','kota.id_kota','=', 'kota.id_kota')
            ->select('tbl_kecamatan.nama_kecamatan as kecamatan','kota.kota as kota'
                    ,'tbl_kecamatan.id_kecamatan as id_kecamatan')
            ->get();

        $kota = \DB::table('kota')
            ->select('*')
            ->get();

        $tahun = $request->tahun;

        return view('master.layanan.hbb.all2',['harga'=>$harga,'jenis'=>$jenis,'kecamatan'=>$kecamatan,'kota'=>$kota,'tahun'=>$tahun]);
                
        }elseif($nama == null && $id_jenis > 0 && $id_kota == 0 && $id_kecamatan == null && !empty($urut)){
            

        $harga = \DB::table('tbl_harga')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_harga.id_kecamatan')
            ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
            ->join('tbl_barang', 'tbl_harga.kode_barang', '=', 'tbl_barang.kode_jenis')
            ->join('bahan_j', 'bahan_j.id_jenis', '=', 'tbl_barang.id_jenis')
            ->select('tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
            ->groupBy('tbl_harga.last_update','tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
            ->Where('tbl_barang.nama_barang', 'like', '%'.$request->nama.'%')
            ->orderBy('tbl_harga.last_update','DESC')
            ->paginate(10);

        $jenis = \DB::table('bahan_j')
            ->select('*')
            ->get();

        $kecamatan = \DB::table('tbl_kecamatan')
            ->join('kota','kota.id_kota','=', 'kota.id_kota')
            ->select('tbl_kecamatan.nama_kecamatan as kecamatan','kota.kota as kota'
                    ,'tbl_kecamatan.id_kecamatan as id_kecamatan')
            ->get();

        $kota = \DB::table('kota')
            ->select('*')
            ->get();

        $tahun = $request->tahun;

        return view('master.layanan.hbb.all2',['harga'=>$harga,'jenis'=>$jenis,'kecamatan'=>$kecamatan,'kota'=>$kota,'tahun'=>$tahun]);
                
        }elseif($nama == null && $id_jenis == 0 && $id_kota > 0 && $id_kecamatan == null && !empty($urut)){
            

        $harga = \DB::table('tbl_harga')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_harga.id_kecamatan')
            ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
            ->join('tbl_barang', 'tbl_harga.kode_barang', '=', 'tbl_barang.kode_jenis')
            ->join('bahan_j', 'bahan_j.id_jenis', '=', 'tbl_barang.id_jenis')
            ->select('tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
            ->groupBy('tbl_harga.last_update','tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
            ->Where('tbl_barang.nama_barang', 'like', '%'.$request->nama.'%')
            ->orderBy('tbl_harga.last_update','DESC')
            ->paginate(10);

        $jenis = \DB::table('bahan_j')
            ->select('*')
            ->get();

        $kecamatan = \DB::table('tbl_kecamatan')
            ->join('kota','kota.id_kota','=', 'kota.id_kota')
            ->select('tbl_kecamatan.nama_kecamatan as kecamatan','kota.kota as kota'
                    ,'tbl_kecamatan.id_kecamatan as id_kecamatan')
            ->get();

        $kota = \DB::table('kota')
            ->select('*')
            ->get();

        $tahun = $request->tahun;

        return view('master.layanan.hbb.all2',['harga'=>$harga,'jenis'=>$jenis,'kecamatan'=>$kecamatan,'kota'=>$kota,'tahun'=>$tahun]);
                
        }elseif($nama == null && $id_jenis == 0 && $id_kota > 0 && $id_kecamatan > 0 && !empty($urut)){
            

        $harga = \DB::table('tbl_harga')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_harga.id_kecamatan')
            ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
            ->join('tbl_barang', 'tbl_harga.kode_barang', '=', 'tbl_barang.kode_jenis')
            ->join('bahan_j', 'bahan_j.id_jenis', '=', 'tbl_barang.id_jenis')
            ->select('tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
            ->groupBy('tbl_harga.last_update','tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
            ->Where('tbl_barang.nama_barang', 'like', '%'.$request->nama.'%')
            ->orderBy('tbl_harga.last_update','DESC')
            ->paginate(10);

        $jenis = \DB::table('bahan_j')
            ->select('*')
            ->get();

        $kecamatan = \DB::table('tbl_kecamatan')
            ->join('kota','kota.id_kota','=', 'kota.id_kota')
            ->select('tbl_kecamatan.nama_kecamatan as kecamatan','kota.kota as kota'
                    ,'tbl_kecamatan.id_kecamatan as id_kecamatan')
            ->get();

        $kota = \DB::table('kota')
            ->select('*')
            ->get();

        $tahun = $request->tahun;

        return view('master.layanan.hbb.all2',['harga'=>$harga,'jenis'=>$jenis,'kecamatan'=>$kecamatan,'kota'=>$kota,'tahun'=>$tahun]);
                
        }elseif(!empty($nama) && $id_jenis == 0 && $id_kota > 0 && $id_kecamatan == 0 && $urut == null){

        $harga = \DB::table('tbl_harga')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_harga.id_kecamatan')
            ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
            ->join('tbl_barang', 'tbl_harga.kode_barang', '=', 'tbl_barang.kode_jenis')
            ->join('bahan_j', 'bahan_j.id_jenis', '=', 'tbl_barang.id_jenis')
            ->select('tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
            ->groupBy('tbl_harga.last_update','tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
            ->Where('tbl_barang.nama_barang', 'like', '%'.$request->nama.'%')
            ->Where('kota.id_kota', '=', $id_kota)
            ->orderBy('tbl_harga.last_update','DESC')
            ->paginate(10);

        $jenis = \DB::table('bahan_j')
            ->select('*')
            ->get();

        $kecamatan = \DB::table('tbl_kecamatan')
            ->join('kota','kota.id_kota','=', 'kota.id_kota')
            ->select('tbl_kecamatan.nama_kecamatan as kecamatan','kota.kota as kota'
                    ,'tbl_kecamatan.id_kecamatan as id_kecamatan')
            ->get();

        $kota = \DB::table('kota')
            ->select('*')
            ->get();

        $tahun = $request->tahun;

        return view('master.layanan.hbb.all2',['harga'=>$harga,'jenis'=>$jenis,'kecamatan'=>$kecamatan,'kota'=>$kota,'tahun'=>$tahun]);
                
        }elseif(!empty($nama) && $id_jenis == 0 && $id_kota > 0 && $id_kecamatan > 0 && $urut == null){
            

        $harga = \DB::table('tbl_harga')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_harga.id_kecamatan')
            ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
            ->join('tbl_barang', 'tbl_harga.kode_barang', '=', 'tbl_barang.kode_jenis')
            ->join('bahan_j', 'bahan_j.id_jenis', '=', 'tbl_barang.id_jenis')
            ->select('tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
            ->groupBy('tbl_harga.last_update','tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
            ->Where('tbl_barang.nama_barang', 'like', '%'.$request->nama.'%')
            ->Where('kota.id_kota', '=', $id_kota)
            ->Where('tbl_harga.id_kecamatan', '=', $id_kecamatan)
            ->orderBy('tbl_harga.last_update','DESC')
            ->paginate(10);

        $jenis = \DB::table('bahan_j')
            ->select('*')
            ->get();

        $kecamatan = \DB::table('tbl_kecamatan')
            ->join('kota','kota.id_kota','=', 'kota.id_kota')
            ->select('tbl_kecamatan.nama_kecamatan as kecamatan','kota.kota as kota'
                    ,'tbl_kecamatan.id_kecamatan as id_kecamatan')
            ->get();

        $kota = \DB::table('kota')
            ->select('*')
            ->get();

        $tahun = $request->tahun;

        return view('master.layanan.hbb.all2',['harga'=>$harga,'jenis'=>$jenis,'kecamatan'=>$kecamatan,'kota'=>$kota,'tahun'=>$tahun]);
                
    }elseif($nama == null && $id_jenis > 0 && $id_kota > 0 && $id_kecamatan > 0 && $urut == null){
            

        $harga = \DB::table('tbl_harga')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_harga.id_kecamatan')
            ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
            ->join('tbl_barang', 'tbl_harga.kode_barang', '=', 'tbl_barang.kode_jenis')
            ->join('bahan_j', 'bahan_j.id_jenis', '=', 'tbl_barang.id_jenis')
            ->select('tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
            ->groupBy('tbl_harga.last_update','tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
            ->Where('tbl_barang.id_jenis', '=', $id_jenis)
            ->Where('kota.id_kota', '=', $id_kota)
            ->Where('tbl_harga.id_kecamatan', '=', $id_kecamatan)
            ->orderBy('tbl_harga.last_update','DESC')
            ->paginate(10);

        $jenis = \DB::table('bahan_j')
            ->select('*')
            ->get();

        $kecamatan = \DB::table('tbl_kecamatan')
            ->join('kota','kota.id_kota','=', 'kota.id_kota')
            ->select('tbl_kecamatan.nama_kecamatan as kecamatan','kota.kota as kota'
                    ,'tbl_kecamatan.id_kecamatan as id_kecamatan')
            ->get();

        $kota = \DB::table('kota')
            ->select('*')
            ->get();

        $tahun = $request->tahun;

        return view('master.layanan.hbb.all2',['harga'=>$harga,'jenis'=>$jenis,'kecamatan'=>$kecamatan,'kota'=>$kota,'tahun'=>$tahun]);
                
    }elseif($nama == null && $id_jenis > 0 && $id_kota > 0 && $id_kecamatan == 0 && $urut == null){
            

        $harga = \DB::table('tbl_harga')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_harga.id_kecamatan')
            ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
            ->join('tbl_barang', 'tbl_harga.kode_barang', '=', 'tbl_barang.kode_jenis')
            ->join('bahan_j', 'bahan_j.id_jenis', '=', 'tbl_barang.id_jenis')
            ->select('tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
            ->groupBy('tbl_harga.last_update','tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
            ->Where('tbl_barang.id_jenis', '=', $id_jenis)
            ->Where('kota.id_kota', '=', $id_kota)
            ->orderBy('tbl_harga.last_update','DESC')
            ->paginate(10);

        $jenis = \DB::table('bahan_j')
            ->select('*')
            ->get();

        $kecamatan = \DB::table('tbl_kecamatan')
            ->join('kota','kota.id_kota','=', 'kota.id_kota')
            ->select('tbl_kecamatan.nama_kecamatan as kecamatan','kota.kota as kota'
                    ,'tbl_kecamatan.id_kecamatan as id_kecamatan')
            ->get();

        $kota = \DB::table('kota')
            ->select('*')
            ->get();

        $tahun = $request->tahun;

        return view('master.layanan.hbb.all2',['harga'=>$harga,'jenis'=>$jenis,'kecamatan'=>$kecamatan,'kota'=>$kota,'tahun'=>$tahun]);
                
        }else{
            
        $harga = \DB::table('tbl_harga')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_harga.id_kecamatan')
            ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
            ->join('tbl_barang', 'tbl_harga.kode_barang', '=', 'tbl_barang.kode_jenis')
            ->join('bahan_j', 'bahan_j.id_jenis', '=', 'tbl_barang.id_jenis')
            ->select('tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
            ->groupBy('tbl_harga.last_update','tbl_harga.kode_barang','tbl_barang.nama_barang','kota.kota','tbl_kecamatan.nama_kecamatan')
            ->orderBy('tbl_harga.last_update','DESC')
            ->paginate(10);

        $jenis = \DB::table('bahan_j')
            ->select('*')
            ->get();

        $kecamatan = \DB::table('tbl_kecamatan')
            ->join('kota','kota.id_kota','=', 'kota.id_kota')
            ->select('tbl_kecamatan.nama_kecamatan as kecamatan','kota.kota as kota'
                    ,'tbl_kecamatan.id_kecamatan as id_kecamatan')
            ->get();

        $kota = \DB::table('kota')
            ->select('*')
            ->get();
        $tahun = $request->tahun;

        return view('master.layanan.hbb.all',['harga'=>$harga,'jenis'=>$jenis,'kecamatan'=>$kecamatan,'kota'=>$kota,'tahun'=>$tahun]);

        }
    }

}

}
