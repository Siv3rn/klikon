<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Models\konsultasi;
use App\Models\tbl_kat_konsul;

class npsmpublikController extends Controller
{
    public function all()
    {
        $konsul = \DB::table('tbl_konsultasi')
            ->select('*')
            ->where('tgl_jawaban', '<>', '')
            ->orWhereNotNull('tgl_jawaban')
            ->orderBy('tgl_jawaban','DESC')
            ->limit(5)
            ->get();

        $video = \DB::table('tbl_video')
            ->select('*')
            ->where('status_video', '=', 'aktif')
            ->get();

        $poster = \DB::table('tbl_poster')
            ->select('*')
            ->where('status_poster', '=', 'tampil')
            ->get();

        $tbl_nspm_kriteria = \DB::table('tbl_nspm_kriteria')
            ->select('*')
            ->orderBy('id_nspm_kriteria','ASC')
            ->get();

		return view('master.layanan.nspm.all',['konsul'=>$konsul,'video'=>$video,
        'poster'=>$poster,'tbl_nspm_kriteria'=>$tbl_nspm_kriteria]);
    } 

    public function detail(Request $request, $id)
    {
            $detail_nspm = \DB::table('tbl_data_nspm')
            ->join('tbl_nspm_kriteria', 'tbl_data_nspm.id_nspm_kriteria', '=', 'tbl_nspm_kriteria.id_nspm_kriteria')
            ->join('tbl_nspm_kriteria1', 'tbl_data_nspm.id_nspm_kriteria1', '=', 'tbl_nspm_kriteria1.id_nspm_kriteria1')
            ->join('tbl_nspm_kategori', 'tbl_data_nspm.id_nspm_kategori', '=', 'tbl_nspm_kategori.id_nspm_kategori')
            ->select(
                'tbl_data_nspm.id_data_nspm as id',
				'tbl_data_nspm.judul_data_nspm as judul',
				'tbl_data_nspm.uraian_data_nspm as uraian',
				'tbl_data_nspm.no_seri_nspm as no_seri',
				'tbl_data_nspm.file_data_nspm as file',
				'tbl_data_nspm.tahun_data_nspm as tahun',
				'tbl_data_nspm.hit as hit',
				'tbl_nspm_kriteria.id_nspm_kriteria as kriteria',
				'tbl_nspm_kriteria1.nama_nspm_kriteria1 as nama_kriteria1',
				'tbl_nspm_kategori.nama_nspm_kategori as kategori'
             )
            ->where('tbl_data_nspm.id_nspm_kriteria','=', $id)
            ->orderBy('tbl_data_nspm.hit', 'DESC')
            ->get();

            $konsul = \DB::table('tbl_konsultasi')
            ->select('*')
            ->where('tgl_jawaban', '<>', '')
            ->orWhereNotNull('tgl_jawaban')
            ->orderBy('tgl_jawaban','DESC')
            ->limit(5)
            ->get();

        $video = \DB::table('tbl_video')
            ->select('*')
            ->where('status_video', '=', 'aktif')
            ->get();

        $poster = \DB::table('tbl_poster')
            ->select('*')
            ->where('status_poster', '=', 'tampil')
            ->get();

        $tbl_nspm_kriteria = \DB::table('tbl_nspm_kriteria')
            ->select('*')
            ->orderBy('id_nspm_kriteria','ASC')
            ->get();

        $kriteria = \DB::table('tbl_nspm_kriteria1')
            ->select('*')
            ->where('id_nspm_kriteria','=',$id)
            ->get();

		return view('master.layanan.nspm.detail',['konsul'=>$konsul,'video'=>$video,
        'poster'=>$poster,'tbl_nspm_kriteria'=>$tbl_nspm_kriteria,'kriteria'=>$kriteria,
        'detail_nspm'=>$detail_nspm]);
    } 

    public function kriteria(Request $request, $id)
    {
            $detail_nspm = \DB::table('tbl_data_nspm')
            ->join('tbl_nspm_kriteria', 'tbl_data_nspm.id_nspm_kriteria', '=', 'tbl_nspm_kriteria.id_nspm_kriteria')
            ->join('tbl_nspm_kriteria1', 'tbl_data_nspm.id_nspm_kriteria1', '=', 'tbl_nspm_kriteria1.id_nspm_kriteria1')
            ->join('tbl_nspm_kategori', 'tbl_data_nspm.id_nspm_kategori', '=', 'tbl_nspm_kategori.id_nspm_kategori')
            ->select(
                'tbl_data_nspm.id_data_nspm as id',
				'tbl_data_nspm.judul_data_nspm as judul',
				'tbl_data_nspm.uraian_data_nspm as uraian',
				'tbl_data_nspm.no_seri_nspm as no_seri',
				'tbl_data_nspm.file_data_nspm as file',
				'tbl_data_nspm.tahun_data_nspm as tahun',
				'tbl_data_nspm.hit as hit',
				'tbl_nspm_kriteria.id_nspm_kriteria as kriteria',
				'tbl_nspm_kriteria1.nama_nspm_kriteria1 as nama_kriteria1',
				'tbl_nspm_kategori.nama_nspm_kategori as kategori'
             )
            ->where('tbl_data_nspm.id_nspm_kriteria1','=', $id)
            ->orderBy('tbl_data_nspm.hit', 'DESC')
            ->get();

            $konsul = \DB::table('tbl_konsultasi')
            ->select('*')
            ->where('tgl_jawaban', '<>', '')
            ->orWhereNotNull('tgl_jawaban')
            ->orderBy('tgl_jawaban','DESC')
            ->limit(5)
            ->get();

        $video = \DB::table('tbl_video')
            ->select('*')
            ->where('status_video', '=', 'aktif')
            ->get();

        $poster = \DB::table('tbl_poster')
            ->select('*')
            ->where('status_poster', '=', 'tampil')
            ->get();

        $tbl_nspm_kriteria = \DB::table('tbl_nspm_kriteria')
            ->select('*')
            ->orderBy('id_nspm_kriteria','ASC')
            ->get();

		return view('master.layanan.nspm.kriteria',['konsul'=>$konsul,'video'=>$video,
        'poster'=>$poster,'tbl_nspm_kriteria'=>$tbl_nspm_kriteria,'detail_nspm'=>$detail_nspm]);
    } 

}
