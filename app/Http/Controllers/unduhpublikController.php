<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class unduhpublikController extends Controller
{
    public function menu()
    {
        $konsul = \DB::table('tbl_konsultasi')
            ->select('*')
            ->where('tgl_jawaban', '<>', '')
            ->orWhereNotNull('tgl_jawaban')
            ->orderBy('tgl_jawaban','DESC')
            ->limit(5)
            ->get();

        $video = \DB::table('tbl_video')
            ->select('*')
            ->where('status_video', '=', 'aktif')
            ->get();

        $poster = \DB::table('tbl_poster')
            ->select('*')
            ->where('status_poster', '=', 'tampil')
            ->get();

        $tbl_nspm_kriteria = \DB::table('tbl_nspm_kriteria')
            ->select('*')
            ->orderBy('id_nspm_kriteria','ASC')
            ->get();

		return view('master.unduh.menu',['konsul'=>$konsul,'video'=>$video,
        'poster'=>$poster,'tbl_nspm_kriteria'=>$tbl_nspm_kriteria]);
    }

    public function poster()
    {
        $konsul = \DB::table('tbl_konsultasi')
            ->select('*')
            ->where('tgl_jawaban', '<>', '')
            ->orWhereNotNull('tgl_jawaban')
            ->orderBy('tgl_jawaban','DESC')
            ->limit(5)
            ->get();

        $video = \DB::table('tbl_video')
            ->select('*')
            ->where('status_video', '=', 'aktif')
            ->get();

        $poster = \DB::table('tbl_poster')
            ->select('*')
            ->where('status_poster', '=', 'tampil')
            ->paginate(8);

        $tbl_nspm_kriteria = \DB::table('tbl_nspm_kriteria')
            ->select('*')
            ->orderBy('id_nspm_kriteria','ASC')
            ->get();

		return view('master.unduh.poster.all',['konsul'=>$konsul,'video'=>$video,
        'poster'=>$poster,'tbl_nspm_kriteria'=>$tbl_nspm_kriteria]);
    }

    public function buku()
    {
        $buku = \DB::table('buku')
        ->select('*')
        ->orderBy('id','DESC')
        ->get();

        $konsul = \DB::table('tbl_konsultasi')
            ->select('*')
            ->where('tgl_jawaban', '<>', '')
            ->orWhereNotNull('tgl_jawaban')
            ->orderBy('tgl_jawaban','DESC')
            ->limit(5)
            ->get();

        $video = \DB::table('tbl_video')
            ->select('*')
            ->where('status_video', '=', 'aktif')
            ->get();

        $poster = \DB::table('tbl_poster')
            ->select('*')
            ->where('status_poster', '=', 'tampil')
            ->get();

        $tbl_nspm_kriteria = \DB::table('tbl_nspm_kriteria')
            ->select('*')
            ->orderBy('id_nspm_kriteria','ASC')
            ->get();

		return view('master.unduh.buku.all',['buku'=>$buku,'konsul'=>$konsul,'video'=>$video,
        'poster'=>$poster,'tbl_nspm_kriteria'=>$tbl_nspm_kriteria]);
    }

    public function peraturan()
    {
        $konsul = \DB::table('tbl_konsultasi')
            ->select('*')
            ->where('tgl_jawaban', '<>', '')
            ->orWhereNotNull('tgl_jawaban')
            ->orderBy('tgl_jawaban','DESC')
            ->limit(5)
            ->get();

        $video = \DB::table('tbl_video')
            ->select('*')
            ->where('status_video', '=', 'aktif')
            ->get();

        $poster = \DB::table('tbl_poster')
            ->select('*')
            ->where('status_poster', '=', 'tampil')
            ->get();

        $tbl_nspm_kriteria = \DB::table('tbl_nspm_kriteria')
            ->select('*')
            ->orderBy('id_nspm_kriteria','ASC')
            ->get();
           
        $peraturan = \DB::table('tbl_peraturan')
            ->select('*')
            ->where('kriteria_peraturan','=','Peraturan')
            ->orderBy('id_peraturan','DESC')
            ->get();

		return view('master.unduh.peraturan.all',['peraturan'=>$peraturan,'konsul'=>$konsul,'video'=>$video,
        'poster'=>$poster,'tbl_nspm_kriteria'=>$tbl_nspm_kriteria]);
    }

    public function sk()
    {
        $sk = \DB::table('tbl_sk')
            ->select('*')
            ->orderBy('id_sk','DESC')
            ->get();

        $konsul = \DB::table('tbl_konsultasi')
            ->select('*')
            ->where('tgl_jawaban', '<>', '')
            ->orWhereNotNull('tgl_jawaban')
            ->orderBy('tgl_jawaban','DESC')
            ->limit(5)
            ->get();

        $video = \DB::table('tbl_video')
            ->select('*')
            ->where('status_video', '=', 'aktif')
            ->get();

        $poster = \DB::table('tbl_poster')
            ->select('*')
            ->where('status_poster', '=', 'tampil')
            ->get();

        $tbl_nspm_kriteria = \DB::table('tbl_nspm_kriteria')
            ->select('*')
            ->orderBy('id_nspm_kriteria','ASC')
            ->get();

		return view('master.unduh.sk.all',['sk'=>$sk,'konsul'=>$konsul,'video'=>$video,
        'poster'=>$poster,'tbl_nspm_kriteria'=>$tbl_nspm_kriteria]);
    }

    public function spesifikasi()
    {
        $konsul = \DB::table('tbl_konsultasi')
            ->select('*')
            ->where('tgl_jawaban', '<>', '')
            ->orWhereNotNull('tgl_jawaban')
            ->orderBy('tgl_jawaban','DESC')
            ->limit(5)
            ->get();

        $video = \DB::table('tbl_video')
            ->select('*')
            ->where('status_video', '=', 'aktif')
            ->get();

        $poster = \DB::table('tbl_poster')
            ->select('*')
            ->where('status_poster', '=', 'tampil')
            ->get();

        $tbl_nspm_kriteria = \DB::table('tbl_nspm_kriteria')
            ->select('*')
            ->orderBy('id_nspm_kriteria','ASC')
            ->get();
        
        $peraturan = \DB::table('tbl_peraturan')
            ->select('*')
            ->where('kriteria_peraturan','=','Spesifikasi Teknis')
            ->orderBy('id_peraturan','DESC')
            ->get();

		return view('master.unduh.spektek.all',['peraturan'=>$peraturan,'konsul'=>$konsul,'video'=>$video,
        'poster'=>$poster,'tbl_nspm_kriteria'=>$tbl_nspm_kriteria]);
    }

    public function covid()
    {
        $spektek = \DB::table('tbl_sk')
            ->select('*')
            ->orderBy('id_sk','DESC')
            ->get();

        $konsul = \DB::table('tbl_konsultasi')
            ->select('*')
            ->where('tgl_jawaban', '<>', '')
            ->orWhereNotNull('tgl_jawaban')
            ->orderBy('tgl_jawaban','DESC')
            ->limit(5)
            ->get();

        $video = \DB::table('tbl_video')
            ->select('*')
            ->where('status_video', '=', 'aktif')
            ->get();

        $poster = \DB::table('tbl_poster')
            ->select('*')
            ->where('status_poster', '=', 'tampil')
            ->get();

        $tbl_nspm_kriteria = \DB::table('tbl_nspm_kriteria')
            ->select('*')
            ->orderBy('id_nspm_kriteria','ASC')
            ->get();
        
        $covid = \DB::table('tbl_peraturan')
            ->select('*')
            ->where('kriteria_peraturan','=','Covid')
            ->orderBy('id_peraturan','DESC')
            ->get();

		return view('master.unduh.covid.all',['covid'=>$covid,'konsul'=>$konsul,'video'=>$video,
        'poster'=>$poster,'tbl_nspm_kriteria'=>$tbl_nspm_kriteria]);
    }

}
