<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Models\artikel;

class artikelController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
		$artikel = \DB::table('artikel')
            ->select('*')
            ->orderBy('tanggal','DESC')
            ->get();
			
		return view('master.informasi.artikel.index',['artikel'=>$artikel]);
    }    

    public function add()
    {
        return view('master.informasi.artikel.add');
    }

    public function edit(Request $request, $id)
    {
		$artikel = \DB::table('artikel')
            ->select('*')
            ->where('id_artikel','=', $id)
            ->get();
			
		return view('master.informasi.artikel.edit',['artikel'=>$artikel]);
    }    

    public function destroy($id)
    {
        $link= \DB::table('artikel')->where('id_artikel', $id)->delete();
    return redirect()->back()->withSuccess('Deleted Success.');
    }

    //Save berita
    public function create(Request $request)
    {         
       $validator = Validator::make($request->all(), [
           'file' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
       ]);

       if ($validator->fails()) {
           return redirect()->back()->withErrors(['Error', 'Upload gagal Karena file yang diupload bukan jpg,png,jpeg,gif,svg atau images ukuran melebihi 2048']);  
       }

        $file = $request->file('file');
        
        $filenameWithExt = $request->file('file')->getClientOriginalName();
        $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        $extension = $request->file('file')->getClientOriginalExtension();
        $filenameSimpan = $filename.'_'.time().'.'.$extension;
        $path = $request->file('file')->storeAs('/public/images/artikel', $filenameSimpan);

        $ip=\request()->ip();
           date_default_timezone_set("Asia/Jakarta");
        $artikel = new \App\Models\artikel();
        $artikel->judul = $request->judul;         
        $artikel->tanggal = date('Y-m-d H:i:s');
        $artikel->ringkasan = $request->ringkasan;
        $artikel->pengarang = $request->pengarang;
        $artikel->kategori = $request->kategori;
        $artikel->petugas = $request->petugas;
        $artikel->isi = $request->isi;
        $artikel->ip = $ip;
        $artikel->file_image = $filenameSimpan;
        $artikel->dibaca = 0;
        $artikel->save();

        
        return redirect('/informasi/artikel')->with('Artikel Berhasil Di Buat','Success');
    }

    public function update(Request $request, $id_artikel)
    {
		$ip=\request()->ip();
            date_default_timezone_set("Asia/Jakarta");
		$validator = Validator::make($request->all(), [
            'judul' => 'required',
            'isi' => 'required',
            'file' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
        ]);

        if(empty($request->file('file'))){

            \DB::table('artikel')->where('id_artikel',$id_artikel)->update([
                'judul' => $request->judul,
                'isi' => $request->isi,
                'ringkasan' => $request->ringkasan,
                'pengarang' => $request->pengarang,
                'kategori' => $request->kategori,
                'petugas' => $request->petugas,
                'tanggal' => date('Y-m-d H:i:s'),
                'ip'    => $ip
                ]);
               
                return redirect('/informasi/artikel')->withSuccess('Artikel Berhasil Di Update','Success');

        }else{

            if ($validator->fails()) {
                return redirect()->back()->withErrors(['Error', 'Upload gagal Karena file yang diupload bukan jpg,png,jpeg,gif,svg atau images ukuran melebihi 2048']);  
            }

            $file = $request->file('file');
		
            $filenameWithExt = $request->file('file')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('file')->getClientOriginalExtension();
            $filenameSimpan = $filename.'_'.time().'.'.$extension;
            $path = $request->file('file')->storeAs('/public/images/artikel', $filenameSimpan);

            \DB::table('artikel')->where('id_artikel',$id_artikel)->update([
                'judul' => $request->judul,
                'isi' => $request->isi,
                'ringkasan' => $request->ringkasan,
                'pengarang' => $request->pengarang,
                'kategori' => $request->kategori,
                'petugas' => $request->petugas,
                'tanggal' => date('Y-m-d H:i:s'),
                'ip'    => $ip,
                'file_image' => $filenameSimpan
                ]);
               
                return redirect('/informasi/artikel')->withSuccess('success','Artikel Berhasil Di Update');

        }

	}

}
