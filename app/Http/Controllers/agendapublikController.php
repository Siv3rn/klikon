<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Models\tbl_agenda;

class agendapublikController extends Controller
{
    public function all()
    {
        $tbl_agenda = \DB::table('tbl_agenda')
        ->select('*')
        ->orderBy('id_agenda','DESC')
        ->get();

        $konsul = \DB::table('tbl_konsultasi')
            ->select('*')
            ->where('tgl_jawaban', '<>', '')
            ->orWhereNotNull('tgl_jawaban')
            ->orderBy('tgl_jawaban','DESC')
            ->limit(5)
            ->get();

        $video = \DB::table('tbl_video')
            ->select('*')
            ->where('status_video', '=', 'aktif')
            ->get();

		return view('master.informasi.agenda.all',['konsul'=>$konsul,'tbl_agenda'=>$tbl_agenda,'video'=>$video]);
    } 

    public function schedulle(Request $request)
    {
        $tahun = $request->tahun;
        if(!empty($tahun)){
        $tbl_agenda = \DB::table('tbl_agenda')
        ->select('*')
        ->orderBy(\DB::raw('DATE_FORMAT(tgl_agenda, "%m-%Y")'),'DESC')
        ->whereYear('tgl_agenda', $tahun)
        ->get();

        $konsul = \DB::table('tbl_konsultasi')
            ->select('*')
            ->where('tgl_jawaban', '<>', '')
            ->orWhereNotNull('tgl_jawaban')
            ->orderBy('tgl_jawaban','DESC')
            ->limit(5)
            ->get();

        $video = \DB::table('tbl_video')
            ->select('*')
            ->where('status_video', '=', 'aktif')
            ->get();

		return view('master.informasi.agenda.schedulle',['konsul'=>$konsul,'tbl_agenda'=>$tbl_agenda,'video'=>$video]);

        }else{
            $tbl_agenda = \DB::table('tbl_agenda')
        ->select('*')
        ->orderBy('tgl_agenda','DESC')
        ->get();

        $konsul = \DB::table('tbl_konsultasi')
            ->select('*')
            ->where('tgl_jawaban', '<>', '')
            ->orWhereNotNull('tgl_jawaban')
            ->orderBy('tgl_jawaban','DESC')
            ->limit(5)
            ->get();

        $video = \DB::table('tbl_video')
            ->select('*')
            ->where('status_video', '=', 'aktif')
            ->get();

		return view('master.informasi.agenda.schedulle',['konsul'=>$konsul,'tbl_agenda'=>$tbl_agenda,'video'=>$video]);
        }
    } 

    public function detail(Request $request, $id)
    {
            $tbl_agenda = \DB::table('tbl_agenda')
            ->select('*')
            ->where('id_agenda','=', $id)
            ->get();

            $id_kat_konsul = \DB::table('tbl_konsultasi')
            ->select('*')
            ->limit(5)
            ->orderBy('id_konsultasi','DESC')
            ->get();

            $video = \DB::table('tbl_video')
            ->select('*')
            ->where('status_video', '=', 'aktif')
            ->get();

            $poster = \DB::table('tbl_poster')
            ->select('*')
            ->where('status_poster', '=', 'tampil')
            ->get();
			
		return view('master.informasi.agenda.detail',['tbl_agenda'=>$tbl_agenda,
                    'id_kat_konsul'=>$id_kat_konsul,'video'=>$video,'poster'=>$poster]);
    }
}
