<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Models\konsultasi;
use App\Models\tbl_kat_konsul;

class konsultasiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
		    $tbl_konsultasi = \DB::table('tbl_konsultasi')
            ->select('*')
            ->orderBy('id_konsultasi','ASC')
            ->where('tgl_jawaban', '=', '')
            ->orWhereNull('tgl_jawaban')
            ->get();

            $tbl_count = \DB::table('tbl_konsultasi')
            ->select(\DB::raw('COUNT(id_konsultasi) as hit'))
            ->where('tgl_jawaban', '=', '')
            ->orWhereNull('tgl_jawaban')
            ->get();

            $tbl_konsultasi2 = \DB::table('tbl_konsultasi')
            ->join('tbl_kat_konsul', 'tbl_konsultasi.id_kat_konsul', '=', 'tbl_kat_konsul.id_kat_konsul')
            ->select('tbl_konsultasi.id_konsultasi','tbl_konsultasi.nama_konsultasi','tbl_konsultasi.judul_konsultasi',
                     'tbl_kat_konsul.nama_kat_konsul as kategori_konsultasi','tbl_konsultasi.hp_konsultasi','tbl_konsultasi.email_konsultasi',
                     'tbl_konsultasi.tgl_konsultasi','tbl_konsultasi.tgl_jawaban','tbl_konsultasi.pertanyaan_konsultasi',
                     'tbl_konsultasi.jawaban_konsultasi')
            ->where('tbl_konsultasi.tgl_jawaban', '<>', '')
            ->orWhereNotNull('tbl_konsultasi.tgl_jawaban')
            ->orderBy('tbl_konsultasi.id_konsultasi','DESC')
            ->get();

            $tbl_count2 = \DB::table('tbl_konsultasi')
            ->select(\DB::raw('COUNT(id_konsultasi) as hit'))
            ->where('tgl_jawaban', '<>', '')
            ->orWhereNotNull('tgl_jawaban')
            ->get();

            $tbl_kat_konsul = \DB::table('tbl_kat_konsul')
            ->select('*')
            ->get();
			
		return view('master.layanan.konsultasi.index',['tbl_konsultasi'=>$tbl_konsultasi,
                                'tbl_konsultasi2'=>$tbl_konsultasi2,'tbl_count'=>$tbl_count,
                                'tbl_count2'=>$tbl_count2,'tbl_kat_konsul'=>$tbl_kat_konsul]);
    }    

    public function add()
    {
        return view('master.layanan.konsultasi.add');
    }

    public function create(Request $request)
    {         

        $ip=\request()->ip();
        date_default_timezone_set("Asia/Jakarta");
        $jenis = new \App\Models\tbl_kat_konsul();
        $jenis->nama_kat_konsul = $request->nama_kat_konsul;  
        $jenis->save();

        
        return redirect('/layanan/konsultasi')->with('Konsultasi Berhasil Di Buat','Success');
    }

    public function edit(Request $request, $id)
    {
		    $tbl_konsultasi = \DB::table('tbl_konsultasi')
            ->select('*')
            ->where('id_konsultasi','=', $id)
            ->get();

            $id_kat_konsul = \DB::table('tbl_kat_konsul')
            ->select('*')
            ->get();
			
		return view('master.layanan.konsultasi.edit',['tbl_konsultasi'=>$tbl_konsultasi,'id_kat_konsul'=>$id_kat_konsul]);
    } 

    public function update(Request $request, $id)
   {
       $ip=\request()->ip();
           date_default_timezone_set("Asia/Jakarta");

           \DB::table('tbl_konsultasi')->where('id_konsultasi',$id)->update([
            'judul_konsultasi' => $request->judul_konsultasi,
            'pertanyaan_konsultasi' => $request->pertanyaan_konsultasi,
            'id_kat_konsul' => $request->id_kat_konsul,
            'jawaban_konsultasi' => $request->jawaban_konsultasi,
            'tgl_jawaban' => $request->tgl_jawaban
               ]);
              
               return redirect('/layanan/konsultasi')->withSuccess('success','Data Konsultasi Berhasil Di Update');

       }


       public function edit_kategori(Request $request, $id)
       {

            $id_kat_konsul = \DB::table('tbl_kat_konsul')
            ->select('*')
            ->where('id_kat_konsul','=', $id)
            ->get();
			
		return view('master.layanan.konsultasi.edit_kategori',['id_kat_konsul'=>$id_kat_konsul]);
    } 

       public function update_kategori(Request $request, $id)
   {
       $ip=\request()->ip();
           date_default_timezone_set("Asia/Jakarta");

           \DB::table('tbl_kat_konsul')->where('id_kat_konsul',$id)->update([
            'nama_kat_konsul' => $request->nama_kat_konsul
               ]);
              
               return redirect('/layanan/konsultasi')->withSuccess('success','Data Konsultasi Berhasil Di Update');

       }

   public function destroy($id)
   {
       $tbl_konsultasi= \DB::table('tbl_konsultasi')->where('id_konsultasi', $id)->delete();
   return redirect()->back()->withSuccess('Deleted Success.');
   }

   public function destroy_kategori($id)
   {
       $tbl_kat_konsul= \DB::table('tbl_kat_konsul')->where('id_kat_konsul', $id)->delete();
   return redirect()->back()->withSuccess('Deleted Success.');
   }

}
