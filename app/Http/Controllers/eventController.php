<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Models\tbl_event;
use App\Models\tbl_file_pendukung;

class eventController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
		$event = \DB::table('tbl_event')
            ->select('*')
            ->orderBy('id_event','DESC')
            ->get();
			
		return view('master.informasi.event.index',['event'=>$event]);
    }    

    public function create_event(Request $request)
    {         
        $event = new \App\Models\tbl_event();
        $event->judul_event = $request->judul_event;  
        $event->status_event = 'on';    
        $event->save();

        return redirect('/informasi/event')->with('Event Berhasil Di Buat','Success');
    }

    public function create(Request $request)
     {         
        $validator = Validator::make($request->all(), [
            'file' => 'required|max:5048',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors(['Error', 'Upload gagal Karena file yang diupload ukuran melebihi 5mb']);  
        }

         $file = $request->file('file');
         
         $filenameWithExt = $request->file('file')->getClientOriginalName();
         $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
         $extension = $request->file('file')->getClientOriginalExtension();
         $filenameSimpan = $filename.'_'.time().'.'.$extension;
         $path = $request->file('file')->storeAs('/public/images/event', $filenameSimpan);
 
         date_default_timezone_set("Asia/Jakarta");
         $event_file = new \App\Models\tbl_file_pendukung();
         $event_file->id_event = $request->id_event;   
         $event_file->judul_file = $request->judul_file;   
         $event_file->file = $filenameSimpan;
         $event_file->save();
 
         
         return redirect('/informasi/event')->with('Event Berhasil Di Buat','Success');
     }

    public function destroy($id)
    {
        
        $event1= \DB::table('tbl_file_pendukung')->where('id_event', $id)->delete();
        $event= \DB::table('tbl_event')->where('id_event', $id)->delete();
    return redirect()->back()->withSuccess('Deleted Success.');
    }

    public function update_event(Request $request, $id_event)
    {
            \DB::table('tbl_event')->where('id_event',$id_event)->update([
                'judul_event' => $request->judul_event,
                'status_event' => $request->status_event
                ]);
               
                return redirect('/informasi/event')->withSuccess('Agenda Berhasil Di Update','Success');

        }

        public function update(Request $request, $id_file)
    {
            date_default_timezone_set("Asia/Jakarta");
		$validator = Validator::make($request->all(), [
            'file' => 'required|max:2048',
        ]);

        if(empty($request->file('file'))){

            \DB::table('tbl_file_pendukung')->where('id_file',$id_file)->update([
                'judul_file' => $request->judul_file,
                'id_event' => $request->id_event
                ]);
               
                return redirect('/informasi/event')->withSuccess('File Pendukung Berhasil Di Update','Success');

        }else{

            if ($validator->fails()) {
                return redirect()->back()->withErrors(['Error', 'Upload gagal Karena file yang diupload bukan jpg,png,jpeg,gif,svg atau images ukuran melebihi 2048']);  
            }

            $file = $request->file('file');
		
            $filenameWithExt = $request->file('file')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('file')->getClientOriginalExtension();
            $filenameSimpan = $filename.'_'.time().'.'.$extension;
            $path = $request->file('file')->storeAs('/public/images/event', $filenameSimpan);

            \DB::table('tbl_file_pendukung')->where('id_file',$id_file)->update([
                'judul_file' => $request->judul_file,
                'id_event' => $request->id_event,
                'file' => $filenameSimpan
                ]);
               
                return redirect('/informasi/event')->withSuccess('success','File Event Berhasil Di Update');

        }

	}

}
