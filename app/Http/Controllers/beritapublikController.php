<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Models\berita;

class beritapublikController extends Controller
{
    public function all(Request $request)
    {
        
        $bulan = $request->bulan;
        $tahun = $request->tahun;
        if(!empty($bulan) && empty($tahun)){
            $news = \DB::table('berita')
        ->select('*')
        ->whereMonth('tanggal', $bulan)
        ->orderBy('tanggal','DESC')
        ->paginate(3);

        $konsul = \DB::table('berita')
            ->select('*')
            ->where('tanggal', '<>', '')
            ->orWhereNotNull('tanggal')
            ->orderBy('tanggal','DESC')
            ->limit(3)
            ->get();

        

		return view('master.informasi.berita.all',['konsul'=>$konsul,'news'=>$news]);
        
        }else if(empty($bulan) && !empty($tahun)){
        
        $news = \DB::table('berita')
        ->select('*')
        ->whereYear('tanggal', $tahun)
        ->orderBy('tanggal','DESC')
        ->paginate(3);

        $konsul = \DB::table('berita')
            ->select('*')
            ->where('tanggal', '<>', '')
            ->orWhereNotNull('tanggal')
            ->orderBy('tanggal','DESC')
            ->limit(3)
            ->get();
            
            $video = \DB::table('tbl_video')
            ->select('*')
            ->where('status_video', '=', 'aktif')
            ->get();

        return view('master.informasi.berita.all',['konsul'=>$konsul,'news'=>$news,'video'=>$video]);
        
        }else if(!empty($bulan) && !empty($tahun)){
            
            $news = \DB::table('berita')
            ->select('*')
            ->whereYear('tanggal', $tahun)
            ->whereMonth('tanggal', $bulan)
            ->orderBy('tanggal','DESC')
            ->paginate(3);
    
            $konsul = \DB::table('berita')
                ->select('*')
                ->where('tanggal', '<>', '')
                ->orWhereNotNull('tanggal')
                ->orderBy('tanggal','DESC')
                ->limit(3)
                ->get();
    
            $video = \DB::table('tbl_video')
                ->select('*')
                ->where('status_video', '=', 'aktif')
                ->get();
    
            return view('master.informasi.berita.all',['konsul'=>$konsul,'news'=>$news,'video'=>$video]);
                
        }else{
        $news = \DB::table('berita')
        ->select('*')
        ->where('tanggal', '<>', '')
        ->orWhereNotNull('tanggal')
        ->orderBy('tanggal','DESC')
        ->paginate(3);

        $konsul = \DB::table('berita')
            ->select('*')
            ->where('tanggal', '<>', '')
            ->orWhereNotNull('tanggal')
            ->orderBy('tanggal','DESC')
            ->limit(3)
            ->get();

            $video = \DB::table('tbl_video')
                ->select('*')
                ->where('status_video', '=', 'aktif')
                ->get();
    
            return view('master.informasi.berita.all',['konsul'=>$konsul,'news'=>$news,'video'=>$video]);
        }
        
    } 

    public function detail(Request $request, $id)
    {

        $news = \DB::table('berita')
        ->select('*')
        ->where('id_berita','=', $id)
        ->get();

        $konsul = \DB::table('berita')
            ->select('*')
            ->where('tanggal', '<>', '')
            ->orWhereNotNull('tanggal')
            ->orderBy('tanggal','DESC')
            ->limit(3)
            ->get();

        $video = \DB::table('tbl_video')
            ->select('*')
            ->where('status_video', '=', 'aktif')
            ->get();

		return view('master.informasi.berita.detail',['konsul'=>$konsul,'news'=>$news,'video'=>$video]);
    } 
}
