<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Models\tbl_tenaga;

class konstruksiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
		$tbl_tenaga = \DB::table('tbl_tenaga')
            ->select('*')
            ->orderBy('id_tenaga','DESC')
            ->get();
			
		return view('master.layanan.konstruksi.index',['tbl_tenaga'=>$tbl_tenaga]);
    }    

    public function add()
    {
        return view('master.layanan.konstruksi.add');
    }

    public function edit(Request $request, $id)
    {
		$tbl_tenaga = \DB::table('tbl_tenaga')
            ->select('*')
            ->where('id_tenaga','=', $id)
            ->get();
			
		return view('master.layanan.konstruksi.edit',['tbl_tenaga'=>$tbl_tenaga]);
    }    

    public function create(Request $request)
    {         
       $validator = Validator::make($request->all(), [
           'file' => 'required|mimes:jpg,jpeg,png,bmp,gif,svg,webp,pdf,docx',
       ]);

       if ($validator->fails()) {
           return redirect()->back()->withErrors(['Error', 'Upload gagal Karena file yang diupload bukan jpg,jpeg,png,bmp,gif,svg,webp,pdf,docx.']);  
       }

        $file = $request->file('file');
        
        $filenameWithExt = $request->file('file')->getClientOriginalName();
        $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        $extension = $request->file('file')->getClientOriginalExtension();
        $filenameSimpan = $filename.'_'.time().'.'.$extension;
        $path = $request->file('file')->storeAs('/public/images/tenaga', $filenameSimpan);

        $ip=\request()->ip();
           date_default_timezone_set("Asia/Jakarta");
        $banner = new \App\Models\tbl_tenaga();
        $banner->judul_tenaga = $request->judul_tenaga; 
        $banner->deskripsi_tenaga = $request->deskripsi_tenaga;    
        $banner->file_tenaga = $filenameSimpan;
        $banner->kriteria_tenaga = $request->kriteria_tenaga;
        $banner->save();

        
        return redirect('/layanan/konstruksi')->with('Konstruksi Berhasil Di Buat','Success');
    }

    public function update(Request $request, $id)
   {
       $ip=\request()->ip();
           date_default_timezone_set("Asia/Jakarta");
       $validator = Validator::make($request->all(), [
           'file' => 'required|mimes:jpg,jpeg,png,bmp,gif,svg,webp,pdf,docx',
       ]);

       if(empty($request->file('file'))){

           \DB::table('tbl_tenaga')->where('id_tenaga',$id)->update([
               'judul_tenaga' => $request->judul_tenaga,
               'kriteria_tenaga' => $request->kriteria_tenaga,
               'deskripsi_tenaga' => $request->deskripsi_tenaga               
               ]);
              
               return redirect('/layanan/konstruksi')->withSuccess('Data Tenaga Berhasil Di Update','Success');

       }else{

           if ($validator->fails()) {
               return redirect()->back()->withErrors(['Error', 'Upload gagal Karena file yang diupload bukan jpg,png,jpeg,gif,svg.']);  
           }

           $file = $request->file('file');
       
           $filenameWithExt = $request->file('file')->getClientOriginalName();
           $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
           $extension = $request->file('file')->getClientOriginalExtension();
           $filenameSimpan = $filename.'_'.time().'.'.$extension;
           $path = $request->file('file')->storeAs('/public/images/tenaga', $filenameSimpan);

           \DB::table('tbl_tenaga')->where('id_tenaga',$id)->update([
            'judul_tenaga' => $request->judul_tenaga,
            'kriteria_tenaga' => $request->kriteria_tenaga,
            'deskripsi_tenaga' => $request->deskripsi_tenaga,
            'file_tenaga' => $filenameSimpan
               ]);
              
               return redirect('/layanan/konstruksi')->withSuccess('success','Data Tenaga Berhasil Di Update');

       }

   }

   public function destroy($id)
   {
       $tbl_tenaga= \DB::table('tbl_tenaga')->where('id_tenaga', $id)->delete();
   return redirect()->back()->withSuccess('Deleted Success.');
   }

}
