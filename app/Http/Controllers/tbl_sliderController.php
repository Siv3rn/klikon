<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Models\kontak;

class tbl_sliderController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');


    }

    public function index()
    {
        $this->Authorize('isSuperUser');


		$slider = \DB::table('tbl_slider')
            ->select('*')
            ->get();

            $link = \DB::table('tbl_link')
            ->select('*')
            ->get();

            $video = \DB::table('tbl_video')
            ->select('*')
            ->get();

            $peta = \DB::table('peta')
            ->join('bahan_j', 'peta.id_jenis', '=', 'bahan_j.id_jenis')
            ->select('*')
            ->get();

            $popup = \DB::table('tbl_popup')
            ->select('*')
            ->get();
			
		return view('master.menu.index',['popup'=>$popup,'slider'=>$slider,'link'=>$link,'video'=>$video,'peta'=>$peta]);
    }    

    public function add()
    {
        $this->Authorize('isSuperUser');

        return view('master.menu.slider.add');
    }

    public function add_video()
    {
        $this->Authorize('isSuperUser');

        return view('master.menu.video.add');
    }

    //Save Banner
    public function create(Request $request)
    {
        $this->Authorize('isSuperUser');

		$validator = Validator::make($request->all(), [
            'judul_slider' => 'required',
            'keterangan_slider' => 'required',
            'file' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors(['Error', 'Upload gagal Karena file yang diupload bukan jpg,png,jpeg,gif,svg atau images ukuran melebihi 2048']);  
        }
		
		$file = $request->file('file');
		
		$filenameWithExt = $request->file('file')->getClientOriginalName();
		$filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
		$extension = $request->file('file')->getClientOriginalExtension();
		$filenameSimpan = $filename.'_'.time().'.'.$extension;
		$path = $request->file('file')->storeAs('/public/images/slider', $filenameSimpan);

		$banner = new \App\Models\tbl_slider();
        $banner->judul_slider = $request->judul_slider;
        $banner->keterangan_slider = $request->keterangan_slider;
        $banner->url_slider = $request->url_slider;
        $banner->status_slider = $request->status_slider;
        $banner->image_slider = $filenameSimpan;
        $banner->save();

        
		return redirect('/menu')->with('Slider Berhasil Di Buat','Success');
	}

    public function edit(Request $request, $id)
    {
        $this->Authorize('isSuperUser');

		$slider = \DB::table('tbl_slider')
            ->select('id_slider','judul_slider','keterangan_slider','url_slider','status_slider','image_slider')
            ->where('id_slider','=', $id)
            ->get();
			
		return view('master.menu.slider.edit',['slider'=>$slider]);
    }    

    public function update(Request $request, $id_slider)
    {
        $this->Authorize('isSuperUser');

		$validator = Validator::make($request->all(), [
            'judul_slider' => 'required',
            'keterangan_slider' => 'required',
            'file' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
        ]);

        if(empty($request->file('file'))){

            \DB::table('tbl_slider')->where('id_slider',$id_slider)->update([
                'judul_slider' => $request->judul_slider,
                'keterangan_slider' => $request->keterangan_slider,
                'url_slider' => $request->url_slider,
                'status_slider' => $request->status_slider
                ]);
               
                return redirect('/menu')->withSuccess('Slider Berhasil Di Update','Success');

        }else{

            if ($validator->fails()) {
                return redirect()->back()->withErrors(['Error', 'Upload gagal Karena file yang diupload bukan jpg,png,jpeg,gif,svg atau images ukuran melebihi 2048']);  
            }

            $file = $request->file('file');
		
            $filenameWithExt = $request->file('file')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('file')->getClientOriginalExtension();
            $filenameSimpan = $filename.'_'.time().'.'.$extension;
            $path = $request->file('file')->storeAs('/public/images/slider', $filenameSimpan);

            \DB::table('tbl_slider')->where('id_slider',$id_slider)->update([
                'judul_slider' => $request->judul_slider,
                'keterangan_slider' => $request->keterangan_slider,
                'url_slider' => $request->url_slider,
                'status_slider' => $request->status_slider,
                'image_slider' => $filenameSimpan
                ]);
               
                return redirect('/menu')->withSuccess('success','Slider Berhasil Di Update');

        }

	}

    public function create_kontak(Request $request)
    {
        $this->Authorize('isSuperUser');

		
		$validator = Validator::make($request->all(), [
            'telepon' => 'required',
            'address' => 'required',
            'file' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors(['Error', 'Upload gagal Karena file yang diupload bukan jpg,png,jpeg,gif,svg atau images ukuran melebihi 2048']);  
        }
		
		$file = $request->file('file');
		
		$filenameWithExt = $request->file('file')->getClientOriginalName();
		$filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
		$extension = $request->file('file')->getClientOriginalExtension();
		$filenameSimpan = $filename.'_'.time().'.'.$extension;
		$path = $request->file('file')->storeAs('/public/images/logo', $filenameSimpan);

		$kontak = new \App\Models\kontak();
        $kontak->instansi = $request->instansi;
        $kontak->address = $request->address;
        $kontak->telepon = $request->telepon;
        $kontak->email = $request->email;
        $kontak->fax = $request->fax;
        $kontak->deskripsi = $request->deskripsi;
        $kontak->logo = $filenameSimpan;
        $kontak->save();

        
		return redirect('/menu')->with('Kontak Berhasil Di Update','Success');
	}

    public function create_video(Request $request)
    {
        

		$video = new \App\Models\tbl_video();
        $video->judul_video = $request->judul_video;
        $video->iframe_video = $request->iframe_video;
        $video->status_video = $request->status_video;
        $video->save();

        
		return redirect('/menu')->with('Video Berhasil Di Update','Success');
	}

    public function edit_video(Request $request, $id)
    {
        $this->Authorize('isSuperUser');

		$video = \DB::table('tbl_video')
            ->select('*')
            ->where('id_video','=', $id)
            ->get();
			
		return view('master.menu.video.edit',['video'=>$video]);
    }   

    public function create_peta(Request $request)
    {

		$peta = new \App\Models\peta();
        $peta->judul_peta = $request->judul_peta;
        $peta->iframe_peta = $request->iframe_peta;
        $peta->id_jenis = $request->id_jenis;
        $peta->status_peta = $request->status_peta;
        $peta->save();

        
		return redirect('/menu')->with('Kontak Berhasil Di Update','Success');
	}

    public function update_kontak(Request $request, $id)
    {
		
		$validator = Validator::make($request->all(), [
            'telepon' => 'required',
            'address' => 'required',
            'file' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
        ]);

        if(empty($request->file('file'))){

            \DB::table('kontak')->where('id',$id)->update([
                'instansi' => $request->instansi,
                'telepon' => $request->telepon,
                'email' => $request->email,
                'deskripsi' => $request->deskripsi,
                'address' => $request->address,
                'fax' => $request->fax
                ]);
               
                return redirect('/menu')->withSuccess('Kontak Berhasil Di Update','Success');

        }else{

            if ($validator->fails()) {
                return redirect()->back()->withErrors(['Error', 'Upload gagal Karena file yang diupload bukan jpg,png,jpeg,gif,svg atau images ukuran melebihi 2048']);  
            }

            $file = $request->file('file');
		
            $filenameWithExt = $request->file('file')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('file')->getClientOriginalExtension();
            $filenameSimpan = $filename.'_'.time().'.'.$extension;
            $path = $request->file('file')->storeAs('/public/images/logo', $filenameSimpan);

            \DB::table('kontak')->where('id',$id)->update([
                'telepon' => $request->telepon,
                'email' => $request->email,
                'deskripsi' => $request->deskripsi,
                'address' => $request->address,
                'fax' => $request->fax,
                'logo' => $filenameSimpan
                ]);
               
                return redirect('/menu')->withSuccess('Peta Berhasil Di Update','success');

        }

	}

    public function update_peta(Request $request, $id)
    {
            \DB::table('peta')->where('id',$id)->update([
                'judul_peta' => $request->judul_peta,
                'iframe_peta' => $request->iframe_peta,
                'id_jenis' => $request->id_jenis,
                'status_peta' => $request->status_peta
                ]);
               
                return redirect('/menu')->withSuccess('Peta Berhasil Di Update','success');

	}

    public function update_video(Request $request, $id)
    {
            \DB::table('tbl_video')->where('id_video',$id)->update([
                'judul_video' => $request->judul_video,
                'iframe_video' => $request->iframe_video,
                'status_video' => $request->status_video
                ]);
               
                return redirect('/menu')->withSuccess('Video Berhasil Di Update','success');

	}

    public function add_link()
    {
        $this->Authorize('isSuperUser');

        return view('master.menu.link.add');
    }

    public function create_link(Request $request)
    {
		
		$validator = Validator::make($request->all(), [
            'nama_link' => 'required',
            'url_link' => 'required',
            'file' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors(['Error', 'Upload gagal Karena file yang diupload bukan jpg,png,jpeg,gif,svg atau images ukuran melebihi 2048']);  
        }
		
		$file = $request->file('file');
		
		$filenameWithExt = $request->file('file')->getClientOriginalName();
		$filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
		$extension = $request->file('file')->getClientOriginalExtension();
		$filenameSimpan = $filename.'_'.time().'.'.$extension;
		$path = $request->file('file')->storeAs('/public/images/link', $filenameSimpan);

		$link = new \App\Models\tbl_link();
        $link->nama_link = $request->nama_link;
        $link->url_link = $request->url_link;
        $link->image_link = $filenameSimpan;
        $link->save();

        
		return redirect('/menu')->with('Link Berhasil Di Buat','Success');
	}

    public function create_popup(Request $request)
    {
		
		$validator = Validator::make($request->all(), [
            'file' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:3048',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors(['Error', 'Upload gagal Karena file yang diupload bukan jpg,png,jpeg,gif,svg atau images ukuran melebihi 2048']);  
        }
		
		$file = $request->file('file');
		
		$filenameWithExt = $request->file('file')->getClientOriginalName();
		$filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
		$extension = $request->file('file')->getClientOriginalExtension();
		$filenameSimpan = $filename.'_'.time().'.'.$extension;
		$path = $request->file('file')->storeAs('/public/images/popup', $filenameSimpan);

		$link = new \App\Models\tbl_popup();
        $link->judul_popup = $request->judul_popup;
        $link->image_popup = $filenameSimpan;
        $link->url_popup = $request->url_popup;
        $link->status_popup = $request->status_popup;
        $link->save();

        
		return redirect('/menu')->with('popup Berhasil Di Buat','Success');
	}

    public function edit_link(Request $request, $id)
    {
        $this->Authorize('isSuperUser');

		$link = \DB::table('tbl_link')
            ->select('*')
            ->where('id_link','=', $id)
            ->get();
			
		return view('master.menu.link.edit',['link'=>$link]);
    }    

    public function update_link(Request $request, $id)
    {
		
		$validator = Validator::make($request->all(), [
            'nama_link' => 'required',
            'url_link' => 'required',
            'file' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
        ]);

        if(empty($request->file('file'))){

            \DB::table('tbl_link')->where('id_link',$id)->update([
                'nama_link' => $request->nama_link,
                'url_link' => $request->url_link
                ]);
               
                return redirect('/menu')->withSuccess('Link Berhasil Di Update','Success');

        }else{

            if ($validator->fails()) {
                return redirect()->back()->withErrors(['Error', 'Upload gagal Karena file yang diupload bukan jpg,png,jpeg,gif,svg atau images ukuran melebihi 2048']);  
            }

            $file = $request->file('file');
		
            $filenameWithExt = $request->file('file')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('file')->getClientOriginalExtension();
            $filenameSimpan = $filename.'_'.time().'.'.$extension;
            $path = $request->file('file')->storeAs('/public/images/link', $filenameSimpan);

            \DB::table('tbl_link')->where('id_link',$id)->update([
                'nama_link' => $request->nama_link,
                'url_link' => $request->url_link,
                'image_link' => $filenameSimpan
                ]);
               
                return redirect('/menu')->withSuccess('Link Berhasil Di Update','success');

        }

	}

    public function update_popup(Request $request, $id)
    {
		
		$validator = Validator::make($request->all(), [
            'file' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:3048',
        ]);

        if(empty($request->file('file'))){

            \DB::table('tbl_popup')->where('id_popup',$id)->update([
                'judul_popup' => $request->judul_popup,
                'url_popup' => $request->url_popup,
                'status_popup' => $request->status_popup
                ]);
               
                return redirect('/menu')->withSuccess('Link Berhasil Di Update','Success');

        }else{

            if ($validator->fails()) {
                return redirect()->back()->withErrors(['Error', 'Upload gagal Karena file yang diupload bukan jpg,png,jpeg,gif,svg atau images ukuran melebihi 2048']);  
            }

            $file = $request->file('file');
		
            $filenameWithExt = $request->file('file')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('file')->getClientOriginalExtension();
            $filenameSimpan = $filename.'_'.time().'.'.$extension;
            $path = $request->file('file')->storeAs('/public/images/popup', $filenameSimpan);

            \DB::table('tbl_popup')->where('id_popup',$id)->update([
                'judul_popup' => $request->judul_popup,
                'url_popup' => $request->url_popup,
                'status_popup' => $request->status_popup,
                'image_popup' => $filenameSimpan
                ]);
               
                return redirect('/menu')->withSuccess('popup Berhasil Di Update','success');

        }

	}

    public function destroy_link($id)
    {
        $link= \DB::table('tbl_link')->where('id_link', $id)->delete();
    return redirect()->back()->withSuccess('Deleted Success.');
    }

    public function destroy_peta($id)
    {
        $link= \DB::table('peta')->where('id', $id)->delete();
    return redirect()->back()->withSuccess('Deleted Success.');
    }

    public function destroy_video($id)
    {
        $link= \DB::table('tbl_video')->where('id_video', $id)->delete();
    return redirect()->back()->withSuccess('Deleted Success.');
    }

    public function destroy_popup($id)
    {
        $link= \DB::table('tbl_popup')->where('id_popup', $id)->delete();
    return redirect()->back()->withSuccess('Deleted Success.');
    }


}
