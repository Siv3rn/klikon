<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Models\berita;

class beritaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
		$berita = \DB::table('berita')
            ->select('*')
            ->orderBy('tanggal','DESC')
            ->get();
			
		return view('master.informasi.berita.index',['berita'=>$berita]);
    }    

    public function add()
    {
        return view('master.informasi.berita.add');
    }

    public function edit(Request $request, $id)
    {
		$berita = \DB::table('berita')
            ->select('*')
            ->where('id_berita','=', $id)
            ->get();
			
		return view('master.informasi.berita.edit',['berita'=>$berita]);
    }    

     //Save berita
     public function create(Request $request)
     {         
        $validator = Validator::make($request->all(), [
            'file' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors(['Error', 'Upload gagal Karena file yang diupload bukan jpg,png,jpeg,gif,svg atau images ukuran melebihi 2048']);  
        }

         $file = $request->file('file');
         
         $filenameWithExt = $request->file('file')->getClientOriginalName();
         $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
         $extension = $request->file('file')->getClientOriginalExtension();
         $filenameSimpan = $filename.'_'.time().'.'.$extension;
         $path = $request->file('file')->storeAs('/public/images/berita', $filenameSimpan);
 
         $ip=\request()->ip();
            date_default_timezone_set("Asia/Jakarta");
         $banner = new \App\Models\berita();
         $banner->judul = $request->judul;         
         $banner->tanggal = date('Y-m-d H:i:s');
         $banner->ringkasan = $request->ringkasan;
         $banner->petugas = $request->petugas;
         $banner->isi = $request->isi;
         $banner->ip = $ip;
         $banner->image_berita = $filenameSimpan;
         $banner->dibaca = 0;
         $banner->save();
 
         
         return redirect('/informasi/berita')->with('Berita Berhasil Di Buat','Success');
     }

     public function update(Request $request, $id_berita)
    {
		$ip=\request()->ip();
            date_default_timezone_set("Asia/Jakarta");
		$validator = Validator::make($request->all(), [
            'judul' => 'required',
            'isi' => 'required',
            'file' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
        ]);

        if(empty($request->file('file'))){

            \DB::table('berita')->where('id_berita',$id_berita)->update([
                'judul' => $request->judul,
                'isi' => $request->isi,
                'ringkasan' => $request->ringkasan,
                'petugas' => $request->petugas,
                'tanggal' => date('Y-m-d H:i:s'),
                'ip'    => $ip
                ]);
               
                return redirect('/informasi/berita')->withSuccess('Berita Berhasil Di Update','Success');

        }else{

            if ($validator->fails()) {
                return redirect()->back()->withErrors(['Error', 'Upload gagal Karena file yang diupload bukan jpg,png,jpeg,gif,svg atau images ukuran melebihi 2048']);  
            }

            $file = $request->file('file');
		
            $filenameWithExt = $request->file('file')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('file')->getClientOriginalExtension();
            $filenameSimpan = $filename.'_'.time().'.'.$extension;
            $path = $request->file('file')->storeAs('/public/images/berita', $filenameSimpan);

            \DB::table('berita')->where('id_berita',$id_berita)->update([
                'judul' => $request->judul,
                'isi' => $request->isi,
                'ringkasan' => $request->ringkasan,
                'petugas' => $request->petugas,
                'tanggal' => date('Y-m-d H:i:s'),
                'ip'    => $ip,
                'image_berita' => $filenameSimpan
                ]);
               
                return redirect('/informasi/berita')->withSuccess('success','Berita Berhasil Di Update');

        }

	}

    public function destroy($id)
    {
        $berita= \DB::table('berita')->where('id_berita', $id)->delete();
    return redirect()->back()->withSuccess('Deleted Success.');
    }

}