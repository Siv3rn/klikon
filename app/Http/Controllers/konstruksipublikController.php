<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Models\tbl_tenaga;
use App\Models\tbl_kat_konsul;
use App\Models\harga;

class konstruksipublikController extends Controller
{
    public function all()
    {
        $konstruksi = \DB::table('tbl_tenaga')
        ->select('*')
        ->where('kriteria_tenaga','=','Tenaga Terampil')
        ->orderBy('id_tenaga', 'DESC')
        ->get();

        $konstruksi2 = \DB::table('tbl_tenaga')
        ->select('*')
        ->where('kriteria_tenaga','=','Tenaga Ahli')
        ->orderBy('id_tenaga', 'DESC')
        ->get();

        $konsul = \DB::table('tbl_konsultasi')
            ->select('*')
            ->where('tgl_jawaban', '<>', '')
            ->orWhereNotNull('tgl_jawaban')
            ->orderBy('tgl_jawaban','DESC')
            ->limit(5)
            ->get();

        $video = \DB::table('tbl_video')
            ->select('*')
            ->where('status_video', '=', 'aktif')
            ->get();

        $poster = \DB::table('tbl_poster')
            ->select('*')
            ->where('status_poster', '=', 'tampil')
            ->get();

		return view('master.layanan.konstruksi.all',['konsul'=>$konsul,'konstruksi'=>$konstruksi,'poster'=>$poster,
                                                    'konstruksi2'=>$konstruksi2,'video'=>$video]);
    } 
}
