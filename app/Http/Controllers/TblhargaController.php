<?php

namespace App\Http\Controllers;

use App\Imports\NewHargaImport;
use App\Imports\NewBarangImport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Models\tbl_harga;
use App\Models\bahan_j;
use App\Models\tbl_kecamatan;
use App\Models\kota;
use App\Models\importnewharga;
use App\Models\importnewbarang;
use Maatwebsite\Excel\Facades\Excel;

class TblhargaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
            if(empty($request->keyword)){
		    $tbl_harga = \DB::table('tbl_harga')
            ->select('id as id_harga','id_kecamatan','kode_barang')
            ->groupBy('id','id_kecamatan','kode_barang')
            ->orderBy('last_update','DESC')
            ->limit('1')
            ->paginate(500);

            $jenis = \DB::table('bahan_j')
            ->join('tbl_barang', 'bahan_j.id_jenis', '=', 'tbl_barang.id_jenis')
            ->select('tbl_barang.id','bahan_j.id_jenis','bahan_j.jenis','tbl_barang.nama_barang')
            ->get();
            }else{
            
            $tbl_harga = \DB::table('tbl_harga')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_harga.id_kecamatan')
            ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
            ->join('tbl_barang', 'tbl_harga.kode_barang', '=', 'tbl_barang.kode_jenis')
            ->join('bahan_j', 'bahan_j.id_jenis', '=', 'tbl_barang.id_jenis')
            ->select('tbl_harga.id as id_harga','tbl_harga.id_kecamatan','tbl_harga.kode_barang')
            ->groupBy('tbl_harga.id','tbl_harga.id_kecamatan','tbl_harga.kode_barang')
            ->orderBy('tbl_harga.last_update','DESC')
            ->where('tbl_harga.kode_barang','like','%'.$request->keyword.'%')
            ->Orwhere('tbl_barang.nama_barang','like','%'.$request->keyword.'%')
            ->Orwhere('tbl_harga.harga','like','%'.$request->keyword.'%')
            ->Orwhere('tbl_kecamatan.nama_kecamatan','like','%'.$request->keyword.'%')
            ->limit('1')
            ->paginate(500);

            $jenis = \DB::table('bahan_j')
            ->join('tbl_barang', 'bahan_j.id_jenis', '=', 'tbl_barang.id_jenis')
            ->select('tbl_barang.id','bahan_j.id_jenis','bahan_j.jenis','tbl_barang.nama_barang')
            ->get();
            }
			
		return view('master.layanan.hbb.index',['tbl_harga'=>$tbl_harga,'jenis'=>$jenis,'keyword'=>$request->keyword]);
    }
    
    public function barang()
    {
		    $tbl_harga = \DB::table('tbl_harga')
            ->join('tbl_barang', 'tbl_barang.kode_jenis', '=', 'tbl_harga.kode_barang')
            ->join('bahan_j', 'bahan_j.id_jenis', '=', 'tbl_barang.id_jenis')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_harga.id_kecamatan')
            ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
            ->select('tbl_harga.id as id_harga','tbl_barang.nama_barang as barang','bahan_j.jenis as jenis','tbl_harga.harga as harga',
            'tbl_harga.last_update as last_update','tbl_kecamatan.nama_kecamatan as nama_kecamatan','kota.kota as kota')
            ->orderBy('tbl_harga.id','DESC')
            ->get();

            $jenis = \DB::table('tbl_barang')
            ->join('bahan_j', 'bahan_j.id_jenis', '=', 'tbl_barang.id_jenis')
            ->select('tbl_barang.id','bahan_j.id_jenis','tbl_barang.kode_jenis','bahan_j.jenis','tbl_barang.nama_barang')
            ->orderBy('tbl_barang.kode_jenis','ASC')
            ->get();
			
		return view('master.layanan.hbb.barang',['tbl_harga'=>$tbl_harga,'jenis'=>$jenis]);
    }
    
    public function add()
    {
        return view('master.layanan.hbb.add');
    }

    public function add_barang()
    {
        return view('master.layanan.hbb.add_barang');
    }

    public function create_barang(Request $request)
    {  
        $count = \DB::table('tbl_barang')
            ->select('nama_barang')
            ->where('nama_barang',$request->nama_barang)
            ->count();  
            
        if($count > 0){
            return redirect()->back()->withErrors(['Error', 'Nama barang sudah ada']);
        }else{
            $count2 = \DB::table('tbl_barang')
            ->select('id_jenis')
            ->where('id_jenis',$request->id_jenis)
            ->count(); 
        
        $string = \DB::table('bahan_j')
            ->select('kode')
            ->where('id_jenis',$request->id_jenis)
            ->get(); 
        
        foreach($string as $st){
        
        $ip=\request()->ip();
        date_default_timezone_set("Asia/Jakarta");
        $jenis = new \App\Models\tbl_barang();
        $jenis->id_jenis = $request->id_jenis;  
        $jenis->nama_barang = $request->nama_barang; 
        $jenis->ket_barang = $request->nama_barang;  
        $jenis->status = '1'; 
       
        if($count2 > 100){
            $jenis->kode_jenis =  $st->kode.$count2+1; 
        }else if($count2 > 10){
            $jenis->kode_jenis =   $st->kode.'0'.$count2+1; 
        }else{
            $jenis->kode_jenis =  $st->kode.'00'.$count2+1; 
        }
        
        }
        $jenis->save();

        
        return redirect('/layanan/barang')->with('Barang Berhasil Di Buat','Success');
        }
        
    }


    public function edit(Request $request, $id)
    {
		$harga = \DB::table('tbl_harga')
            ->select('id','ukuran','harga','last_update','kode_barang',
                     '.agen','id_kecamatan','stock','kwalitas','keterangan')
            ->where('id','=', $id)
            ->get();
			
		return view('master.layanan.hbb.edit',['harga'=>$harga]);
    }    

    public function edit_barang(Request $request, $id)
    {
		$barang = \DB::table('bahan_j')
        ->join('tbl_barang', 'bahan_j.id_jenis', '=', 'tbl_barang.id_jenis')
        ->select('tbl_barang.id','bahan_j.id_jenis','bahan_j.jenis','tbl_barang.nama_barang')
        ->where('tbl_barang.id','=', $id)
        ->get();
			
		return view('master.layanan.hbb.edit_barang',['barang'=>$barang]);
    }    

    public function create(Request $request)
    {         

        $ip=\request()->ip();
           date_default_timezone_set("Asia/Jakarta");
        $agenda = new \App\Models\tbl_harga();      
        $agenda->kode_barang = $request->kode_barang;
        $agenda->harga = $request->harga;
        $agenda->ukuran = $request->ukuran;
        $agenda->id_kecamatan = $request->id_kecamatan;
        $agenda->last_update = date('Y-m-d');
        $agenda->agen = $request->agen;
        $agenda->stock = $request->stock;
        $agenda->kwalitas = 'Baik';
        $agenda->keterangan = $request->kode_barang;;
        $agenda->save();

        
        return redirect('/layanan/hbb')->with('tbl_harga Berhasil Di Buat','Success');
    }

    public function update(Request $request, $id)
    {
        /*
        $count = \DB::table('tbl_harga')
            ->select('nama_barang')
            ->where('kode_barang',$request->kode_barang)
            ->where('id_kecamatan',$request->id_kecamatan)
            ->where('harga','<>',$request->harga)
            ->count();  
            
        if($count > 0){
            return redirect()->back()->withErrors(['Error', 'Kode barang dan Kecamatan sudah ada']);
        }else{
            */
            \DB::table('tbl_harga')->where('id',$id)->update([
                'kode_barang' => $request->kode_barang,
                'ukuran' => $request->ukuran,
                'agen' => $request->agen,
                'harga' => $request->harga,
                'last_update' => date('Y-m-d'),
                'id_kecamatan' => $request->id_kecamatan,
                'updated_at' => date('Y-m-d')
                ]);
               
                return redirect('/layanan/hbb')->withSuccess('tbl_harga Berhasil Di Update','Success');
        /*
        }
        */
        }

        public function update_barang(Request $request, $id)
        {

            \DB::table('tbl_barang')->where('id',$id)->update([
                'id_jenis' => $request->id_jenis,                
                'nama_barang' => $request->nama_barang
                ]);
               
                return redirect('/layanan/barang')->withSuccess('Barang Berhasil Di Update','Success');
            
        }

    public function destroy($id)
    {
        $agenda= \DB::table('tbl_harga')->where('id', $id)->delete();
        return redirect()->back()->withSuccess('Deleted Success.');
    }

    public function destroy_search(Request $request)
    {
        $ikc = $request->id_kecamatan;
        $tgl = date('Y-m', strtotime($request->tgl));
        $agenda= \DB::table('tbl_harga')->where('id_kecamatan', $ikc)->where(\DB::raw('DATE_FORMAT(last_update, "%Y-%m")'), $tgl)->delete();
        return redirect()->back()->withSuccess('Deleted Success.');
    }

    public function destroy_barang($id)
    {
        $agenda= \DB::table('tbl_barang')->where('id', $id)->delete();
        return redirect()->back()->withSuccess('Deleted Success.');
    }

    public function importexcelnew(Request $request) 
	{
		// validasi
        $validator = Validator::make($request->all(), [
            'file' => 'required|mimes:csv,xls,xlsx',
        ]);
        
        if ($validator->fails()) {
            return redirect()->back()->withErrors(['Error', 'Upload gagal karena file bukan, xls,xlsx']);  
        }else{
            // menangkap file excel
		$file = $request->file('file');
 
		// membuat nama file unik
		$nama_file = rand().$file->getClientOriginalName();
 
		// upload ke folder file/tbl_harga di dalam folder public
		$file->move('file_harga',$nama_file);
 
		// import data
		Excel::import(new NewHargaImport(), public_path('/file_harga/'.$nama_file));
 
		// alihkan halaman kembali
        return redirect('/layanan/hbb')->withSuccess('Harga Berhasil Di Import','Success');
        }
 
	}

    public function importexcelnewbarang(Request $request) 
	{
		// validasi
        $validator = Validator::make($request->all(), [
            'file' => 'required|mimes:csv,xls,xlsx',
        ]);
        
        if ($validator->fails()) {
            return redirect()->back()->withErrors(['Error', 'Upload gagal karena file bukan, xls,xlsx']);  
        }else{
            // menangkap file excel
		$file = $request->file('file');
 
		// membuat nama file unik
		$nama_file = rand().$file->getClientOriginalName();
 
		// upload ke folder file/tbl_harga di dalam folder public
		$file->move('file_harga',$nama_file);
 
		// import data
		Excel::import(new NewBarangImport(), public_path('/file_harga/'.$nama_file));
 
		// alihkan halaman kembali
        return redirect('/layanan/barang')->withSuccess('Barang Berhasil Di Import','Success');
        }
 
	}

}
