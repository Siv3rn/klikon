<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Models\tbl_data_nspm;
use App\Models\tbl_nspm_kategori;
use App\Models\tbl_nspm_kriteria;
use App\Models\tbl_nspm_kriteria1;

class nspmController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

    }

    public function nspm()
    {
        $this->Authorize('isSuperUser');

		$nspm = \DB::table('tbl_data_nspm')
            ->join('tbl_nspm_kategori', 'tbl_nspm_kategori.id_nspm_kategori', '=', 'tbl_data_nspm.id_nspm_kategori')
            ->join('tbl_nspm_kriteria', 'tbl_nspm_kriteria.id_nspm_kriteria', '=', 'tbl_data_nspm.id_nspm_kriteria')
            ->join('tbl_nspm_kriteria1', 'tbl_nspm_kriteria1.id_nspm_kriteria1', '=', 'tbl_data_nspm.id_nspm_kriteria1')
            ->select('tbl_data_nspm.id_data_nspm','.tbl_data_nspm.judul_data_nspm','tbl_data_nspm.uraian_data_nspm',
                     'tbl_data_nspm.no_seri_nspm','tbl_data_nspm.tahun_data_nspm','tbl_nspm_kategori.nama_nspm_kategori',
                     'tbl_nspm_kriteria1.nama_nspm_kriteria1','tbl_nspm_kriteria.nama_nspm_kriteria',
                     'tbl_data_nspm.id_nspm_kriteria','tbl_data_nspm.id_nspm_kriteria1',
                     'tbl_data_nspm.file_data_nspm','tbl_data_nspm.id_nspm_kategori')
            ->orderBy('id_data_nspm','DESC')
            ->get();
           
        $kategori = \DB::table('tbl_nspm_kategori')
            ->select('*')
            ->orderBy('id_nspm_kategori','DESC')
            ->get();
        
        $kriteria = \DB::table('tbl_nspm_kriteria')
            ->select('*')
            ->orderBy('id_nspm_kriteria','DESC')
            ->get();
        
        $kriteria1 = \DB::table('tbl_nspm_kriteria1')
            ->join('tbl_nspm_kriteria', 'tbl_nspm_kriteria.id_nspm_kriteria', '=', 'tbl_nspm_kriteria1.id_nspm_kriteria')
            ->select('*')
            ->orderBy('tbl_nspm_kriteria1.id_nspm_kriteria1','DESC')
            ->get();
			
		return view('master.layanan.nspm.nspm1',['nspm'=>$nspm,'kategori'=>$kategori,
                                                'kriteria'=>$kriteria,'kriteria1'=>$kriteria1]);
    }    

    public function add_kategori(Request $request)
    {         

        $ip=\request()->ip();
        date_default_timezone_set("Asia/Jakarta");
        $nama = new \App\Models\tbl_nspm_kategori();
        $nama->nama_nspm_kategori = $request->nama_nspm_kategori;  
        $nama->save();

        
        return redirect('/menu/nspm')->withSuccess('Kategori Berhasil Di buat');
    }

    public function add_kriteria(Request $request)
    {         

        $ip=\request()->ip();
        date_default_timezone_set("Asia/Jakarta");
        $nama = new \App\Models\tbl_nspm_kriteria();
        $nama->nama_nspm_kriteria = $request->nama_nspm_kriteria;  
        $nama->save();

        
        return redirect('/menu/nspm')->withSuccess('Kriteria Berhasil Di buat');
    }

    public function add_kriteria1(Request $request)
    {         

        $ip=\request()->ip();
        date_default_timezone_set("Asia/Jakarta");
        $nama = new \App\Models\tbl_nspm_kriteria1();
        $nama->nama_nspm_kriteria1 = $request->nama_nspm_kriteria1;  
        $nama->id_nspm_kriteria = $request->id_nspm_kriteria;  
        $nama->save();

        
        return redirect('/menu/nspm')->withSuccess('Kriteria Berhasil Di buat');
    }

    public function add_nspm(Request $request)
    {         
        if(empty($request->file)){
            $ip=\request()->ip();
               date_default_timezone_set("Asia/Jakarta");
            $nspm = new \App\Models\tbl_data_nspm();
            $nspm->judul_data_nspm = $request->judul_data_nspm; 
            $nspm->uraian_data_nspm = $request->uraian_data_nspm;  
            $nspm->tahun_data_nspm = $request->tahun_data_nspm;  
            $nspm->id_nspm_kategori = $request->id_nspm_kategori; 
            $nspm->id_nspm_kriteria1 = $request->id_nspm_kriteria1; 
            $fae = \DB::table('tbl_nspm_kriteria1')
                ->select('*')
                ->where('id_nspm_kriteria1','=',$request->id_nspm_kriteria1)
                ->get();
            foreach($fae as $f){
            $nspm->id_nspm_kriteria = $f->id_nspm_kriteria; 
            }
            $nspm->no_seri_nspm = $request->no_seri_nspm; 
            $nspm->save();
    
            
            return redirect('/menu/nspm')->withSuccess('NSPM Berhasil Di Buat','Success');
        }else{
            $validator = Validator::make($request->all(), [
                'file' => 'required|mimes:jpg,jpeg,png,bmp,gif,svg,webp,pdf,docx',
            ]);
    
           if ($validator->fails()) {
               return redirect()->back()->withErrors(['Error', 'Upload gagal Karena file yang diupload bukan jpg,png,jpeg,gif,svg atau pdf']);  
           }
    
            $file = $request->file('file');
            
            $filenameWithExt = $request->file('file')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('file')->getClientOriginalExtension();
            $filenameSimpan = $filename.'_'.time().'.'.$extension;
            $path = $request->file('file')->storeAs('/public/images/nspm', $filenameSimpan);
    
            $ip=\request()->ip();
               date_default_timezone_set("Asia/Jakarta");
            $nspm = new \App\Models\tbl_data_nspm();
            $nspm->judul_data_nspm = $request->judul_data_nspm; 
            $nspm->uraian_data_nspm = $request->uraian_data_nspm;  
            $nspm->tahun_data_nspm = $request->tahun_data_nspm;  
            $nspm->id_nspm_kategori = $request->id_nspm_kategori; 
            $nspm->id_nspm_kriteria1 = $request->id_nspm_kriteria1; 
            $fae = \DB::table('tbl_nspm_kriteria1')
            ->select('*')
            ->where('id_nspm_kriteria1','=',$request->id_nspm_kriteria1)
            ->get();
            foreach($fae as $f){
            $nspm->id_nspm_kriteria = $f->id_nspm_kriteria; 
            }
            $nspm->no_seri_nspm = $request->no_seri_nspm; 
            $nspm->file_data_nspm = $filenameSimpan;
            $nspm->save();
    
            
            return redirect('/menu/nspm')->withSuccess('NSPM Berhasil Di Buat','Success');
        }
    }

    public function update_nspm(Request $request, $id)
   {
       $ip=\request()->ip();
           date_default_timezone_set("Asia/Jakarta");
           $validator = Validator::make($request->all(), [
            'file' => 'required|mimes:jpg,jpeg,png,bmp,gif,svg,webp,pdf,docx',
        ]);

       if(empty($request->file('file'))){

        $fae = \DB::table('tbl_nspm_kriteria1')
               ->select('*')
               ->where('id_nspm_kriteria1','=',$request->id_nspm_kriteria1)
               ->get();
               foreach($fae as $f){
               $id_nspm_kriteria = $f->id_nspm_kriteria;
               }

           \DB::table('tbl_data_nspm')->where('id_data_nspm',$id)->update([
               'judul_data_nspm' => $request->judul_data_nspm,
               'uraian_data_nspm' => $request->uraian_data_nspm,
               'tahun_data_nspm' => $request->tahun_data_nspm,
               'no_seri_nspm' => $request->no_seri_nspm,
               'id_nspm_kategori' => $request->id_nspm_kategori,
               'id_nspm_kriteria1' => $request->id_nspm_kriteria1,
               'id_nspm_kriteria' => $id_nspm_kriteria
               ]);
              
               return redirect('/menu/nspm')->withSuccess('NSPM Berhasil Di Update','Success');

       }else{

           if ($validator->fails()) {
               return redirect()->back()->withErrors(['Error', 'Upload gagal Karena file yang diupload bukan jpg,png,jpeg,gif,svg.']);  
           }

           $file = $request->file('file');
       
           $filenameWithExt = $request->file('file')->getClientOriginalName();
           $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
           $extension = $request->file('file')->getClientOriginalExtension();
           $filenameSimpan = $filename.'_'.time().'.'.$extension;
           $path = $request->file('file')->storeAs('/public/images/nspm', $filenameSimpan);

               $fae = \DB::table('tbl_nspm_kriteria1')
               ->select('*')
               ->where('id_nspm_kriteria1','=',$request->id_nspm_kriteria1)
               ->get();
               foreach($fae as $f){
               $id_nspm_kriteria = $f->id_nspm_kriteria;
               }

           \DB::table('tbl_data_nspm')->where('id_data_nspm',$id)->update([
               'judul_data_nspm' => $request->judul_data_nspm,
               'uraian_data_nspm' => $request->uraian_data_nspm,
               'tahun_data_nspm' => $request->tahun_data_nspm,
               'no_seri_nspm' => $request->no_seri_nspm,
               'id_nspm_kategori' => $request->id_nspm_kategori,
               'id_nspm_kriteria1' => $request->id_nspm_kriteria1,
               'id_nspm_kriteria' => $id_nspm_kriteria,
               'file_data_nspm' => $filenameSimpan
               ]);
              
               return redirect('/menu/nspm')->withSuccess('success','NSPM Berhasil Di Update');

       }

   }

    public function kategori_update(Request $request, $id)
        {
       $ip=\request()->ip();
           date_default_timezone_set("Asia/Jakarta");

           \DB::table('tbl_nspm_kategori')->where('id_nspm_kategori',$id)->update([
            'nama_nspm_kategori' => $request->nama_nspm_kategori
               ]);
              
               return redirect('/menu/nspm')->withSuccess('success','Data Kategori Berhasil Di Update');

       }

    public function kriteria_update(Request $request, $id)
        {
       $ip=\request()->ip();
           date_default_timezone_set("Asia/Jakarta");

           \DB::table('tbl_nspm_kriteria')->where('id_nspm_kriteria',$id)->update([
            'nama_nspm_kriteria' => $request->nama_nspm_kriteria
               ]);
              
               return redirect('/menu/nspm')->withSuccess('success','Data kriteria Berhasil Di Update');

       }

       public function kriteria1_update(Request $request, $id)
        {
       $ip=\request()->ip();
           date_default_timezone_set("Asia/Jakarta");

           \DB::table('tbl_nspm_kriteria1')->where('id_nspm_kriteria1',$id)->update([
            'nama_nspm_kriteria1' => $request->nama_nspm_kriteria1,
            'id_nspm_kriteria' => $request->id_nspm_kriteria
               ]);
              
               return redirect('/menu/nspm')->withSuccess('success','Data kriteria Berhasil Di Update');

       }

    public function destroy_kategori($id)
    {
        $tbl_tenaga= \DB::table('tbl_nspm_kategori')->where('id_nspm_kategori', $id)->delete();
            return redirect()->back()->withSuccess('Deleted Success.');
    }

    public function destroy_kriteria($id)
    {
        $tbl_tenaga= \DB::table('tbl_nspm_kriteria')->where('id_nspm_kriteria', $id)->delete();

        $tbl_tenaga1= \DB::table('tbl_nspm_kriteria1')->where('id_nspm_kriteria', $id)->delete();
        return redirect()->back()->withSuccess('Deleted Success.');

        $tbl_tenaga1= \DB::table('tbl_data_nspm')->where('id_nspm_kriteria', $id)->delete();
        return redirect()->back()->withSuccess('Deleted Success.');
    }

    public function destroy_kriteria1($id)
    {
        $tbl_tenaga= \DB::table('tbl_nspm_kriteria1')->where('id_nspm_kriteria1', $id)->delete();
            return redirect()->back()->withSuccess('Deleted Success.');
    }

    public function destroy_nspm($id)
    {
        $tbl_tenaga= \DB::table('tbl_data_nspm')->where('id_data_nspm', $id)->delete();
            return redirect()->back()->withSuccess('Deleted Success.');
    }


}
