<?php

namespace App\Http\Controllers\Harga;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Models\harga;
use App\Http\Controllers\Controller;

class StatistikController extends Controller
{
    public function all(Request $request)
    {
        $from=date('Y-m-d');
        $min=date('Y')-3;
        $to=date($min.'-m-d');
        $id = $request->jenis;

	        $harga = \DB::table('harga')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'harga.id_kecamatan')
            ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
            ->join('bahan_j', 'bahan_j.id_jenis', '=', 'harga.id_jenis')
            ->select('harga.id_kecamatan','kota.id_kota','harga.id_harga as id_harga','harga.nama as barang','bahan_j.jenis as jenis','harga.harga as harga',
            'harga.last_update as last_update','tbl_kecamatan.nama_kecamatan as nama_kecamatan','kota.kota as kota','bahan_j.file')
            ->whereBetween('harga.last_update', [$to, $from])
            ->orderBy('harga.last_update','DESC')
            ->get();
        
            $jenis = \DB::table('bahan_j')
            ->select('*')
            ->get();

        
        $harga1 = \DB::table('harga')
            ->select('last_update')
            ->orderBy('last_update','DESC')
            ->limit(1)
            ->get();

        foreach($harga1 as $data1){
            date_default_timezone_set("Asia/Jakarta");
            $bulan=date('M',strtotime($data1->last_update));
            $bulan2=date('M',strtotime('-1 month',strtotime($data1->last_update)));
            $bulan3=date('M',strtotime('-2 month',strtotime($data1->last_update)));
            $bulana=date('m',strtotime($data1->last_update));
            $bulana2=date('m',strtotime('-1 month',strtotime($data1->last_update)));
            $bulana3=date('m',strtotime('-2 month',strtotime($data1->last_update)));
            $tahun=date('Y',strtotime($data1->last_update));
        }

        if(empty($id)){
            
            $bahan = \DB::table('bahan_j')
            ->select('*')
            ->where('id_jenis','=',1)
            ->get();

        $bantul1 = \DB::table('harga')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'harga.id_kecamatan')
            ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
            ->join('bahan_j', 'bahan_j.id_jenis', '=', 'harga.id_jenis')
            ->select(\DB::raw('round(AVG(harga.harga),0) as harga'))
            ->where('harga.id_jenis', 1)
            ->where('kota.id_kota', 2)
            ->Where('harga.last_update', 'like', '%'.$tahun.'-'.$bulana.'%')
            ->get();
        $bantul2 = \DB::table('harga')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'harga.id_kecamatan')
            ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
            ->join('bahan_j', 'bahan_j.id_jenis', '=', 'harga.id_jenis')
            ->select(\DB::raw('round(AVG(harga.harga),0) as harga'))
            ->where('harga.id_jenis', 1)
            ->where('kota.id_kota', 2)
            ->Where('harga.last_update', 'like', '%'.$tahun.'-'.$bulana2.'%')
            ->get();
        $bantul3 = \DB::table('harga')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'harga.id_kecamatan')
            ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
            ->join('bahan_j', 'bahan_j.id_jenis', '=', 'harga.id_jenis')
            ->select(\DB::raw('round(AVG(harga.harga),0) as harga'))
            ->where('harga.id_jenis', 1)
            ->where('kota.id_kota', 2)
            ->Where('harga.last_update', 'like', '%'.$tahun.'-'.$bulana3.'%')
            ->get();

        $gk3 = \DB::table('harga')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'harga.id_kecamatan')
            ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
            ->join('bahan_j', 'bahan_j.id_jenis', '=', 'harga.id_jenis')
            ->select(\DB::raw('round(AVG(harga.harga),0) as harga'))
            ->where('harga.id_jenis', 1)
            ->where('kota.id_kota', 3)
            ->Where('harga.last_update', 'like', '%'.$tahun.'-'.$bulana3.'%')
            ->get();
        $gk2 = \DB::table('harga')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'harga.id_kecamatan')
            ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
            ->join('bahan_j', 'bahan_j.id_jenis', '=', 'harga.id_jenis')
            ->select(\DB::raw('round(AVG(harga.harga),0) as harga'))
            ->where('harga.id_jenis', 1)
            ->where('kota.id_kota', 3)
            ->Where('harga.last_update', 'like', '%'.$tahun.'-'.$bulana2.'%')
            ->get();
        $gk1 = \DB::table('harga')
                ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'harga.id_kecamatan')
                ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
                ->join('bahan_j', 'bahan_j.id_jenis', '=', 'harga.id_jenis')
                ->select(\DB::raw('round(AVG(harga.harga),0) as harga'))
                ->where('harga.id_jenis', 1)
                ->where('kota.id_kota', 3)
                ->Where('harga.last_update', 'like', '%'.$tahun.'-'.$bulana.'%')
                ->get();
                
        $ky1 = \DB::table('harga')
                ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'harga.id_kecamatan')
                ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
                ->join('bahan_j', 'bahan_j.id_jenis', '=', 'harga.id_jenis')
                ->select(\DB::raw('round(AVG(harga.harga),0) as harga'))
                ->where('harga.id_jenis', 1)
                ->where('kota.id_kota', 1)
                ->Where('harga.last_update', 'like', '%'.$tahun.'-'.$bulana.'%')
                ->get();
        $ky2 = \DB::table('harga')
                ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'harga.id_kecamatan')
                ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
                ->join('bahan_j', 'bahan_j.id_jenis', '=', 'harga.id_jenis')
                ->select(\DB::raw('round(AVG(harga.harga),0) as harga'))
                ->where('harga.id_jenis', 1)
                ->where('kota.id_kota', 1)
                ->Where('harga.last_update', 'like', '%'.$tahun.'-'.$bulana2.'%')
                ->get();
        $ky3 = \DB::table('harga')
                ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'harga.id_kecamatan')
                ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
                ->join('bahan_j', 'bahan_j.id_jenis', '=', 'harga.id_jenis')
                ->select(\DB::raw('round(AVG(harga.harga),0) as harga'))
                ->where('harga.id_jenis', 1)
                ->where('kota.id_kota', 1)
                ->Where('harga.last_update', 'like', '%'.$tahun.'-'.$bulana3.'%')
                ->get();

        $kp3 = \DB::table('harga')
                ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'harga.id_kecamatan')
                ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
                ->join('bahan_j', 'bahan_j.id_jenis', '=', 'harga.id_jenis')
                ->select(\DB::raw('round(AVG(harga.harga),0) as harga'))
                ->where('harga.id_jenis', 1)
                ->where('kota.id_kota', 6)
                ->Where('harga.last_update', 'like', '%'.$tahun.'-'.$bulana3.'%')
                ->get();
        $kp2 = \DB::table('harga')
                ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'harga.id_kecamatan')
                ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
                ->join('bahan_j', 'bahan_j.id_jenis', '=', 'harga.id_jenis')
                ->select(\DB::raw('round(AVG(harga.harga),0) as harga'))
                ->where('harga.id_jenis', 1)
                ->where('kota.id_kota', 6)
                ->Where('harga.last_update', 'like', '%'.$tahun.'-'.$bulana2.'%')
                ->get();
        $kp1 = \DB::table('harga')
                ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'harga.id_kecamatan')
                ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
                ->join('bahan_j', 'bahan_j.id_jenis', '=', 'harga.id_jenis')
                ->select(\DB::raw('round(AVG(harga.harga),0) as harga'))
                ->where('harga.id_jenis', 1)
                ->where('kota.id_kota', 6)
                ->Where('harga.last_update', 'like', '%'.$tahun.'-'.$bulana.'%')
                ->get();
        
        $slm3 = \DB::table('harga')
                ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'harga.id_kecamatan')
                ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
                ->join('bahan_j', 'bahan_j.id_jenis', '=', 'harga.id_jenis')
                ->select(\DB::raw('round(AVG(harga.harga),0) as harga'))
                ->where('harga.id_jenis', 1)
                ->where('kota.id_kota', 5)
                ->Where('harga.last_update', 'like', '%'.$tahun.'-'.$bulana3.'%')
                ->get();
        $slm2 = \DB::table('harga')
                ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'harga.id_kecamatan')
                ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
                ->join('bahan_j', 'bahan_j.id_jenis', '=', 'harga.id_jenis')
                ->select(\DB::raw('round(AVG(harga.harga),0) as harga'))
                ->where('harga.id_jenis', 1)
                ->where('kota.id_kota', 5)
                ->Where('harga.last_update', 'like', '%'.$tahun.'-'.$bulana2.'%')
                ->get();
        $slm1 = \DB::table('harga')
                ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'harga.id_kecamatan')
                ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
                ->join('bahan_j', 'bahan_j.id_jenis', '=', 'harga.id_jenis')
                ->select(\DB::raw('round(AVG(harga.harga),0) as harga'))
                ->where('harga.id_jenis', 1)
                ->where('kota.id_kota', 5)
                ->Where('harga.last_update', 'like', '%'.$tahun.'-'.$bulana.'%')
                ->get();
        

        }else{

            $bahan = \DB::table('bahan_j')
            ->select('*')
            ->where('id_jenis','=',$id)
            ->get();

            $bantul1 = \DB::table('harga')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'harga.id_kecamatan')
            ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
            ->join('bahan_j', 'bahan_j.id_jenis', '=', 'harga.id_jenis')
            ->select(\DB::raw('round(AVG(harga.harga),0) as harga'))
            ->where('harga.id_jenis', $id)
            ->where('kota.id_kota', 2)
            ->Where('harga.last_update', 'like', '%'.$tahun.'-'.$bulana.'%')
            ->get();
        $bantul2 = \DB::table('harga')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'harga.id_kecamatan')
            ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
            ->join('bahan_j', 'bahan_j.id_jenis', '=', 'harga.id_jenis')
            ->select(\DB::raw('round(AVG(harga.harga),0) as harga'))
            ->where('harga.id_jenis', $id)
            ->where('kota.id_kota', 2)
            ->Where('harga.last_update', 'like', '%'.$tahun.'-'.$bulana2.'%')
            ->get();
        $bantul3 = \DB::table('harga')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'harga.id_kecamatan')
            ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
            ->join('bahan_j', 'bahan_j.id_jenis', '=', 'harga.id_jenis')
            ->select(\DB::raw('round(AVG(harga.harga),0) as harga'))
            ->where('harga.id_jenis', $id)
            ->where('kota.id_kota', 2)
            ->Where('harga.last_update', 'like', '%'.$tahun.'-'.$bulana3.'%')
            ->get();

        $gk3 = \DB::table('harga')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'harga.id_kecamatan')
            ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
            ->join('bahan_j', 'bahan_j.id_jenis', '=', 'harga.id_jenis')
            ->select(\DB::raw('round(AVG(harga.harga),0) as harga'))
            ->where('harga.id_jenis', $id)
            ->where('kota.id_kota', 3)
            ->Where('harga.last_update', 'like', '%'.$tahun.'-'.$bulana3.'%')
            ->get();
        $gk2 = \DB::table('harga')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'harga.id_kecamatan')
            ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
            ->join('bahan_j', 'bahan_j.id_jenis', '=', 'harga.id_jenis')
            ->select(\DB::raw('round(AVG(harga.harga),0) as harga'))
            ->where('harga.id_jenis', $id)
            ->where('kota.id_kota', 3)
            ->Where('harga.last_update', 'like', '%'.$tahun.'-'.$bulana2.'%')
            ->get();
        $gk1 = \DB::table('harga')
                ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'harga.id_kecamatan')
                ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
                ->join('bahan_j', 'bahan_j.id_jenis', '=', 'harga.id_jenis')
                ->select(\DB::raw('round(AVG(harga.harga),0) as harga'))
                ->where('harga.id_jenis', $id)
                ->where('kota.id_kota', 3)
                ->Where('harga.last_update', 'like', '%'.$tahun.'-'.$bulana.'%')
                ->get();
                
        $ky1 = \DB::table('harga')
                ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'harga.id_kecamatan')
                ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
                ->join('bahan_j', 'bahan_j.id_jenis', '=', 'harga.id_jenis')
                ->select(\DB::raw('round(AVG(harga.harga),0) as harga'))
                ->where('harga.id_jenis', $id)
                ->where('kota.id_kota', 1)
                ->Where('harga.last_update', 'like', '%'.$tahun.'-'.$bulana.'%')
                ->get();
        $ky2 = \DB::table('harga')
                ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'harga.id_kecamatan')
                ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
                ->join('bahan_j', 'bahan_j.id_jenis', '=', 'harga.id_jenis')
                ->select(\DB::raw('round(AVG(harga.harga),0) as harga'))
                ->where('harga.id_jenis', $id)
                ->where('kota.id_kota', 1)
                ->Where('harga.last_update', 'like', '%'.$tahun.'-'.$bulana2.'%')
                ->get();
        $ky3 = \DB::table('harga')
                ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'harga.id_kecamatan')
                ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
                ->join('bahan_j', 'bahan_j.id_jenis', '=', 'harga.id_jenis')
                ->select(\DB::raw('round(AVG(harga.harga),0) as harga'))
                ->where('harga.id_jenis', $id)
                ->where('kota.id_kota', 1)
                ->Where('harga.last_update', 'like', '%'.$tahun.'-'.$bulana3.'%')
                ->get();

        $kp3 = \DB::table('harga')
                ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'harga.id_kecamatan')
                ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
                ->join('bahan_j', 'bahan_j.id_jenis', '=', 'harga.id_jenis')
                ->select(\DB::raw('round(AVG(harga.harga),0) as harga'))
                ->where('harga.id_jenis', $id)
                ->where('kota.id_kota', 6)
                ->Where('harga.last_update', 'like', '%'.$tahun.'-'.$bulana3.'%')
                ->get();
        $kp2 = \DB::table('harga')
                ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'harga.id_kecamatan')
                ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
                ->join('bahan_j', 'bahan_j.id_jenis', '=', 'harga.id_jenis')
                ->select(\DB::raw('round(AVG(harga.harga),0) as harga'))
                ->where('harga.id_jenis', $id)
                ->where('kota.id_kota', 6)
                ->Where('harga.last_update', 'like', '%'.$tahun.'-'.$bulana2.'%')
                ->get();
        $kp1 = \DB::table('harga')
                ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'harga.id_kecamatan')
                ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
                ->join('bahan_j', 'bahan_j.id_jenis', '=', 'harga.id_jenis')
                ->select(\DB::raw('round(AVG(harga.harga),0) as harga'))
                ->where('harga.id_jenis', $id)
                ->where('kota.id_kota', 6)
                ->Where('harga.last_update', 'like', '%'.$tahun.'-'.$bulana.'%')
                ->get();
        
        $slm3 = \DB::table('harga')
                ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'harga.id_kecamatan')
                ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
                ->join('bahan_j', 'bahan_j.id_jenis', '=', 'harga.id_jenis')
                ->select(\DB::raw('round(AVG(harga.harga),0) as harga'))
                ->where('harga.id_jenis', $id)
                ->where('kota.id_kota', 5)
                ->Where('harga.last_update', 'like', '%'.$tahun.'-'.$bulana3.'%')
                ->get();
        $slm2 = \DB::table('harga')
                ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'harga.id_kecamatan')
                ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
                ->join('bahan_j', 'bahan_j.id_jenis', '=', 'harga.id_jenis')
                ->select(\DB::raw('round(AVG(harga.harga),0) as harga'))
                ->where('harga.id_jenis', $id)
                ->where('kota.id_kota', 5)
                ->Where('harga.last_update', 'like', '%'.$tahun.'-'.$bulana2.'%')
                ->get();
        $slm1 = \DB::table('harga')
                ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'harga.id_kecamatan')
                ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
                ->join('bahan_j', 'bahan_j.id_jenis', '=', 'harga.id_jenis')
                ->select(\DB::raw('round(AVG(harga.harga),0) as harga'))
                ->where('harga.id_jenis', $id)
                ->where('kota.id_kota', 5)
                ->Where('harga.last_update', 'like', '%'.$tahun.'-'.$bulana.'%')
                ->get();

        }
            
		return view('master.layanan.statistik.all',[
                                                    'harga'=>$harga,'harga1'=>$harga1,
                                                    'bahan'=>$bahan,
                                                    'bulan'=>$bulan,'bulan2'=>$bulan2,'bulan3'=>$bulan3,
                                                    'tahun'=>$tahun,
                                                    'bantul1'=>$bantul1,'bantul2'=>$bantul2,'bantul3'=>$bantul3,
                                                    'gk1'=>$gk1,'gk2'=>$gk2,'gk3'=>$gk3,
                                                    'ky1'=>$ky1,'ky2'=>$ky2,'ky3'=>$ky3,
                                                    'kp1'=>$kp1,'kp2'=>$kp2,'kp3'=>$kp3,
                                                    'slm1'=>$slm1,'slm2'=>$slm2,'slm3'=>$slm3,'jenis'=>$jenis
                                                    ]);
    }    
}
