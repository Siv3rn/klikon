<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Models\tbl_buku;

class bukuController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
		$buku = \DB::table('buku')
            ->select('*')
            ->orderBy('id','DESC')
            ->get();
			
		return view('master.unduh.buku.index',['buku'=>$buku]);
    }    

    public function add()
    {
        return view('master.unduh.buku.add');
    }

    public function edit(Request $request, $id)
    {
		$buku = \DB::table('buku')
            ->select('*')
            ->where('id','=', $id)
            ->get();
			
		return view('master.unduh.buku.edit',['buku'=>$buku]);
    }    

    public function create(Request $request)
    {         
       $validator = Validator::make($request->all(), [
           'file' => 'required|mimes:jpg,jpeg,png,bmp,gif,svg,webp,pdf,docx',
       ]);

       if ($validator->fails()) {
           return redirect()->back()->withErrors(['Error', 'Upload gagal Karena file yang diupload bukan jpg,jpeg,png,bmp,gif,svg,webp,pdf,docx.']);  
       }

        $file = $request->file('file');
        
        $filenameWithExt = $request->file('file')->getClientOriginalName();
        $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        $extension = $request->file('file')->getClientOriginalExtension();
        $filenameSimpan = $filename.'_'.time().'.'.$extension;
        $path = $request->file('file')->storeAs('/public/images/buku', $filenameSimpan);

        $ip=\request()->ip();
           date_default_timezone_set("Asia/Jakarta");
        $banner = new \App\Models\tbl_buku();
        $banner->judul_buku = $request->judul_buku;   
        $banner->file_buku = $filenameSimpan;
        $banner->status_buku = 'tampil';
        $banner->save();

        
        return redirect('/unduh/buku')->with('buku Berhasil Di Buat','Success');
    }

    public function update(Request $request, $id)
   {
       if(empty($request->file('file'))){

           \DB::table('buku')->where('id',$id)->update([
               'judul_buku' => $request->judul_buku,
               'status_buku' => $request->status_buku
               ]);
              
               return redirect('/unduh/buku')->withSuccess('buku Berhasil Di Update','Success');

       }else{
                $ip=\request()->ip();
                date_default_timezone_set("Asia/Jakarta");
                $validator = Validator::make($request->all(), [
                    'judul_buku' => 'required',
                    'file' => 'required|mimes:jpg,jpeg,png,bmp,gif,svg,webp,pdf,docx',
                ]);

           if ($validator->fails()) {
               return redirect()->back()->withErrors(['Error', 'Upload gagal Karena file yang diupload bukan jpg,png,jpeg,gif,svg.']);  
           }

           $file = $request->file('file');
       
           $filenameWithExt = $request->file('file')->getClientOriginalName();
           $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
           $extension = $request->file('file')->getClientOriginalExtension();
           $filenameSimpan = $filename.'_'.time().'.'.$extension;
           $path = $request->file('file')->storeAs('/public/images/buku', $filenameSimpan);

           \DB::table('buku')->where('id',$id)->update([
               'judul_buku' => $request->judul_buku,
               'status_buku' => $request->status_buku,
               'file_buku' => $filenameSimpan
               ]);
              
               return redirect('/unduh/buku')->withSuccess('success','buku Berhasil Di Update');

       }

   }

   public function destroy($id)
   {
       $buku= \DB::table('buku')->where('id', $id)->delete();
   return redirect()->back()->withSuccess('Deleted Success.');
   }
}
