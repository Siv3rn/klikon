<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Models\konsultasi;
use App\Models\tbl_kat_konsul;
use App\Models\harga;
use Illuminate\Support\Facades\DB;

class hargapublikController extends Controller
{
    public function all()
    {

        $konsul = \DB::table('tbl_konsultasi')
            ->select('*')
            ->where('tgl_jawaban', '<>', '')
            ->orWhereNotNull('tgl_jawaban')
            ->orderBy('tgl_jawaban','DESC')
            ->limit(5)
            ->get();

        $from=date('Y-m-d');
        $min=date('Y')-3;
        $to=date($min.'-m-d');

        $harga = \DB::table('harga')
        ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'harga.id_kecamatan')
        ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
        ->join('bahan_j', 'bahan_j.id_jenis', '=', 'harga.id_jenis')
        ->select('harga.id_kecamatan','kota.id_kota','harga.id_harga as id_harga','harga.nama as barang','bahan_j.jenis as jenis','harga.harga as harga',
        'harga.last_update as last_update','tbl_kecamatan.nama_kecamatan as nama_kecamatan','kota.kota as kota','bahan_j.file')
        ->whereBetween('harga.last_update', [$to, $from])
        ->orderBy('harga.last_update','DESC')
            ->paginate(10);

        $video = \DB::table('tbl_video')
            ->select('*')
            ->where('status_video', '=', 'aktif')
            ->get();

        $jenis = \DB::table('bahan_j')
            ->select('*')
            ->get();

        $kecamatan = \DB::table('tbl_kecamatan')
            ->join('kota','kota.id_kota','=', 'kota.id_kota')
            ->select('tbl_kecamatan.nama_kecamatan as kecamatan','kota.kota as kota'
                    ,'tbl_kecamatan.id_kecamatan as id_kecamatan')
            ->get();

            $kota = \DB::table('kota')
            ->select('*')
            ->get();

        return view('master.layanan.harga.all2',['konsul'=>$konsul,'video'=>$video,
                    'harga'=>$harga,'jenis'=>$jenis,'kecamatan'=>$kecamatan,'kota'=>$kota]);
    } 

    public function all2()
    {

        $konsul = \DB::table('tbl_konsultasi')
            ->select('*')
            ->where('tgl_jawaban', '<>', '')
            ->orWhereNotNull('tgl_jawaban')
            ->orderBy('tgl_jawaban','DESC')
            ->limit(5)
            ->get();

        $from=date('Y-m-d');
        $min=date('Y')-3;
        $to=date($min.'-m-d');

        $harga = \DB::table('harga')
        ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'harga.id_kecamatan')
        ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
        ->join('bahan_j', 'bahan_j.id_jenis', '=', 'harga.id_jenis')
        ->select('harga.id_kecamatan','kota.id_kota','harga.id_harga as id_harga','harga.nama as barang','bahan_j.jenis as jenis','harga.harga as harga',
        'harga.last_update as last_update','tbl_kecamatan.nama_kecamatan as nama_kecamatan','kota.kota as kota','bahan_j.file')
        ->whereBetween('harga.last_update', [$to, $from])
        ->orderBy('harga.last_update','DESC')
            ->paginate(10);

        $video = \DB::table('tbl_video')
            ->select('*')
            ->where('status_video', '=', 'aktif')
            ->get();

        $jenis = \DB::table('bahan_j')
            ->select('*')
            ->get();

        $kecamatan = \DB::table('tbl_kecamatan')
            ->join('kota','kota.id_kota','=', 'kota.id_kota')
            ->select('tbl_kecamatan.nama_kecamatan as kecamatan','kota.kota as kota'
                    ,'tbl_kecamatan.id_kecamatan as id_kecamatan')
            ->get();

            $kota = \DB::table('kota')
            ->select('*')
            ->get();

        return view('master.layanan.harga.all2',['konsul'=>$konsul,'video'=>$video,
                    'harga'=>$harga,'jenis'=>$jenis,'kecamatan'=>$kecamatan,'kota'=>$kota]);
    }

    //search by
    public function search(Request $request)
    {         
        
        $ip=\request()->ip();
        $nama = $request->nama;
        $id_jenis = $request->id_jenis;
        $id_kota = $request->id_kota;
        $id_kecamatan = $request->id_kecamatan; 
        $urut = $request->urut;
        $from=date('Y-m-d');
        $min=date('Y')-3;
        $to=date($min.'-m-d');

        if(!empty($nama) && $id_jenis == 0 && $id_kota == 0 && $id_kecamatan == null && $urut == null){
            $konsul = \DB::table('tbl_konsultasi')
            ->select('*')
            ->where('tgl_jawaban', '<>', '')
            ->orWhereNotNull('tgl_jawaban')
            ->orderBy('tgl_jawaban','DESC')
            ->limit(5)
            ->get();

            $from=date('Y-m-d');
            $min=date('Y')-3;
            $to=date($min.'-m-d');

        $harga = \DB::table('harga')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'harga.id_kecamatan')
            ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
            ->join('bahan_j', 'bahan_j.id_jenis', '=', 'harga.id_jenis')
            ->select('harga.id_kecamatan','kota.id_kota','harga.id_harga as id_harga','harga.nama as barang','bahan_j.jenis as jenis','harga.harga as harga',
            'harga.last_update as last_update','tbl_kecamatan.nama_kecamatan as nama_kecamatan','kota.kota as kota','bahan_j.file')
            ->Where('harga.nama', 'like', '%'.$request->nama.'%')
            ->whereBetween('harga.last_update', [$to, $from])
            ->orderBy('harga.last_update','DESC')
            ->paginate(39);

            $video = \DB::table('tbl_video')
                ->select('*')
                ->where('status_video', '=', 'aktif')
                ->get();

            $jenis = \DB::table('bahan_j')
                ->select('*')
                ->get();

            $kecamatan = \DB::table('tbl_kecamatan')
                ->join('kota','kota.id_kota','=', 'kota.id_kota')
                ->select('tbl_kecamatan.nama_kecamatan as kecamatan','kota.kota as kota'
                        ,'tbl_kecamatan.id_kecamatan as id_kecamatan')
                ->get();

            $kota = \DB::table('kota')
                ->select('*')
                ->get();

            return view('master.layanan.harga.all2',['konsul'=>$konsul,'video'=>$video,
                        'harga'=>$harga,'jenis'=>$jenis,'kecamatan'=>$kecamatan,'kota'=>$kota]);
            
        }elseif($nama == null && $id_jenis > 0 && $id_kota == 0 && $id_kecamatan == null && $urut == null){
            $konsul = \DB::table('tbl_konsultasi')
            ->select('*')
            ->where('tgl_jawaban', '<>', '')
            ->orWhereNotNull('tgl_jawaban')
            ->orderBy('tgl_jawaban','DESC')
            ->limit(5)
            ->get();

            $from=date('Y-m-d');
            $min=date('Y')-3;
            $to=date($min.'-m-d');

        $harga = \DB::table('harga')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'harga.id_kecamatan')
            ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
            ->join('bahan_j', 'bahan_j.id_jenis', '=', 'harga.id_jenis')
            ->select('harga.id_kecamatan','kota.id_kota','harga.id_harga as id_harga','harga.nama as barang','bahan_j.jenis as jenis','harga.harga as harga',
            'harga.last_update as last_update','tbl_kecamatan.nama_kecamatan as nama_kecamatan','kota.kota as kota','bahan_j.file')
            ->Where('harga.id_jenis', '=', $id_jenis)
            ->whereBetween('harga.last_update', [$to, $from])
            ->orderBy('harga.last_update','DESC')
            ->paginate(39);

            $video = \DB::table('tbl_video')
                ->select('*')
                ->where('status_video', '=', 'aktif')
                ->get();

            $jenis = \DB::table('bahan_j')
                ->select('*')
                ->get();

            $kecamatan = \DB::table('tbl_kecamatan')
                ->join('kota','kota.id_kota','=', 'kota.id_kota')
                ->select('tbl_kecamatan.nama_kecamatan as kecamatan','kota.kota as kota'
                        ,'tbl_kecamatan.id_kecamatan as id_kecamatan')
                ->get();

            $kota = \DB::table('kota')
                ->select('*')
                ->get();

            return view('master.layanan.harga.all2',['konsul'=>$konsul,'video'=>$video,
                        'harga'=>$harga,'jenis'=>$jenis,'kecamatan'=>$kecamatan,'kota'=>$kota]);

        }elseif(!empty($nama) && $id_jenis > 0 && $id_kota == 0 && $id_kecamatan == null && $urut == null){
            $konsul = \DB::table('tbl_konsultasi')
            ->select('*')
            ->where('tgl_jawaban', '<>', '')
            ->orWhereNotNull('tgl_jawaban')
            ->orderBy('tgl_jawaban','DESC')
            ->limit(5)
            ->get();

            $from=date('Y-m-d');
            $min=date('Y')-3;
            $to=date($min.'-m-d');

        $harga = \DB::table('harga')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'harga.id_kecamatan')
            ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
            ->join('bahan_j', 'bahan_j.id_jenis', '=', 'harga.id_jenis')
            ->select('harga.id_kecamatan','kota.id_kota','harga.id_harga as id_harga','harga.nama as barang','bahan_j.jenis as jenis','harga.harga as harga',
            'harga.last_update as last_update','tbl_kecamatan.nama_kecamatan as nama_kecamatan','kota.kota as kota','bahan_j.file')
            ->Where('harga.nama', 'like', '%'.$request->nama.'%')
            ->Where('harga.id_jenis', '=', $id_jenis)
            ->whereBetween('harga.last_update', [$to, $from])
            ->orderBy('harga.last_update','DESC')
            ->paginate(39);

            $video = \DB::table('tbl_video')
                ->select('*')
                ->where('status_video', '=', 'aktif')
                ->get();

            $jenis = \DB::table('bahan_j')
                ->select('*')
                ->get();

            $kecamatan = \DB::table('tbl_kecamatan')
                ->join('kota','kota.id_kota','=', 'kota.id_kota')
                ->select('tbl_kecamatan.nama_kecamatan as kecamatan','kota.kota as kota'
                        ,'tbl_kecamatan.id_kecamatan as id_kecamatan')
                ->get();

            $kota = \DB::table('kota')
                ->select('*')
                ->get();

            return view('master.layanan.harga.all2',['konsul'=>$konsul,'video'=>$video,
                        'harga'=>$harga,'jenis'=>$jenis,'kecamatan'=>$kecamatan,'kota'=>$kota]);
                
        }elseif($nama == null && $id_jenis == 0 && $id_kota > 0 && $id_kecamatan == 0 && $urut == null){
            $konsul = \DB::table('tbl_konsultasi')
            ->select('*')
            ->where('tgl_jawaban', '<>', '')
            ->orWhereNotNull('tgl_jawaban')
            ->orderBy('tgl_jawaban','DESC')
            ->limit(5)
            ->get();

            $from=date('Y-m-d');
            $min=date('Y')-3;
            $to=date($min.'-m-d');

        $harga = \DB::table('harga')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'harga.id_kecamatan')
            ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
            ->join('bahan_j', 'bahan_j.id_jenis', '=', 'harga.id_jenis')
            ->select('harga.id_kecamatan','kota.id_kota','harga.id_harga as id_harga','harga.nama as barang','bahan_j.jenis as jenis','harga.harga as harga',
            'harga.last_update as last_update','tbl_kecamatan.nama_kecamatan as nama_kecamatan','kota.kota as kota','bahan_j.file')
            ->Where('kota.id_kota', '=', $id_kota)
            ->whereBetween('harga.last_update', [$to, $from])
            ->orderBy('harga.last_update','DESC')
            ->paginate(39);

            $video = \DB::table('tbl_video')
                ->select('*')
                ->where('status_video', '=', 'aktif')
                ->get();

            $jenis = \DB::table('bahan_j')
                ->select('*')
                ->get();

            $kecamatan = \DB::table('tbl_kecamatan')
                ->join('kota','kota.id_kota','=', 'kota.id_kota')
                ->select('tbl_kecamatan.nama_kecamatan as kecamatan','kota.kota as kota'
                        ,'tbl_kecamatan.id_kecamatan as id_kecamatan')
                ->get();

            $kota = \DB::table('kota')
                ->select('*')
                ->get();

            return view('master.layanan.harga.all2',['konsul'=>$konsul,'video'=>$video,
                        'harga'=>$harga,'jenis'=>$jenis,'kecamatan'=>$kecamatan,'kota'=>$kota]);
                
        }elseif($nama == null && $id_jenis == 0 && $id_kota > 0 && $id_kecamatan > 0 && $urut == null){
            $konsul = \DB::table('tbl_konsultasi')
            ->select('*')
            ->where('tgl_jawaban', '<>', '')
            ->orWhereNotNull('tgl_jawaban')
            ->orderBy('tgl_jawaban','DESC')
            ->limit(5)
            ->get();

            $from=date('Y-m-d');
            $min=date('Y')-3;
            $to=date($min.'-m-d');

        $harga = \DB::table('harga')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'harga.id_kecamatan')
            ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
            ->join('bahan_j', 'bahan_j.id_jenis', '=', 'harga.id_jenis')
            ->select('harga.id_kecamatan','kota.id_kota','harga.id_harga as id_harga','harga.nama as barang','bahan_j.jenis as jenis','harga.harga as harga',
            'harga.last_update as last_update','tbl_kecamatan.nama_kecamatan as nama_kecamatan','kota.kota as kota','bahan_j.file')
            ->Where('harga.id_kecamatan', '=', $id_kecamatan)
            ->whereBetween('harga.last_update', [$to, $from])
            ->orderBy('harga.last_update','DESC')
            ->paginate(39);

            $video = \DB::table('tbl_video')
                ->select('*')
                ->where('status_video', '=', 'aktif')
                ->get();

            $jenis = \DB::table('bahan_j')
                ->select('*')
                ->get();

            $kecamatan = \DB::table('tbl_kecamatan')
                ->join('kota','kota.id_kota','=', 'kota.id_kota')
                ->select('tbl_kecamatan.nama_kecamatan as kecamatan','kota.kota as kota'
                        ,'tbl_kecamatan.id_kecamatan as id_kecamatan')
                ->get();

            $kota = \DB::table('kota')
                ->select('*')
                ->get();

            return view('master.layanan.harga.all2',['konsul'=>$konsul,'video'=>$video,
                        'harga'=>$harga,'jenis'=>$jenis,'kecamatan'=>$kecamatan,'kota'=>$kota]);
                
        }elseif($nama == null && $id_jenis == 0 && $id_kota == 0 && $id_kecamatan == null && !empty($urut)){
            $konsul = \DB::table('tbl_konsultasi')
            ->select('*')
            ->where('tgl_jawaban', '<>', '')
            ->orWhereNotNull('tgl_jawaban')
            ->orderBy('tgl_jawaban','DESC')
            ->limit(5)
            ->get();

            $from=date('Y-m-d');
            $min=date('Y')-3;
            $to=date($min.'-m-d');

        $harga = \DB::table('harga')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'harga.id_kecamatan')
            ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
            ->join('bahan_j', 'bahan_j.id_jenis', '=', 'harga.id_jenis')
            ->select('harga.id_kecamatan','kota.id_kota','harga.id_harga as id_harga','harga.nama as barang','bahan_j.jenis as jenis','harga.harga as harga',
            'harga.last_update as last_update','tbl_kecamatan.nama_kecamatan as nama_kecamatan','kota.kota as kota','bahan_j.file')
            ->whereBetween('harga.last_update', [$to, $from])
            ->orderBy('harga.harga',$urut)
            ->paginate(39);

            $video = \DB::table('tbl_video')
                ->select('*')
                ->where('status_video', '=', 'aktif')
                ->get();

            $jenis = \DB::table('bahan_j')
                ->select('*')
                ->get();

            $kecamatan = \DB::table('tbl_kecamatan')
                ->join('kota','kota.id_kota','=', 'kota.id_kota')
                ->select('tbl_kecamatan.nama_kecamatan as kecamatan','kota.kota as kota'
                        ,'tbl_kecamatan.id_kecamatan as id_kecamatan')
                ->get();

            $kota = \DB::table('kota')
                ->select('*')
                ->get();

            return view('master.layanan.harga.all2',['konsul'=>$konsul,'video'=>$video,
                        'harga'=>$harga,'jenis'=>$jenis,'kecamatan'=>$kecamatan,'kota'=>$kota]);
                
        }elseif(!empty($nama) && $id_jenis == 0 && $id_kota == 0 && $id_kecamatan == null && !empty($urut)){
            $konsul = \DB::table('tbl_konsultasi')
            ->select('*')
            ->where('tgl_jawaban', '<>', '')
            ->orWhereNotNull('tgl_jawaban')
            ->orderBy('tgl_jawaban','DESC')
            ->limit(5)
            ->get();

            $from=date('Y-m-d');
            $min=date('Y')-3;
            $to=date($min.'-m-d');

        $harga = \DB::table('harga')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'harga.id_kecamatan')
            ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
            ->join('bahan_j', 'bahan_j.id_jenis', '=', 'harga.id_jenis')
            ->select('harga.id_kecamatan','kota.id_kota','harga.id_harga as id_harga','harga.nama as barang','bahan_j.jenis as jenis','harga.harga as harga',
            'harga.last_update as last_update','tbl_kecamatan.nama_kecamatan as nama_kecamatan','kota.kota as kota','bahan_j.file')
            ->Where('harga.nama', 'like', '%'.$request->nama.'%')
            ->whereBetween('harga.last_update', [$to, $from])
            ->orderBy('harga.harga',$urut)
            ->paginate(39);

            $video = \DB::table('tbl_video')
                ->select('*')
                ->where('status_video', '=', 'aktif')
                ->get();

            $jenis = \DB::table('bahan_j')
                ->select('*')
                ->get();

            $kecamatan = \DB::table('tbl_kecamatan')
                ->join('kota','kota.id_kota','=', 'kota.id_kota')
                ->select('tbl_kecamatan.nama_kecamatan as kecamatan','kota.kota as kota'
                        ,'tbl_kecamatan.id_kecamatan as id_kecamatan')
                ->get();

            $kota = \DB::table('kota')
                ->select('*')
                ->get();

            return view('master.layanan.harga.all2',['konsul'=>$konsul,'video'=>$video,
                        'harga'=>$harga,'jenis'=>$jenis,'kecamatan'=>$kecamatan,'kota'=>$kota]);
                
        }elseif($nama == null && $id_jenis > 0 && $id_kota == 0 && $id_kecamatan == null && !empty($urut)){
            $konsul = \DB::table('tbl_konsultasi')
            ->select('*')
            ->where('tgl_jawaban', '<>', '')
            ->orWhereNotNull('tgl_jawaban')
            ->orderBy('tgl_jawaban','DESC')
            ->limit(5)
            ->get();

            $from=date('Y-m-d');
            $min=date('Y')-3;
            $to=date($min.'-m-d');

        $harga = \DB::table('harga')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'harga.id_kecamatan')
            ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
            ->join('bahan_j', 'bahan_j.id_jenis', '=', 'harga.id_jenis')
            ->select('harga.id_kecamatan','kota.id_kota','harga.id_harga as id_harga','harga.nama as barang','bahan_j.jenis as jenis','harga.harga as harga',
            'harga.last_update as last_update','tbl_kecamatan.nama_kecamatan as nama_kecamatan','kota.kota as kota','bahan_j.file')
            ->Where('harga.id_jenis', '=', $id_jenis)
            ->whereBetween('harga.last_update', [$to, $from])
            ->orderBy('harga.harga',$urut)
            ->paginate(39);

            $video = \DB::table('tbl_video')
                ->select('*')
                ->where('status_video', '=', 'aktif')
                ->get();

            $jenis = \DB::table('bahan_j')
                ->select('*')
                ->get();

            $kecamatan = \DB::table('tbl_kecamatan')
                ->join('kota','kota.id_kota','=', 'kota.id_kota')
                ->select('tbl_kecamatan.nama_kecamatan as kecamatan','kota.kota as kota'
                        ,'tbl_kecamatan.id_kecamatan as id_kecamatan')
                ->get();

            $kota = \DB::table('kota')
                ->select('*')
                ->get();

            return view('master.layanan.harga.all2',['konsul'=>$konsul,'video'=>$video,
                        'harga'=>$harga,'jenis'=>$jenis,'kecamatan'=>$kecamatan,'kota'=>$kota]);
                
        }elseif($nama == null && $id_jenis == 0 && $id_kota > 0 && $id_kecamatan == null && !empty($urut)){
            $konsul = \DB::table('tbl_konsultasi')
            ->select('*')
            ->where('tgl_jawaban', '<>', '')
            ->orWhereNotNull('tgl_jawaban')
            ->orderBy('tgl_jawaban','DESC')
            ->limit(5)
            ->get();

            $from=date('Y-m-d');
            $min=date('Y')-3;
            $to=date($min.'-m-d');

        $harga = \DB::table('harga')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'harga.id_kecamatan')
            ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
            ->join('bahan_j', 'bahan_j.id_jenis', '=', 'harga.id_jenis')
            ->select('harga.id_kecamatan','kota.id_kota','harga.id_harga as id_harga','harga.nama as barang','bahan_j.jenis as jenis','harga.harga as harga',
            'harga.last_update as last_update','tbl_kecamatan.nama_kecamatan as nama_kecamatan','kota.kota as kota','bahan_j.file')
            ->Where('harga.id_kota', '=', $id_kota)
            ->whereBetween('harga.last_update', [$to, $from])
            ->orderBy('harga.harga',$urut)
            ->paginate(39);

            $video = \DB::table('tbl_video')
                ->select('*')
                ->where('status_video', '=', 'aktif')
                ->get();

            $jenis = \DB::table('bahan_j')
                ->select('*')
                ->get();

            $kecamatan = \DB::table('tbl_kecamatan')
                ->join('kota','kota.id_kota','=', 'kota.id_kota')
                ->select('tbl_kecamatan.nama_kecamatan as kecamatan','kota.kota as kota'
                        ,'tbl_kecamatan.id_kecamatan as id_kecamatan')
                ->get();

            $kota = \DB::table('kota')
                ->select('*')
                ->get();

            return view('master.layanan.harga.all2',['konsul'=>$konsul,'video'=>$video,
                        'harga'=>$harga,'jenis'=>$jenis,'kecamatan'=>$kecamatan,'kota'=>$kota]);
                
        }elseif($nama == null && $id_jenis == 0 && $id_kota > 0 && $id_kecamatan > 0 && !empty($urut)){
            $konsul = \DB::table('tbl_konsultasi')
            ->select('*')
            ->where('tgl_jawaban', '<>', '')
            ->orWhereNotNull('tgl_jawaban')
            ->orderBy('tgl_jawaban','DESC')
            ->limit(5)
            ->get();

            $from=date('Y-m-d');
            $min=date('Y')-3;
            $to=date($min.'-m-d');

        $harga = \DB::table('harga')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'harga.id_kecamatan')
            ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
            ->join('bahan_j', 'bahan_j.id_jenis', '=', 'harga.id_jenis')
            ->select('harga.id_kecamatan','kota.id_kota','harga.id_harga as id_harga','harga.nama as barang','bahan_j.jenis as jenis','harga.harga as harga',
            'harga.last_update as last_update','tbl_kecamatan.nama_kecamatan as nama_kecamatan','kota.kota as kota','bahan_j.file')
            ->Where('harga.id_kecamatan', '=', $id_kecamatan)
            ->whereBetween('harga.last_update', [$to, $from])
            ->orderBy('harga.harga',$urut)
            ->paginate(39);

            $video = \DB::table('tbl_video')
                ->select('*')
                ->where('status_video', '=', 'aktif')
                ->get();

            $jenis = \DB::table('bahan_j')
                ->select('*')
                ->get();

            $kecamatan = \DB::table('tbl_kecamatan')
                ->join('kota','kota.id_kota','=', 'kota.id_kota')
                ->select('tbl_kecamatan.nama_kecamatan as kecamatan','kota.kota as kota'
                        ,'tbl_kecamatan.id_kecamatan as id_kecamatan')
                ->get();

            $kota = \DB::table('kota')
                ->select('*')
                ->get();

            return view('master.layanan.harga.all2',['konsul'=>$konsul,'video'=>$video,
                        'harga'=>$harga,'jenis'=>$jenis,'kecamatan'=>$kecamatan,'kota'=>$kota]);
                
        }elseif(!empty($nama) && $id_jenis == 0 && $id_kota > 0 && $id_kecamatan == 0 && $urut == null){
            $konsul = \DB::table('tbl_konsultasi')
            ->select('*')
            ->where('tgl_jawaban', '<>', '')
            ->orWhereNotNull('tgl_jawaban')
            ->orderBy('tgl_jawaban','DESC')
            ->limit(5)
            ->get();

            $from=date('Y-m-d');
            $min=date('Y')-3;
            $to=date($min.'-m-d');

        $harga = \DB::table('harga')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'harga.id_kecamatan')
            ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
            ->join('bahan_j', 'bahan_j.id_jenis', '=', 'harga.id_jenis')
            ->select('harga.id_kecamatan','kota.id_kota','harga.id_harga as id_harga','harga.nama as barang','bahan_j.jenis as jenis','harga.harga as harga',
            'harga.last_update as last_update','tbl_kecamatan.nama_kecamatan as nama_kecamatan','kota.kota as kota','bahan_j.file')
            ->Where('harga.nama', 'like', '%'.$request->nama.'%')
            ->Where('harga.id_kecamatan', '=', $id_kecamatan)
            ->whereBetween('harga.last_update', [$to, $from])
            ->orderBy('harga.last_update','DESC')
            ->paginate(39);

            $video = \DB::table('tbl_video')
                ->select('*')
                ->where('status_video', '=', 'aktif')
                ->get();

            $jenis = \DB::table('bahan_j')
                ->select('*')
                ->get();

            $kecamatan = \DB::table('tbl_kecamatan')
                ->join('kota','kota.id_kota','=', 'kota.id_kota')
                ->select('tbl_kecamatan.nama_kecamatan as kecamatan','kota.kota as kota'
                        ,'tbl_kecamatan.id_kecamatan as id_kecamatan')
                ->get();

            $kota = \DB::table('kota')
                ->select('*')
                ->get();

            return view('master.layanan.harga.all2',['konsul'=>$konsul,'video'=>$video,
                        'harga'=>$harga,'jenis'=>$jenis,'kecamatan'=>$kecamatan,'kota'=>$kota]);
                
        }elseif(!empty($nama) && $id_jenis == 0 && $id_kota > 0 && $id_kecamatan > 0 && $urut == null){
            $konsul = \DB::table('tbl_konsultasi')
            ->select('*')
            ->where('tgl_jawaban', '<>', '')
            ->orWhereNotNull('tgl_jawaban')
            ->orderBy('tgl_jawaban','DESC')
            ->limit(5)
            ->get();

            $from=date('Y-m-d');
            $min=date('Y')-3;
            $to=date($min.'-m-d');

        $harga = \DB::table('harga')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'harga.id_kecamatan')
            ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
            ->join('bahan_j', 'bahan_j.id_jenis', '=', 'harga.id_jenis')
            ->select('harga.id_kecamatan','kota.id_kota','harga.id_harga as id_harga','harga.nama as barang','bahan_j.jenis as jenis','harga.harga as harga',
            'harga.last_update as last_update','tbl_kecamatan.nama_kecamatan as nama_kecamatan','kota.kota as kota','bahan_j.file')
            ->Where('harga.nama', 'like', '%'.$request->nama.'%')
            ->Where('harga.id_jenis', '=', $id_jenis)
            ->whereBetween('harga.last_update', [$to, $from])
            ->orderBy('harga.last_update','DESC')
            ->paginate(39);

            $video = \DB::table('tbl_video')
                ->select('*')
                ->where('status_video', '=', 'aktif')
                ->get();

            $jenis = \DB::table('bahan_j')
                ->select('*')
                ->get();

            $kecamatan = \DB::table('tbl_kecamatan')
                ->join('kota','kota.id_kota','=', 'kota.id_kota')
                ->select('tbl_kecamatan.nama_kecamatan as kecamatan','kota.kota as kota'
                        ,'tbl_kecamatan.id_kecamatan as id_kecamatan')
                ->get();

            $kota = \DB::table('kota')
                ->select('*')
                ->get();

            return view('master.layanan.harga.all2',['konsul'=>$konsul,'video'=>$video,
                        'harga'=>$harga,'jenis'=>$jenis,'kecamatan'=>$kecamatan,'kota'=>$kota]);
                
        }else{
            
            $konsul = \DB::table('tbl_konsultasi')
            ->select('*')
            ->where('tgl_jawaban', '<>', '')
            ->orWhereNotNull('tgl_jawaban')
            ->orderBy('tgl_jawaban','DESC')
            ->limit(5)
            ->get();

            $from=date('Y-m-d');
            $min=date('Y')-3;
            $to=date($min.'-m-d');

        $harga = \DB::table('harga')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'harga.id_kecamatan')
            ->join('kota', 'kota.id_kota', '=', 'tbl_kecamatan.id_kota')
            ->join('bahan_j', 'bahan_j.id_jenis', '=', 'harga.id_jenis')
            ->select('harga.id_kecamatan','kota.id_kota','harga.id_harga as id_harga','harga.nama as barang','bahan_j.jenis as jenis','harga.harga as harga',
            'harga.last_update as last_update','tbl_kecamatan.nama_kecamatan as nama_kecamatan','kota.kota as kota','bahan_j.file')
            ->whereBetween('harga.last_update', [$to, $from])
            ->orderBy('harga.last_update','DESC')
            ->paginate(39);

            $video = \DB::table('tbl_video')
                ->select('*')
                ->where('status_video', '=', 'aktif')
                ->get();

            $jenis = \DB::table('bahan_j')
                ->select('*')
                ->get();

            $kecamatan = \DB::table('tbl_kecamatan')
                ->join('kota','kota.id_kota','=', 'kota.id_kota')
                ->select('tbl_kecamatan.nama_kecamatan as kecamatan','kota.kota as kota'
                        ,'tbl_kecamatan.id_kecamatan as id_kecamatan')
                ->get();

            $kota = \DB::table('kota')
                ->select('*')
                ->get();

            return view('master.layanan.harga.all2',['konsul'=>$konsul,'video'=>$video,
                        'harga'=>$harga,'jenis'=>$jenis,'kecamatan'=>$kecamatan,'kota'=>$kota]);

        }

    }

}
