<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Models\konsultasi;
use App\Models\tbl_kat_konsul;

class konsultasipublikController extends Controller
{
    public function tanya()
    {
        $konsul = \DB::table('tbl_konsultasi')
            ->select('*')
            ->where('tgl_jawaban', '<>', '')
            ->orWhereNotNull('tgl_jawaban')
            ->orderBy('tgl_jawaban','DESC')
            ->limit(5)
            ->get();


        $id_kat_konsul = \DB::table('tbl_kat_konsul')
            ->select('*')
            ->get();

        $video = \DB::table('tbl_video')
            ->select('*')
            ->where('status_video', '=', 'aktif')
            ->get();

        $poster = \DB::table('tbl_poster')
            ->select('*')
            ->where('status_poster', '=', 'tampil')
            ->get();
			
		return view('master.layanan.konsultasi.tanya',['konsul'=>$konsul,'id_kat_konsul'=>$id_kat_konsul,
                                                       'video'=>$video,'poster'=>$poster]);
    }

    public function create(Request $request)
    {         

        $ip=\request()->ip();
        date_default_timezone_set("Asia/Jakarta");
        $jenis = new \App\Models\konsultasi();
        $jenis->nama_konsultasi = $request->nama_konsultasi;  
        $jenis->hp_konsultasi = $request->hp_konsultasi;  
        $jenis->email_konsultasi = $request->email_konsultasi; 
        $jenis->judul_konsultasi = $request->judul_konsultasi; 
        $jenis->id_kat_konsul = $request->id_kat_konsul;  
        $jenis->tgl_konsultasi = date('Y-m-d H:i:s');    
        $jenis->pertanyaan_konsultasi = $request->pertanyaan_konsultasi;  
        $jenis->save();

        
        return redirect('/konsultasi/all')->with('Konsultasi Berhasil Di Buat','Success');
    }

    public function all()
    {
        $tbl_konsultasi = \DB::table('tbl_konsultasi')
        ->join('tbl_kat_konsul', 'tbl_konsultasi.id_kat_konsul', '=', 'tbl_kat_konsul.id_kat_konsul')
        ->select('tbl_konsultasi.id_konsultasi','tbl_konsultasi.nama_konsultasi','tbl_konsultasi.judul_konsultasi',
                 'tbl_kat_konsul.nama_kat_konsul as kategori_konsultasi','tbl_konsultasi.hp_konsultasi','tbl_konsultasi.email_konsultasi',
                 'tbl_konsultasi.tgl_konsultasi','tbl_konsultasi.tgl_jawaban','tbl_konsultasi.pertanyaan_konsultasi',
                 'tbl_konsultasi.jawaban_konsultasi')
                 ->where('tbl_konsultasi.tgl_jawaban', '<>', '')
                 ->orWhereNotNull('tbl_konsultasi.tgl_jawaban')
                 ->orderBy('tbl_konsultasi.id_konsultasi','DESC')        
                ->paginate(10);

        $konsul = \DB::table('tbl_konsultasi')
            ->select('*')
            ->where('tgl_jawaban', '<>', '')
            ->orWhereNotNull('tgl_jawaban')
            ->orderBy('tgl_jawaban','DESC')
            ->limit(5)
            ->get();

        $video = \DB::table('tbl_video')
            ->select('*')
            ->where('status_video', '=', 'aktif')
            ->get();

		return view('master.layanan.konsultasi.all',['konsul'=>$konsul,'tbl_konsultasi'=>$tbl_konsultasi,'video'=>$video]);
    } 

    public function detail(Request $request, $id)
    {
            $tbl_konsultasi = \DB::table('tbl_konsultasi')
            ->join('tbl_kat_konsul', 'tbl_konsultasi.id_kat_konsul', '=', 'tbl_kat_konsul.id_kat_konsul')
            ->select('tbl_konsultasi.id_konsultasi','tbl_konsultasi.nama_konsultasi','tbl_konsultasi.judul_konsultasi',
                    'tbl_kat_konsul.nama_kat_konsul as kategori_konsultasi','tbl_konsultasi.hp_konsultasi','tbl_konsultasi.email_konsultasi',
                    'tbl_konsultasi.tgl_konsultasi','tbl_konsultasi.tgl_jawaban','tbl_konsultasi.pertanyaan_konsultasi',
                    'tbl_konsultasi.jawaban_konsultasi')
            ->where('id_konsultasi','=', $id)
            ->get();

            $id_kat_konsul = \DB::table('tbl_konsultasi')
            ->select('*')
            ->limit(4)
            ->orderBy('id_konsultasi','DESC')
            ->get();

            $video = \DB::table('tbl_video')
            ->select('*')
            ->where('status_video', '=', 'aktif')
            ->get();

            $poster = \DB::table('tbl_poster')
            ->select('*')
            ->where('status_poster', '=', 'tampil')
            ->get();
			
		return view('master.layanan.konsultasi.detail',['tbl_konsultasi'=>$tbl_konsultasi,
                    'id_kat_konsul'=>$id_kat_konsul,'video'=>$video,'poster'=>$poster]);
    } 

}
