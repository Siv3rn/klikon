<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Models\tukang;

class kangjogjapublikController extends Controller
{
    public function index()
    {
        $kang = \DB::table('tukang')
        ->select('*')
        ->orderBy('id', 'DESC')
        ->get();

		return view('master.layanan.kangjogja.index',['kang'=>$kang]);
    } 

    public function tukang()
    {
        $kang = \DB::table('tukang')
        ->join('jenis_tukang','tukang.id_jenis','=','jenis_tukang.id_jenis')
        ->join('users','tukang.id_user','=','users.id')
        ->join('tukang_sertifikasi','tukang.id_user','=','tukang_sertifikasi.id_tukang')
        ->select('users.name as name','tukang.alamat as alamat','jenis_tukang.jenis as jenis'
                    ,'tukang.file as file','tukang.usia as usia','tukang.id as id','tukang_sertifikasi.nama as nama_ser'
                    ,'tukang_sertifikasi.nomor as nomor','tukang_sertifikasi.masa_berlaku as masa_berlaku'
                    ,'tukang.wa as wa','tukang.hp as hp','tukang.tgl_lahir as tgl_lahir','tukang.status as status')
        ->orderBy('tukang.id', 'DESC')
        ->paginate(20);

        $jenis = \DB::table('jenis_tukang')
        ->select('*')
        ->get();

		return view('master.layanan.kangjogja.tukang',['kang'=>$kang,'jenis'=>$jenis]);
    } 

    public function search(Request $request)
    { 
        
        $nama = $request->nama;
        $jenis = $request->jenis;

        if(!empty($nama) && $jenis == 0){
          
            $kang = \DB::table('tukang')
            ->join('jenis_tukang','tukang.id_jenis','=','jenis_tukang.id_jenis')
            ->join('users','tukang.id_user','=','users.id')
            ->join('tukang_sertifikasi','tukang.id_user','=','tukang_sertifikasi.id_tukang')
            ->select('users.name as name','tukang.alamat as alamat','jenis_tukang.jenis as jenis'
                    ,'tukang.file as file','tukang.usia as usia','tukang.id as id','tukang_sertifikasi.nama as nama_ser'
                    ,'tukang_sertifikasi.nomor as nomor','tukang_sertifikasi.masa_berlaku as masa_berlaku'
                    ,'tukang.wa as wa','tukang.hp as hp','tukang.tgl_lahir as tgl_lahir','tukang.status as status')
            ->orderBy('tukang.id', 'DESC')
            ->Where('users.name', 'like', '%'.$nama.'%')
            ->orWhere('jenis_tukang.jenis', 'like', '%'.$nama.'%')
            ->paginate(20);
    
            $jenis = \DB::table('jenis_tukang')
            ->select('*')
            ->get();
    
            return view('master.layanan.kangjogja.tukang',['kang'=>$kang,'jenis'=>$jenis]);
            
        }else if(!empty($nama) && !empty($jenis)){
            $kang = \DB::table('tukang')
            ->join('jenis_tukang','tukang.id_jenis','=','jenis_tukang.id_jenis')
            ->join('users','tukang.id_user','=','users.id')
            ->join('tukang_sertifikasi','tukang.id_user','=','tukang_sertifikasi.id_tukang')
            ->select('users.name as name','tukang.alamat as alamat','jenis_tukang.jenis as jenis'
                    ,'tukang.file as file','tukang.usia as usia','tukang.id as id','tukang_sertifikasi.nama as nama_ser'
                    ,'tukang_sertifikasi.nomor as nomor','tukang_sertifikasi.masa_berlaku as masa_berlaku'
                    ,'tukang.wa as wa','tukang.hp as hp','tukang.tgl_lahir as tgl_lahir','tukang.status as status')
            ->orderBy('tukang.id', 'DESC')
            ->Where('users.name', 'like', '%'.$nama.'%')
            ->Where('tukang.id_jenis', '=', $jenis)
            ->paginate(20);
    
            $jenis = \DB::table('jenis_tukang')
            ->select('*')
            ->get();
    
            return view('master.layanan.kangjogja.tukang',['kang'=>$kang,'jenis'=>$jenis]);
         }else if(empty($nama) && !empty($jenis)){
            $kang = \DB::table('tukang')
            ->join('jenis_tukang','tukang.id_jenis','=','jenis_tukang.id_jenis')
            ->join('users','tukang.id_user','=','users.id')
            ->join('tukang_sertifikasi','tukang.id_user','=','tukang_sertifikasi.id_tukang')
            ->select('users.name as name','tukang.alamat as alamat','jenis_tukang.jenis as jenis'
                    ,'tukang.file as file','tukang.usia as usia','tukang.id as id','tukang_sertifikasi.nama as nama_ser'
                    ,'tukang_sertifikasi.nomor as nomor','tukang_sertifikasi.masa_berlaku as masa_berlaku'
                    ,'tukang.wa as wa','tukang.hp as hp','tukang.tgl_lahir as tgl_lahir','tukang.status as status')
            ->orderBy('tukang.id', 'DESC')
            ->Where('tukang.id_jenis', '=', $jenis)
            ->paginate(20);
    
            $jenis = \DB::table('jenis_tukang')
            ->select('*')
            ->get();
    
            return view('master.layanan.kangjogja.tukang',['kang'=>$kang,'jenis'=>$jenis]);
         }else{
            
            $kang = \DB::table('tukang')
            ->join('jenis_tukang','tukang.id_jenis','=','jenis_tukang.id_jenis')
            ->join('users','tukang.id_user','=','users.id')
            ->join('tukang_sertifikasi','tukang.id_user','=','tukang_sertifikasi.id_tukang')
            ->select('users.name as name','tukang.alamat as alamat','jenis_tukang.jenis as jenis'
                    ,'tukang.file as file','tukang.usia as usia','tukang.id as id','tukang_sertifikasi.nama as nama_ser'
                    ,'tukang_sertifikasi.nomor as nomor','tukang_sertifikasi.masa_berlaku as masa_berlaku'
                    ,'tukang.wa as wa','tukang.hp as hp','tukang.tgl_lahir as tgl_lahir','tukang.status as status')
            ->orderBy('tukang.id', 'DESC')
            ->paginate(20);
    
            $jenis = \DB::table('jenis_tukang')
            ->select('*')
            ->get();
    
            return view('master.layanan.kangjogja.tukang',['kang'=>$kang,'jenis'=>$jenis]);

         }

        
    }


    public function toko()
    {
        $toko = \DB::table('toko')
        ->join('users','toko.id_user','=','users.id')
        ->join('m_kabupaten','toko.id_kabupaten','=','m_kabupaten.id_kabupaten')
        ->join('m_propinsi','toko.id_propinsi','=','m_propinsi.id_propinsi')
        ->select('toko.id_user','users.name as nama','toko.id_kabupaten','toko.id_propinsi',
                 'toko.alamat_map','toko.longtitude','toko.latitude','toko.wa','toko.hp',
                 'toko.alamat','toko.file','toko.status','toko.info','m_propinsi.propinsi',
                 'm_kabupaten.kabupaten','toko.id as id')
        ->orderBy('toko.id', 'DESC')
        ->paginate(20);
    
    $kabupaten = \DB::table('m_kabupaten')
        ->select('*')
        ->get();

		return view('master.layanan.kangjogja.toko',['toko'=>$toko,'kabupaten'=>$kabupaten]);
    } 

    public function peta()
    {
        $toko = \DB::table('toko')
        ->join('users','toko.id_user','=','users.id')
        ->join('m_kabupaten','toko.id_kabupaten','=','m_kabupaten.id_kabupaten')
        ->join('m_propinsi','toko.id_propinsi','=','m_propinsi.id_propinsi')
        ->select('toko.id_user','users.name as nama','toko.id_kabupaten','toko.id_propinsi',
                 'toko.alamat_map','toko.longtitude','toko.latitude','toko.wa','toko.hp',
                 'toko.alamat','toko.file','toko.status','toko.info','m_propinsi.propinsi',
                 'm_kabupaten.kabupaten','toko.id as id')
        ->orderBy('toko.id', 'DESC')
        ->paginate(20);
    
    $kabupaten = \DB::table('m_kabupaten')
        ->select('*')
        ->get();

		return view('master.layanan.kangjogja.peta',['toko'=>$toko,'kabupaten'=>$kabupaten]);
    } 

    public function search_toko(Request $request)
    {
        $nama = $request->nama;
        $kabupaten = $request->id_kabupaten;

        if(!empty($nama) && $kabupaten == 0){

            $toko = \DB::table('toko')
            ->join('users','toko.id_user','=','users.id')
            ->join('m_kabupaten','toko.id_kabupaten','=','m_kabupaten.id_kabupaten')
            ->join('m_propinsi','toko.id_propinsi','=','m_propinsi.id_propinsi')
            ->select('toko.id_user','users.name as nama','toko.id_kabupaten','toko.id_propinsi',
                    'toko.alamat_map','toko.longtitude','toko.latitude','toko.wa','toko.hp',
                    'toko.alamat','toko.file','toko.status','toko.info','m_propinsi.propinsi',
                    'm_kabupaten.kabupaten','toko.id as id')
            ->Where('users.name', 'like', '%'.$nama.'%')
            ->orWhere('m_kabupaten.kabupaten', 'like', '%'.$nama.'%')
            ->orderBy('toko.id', 'DESC')
            ->paginate(20);
        
            $kabupaten = \DB::table('m_kabupaten')
            ->select('*')
            ->get();

		return view('master.layanan.kangjogja.toko',['toko'=>$toko,'kabupaten'=>$kabupaten]);

        }else if(!empty($nama) && !empty($kabupaten)){

            $toko = \DB::table('toko')
            ->join('users','toko.id_user','=','users.id')
            ->join('m_kabupaten','toko.id_kabupaten','=','m_kabupaten.id_kabupaten')
            ->join('m_propinsi','toko.id_propinsi','=','m_propinsi.id_propinsi')
            ->select('toko.id_user','users.name as nama','toko.id_kabupaten','toko.id_propinsi',
                    'toko.alamat_map','toko.longtitude','toko.latitude','toko.wa','toko.hp',
                    'toko.alamat','toko.file','toko.status','toko.info','m_propinsi.propinsi',
                    'm_kabupaten.kabupaten','toko.id as id')
            ->Where('users.name', 'like', '%'.$nama.'%')
            ->Where('toko.id_kabupaten', '=', $kabupaten)
            ->orderBy('toko.id', 'DESC')
            ->paginate(20);
        
            $kabupaten = \DB::table('m_kabupaten')
            ->select('*')
            ->get();

		return view('master.layanan.kangjogja.toko',['toko'=>$toko,'kabupaten'=>$kabupaten]);
        
        }else if(empty($nama) && !empty($kabupaten)){
            $toko = \DB::table('toko')
            ->join('users','toko.id_user','=','users.id')
            ->join('m_kabupaten','toko.id_kabupaten','=','m_kabupaten.id_kabupaten')
            ->join('m_propinsi','toko.id_propinsi','=','m_propinsi.id_propinsi')
            ->select('toko.id_user','users.name as nama','toko.id_kabupaten','toko.id_propinsi',
                    'toko.alamat_map','toko.longtitude','toko.latitude','toko.wa','toko.hp',
                    'toko.alamat','toko.file','toko.status','toko.info','m_propinsi.propinsi',
                    'm_kabupaten.kabupaten','toko.id as id')
            ->Where('toko.id_kabupaten', '=', $kabupaten)
            ->orderBy('toko.id', 'DESC')
            ->paginate(20);
        
            $kabupaten = \DB::table('m_kabupaten')
            ->select('*')
            ->get();

		return view('master.layanan.kangjogja.toko',['toko'=>$toko,'kabupaten'=>$kabupaten]);
        }else{

            $toko = \DB::table('toko')
            ->join('users','toko.id_user','=','users.id')
            ->join('m_kabupaten','toko.id_kabupaten','=','m_kabupaten.id_kabupaten')
            ->join('m_propinsi','toko.id_propinsi','=','m_propinsi.id_propinsi')
            ->select('toko.id_user','users.name as nama','toko.id_kabupaten','toko.id_propinsi',
                    'toko.alamat_map','toko.longtitude','toko.latitude','toko.wa','toko.hp',
                    'toko.alamat','toko.file','toko.status','toko.info','m_propinsi.propinsi',
                    'm_kabupaten.kabupaten','toko.id as id')
            ->orderBy('toko.id', 'DESC')
            ->paginate(20);
        
            $kabupaten = \DB::table('m_kabupaten')
            ->select('*')
            ->get();

		return view('master.layanan.kangjogja.toko',['toko'=>$toko,'kabupaten'=>$kabupaten]);

        }

    } 

    public function about()
    {
        $kang = \DB::table('tukang')
        ->select('*')
        ->orderBy('id', 'DESC')
        ->get();

		return view('master.layanan.kangjogja.tentangkami',['kang'=>$kang]);
    } 
}
