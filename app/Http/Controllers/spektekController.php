<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Models\tbl_peraturan;

class spektekController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
		$peraturan = \DB::table('tbl_peraturan')
            ->select('*')
            ->where('kriteria_peraturan','=','Spesifikasi Teknis')
            ->orderBy('id_peraturan','DESC')
            ->get();
			
		return view('master.unduh.spektek.index',['peraturan'=>$peraturan]);
    }    

    public function add()
    {
        return view('master.unduh.spektek.add');
    }

    public function edit(Request $request, $id)
    {
		$peraturan = \DB::table('tbl_peraturan')
            ->select('*')
            ->where('id_peraturan','=', $id)
            ->get();
			
		return view('master.unduh.spektek.edit',['peraturan'=>$peraturan]);
    }    

     //Save peraturan
     public function create(Request $request)
     {         
        $validator = Validator::make($request->all(), [
            'file' => 'required|mimes:jpg,jpeg,png,bmp,gif,svg,webp,pdf,docx',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors(['Error', 'Upload gagal Karena file yang diupload bukan jpg,jpeg,png,bmp,gif,svg,webp,pdf,docx']);  
        }

         $file = $request->file('file');
         
         $filenameWithExt = $request->file('file')->getClientOriginalName();
         $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
         $extension = $request->file('file')->getClientOriginalExtension();
         $filenameSimpan = $filename.'_'.time().'.'.$extension;
         $path = $request->file('file')->storeAs('/public/images/peraturan', $filenameSimpan);
 
         $ip=\request()->ip();
            date_default_timezone_set("Asia/Jakarta");
         $peraturan = new \App\Models\tbl_peraturan();
         $peraturan->judul_peraturan = $request->judul_peraturan;      
         $peraturan->deskripsi_peraturan = $request->deskripsi_peraturan;
         $peraturan->file_peraturan = $filenameSimpan;
         $peraturan->kriteria_peraturan = 'Spesifikasi Teknis';
         $peraturan->save();
 
         
         return redirect('/unduh/spektek')->with('Spesifikasi Berhasil Di Buat','Success');
     }

     public function update(Request $request, $id_peraturan)
    {
		$validator = Validator::make($request->all(), [
            'file' => 'required|mimes:jpg,jpeg,png,bmp,gif,svg,webp,pdf,docx',
        ]);

        if(empty($request->file('file'))){

            \DB::table('tbl_peraturan')->where('id_peraturan',$id_peraturan)->update([
                'judul_peraturan' => $request->judul_peraturan,
                'deskripsi_peraturan' => $request->deskripsi_peraturan
                ]);
               
                return redirect('/unduh/spektek')->withSuccess('peraturan Berhasil Di Update','Success');

        }else{

            if ($validator->fails()) {
            return redirect()->back()->withErrors(['Error', 'Upload gagal Karena file yang diupload bukan jpg,jpeg,png,bmp,gif,svg,webp,pdf,docx']);  
            }

            $file = $request->file('file');
		
            $filenameWithExt = $request->file('file')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('file')->getClientOriginalExtension();
            $filenameSimpan = $filename.'_'.time().'.'.$extension;
            $path = $request->file('file')->storeAs('/public/images/peraturan', $filenameSimpan);

            \DB::table('tbl_peraturan')->where('id_peraturan',$id_peraturan)->update([
                'judul_peraturan' => $request->judul_peraturan,
                'deskripsi_peraturan' => $request->deskripsi_peraturan,
                'file_peraturan' => $filenameSimpan
                ]);
               
                return redirect('/unduh/spektek')->withSuccess('success','peraturan Berhasil Di Update');

        }

	}

    public function destroy($id)
    {
        $peraturan= \DB::table('tbl_peraturan')->where('id_peraturan', $id)->delete();
    return redirect()->back()->withSuccess('Deleted Success.');
    }
}
