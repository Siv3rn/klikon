<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_harga', function (Blueprint $table) {
            $table->id();
            $table->string('kode_barang')->nullable();
            $table->string('ukuran')->nullable();
            $table->Biginteger('harga')->nullable();
            $table->date('last_update')->nullable();
            $table->string('agen')->nullable();
            $table->string('stock');
            $table->unsignedBigInteger('id_kecamatan')->nullable();
            $table->string('kwalitas')->nullable();
            $table->text('keterangan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_harga');
    }
};
