<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peta', function (Blueprint $table) {
            $table->id();
            $table->string('judul_peta');
            $table->text('iframe_peta');
            $table->unsignedBigInteger('id_jenis');
            $table->string('status_peta');
            $table->timestamps();
        });

        DB::table('peta')->insert([
            'judul_peta' => 'Peta Sebaran Harga Bahan Bangunan',
            'iframe_peta' => '<style>.embed-container {position: relative; padding-bottom: 80%; height: 0; max-width: 100%;} .embed-container iframe, .embed-container object, .embed-container iframe{position: absolute; top: 0; left: 0; width: 100%; height: 100%;} small{position: absolute; z-index: 40; bottom: 0; margin-bottom: -15px;}</style><div class="embed-container"><iframe width="500" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" title="Sebaran Harga Semen Holcim Serbaguna Yogyakarta Desember 2021" src="//www.arcgis.com/apps/Embed/index.html?webmap=d43bae813efe47e0bcdd3e5d0ba2886c&extent=110.006,-7.9838,110.8148,-7.5613&zoom=true&previewImage=false&scale=true&legend=true&disable_scroll=false&theme=light"></iframe></div>',
            'id_jenis' => '1',
            'status_peta' => 'aktif'
        ]
    );

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('peta');
    }
};
