<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('toko', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_user');
            $table->unsignedBigInteger('id_kabupaten');
            $table->unsignedBigInteger('id_propinsi');
            $table->string('alamat_map')->nullable();
            $table->string('longtitude')->nullable();
            $table->string('latitude')->nullable();
            $table->string('wa')->nullable();
            $table->string('hp')->nullable();
            $table->text('alamat');
            $table->string('file');
            $table->integer('status');
            $table->string('info');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('toko');
    }
};
